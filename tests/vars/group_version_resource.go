package vars

import "k8s.io/apimachinery/pkg/runtime/schema"

var (
	GroupVersionResources = []schema.GroupVersionResource{
		{
			Group:    "",
			Version:  "v1",
			Resource: "namespaces",
		},
	}
)
