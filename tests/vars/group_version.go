package vars

import v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

var (
	//? May be not needed? use GroupVersionResources?
	ApiGroups = []*v1.APIGroup{
		{
			Name: "namespaces",
			PreferredVersion: v1.GroupVersionForDiscovery{
				GroupVersion: "v1",
				Version:      "v1",
			},
			Versions: []v1.GroupVersionForDiscovery{
				{
					GroupVersion: "v1",
					Version:      "v1",
				},
			},
		},
	}
)
