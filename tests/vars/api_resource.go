package vars

import v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

var (
	ApiResources = []*v1.APIResource{
		{
			Name:    "namespaces",
			Version: "v1",
			Group:   "",
		},
	}
)
