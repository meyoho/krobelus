package tests

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"

	discoveryFake "krobelus/kubernetes/fake"
	"krobelus/tests/vars"

	"github.com/alauda/kvlog"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/dynamic"
	dynamicFake "k8s.io/client-go/dynamic/fake"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	restFake "k8s.io/client-go/rest/fake"
)

func Init() {
	formatter := kvlog.New(kvlog.WithPrimaryFields("request_id", "action"), kvlog.IncludeCaller())
	log.SetFormatter(formatter)
	log.SetOutput(os.Stderr)
	log.SetLevel(log.DebugLevel)

	viper.Set("UnitTest", true)
}

func LoadFile(path string) []byte {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return plan
}

func NewFakeKubeClient() *discoveryFake.KubeClient {
	return &discoveryFake.KubeClient{
		Versions:  vars.ApiGroups,
		Resources: vars.ApiResources,
	}
}

func getGroupVersionResource(resource string) schema.GroupVersionResource {
	for _, item := range vars.GroupVersionResources {
		if item.Resource == resource {
			return item
		}
	}
	return schema.GroupVersionResource{}
}

func NewFakeDynamicInterface(resource string, objects ...runtime.Object) dynamic.ResourceInterface {
	return dynamicFake.NewSimpleDynamicClient(runtime.NewScheme(), objects...).Resource(getGroupVersionResource(resource))
}

func DefaultHandler(data []byte) func(*http.Request) (*http.Response, error) {
	return func(r *http.Request) (*http.Response, error) {
		t := http.Response{
			Status:     "200 OK",
			StatusCode: 200,
			Proto:      "HTTP/1.1",
			Body:       ioutil.NopCloser(bytes.NewBuffer(data)),
			Request:    r,
		}
		return &t, nil
	}
}

func NewFakeRestClient(groupVersion schema.GroupVersion,
	roundTripper func(*http.Request) (*http.Response, error)) rest.Interface {
	hc := restFake.CreateHTTPClient(roundTripper)
	rc := restFake.RESTClient{
		Client:       hc,
		GroupVersion: groupVersion,
	}

	gv := "/" + groupVersion.Version
	if groupVersion.Group != "" {
		gv = "/" + groupVersion.Group + gv
	}

	switch groupVersion.Group {
	case "":
		rc.VersionedAPIPath = "/api" + gv
	default:
		rc.VersionedAPIPath = "/apis" + gv
	}
	rc.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	return &rc
}
