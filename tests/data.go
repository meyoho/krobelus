package tests

const (
	SecretListData = `{
    "apiVersion": "v1",
    "items": [
        {
            "apiVersion": "v1",
            "data": {
                "ca.crt": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRFNE1EY3lOREEzTWpVd09Wb1hEVEk0TURjeU1UQTNNalV3T1Zvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTHNkCi9SZkU2aVkvNlQzeUhLQkdxU3hVOWlBemJEWUorditJRmNMbVJDZnROSXROM2ZmRmx2RG1FeVE0THQyZ25LbWsKQkpTY04vRGs3NVRVSEhaUFdBKzMwU3VxVkhreUQyRGJ0TExFVlMyZXFnenNYUEx0WDA2alpsNVZhd044KzBIcwpmcWVEZlEvckYvMmVmK1BweDFDS0tBOEpCRmZPMmpGSWtmOWIrSWRVVm1BYUduUE41RHRjQlYyNkZ2TmxWNWhCCnlibzEzRXJxajV4cjhaRTdzTC9hWVlFVElYbWg4bTg0NWkzaVZUNXlybGZsUS9jL3orV25EQi9vQm1lRlc0QWEKSHVUMERrd2lRcFVqRzlXNk5pVW44NHhiTGFpWXFqMHh4N1YwQmhSY3BTcmZlTGRBazVjSGhOSUUwWDFoMmI5TApHbUpmOEJXbnI2c2RNWnJKbDU4Q0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFEdFc2Y0tERm1OYzNDdnAzR3ZsRjgvUG9LSysKbVA4VVRRUll4M3ZDRXY2MWZUWWhFSkxtaEJnMU9jay9TdlRKbE5GbytUeG5NYjFsSG9XQjVxWnN0TzVxUWw2agpHM25EQlh1Y1A4WkJtdEZ6NHpERkRYb1AyQ0NYd280c21zMmRuaGFXL3F1R01sNVVXZUpwMEZKSjZMT0JsNjhxCjJjQ29BLzJQeFFtaStqSmJaVUlsZXQ0KzQvWUtMMDVkbW4rdXZYUHBKV0s0QVVhcjArN2UwbFJWazR4d21laU0KNGhKTGdGNVFPNGd4enBVb2NzSkg1dGpRZUR4Z3U3akNqZTVRV3kveVpmQUJuQytRNjVtT0Y1RU9Tb091VFFmTgpjZjZDMVVINzAwK1dWZ1lkanpnS25MTDBFemFGcFpVMXJiVjB4QktPYm52ekNzdktHajAreUpZNWZLMD0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=",
                "namespace": "ZGVmYXVsdA==",
                "token": "ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklpSjkuZXlKcGMzTWlPaUpyZFdKbGNtNWxkR1Z6TDNObGNuWnBZMlZoWTJOdmRXNTBJaXdpYTNWaVpYSnVaWFJsY3k1cGJ5OXpaWEoyYVdObFlXTmpiM1Z1ZEM5dVlXMWxjM0JoWTJVaU9pSmtaV1poZFd4MElpd2lhM1ZpWlhKdVpYUmxjeTVwYnk5elpYSjJhV05sWVdOamIzVnVkQzl6WldOeVpYUXVibUZ0WlNJNkltRmlZeTEwYjJ0bGJpMWpiV1I2YmlJc0ltdDFZbVZ5Ym1WMFpYTXVhVzh2YzJWeWRtbGpaV0ZqWTI5MWJuUXZjMlZ5ZG1salpTMWhZMk52ZFc1MExtNWhiV1VpT2lKaFltTWlMQ0pyZFdKbGNtNWxkR1Z6TG1sdkwzTmxjblpwWTJWaFkyTnZkVzUwTDNObGNuWnBZMlV0WVdOamIzVnVkQzUxYVdRaU9pSmpNV0kzTURjd1pTMDVaVEZoTFRFeFpUZ3RZalkyTnkwMU1qVTBNREEwWmpKaFpESWlMQ0p6ZFdJaU9pSnplWE4wWlcwNmMyVnlkbWxqWldGalkyOTFiblE2WkdWbVlYVnNkRHBoWW1NaWZRLmFkcTFHQW9yN2tKcV9mZmdvUC1BZ2NuN2dpSkRwZlNiM0Q4WW5IWHNQVmxBSUlPRTJJWDZHLTcySjcxcjcybUtlZzRpYmFnNTVyb0h1VzJhclJDWGdQd082Wk9lOVRZS0xTdE5NOGpQaTE1MkotWFBRdURtRmVKVTZVeEUtb2JEMk0wQ2lEc2JXYkpBNDVjOFRXTHUwVmw3RGxuM2ZqRV9NSHUxcVdDSjZsYS03QmZkVWJ6STl3YVZlc1hZLVFrNWh1Z0Z6VTZnTi1IYkJQY3U0bjQ2SUhLYjN0eHZhYi1tdUtJQXVlMm5lemVDaGlHT2p3cHRNbXI0YTlxdUdjemtBRUl0SXpzUjQtRExBZF9sc005eUsyOUx1SFBSZloxX0U1ZVZrQnRHR083VmtyT3lfaEFrb2NxX0VmYTFrcVpVT2FjR0Q5anNRTUFjSVc2cUFQNlVmQQ=="
            },
            "kind": "Secret",
            "metadata": {
                "annotations": {
                    "kubernetes.io/service-account.name": "abc",
                    "kubernetes.io/service-account.uid": "c1b7070e-9e1a-11e8-b667-5254004f2ad2"
                },
                "creationTimestamp": "2018-08-12T10:30:39Z",
                "name": "abc-token-cmdzn",
                "namespace": "default",
                "resourceVersion": "2295565",
                "selfLink": "/api/v1/namespaces/default/secrets/abc-token-cmdzn",
                "uid": "c1b866b7-9e1a-11e8-b667-5254004f2ad2"
            },
            "type": "kubernetes.io/service-account-token"
        },
        {
            "apiVersion": "v1",
            "data": {
                "ca.crt": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRFNE1EY3lOREEzTWpVd09Wb1hEVEk0TURjeU1UQTNNalV3T1Zvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTHNkCi9SZkU2aVkvNlQzeUhLQkdxU3hVOWlBemJEWUorditJRmNMbVJDZnROSXROM2ZmRmx2RG1FeVE0THQyZ25LbWsKQkpTY04vRGs3NVRVSEhaUFdBKzMwU3VxVkhreUQyRGJ0TExFVlMyZXFnenNYUEx0WDA2alpsNVZhd044KzBIcwpmcWVEZlEvckYvMmVmK1BweDFDS0tBOEpCRmZPMmpGSWtmOWIrSWRVVm1BYUduUE41RHRjQlYyNkZ2TmxWNWhCCnlibzEzRXJxajV4cjhaRTdzTC9hWVlFVElYbWg4bTg0NWkzaVZUNXlybGZsUS9jL3orV25EQi9vQm1lRlc0QWEKSHVUMERrd2lRcFVqRzlXNk5pVW44NHhiTGFpWXFqMHh4N1YwQmhSY3BTcmZlTGRBazVjSGhOSUUwWDFoMmI5TApHbUpmOEJXbnI2c2RNWnJKbDU4Q0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFEdFc2Y0tERm1OYzNDdnAzR3ZsRjgvUG9LSysKbVA4VVRRUll4M3ZDRXY2MWZUWWhFSkxtaEJnMU9jay9TdlRKbE5GbytUeG5NYjFsSG9XQjVxWnN0TzVxUWw2agpHM25EQlh1Y1A4WkJtdEZ6NHpERkRYb1AyQ0NYd280c21zMmRuaGFXL3F1R01sNVVXZUpwMEZKSjZMT0JsNjhxCjJjQ29BLzJQeFFtaStqSmJaVUlsZXQ0KzQvWUtMMDVkbW4rdXZYUHBKV0s0QVVhcjArN2UwbFJWazR4d21laU0KNGhKTGdGNVFPNGd4enBVb2NzSkg1dGpRZUR4Z3U3akNqZTVRV3kveVpmQUJuQytRNjVtT0Y1RU9Tb091VFFmTgpjZjZDMVVINzAwK1dWZ1lkanpnS25MTDBFemFGcFpVMXJiVjB4QktPYm52ekNzdktHajAreUpZNWZLMD0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=",
                "namespace": "ZGVmYXVsdA==",
                "token": "ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklpSjkuZXlKcGMzTWlPaUpyZFdKbGNtNWxkR1Z6TDNObGNuWnBZMlZoWTJOdmRXNTBJaXdpYTNWaVpYSnVaWFJsY3k1cGJ5OXpaWEoyYVdObFlXTmpiM1Z1ZEM5dVlXMWxjM0JoWTJVaU9pSmtaV1poZFd4MElpd2lhM1ZpWlhKdVpYUmxjeTVwYnk5elpYSjJhV05sWVdOamIzVnVkQzl6WldOeVpYUXVibUZ0WlNJNkltTnphUzF6WlhKMmFXTmxMV0ZqWTI5MWJuUXRkRzlyWlc0dGVHTmpabXdpTENKcmRXSmxjbTVsZEdWekxtbHZMM05sY25acFkyVmhZMk52ZFc1MEwzTmxjblpwWTJVdFlXTmpiM1Z1ZEM1dVlXMWxJam9pWTNOcExYTmxjblpwWTJVdFlXTmpiM1Z1ZENJc0ltdDFZbVZ5Ym1WMFpYTXVhVzh2YzJWeWRtbGpaV0ZqWTI5MWJuUXZjMlZ5ZG1salpTMWhZMk52ZFc1MExuVnBaQ0k2SW1NNE5UZ3paREl4TFRreE5tTXRNVEZsT0MwNU9EUTNMVFV5TlRRd01EUm1NbUZrTWlJc0luTjFZaUk2SW5ONWMzUmxiVHB6WlhKMmFXTmxZV05qYjNWdWREcGtaV1poZFd4ME9tTnphUzF6WlhKMmFXTmxMV0ZqWTI5MWJuUWlmUS5BVkYwR1VpQzRoMmN6REV6aFNHeXJZWVUxUGVuWml3RHNkUUU3RlNiNUgtQnVacDJjdmM1N2FBRXBVNk5lLWtmZG43bW1fTUwySjNXU0ZWVUtkWkg1SVhoNkVVTjQwU0lqVHMwTTJBTjVpNWdLZ2swVlN4dmxUSV9xa1dNbVlPMnlFTm4tekdnRVA1UzFkakVIODNnOFllX01Scy1qb3l6aGd1MXRHdGFaQWIycllGZzFfMmRCUzRSVW5CVWJDM0lQYnhVSkhGYWhVYzVpZ2x5WU4zUlRsUjF6NU5lV29YYXBfSWZYTlMyQXFsT1BXUXpWemhQV2JqdHJjb1FQRy1GMFk5cE5EcDVGbzEtY2VWN3VZSmRudElkNXZPaUJvSTVZNTJTcW9aZGxxbGR3ai1EeWs2QmFxUzE5VFR5cHlYNFg1TDQ4VEVvOVIyU2diazFpeEhHUmc="
            },
            "kind": "Secret",
            "metadata": {
                "annotations": {
                    "kubernetes.io/service-account.name": "csi-service-account",
                    "kubernetes.io/service-account.uid": "c8583d21-916c-11e8-9847-5254004f2ad2"
                },
                "creationTimestamp": "2018-07-27T07:15:03Z",
                "name": "csi-service-account-token-xccfl",
                "namespace": "default",
                "resourceVersion": "656392",
                "selfLink": "/api/v1/namespaces/default/secrets/csi-service-account-token-xccfl",
                "uid": "c859f0dd-916c-11e8-9847-5254004f2ad2"
            },
            "type": "kubernetes.io/service-account-token"
        },
        {
            "apiVersion": "v1",
            "data": {
                "ca.crt": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRFNE1EY3lOREEzTWpVd09Wb1hEVEk0TURjeU1UQTNNalV3T1Zvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTHNkCi9SZkU2aVkvNlQzeUhLQkdxU3hVOWlBemJEWUorditJRmNMbVJDZnROSXROM2ZmRmx2RG1FeVE0THQyZ25LbWsKQkpTY04vRGs3NVRVSEhaUFdBKzMwU3VxVkhreUQyRGJ0TExFVlMyZXFnenNYUEx0WDA2alpsNVZhd044KzBIcwpmcWVEZlEvckYvMmVmK1BweDFDS0tBOEpCRmZPMmpGSWtmOWIrSWRVVm1BYUduUE41RHRjQlYyNkZ2TmxWNWhCCnlibzEzRXJxajV4cjhaRTdzTC9hWVlFVElYbWg4bTg0NWkzaVZUNXlybGZsUS9jL3orV25EQi9vQm1lRlc0QWEKSHVUMERrd2lRcFVqRzlXNk5pVW44NHhiTGFpWXFqMHh4N1YwQmhSY3BTcmZlTGRBazVjSGhOSUUwWDFoMmI5TApHbUpmOEJXbnI2c2RNWnJKbDU4Q0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFEdFc2Y0tERm1OYzNDdnAzR3ZsRjgvUG9LSysKbVA4VVRRUll4M3ZDRXY2MWZUWWhFSkxtaEJnMU9jay9TdlRKbE5GbytUeG5NYjFsSG9XQjVxWnN0TzVxUWw2agpHM25EQlh1Y1A4WkJtdEZ6NHpERkRYb1AyQ0NYd280c21zMmRuaGFXL3F1R01sNVVXZUpwMEZKSjZMT0JsNjhxCjJjQ29BLzJQeFFtaStqSmJaVUlsZXQ0KzQvWUtMMDVkbW4rdXZYUHBKV0s0QVVhcjArN2UwbFJWazR4d21laU0KNGhKTGdGNVFPNGd4enBVb2NzSkg1dGpRZUR4Z3U3akNqZTVRV3kveVpmQUJuQytRNjVtT0Y1RU9Tb091VFFmTgpjZjZDMVVINzAwK1dWZ1lkanpnS25MTDBFemFGcFpVMXJiVjB4QktPYm52ekNzdktHajAreUpZNWZLMD0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=",
                "namespace": "ZGVmYXVsdA==",
                "token": "ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklpSjkuZXlKcGMzTWlPaUpyZFdKbGNtNWxkR1Z6TDNObGNuWnBZMlZoWTJOdmRXNTBJaXdpYTNWaVpYSnVaWFJsY3k1cGJ5OXpaWEoyYVdObFlXTmpiM1Z1ZEM5dVlXMWxjM0JoWTJVaU9pSmtaV1poZFd4MElpd2lhM1ZpWlhKdVpYUmxjeTVwYnk5elpYSjJhV05sWVdOamIzVnVkQzl6WldOeVpYUXVibUZ0WlNJNkltUmxabUYxYkhRdGRHOXJaVzR0YW1aMFpqZ2lMQ0pyZFdKbGNtNWxkR1Z6TG1sdkwzTmxjblpwWTJWaFkyTnZkVzUwTDNObGNuWnBZMlV0WVdOamIzVnVkQzV1WVcxbElqb2laR1ZtWVhWc2RDSXNJbXQxWW1WeWJtVjBaWE11YVc4dmMyVnlkbWxqWldGalkyOTFiblF2YzJWeWRtbGpaUzFoWTJOdmRXNTBMblZwWkNJNkltUTFNV1ZqWmpVMUxUaG1NVEl0TVRGbE9DMWlPVE16TFRVeU5UUXdNRFJtTW1Ga01pSXNJbk4xWWlJNkluTjVjM1JsYlRwelpYSjJhV05sWVdOamIzVnVkRHBrWldaaGRXeDBPbVJsWm1GMWJIUWlmUS5rTUdNdWZNWGduVzhVLTFlYkpNRXVIOU5kS3hpcldwellSYmx2Mmk4RkI2d2VsVzdhTWEzeHB1aFBKOHo4dHNCSkxDREpWLURhbEN3OWlhSnYzd2lpdlEzaVRHclRiS3FBLUN0alZneEJaUVBmRDdjWUxyZGhsOGlNY1l2NGV4OWJibDVqT1lDY2JwWGp4ZW85akJLNm1Id1RWUGJJRWhrbUNEZVRhNG5mekc1UjVlMGp4R24tV09CaER2OWJFUHpJQlk4bE5oRTFvTWFkZHd3UmNLTlpQNmdUU2VZeDFBTEVYTFdRbG9lVjJpWFE0UWdvcUFRY0VKUENkSGdVR192Z0dsUjZqc0hfR2VNcDFxbndzT21WVHBOSV9aTFN4azhqVWYzRUU4S1lKWmRUeVVkc1I3bEVJWXJyTWJ4amZmTkFzZDdZYTE1bkRNZl9QcXh6TngyQmc="
            },
            "kind": "Secret",
            "metadata": {
                "annotations": {
                    "kubernetes.io/service-account.name": "default",
                    "kubernetes.io/service-account.uid": "d51ecf55-8f12-11e8-b933-5254004f2ad2"
                },
                "creationTimestamp": "2018-07-24T07:26:08Z",
                "name": "default-token-jftf8",
                "namespace": "default",
                "resourceVersion": "324",
                "selfLink": "/api/v1/namespaces/default/secrets/default-token-jftf8",
                "uid": "d520f55d-8f12-11e8-b933-5254004f2ad2"
            },
            "type": "kubernetes.io/service-account-token"
        },
        {
            "apiVersion": "v1",
            "data": {
                "password": "MWYyZDFlMmU2N2Rm",
                "username": "YWRtaW4="
            },
            "kind": "Secret",
            "metadata": {
                "creationTimestamp": "2018-09-11T07:15:08Z",
                "name": "mysecret",
                "namespace": "default",
                "resourceVersion": "5389002",
                "selfLink": "/api/v1/namespaces/default/secrets/mysecret",
                "uid": "6a36ff2b-b592-11e8-a58a-5254004f2ad2"
            },
            "type": "Opaque"
        },
        {
            "apiVersion": "v1",
            "data": {
                ".dockerconfigjson": "eyJhdXRocyI6eyJodHRwOi8veW91ci1yZWdpc3RyeS1zZXJ2ZXIiOnsidXNlcm5hbWUiOiJ5b3VyLW5hbWUiLCJwYXNzd29yZCI6InlvdXItcHdvcmQiLCJlbWFpbCI6InlvdXItZW1haWwiLCJhdXRoIjoiZVc5MWNpMXVZVzFsT25sdmRYSXRjSGR2Y21RPSJ9fX0="
            },
            "kind": "Secret",
            "metadata": {
                "creationTimestamp": "2018-09-19T03:20:46Z",
                "name": "regcred",
                "namespace": "default",
                "resourceVersion": "6040961",
                "selfLink": "/api/v1/namespaces/default/secrets/regcred",
                "uid": "ffbfe53c-bbba-11e8-a654-5254004f2ad2"
            },
            "type": "kubernetes.io/dockerconfigjson"
        }
    ],
    "kind": "List",
    "metadata": {
        "resourceVersion": "",
        "selfLink": ""
    }
}`

	PodData = `{
    "apiVersion": "v1",
    "kind": "Pod",
    "metadata": {
        "creationTimestamp": "2018-10-10T07:05:09Z",
        "generateName": "nginx-64dc7cf49d-",
        "labels": {
            "ff": "ff",
            "pod-template-hash": "2087379058",
            "run": "nginx",
            "service.alauda.io/name": "tom-10111-2",
            "service.alauda.io/uuid": "d80d8550-e553-4903-8dfc-8639d7a06824"
        },
        "name": "nginx-64dc7cf49d-pmdsz",
        "namespace": "default",
        "ownerReferences": [
            {
                "apiVersion": "apps/v1",
                "blockOwnerDeletion": true,
                "controller": true,
                "kind": "ReplicaSet",
                "name": "nginx-64dc7cf49d",
                "uid": "847de5bb-9ad0-11e8-b667-5254004f2ad2"
            }
        ],
        "resourceVersion": "8594294",
        "selfLink": "/api/v1/namespaces/default/pods/nginx-64dc7cf49d-pmdsz",
        "uid": "d32490ff-cc5a-11e8-b051-5254004f2ad2"
    },
    "spec": {
        "containers": [
            {
                "env": [
                    {
                        "name": "__ALAUDA_SERVICE_ID__",
                        "value": "d80d8550-e553-4903-8dfc-8639d7a06824"
                    },
                    {
                        "name": "__ALAUDA_SERVICE_NAME__",
                        "value": "tom-10111-2"
                    }
                ],
                "image": "nginx:1.9.0",
                "imagePullPolicy": "IfNotPresent",
                "name": "nginx",
                "ports": [
                    {
                        "containerPort": 80,
                        "name": "web",
                        "protocol": "TCP"
                    }
                ],
                "resources": {},
                "terminationMessagePath": "/dev/termination-log",
                "terminationMessagePolicy": "File",
                "volumeMounts": [
                    {
                        "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount",
                        "name": "default-token-jftf8",
                        "readOnly": true
                    }
                ]
            }
        ],
        "dnsPolicy": "ClusterFirst",
        "nodeName": "vm-16-12-ubuntu",
        "priority": 0,
        "restartPolicy": "Always",
        "schedulerName": "default-scheduler",
        "securityContext": {},
        "serviceAccount": "default",
        "serviceAccountName": "default",
        "terminationGracePeriodSeconds": 30,
        "tolerations": [
            {
                "effect": "NoExecute",
                "key": "node.kubernetes.io/not-ready",
                "operator": "Exists",
                "tolerationSeconds": 300
            },
            {
                "effect": "NoExecute",
                "key": "node.kubernetes.io/unreachable",
                "operator": "Exists",
                "tolerationSeconds": 300
            }
        ],
        "volumes": [
            {
                "name": "default-token-jftf8",
                "secret": {
                    "defaultMode": 420,
                    "secretName": "default-token-jftf8"
                }
            }
        ]
    },
    "status": {
        "conditions": [
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2018-10-10T07:05:09Z",
                "status": "True",
                "type": "Initialized"
            },
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2018-10-18T07:13:06Z",
                "status": "True",
                "type": "Ready"
            },
            {
                "lastProbeTime": null,
                "lastTransitionTime": null,
                "status": "True",
                "type": "ContainersReady"
            },
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2018-10-10T07:05:09Z",
                "status": "True",
                "type": "PodScheduled"
            }
        ],
        "containerStatuses": [
            {
                "containerID": "docker://be552d8f8b2199567bdf497c2e14c222f6f3d23a4522c36fe3bdac999bfc6e18",
                "image": "nginx:1.9.0",
                "imageID": "docker-pullable://nginx@sha256:4157ed7179858886f21024583247a875bafd8e8966e1cf68b4c8916963b20b62",
                "lastState": {
                    "terminated": {
                        "containerID": "docker://b4f3f3d9b8182e5847caace8703a960f2c61866ec1fe292a96ed2c9ec51ebb4c",
                        "exitCode": 0,
                        "finishedAt": "2018-10-18T07:10:47Z",
                        "reason": "Completed",
                        "startedAt": "2018-10-10T07:05:21Z"
                    }
                },
                "name": "nginx",
                "ready": true,
                "restartCount": 1,
                "state": {
                    "running": {
                        "startedAt": "2018-10-18T07:13:06Z"
                    }
                }
            }
        ],
        "hostIP": "172.19.16.12",
        "phase": "Running",
        "podIP": "10.244.0.233",
        "qosClass": "BestEffort",
        "startTime": "2018-10-10T07:05:09Z"
    }
}`
)
