package store

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"krobelus/common"
	"krobelus/model"
	"krobelus/tests"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func LoadObject(path string) *model.KubernetesObject {
	plan, _ := ioutil.ReadFile(path)
	object, _ := model.BytesToKubernetesObject(plan)
	return object
}

func LoadObjects(path string) *unstructured.UnstructuredList {
	plan, _ := ioutil.ReadFile(path)
	var data unstructured.UnstructuredList
	json.Unmarshal(plan, &data)
	return &data
}

func TestRestGet(t *testing.T) {
	tests.Init()
	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &model.Query{
				Type: "namespaces",
				Name: "f8",
			},
			Logger: common.GetLoggerByID("get-namespace"),
		},
		ic: tests.NewFakeKubeClient(),
	}
	if err := s.loadClient(); err != nil {
		t.Error("load client error:", err)
	}
	s.Client = tests.NewFakeRestClient(schema.GroupVersion{Group: "", Version: "v1"},
		tests.DefaultHandler(tests.LoadFile("../tests/fixtures/namespace.json")))
	result, err := s.Request(common.HTTPGet, nil)
	if err != nil {
		t.Errorf("request get error: %+v %+v", result, err)
	}

	// test parse result
	if err := s.ParseResultData(result); err != nil {
		t.Error("parse result error:", err)
	}

	if s.Object.GetName() != "f8" || s.Object.GetKind() != "Namespace" {
		t.Error(" get object error:", s.Object)
	}

}

func TestList(t *testing.T) {
	tests.Init()
	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &model.Query{
				Type: "namespaces",
			},
			Logger: common.GetLoggerByID("list-namespace"),
		},
		ic: tests.NewFakeKubeClient(),
	}
	err := s.init()
	if err != nil {
		t.Error("init store error: ", err)
	}
	data := LoadObjects("../tests/fixtures/namespace_list.json")
	var objects []runtime.Object
	for _, o := range data.Items {
		o := o
		objects = append(objects, &o)
	}

	options := &model.ListOptions{}
	s.ResourceClient = tests.NewFakeDynamicInterface("namespaces", objects...)
	s.Client = tests.NewFakeRestClient(schema.GroupVersion{Version: "v1"}, func(req *http.Request) (*http.Response, error) {
		listOpt := options.Options
		res, _ := s.ResourceClient.List(listOpt)
		res.SetKind("Namespace")
		res.SetAPIVersion("v1")
		body, _ := res.MarshalJSON()
		resp := &http.Response{
			Status:        "200 OK",
			StatusCode:    200,
			Proto:         "HTTP/1.1",
			ProtoMajor:    1,
			ProtoMinor:    1,
			Body:          ioutil.NopCloser(bytes.NewReader(body)),
			ContentLength: int64(len(body)),
			Request:       req,
			Header:        make(http.Header),
		}
		resp.Header.Add("Content-Type", "application/json")
		return resp, nil
	})

	// list with labelSelector
	options.Options = v1.ListOptions{
		LabelSelector: "api=default",
	}

	if err := s.FetchResourceList(*options); err != nil {
		t.Errorf("list error: %v", err)
		return
	}
	names := []string{"default-system", "f8"}
	sObjects := s.GetObjects()
	if len(sObjects.Items) != 2 || !common.StringInSlice(sObjects.Items[0].GetName(), names) || !common.StringInSlice(sObjects.Items[1].GetName(), names) {
		t.Error("bad label selector result:", s.Objects)
	}

	// name search
	options.Options = v1.ListOptions{}
	options.NameSearch = "f8"
	if err := s.FetchResourceList(*options); err != nil {
		t.Error("list error")
	}
	sObjects = s.GetObjects()
	if len(sObjects.Items) != 1 || sObjects.Items[0].GetName() != "f8" {
		t.Error("list search error", s.Objects)
	}

	// name filter
	options.NameSearch = ""
	options.NameFilters = []model.ObjectNameTuple{
		{
			Name: "3",
		},
		{
			Name: "alauda-system",
		},
		{
			Name: "4",
		},
	}
	if err := s.FetchResourceList(*options); err != nil {
		t.Error("list error")
	}
	sObjects = s.GetObjects()
	if len(sObjects.Items) != 2 || sObjects.Items[0].GetName() != "3" {
		for _, o := range sObjects.Items {
			found := false
			for _, f := range options.NameFilters {
				if o.GetName() == f.Name && o.GetNamespace() == f.Namespace {
					found = true
					break
				}
			}
			if !found {
				t.Error("list name filter error")
			}
		}
	}
}

func TestGet(t *testing.T) {
	tests.Init()
	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &model.Query{
				Type: "namespaces",
				Name: "f8",
			},
			Logger: common.GetLoggerByID("get-namespace"),
		},
		ic: tests.NewFakeKubeClient(),
	}
	err := s.init()
	if err != nil {
		t.Error("init store error: ", err)
	}
	object := LoadObject("../tests/fixtures/namespace.json")
	s.ResourceClient = tests.NewFakeDynamicInterface("namespaces", []runtime.Object{object}...)
	result, err := s.Get()
	if err != nil {
		t.Error("get namespace error: ", err)
	}
	if result.GetName() != "f8" || result.GetKind() != "Namespace" {
		t.Error(" get namespace error:", result)
	}

}

func TestCreateOrUpdateResource(t *testing.T) {
	tests.Init()
	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &model.Query{
				Type: "namespaces",
			},
			Object: LoadObject("../tests/fixtures/namespace.json"),
			Logger: common.GetLoggerByID("create-or-update-namespaces"),
		},
		ic: tests.NewFakeKubeClient(),
	}
	_, err := s.CreateOrUpdateResource()
	if err != nil {
		t.Error("create or update namespace error:", err)
	}
	s.Object.SetAnnotations(map[string]string{"new": "new"})

	s.ResourceClient = tests.NewFakeDynamicInterface("namespaces", []runtime.Object{s.Object}...)
	result, err := s.CreateOrUpdateResource()
	if err != nil {
		t.Error("update namespace error:", err)
	}
	if result.GetAnnotations()["new"] != "new" {
		t.Error("wrong result after update namespace:", result)
	}

}
