package store

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"sync"
	"time"

	"krobelus/common"
	"krobelus/model"
	"krobelus/pkg/application/v1beta1"

	"github.com/Jeffail/gabs"
	ac "github.com/alauda/cyborg/pkg/client"
	"github.com/opentracing/opentracing-go"
	"github.com/spf13/cast"
	"github.com/thoas/go-funk"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type AppStoreInterface interface {
	ResourceManagerInterface

	InstallAppCrd() error

	GetApplication() (*model.Application, error)

	CreateAppResources(resources []*model.KubernetesObject) ([]*unstructured.Unstructured, []error)

	GetAppSubResources(app *v1beta1.Application) (objects []*model.KubernetesObject, err error)

	CheckResourcesExist(resources []*model.KubernetesObject) (bool, error)

	UpdateAppResources(resources []*model.KubernetesObject) ([]*unstructured.Unstructured, []error)

	UpdateApplication(request *model.ApplicationRequest, oldApp *model.Application) (*rest.Result, []error)

	CreateAppCrdResource() (*rest.Result, error)

	StartApplication() error

	StopApplication() error

	DeleteApplication(app *model.Application) error

	ImportResourcesToApplication(request *model.ApplicationImportResourcesRequest, app *model.Application) (*rest.Result, error)

	ExportResourcesFromApplication(request *model.ApplicationExportResourcesRequest, app *model.Application) (*rest.Result, error)

	ListApplication(ctx context.Context, params map[string]string) ([]*model.ApplicationResponse, int, error)

	UpdateAppPhase(phase string, key, val string) (*rest.Result, error)

	SendApplicationWarningEvent(app *model.Application, err error)
	SendApplicationNormalEvent(app *model.Application, message, reason string)
}

type AppStore struct {
	KubernetesResourceStore
}

func GetAppStore(query *model.Query, logger common.Log, object *model.KubernetesObject) (AppStoreInterface, error) {
	s, err := GetStore(query, logger, object)
	//TODO: find out why
	if err != nil && ac.IsResourceTypeNotFound(err) {
		s.Logger.WithError(err).Warn("use static config for applications types")
		s.TypeMeta = &metav1.TypeMeta{
			Kind:       "Application",
			APIVersion: "app.k8s.io/v1beta1",
		}

		ic, err := dynamic.NewForConfig(s.ic.GetRestConfig())
		if err != nil {
			return nil, err
		}

		s.ResourceClient = ic.Resource(schema.GroupVersionResource{
			Group:    "app.k8s.io",
			Version:  "v1beta1",
			Resource: "applications",
		}).Namespace(s.GetNamespace())

		return &AppStore{KubernetesResourceStore: *s}, nil
	}

	return &AppStore{KubernetesResourceStore: *s}, err
}

func (s *AppStore) InstallAppCrd() error {
	return s.InstallCRD("applications.app.k8s.io", &v1beta1.ApplicationCRD)
}

// CreateAppCrdResource will create application resource first, it the crd def doest not exist, install
// the crd and retry
func (s *AppStore) CreateAppCrdResource() (*rest.Result, error) {
	result, err := s.Request("POST", nil)
	if err != nil {
		if ac.IsResourceTypeNotFound(err) {
			if err := s.InstallAppCrd(); err != nil {
				return result, err
			} else {
				result, err := s.Request("POST", nil)
				return result, err
			}
		}
	}
	return result, err
}

func (s *AppStore) GetApplication() (*model.Application, error) {
	err := s.FetchResource(model.GetOptions{})
	if err != nil {
		return nil, err
	}

	app, err := s.Object.ToApplication()
	if err != nil {
		return nil, err
	}

	appModel := model.Application{
		Cluster: model.ClusterInfo{
			s.UniqueName.ClusterUUID,
			s.UniqueName.ClusterToken,
		},
		Crd: *s.Object,
	}

	appModel.Kubernetes, err = s.GetAppSubResources(app)
	if err != nil {
		return nil, err
	}

	return &appModel, nil
}

func (s *AppStore) UpdateApplication(request *model.ApplicationRequest, oldApp *model.Application) (*rest.Result, []error) {
	result, err := s.UpdateAndRetry()
	if err != nil {
		return result, []error{err}
	}

	diffResources, err := model.ComputeResourceDiffs(oldApp.Kubernetes, request.Kubernetes, model.ObjectKeyWithNamespace)
	if err != nil {
		return nil, []error{err}
	}

	creates, updates, deletes := diffResources.GetDiffResources()

	s.SendApplicationNormalEvent(oldApp, "Preparing to update application sub resources", common.ReasonUpdating)

	var wg sync.WaitGroup
	errChan := make(chan error, len(creates)+len(updates)+len(deletes))
	createOrUpdates := append(creates, updates...)
	for _, obj := range createOrUpdates {
		wg.Add(1)
		go func(o *model.KubernetesObject) {
			defer wg.Done()
			resource, err := s.ic.GetResourceByKind(o.Kind)
			if err != nil {
				if ac.IsResourceTypeNotFound(err) {
					errChan <- err
					return
				}
				errChan <- common.BuildKubernetesError(err.Error())
				return
			}
			krs, err := s.CopyWithNewObj(resource, o)
			if err != nil {
				errChan <- err
				return
			}

			// use retry to avoid conflict
			action := func(attempt uint) error {
				_, err = krs.CreateOrUpdateResource()
				return err
			}

			err = common.BackoffLimitRetryWhenServerErr(action, 3, 1*time.Second)
			if err != nil {
				errChan <- err
				return
			}
		}(obj)
	}

	appCrd := oldApp.Crd.ToAppCrd()
	appSelector := model.GetAppLabelSelector(appCrd)
	for _, obj := range deletes {
		wg.Add(1)
		go func(o *model.KubernetesObject) {
			defer wg.Done()
			resource, err := s.ic.GetResourceByKind(o.Kind)
			if err != nil {
				if ac.IsResourceTypeNotFound(err) {
					errChan <- err
					return
				}
				errChan <- err
				return
			}
			krs, err := s.Copy(resource, false, true)
			if err != nil {
				errChan <- err
				return
			}
			krs.SetNamespaceAndName(o)
			krs.SetObject(o)
			err = krs.CascadeDelete(appSelector)
			if err != nil {
				errChan <- err
				return
			}
		}(obj)
	}
	go func() {
		wg.Wait()
		close(errChan)
	}()

	var errList []error
	for err := range errChan {
		errList = append(errList, err)
	}

	if len(errList) > 0 {
		s.Logger.Errorf("Update app error: %s", errList)
		krobelusErr := common.BuildKrobelusErrors(errList)
		s.SendApplicationWarningEvent(oldApp, krobelusErr)
		PatchApplication(oldApp, model.GenAnnotationPatchData(common.AppErrorReasonKey(), krobelusErr.Error()))
		return nil, errList
	}

	s.Logger.Infof("Update app success")
	s.SendApplicationNormalEvent(oldApp, "All sub resources have been synced", common.ReasonSynced)
	finalRes, err := s.UpdateAppPhase(string(v1beta1.Succeeded), "", "")
	if err != nil {
		s.Logger.Errorf("Update application phase error: %s", err.Error())
		return result, nil
	}
	return finalRes, nil
}

// CreateResources
func (s *AppStore) CreateAppResources(resources []*model.KubernetesObject) ([]*unstructured.Unstructured, []error) {
	if err := s.loadClient(); err != nil {
		return nil, []error{err}
	}

	app := model.Application{Cluster: model.ClusterInfo{UUID: s.UniqueName.ClusterUUID, Token: s.UniqueName.ClusterToken}, Crd: *s.Object}

	resultList := make(chan *unstructured.Unstructured, len(resources))
	errList := make(chan error, len(resources))
	var wg sync.WaitGroup
	for _, item := range resources {
		wg.Add(1)

		go func(obj *model.KubernetesObject) {
			defer wg.Done()
			resource, err := s.ic.GetResourceByKind(obj.Kind)
			if err != nil {
				errList <- err
				return
			}

			ns, err := s.CopyWithNewObj(resource, obj)
			if err != nil {
				errList <- err
				return
			}

			result, err := ns.Create()
			if err != nil {
				s.SendApplicationWarningEvent(&app, err)
				s.UpdateAppPhase(v1beta1.Failed, common.AppErrorReasonKey(), err.Error())
				errList <- err
				return
			}
			msg := fmt.Sprintf("Create resource: %s/%s", obj.Kind, obj.Name)
			s.SendApplicationNormalEvent(&app, msg, common.ReasonSynced)
			resultList <- result
		}(item)
	}
	go func() {
		wg.Wait()
		close(resultList)
		close(errList)
	}()

	var errs []error
	var results []*unstructured.Unstructured

	for item := range resultList {
		results = append(results, item)
	}

	for err := range errList {
		errs = append(errs, err)
	}

	if len(errs) == 0 {
		s.SendApplicationNormalEvent(&app, "All resources synced", common.ReasonSynced)
	}

	return results, errs

}

// CheckResourcesExist check if the resources already exist in cluster.
// If any of the resources exist, return true
func (s *AppStore) CheckResourcesExist(resources []*model.KubernetesObject) (bool, error) {
	if err := s.loadClient(); err != nil {
		return false, err
	}
	// check if sub-resources exist first, if any exist, return error
	for _, item := range resources {
		s.Logger.Debugf("Check if %s/%s already exist", item.Kind, item.Name)
		resource, err := s.ic.GetResourceByKind(item.Kind)
		if err != nil {
			return false, err
		}

		ns := getKubernetesResourceStore(&model.Query{
			ClusterUUID:  s.UniqueName.ClusterUUID,
			ClusterToken: s.UniqueName.ClusterToken,
			Namespace:    item.Namespace,
			Name:         item.Name,
			Type:         resource,
		}, s.Logger, nil)
		if err := ns.FetchResource(model.GetOptions{}); err != nil {
			if k8sErrors.IsNotFound(err) {
				continue
			} else {
				return false, err
			}
		} else {
			return true, fmt.Errorf("resource %s/%s already exist", item.Kind, item.Name)
		}
	}
	return false, nil
}

func (s *KubernetesResourceStore) getResourceByKind(kind string) (string, error) {
	if err := s.loadClient(); err != nil {
		return "", err
	}
	return s.ic.GetResourceByKind(kind)
}

func (s *KubernetesResourceStore) SyncResourceTypes() {
	s.getResourceByKind("")
}

// GetAppSubResources get applications' sub resources,
// the result will be sorted by kind
func (s *AppStore) GetAppSubResources(app *v1beta1.Application) (objects []*model.KubernetesObject, err error) {
	selector := model.GetAppLabelSelector(app)
	if err := s.loadClient(); err != nil {
		return nil, err
	}

	// If selector is empty, use app name selector as default.
	if selector == "" {
		selector = model.MatchLabelsToParamString(model.GetAppSelectorWithName(app))
	}

	// This can be nil, return to avoid api 500
	if app.Spec.ComponentGroupKinds == nil {
		s.Logger.Warnf("This applications is not valid, skip: %s", s.String())
		return
	}

	var wg sync.WaitGroup
	var lock sync.Mutex
	for _, ck := range app.Spec.ComponentGroupKinds {
		ck := ck
		/*		resource, err := s.ResourceTypeForGVK(schema.GroupVersionKind{
				Group: ck.Group,
				Kind:  ck.Kind,
			})*/

		wg.Add(1)
		go func() {
			defer wg.Done()
			resource, err := s.ic.GetResourceByKind(ck.Kind)
			if err != nil {
				s.Logger.Warnf("Find resource type error for %s/%s: %s", ck.Group, ck.Kind, err.Error())
				return
			}

			namespace := s.UniqueName.Namespace
			if _, exist := common.ClusterScopeResourceKind[ck.Kind]; exist {
				namespace = ""
			}

			krs := getKubernetesResourceStore(&model.Query{
				ClusterUUID:  s.UniqueName.ClusterUUID,
				ClusterToken: s.UniqueName.ClusterToken,
				Name:         "",
				Type:         resource,
				Namespace:    namespace,
			}, s.Logger, nil)

			options := model.ListOptions{
				Context: s.Context,
				Options: metav1.ListOptions{
					LabelSelector: selector,
				},
			}

			err = krs.FetchResourceList(options)
			if err != nil {
				s.Logger.WithError(err).Error("list sub resources error: ", resource)
				return
			}

			for idx, item := range krs.GetObjects().Items {
				if item.GetKind() == "" {
					krs.Objects.Items[idx].SetKind(ck.Kind)
					s.Logger.Debugf("set object kind: %s", ck.Kind)
				}
				if item.GetAPIVersion() == "" {
					krs.Objects.Items[idx].SetAPIVersion(krs.Objects.GetAPIVersion())
					s.Logger.Debugf("set object apiVersion: %s", krs.Objects.GetAPIVersion())
				}

			}
			lock.Lock()
			objects = append(objects, krs.GetKubernetesObjects()...)
			lock.Unlock()
		}()
	}
	wg.Wait()
	sort.Slice(objects, func(i, j int) bool {
		return strings.Compare(objects[i].Kind, objects[j].Kind) < 0
	})

	return
}

// UpdateAppResources update application resources, eg: update replicas
func (s *AppStore) UpdateAppResources(resources []*model.KubernetesObject) ([]*unstructured.Unstructured, []error) {
	resultList := make(chan *unstructured.Unstructured, len(resources))
	errList := make(chan error, len(resources))
	var wg sync.WaitGroup
	for _, item := range resources {
		wg.Add(1)

		go func(obj *model.KubernetesObject) {
			defer wg.Done()
			resource, err := s.ic.GetResourceByKind(obj.Kind)
			if err != nil {
				s.Logger.WithError(err).Error("get resource type error when update object:", err)
				errList <- err
				return
			}

			ns, err := s.CopyWithNewObj(resource, obj)
			if err != nil {
				s.Logger.WithError(err).Error("init resource store error when update object:", err)
				errList <- err
				return
			}

			result, err := ns.Update()
			if err != nil {
				s.Logger.WithError(err).Error("update object error:", err)
				errList <- err
				return
			}
			s.Logger.Infof("update app resource done: %s", obj.GetString())
			resultList <- result
		}(item)
	}
	go func() {
		wg.Wait()
		close(resultList)
		close(errList)
	}()

	var errs []error
	var results []*unstructured.Unstructured

	for item := range resultList {
		results = append(results, item)
	}

	for err := range errList {
		errs = append(errs, err)
	}

	return results, errs
}

// StartApplication will scale all workloads objects to it's origin replicas
func (s *AppStore) StartApplication() error {
	crd := s.Object.ToAppCrd()
	app := model.Application{
		Cluster: model.ClusterInfo{
			s.UniqueName.ClusterUUID,
			s.UniqueName.ClusterToken,
		},
		Crd: *s.Object,
	}
	resources, err := s.GetAppSubResources(crd)
	if err != nil {
		return err
	}
	workloads := funk.Filter(resources, func(x *model.KubernetesObject) bool { return common.IsPodController(x.Kind) }).([]*model.KubernetesObject)
	var toBeUpdated []*model.KubernetesObject

	for _, item := range workloads {

		if item.Kind == "DaemonSet" {
			s.Logger.Infof("Skip daemonset when start application: %s", item.GetName())
			continue
		}

		replicas, err := item.GetPodReplicas()
		if err != nil {
			s.Logger.Errorf("parse replicas for resource error: %+v", item)
			s.SendApplicationWarningEvent(&app, err)
			return err
		}

		if replicas != 0 {
			s.Logger.Infof("skip workload when start because replicas!=0: %s/%s", item.Kind, item.GetName())
			continue
		}

		lastReplicas, ok := item.GetAnnotations()[common.AppLastReplicasKey()]
		if !ok {
			s.Logger.Warnf("cannot found last replicas for workload when start， set to 1: %s/%s", item.Kind, item.GetName())
			lastReplicas = "1"
		}
		if lastReplicas == "0" {
			s.Logger.Warnf("workload last replicas is 0, set to 1: %s/%s", item.Kind, item.GetName())
			lastReplicas = "1"
		}
		item.SetPodReplicas(cast.ToFloat64(lastReplicas))
		toBeUpdated = append(toBeUpdated, item)

	}

	_, errs := s.UpdateAppResources(toBeUpdated)
	if err = common.CombineErrors(errs); err != nil {
		s.SendApplicationWarningEvent(&app, err)
		return err
	}
	s.SendApplicationNormalEvent(&app, "Application pods have been started", common.ReasonStarted)
	return PatchApplication(&app, model.GenSimpleApplicationUpdateData(string(v1beta1.Succeeded),
		common.AppActionKey(), "start"))
}

// StopApplication will trying to `stop` a application by scale down all the workloads.
// To do so, we need to keep track the origin replicas of the workload and.
func (s *AppStore) StopApplication() error {
	s.Logger.Debug("stop application")
	app := model.Application{
		Cluster: model.ClusterInfo{
			s.UniqueName.ClusterUUID,
			s.UniqueName.ClusterToken,
		},
		Crd: *s.Object,
	}
	resources, err := s.GetAppSubResources(s.Object.ToAppCrd())
	if err != nil {
		return err
	}

	workloads := funk.Filter(resources, func(x *model.KubernetesObject) bool { return common.IsPodController(x.Kind) }).([]*model.KubernetesObject)
	var toBeUpdated []*model.KubernetesObject

	for _, item := range workloads {
		if item.Kind == "DaemonSet" {
			s.Logger.Infof("Skip daemonset when stop application: %s", item.GetName())
			continue
		}
		replicas, err := item.GetPodReplicas()
		if err != nil {
			s.Logger.Errorf("parse replicas for resource error: %+v", item)
			s.SendApplicationWarningEvent(&app, err)
			return err
		}

		if replicas == 0 {
			s.Logger.Infof("skip workload when stop because replicas=0: %s/%s", item.Kind, item.GetName())
			continue
		}
		item.SetAno(common.AppLastReplicasKey(), cast.ToString(replicas))
		item.SetPodReplicas(0)
		s.Logger.Debug("preparing to stop workload: ", item.GetString())
		toBeUpdated = append(toBeUpdated, item)
	}

	_, errs := s.UpdateAppResources(toBeUpdated)
	if err = common.CombineErrors(errs); err != nil {
		s.SendApplicationWarningEvent(&app, err)
		return err
	}
	s.SendApplicationNormalEvent(&app, "Application pods have been stopped", common.ReasonStopped)
	return PatchApplication(&app, model.GenSimpleApplicationUpdateData(string(v1beta1.Succeeded),
		common.AppActionKey(), "stop"))
}

// DeleteApplication deletes application resources first, and mark the app as deleted.
// The application will be deleted after all app resources are deleted successfully.
func (s *AppStore) DeleteApplication(app *model.Application) error {
	appCrd := s.Object.ToAppCrd()
	appCrd.Spec.AssemblyPhase = v1beta1.Pending
	if _, err := s.UpdateAppPhase(string(v1beta1.Pending), common.AppDeletedAtKey(), time.Now().String()); err != nil {
		return common.BuildKubernetesError(err.Error())
	}

	var wg sync.WaitGroup
	errChan := make(chan error, len(app.Kubernetes))
	appSelector := model.GetAppLabelSelector(appCrd)
	for _, obj := range app.Kubernetes {
		wg.Add(1)
		go func(object *model.KubernetesObject) {
			defer wg.Done()
			resource, err := s.GetResourceTypeByKind(object.GetKind())
			if err != nil {
				errChan <- common.BuildKubernetesError(err.Error())
				return
			}
			krs, err := s.Copy(resource, false, false)
			if err != nil {
				errChan <- common.BuildKubernetesError(err.Error())
				return
			}
			krs.SetNamespaceAndName(object)
			krs.SetObject(object)
			err = krs.CascadeDelete(appSelector)
			if err != nil {
				s.Logger.Errorf("Delete app resource %s:%s error: %s", object.Namespace, object.Name, err.Error())
				errChan <- common.BuildKubernetesError(err.Error())
				return
			}

			s.Logger.Infof("Delete app resource %s %s/%s done", object.Kind, object.Namespace, object.Name)
			s.SendApplicationNormalEvent(app, fmt.Sprintf("Delete app resource %s %s/%s done", object.Kind,
				object.Namespace, object.Name), common.ReasonDeleting)
		}(obj)
	}
	go func() {
		wg.Wait()
		close(errChan)
	}()

	var errList []error
	for err := range errChan {
		errList = append(errList, err)
	}

	if len(errList) > 0 {
		s.Logger.Errorf("Delete app resources error: %s", errList)
		krobelusErr := common.BuildKrobelusErrors(errList)
		s.SendApplicationWarningEvent(app, krobelusErr)
		s.UpdateAppPhase(string(v1beta1.Failed), common.AppErrorReasonKey(), krobelusErr.Error())
		return krobelusErr
	}

	s.Object = nil
	s.Raw = nil
	_, err := s.Request(common.HTTPDelete, nil)
	if err != nil {
		s.Logger.Errorf("Delete app resources error: %s", errList)
		s.UpdateAppPhase(string(v1beta1.Failed), common.AppErrorReasonKey(), err.Error())
		return common.BuildKubernetesError(err.Error())
	}

	s.Logger.Infof("Delete app done")
	s.SendApplicationNormalEvent(app, fmt.Sprintf("Delete app %s/%s done", app.Crd.Namespace, app.Crd.Name), common.ReasonDeleted)
	return nil
}

func (s *AppStore) ImportResourcesToApplication(request *model.ApplicationImportResourcesRequest, app *model.Application) (*rest.Result, error) {
	uuid := app.Crd.Labels[common.AppUIDKey()]
	appSelectorName := model.GetAppSelectorName(app.Crd.Name, app.Crd.Namespace)

	for _, obj := range request.Kubernetes {
		resource, err := s.GetResourceTypeByKind(obj.GetKind())
		if err != nil {
			return nil, err
		}

		namespace := app.Crd.Namespace
		if _, exist := common.ClusterScopeResourceKind[obj.Kind]; exist {
			namespace = ""
		}

		krs := getKubernetesResourceStore(&model.Query{
			ClusterUUID:  s.UniqueName.ClusterUUID,
			ClusterToken: s.UniqueName.ClusterToken,
			Name:         obj.Name,
			Namespace:    namespace,
			Type:         resource,
			PatchType:    string(types.MergePatchType),
		}, s.Logger, nil)

		if common.IsPodController(obj.Kind) {
			//TODO: check error
			krs.FetchResource(model.GetOptions{})
			krs.Object.SetLabel(common.AppNameKey(), appSelectorName)
			krs.Object.SetLabel(common.AppUIDKey(), uuid)
			krs.Object.AddPodTemplateLabels(map[string]string{
				common.AppNameKey(): appSelectorName,
			})
			_, err = krs.Request(common.HTTPPut, nil)
		} else {
			err = krs.addResourceLabels(map[string]string{
				common.AppNameKey(): appSelectorName,
				common.AppUIDKey():  uuid,
			})
		}

		if err != nil {
			return nil, err
		}
	}
	result, err := s.Request(common.HTTPPut, nil)
	return result, err
}

func (s *AppStore) ExportResourcesFromApplication(request *model.ApplicationExportResourcesRequest, app *model.Application) (*rest.Result, error) {
	uuid := app.Crd.Labels[common.AppUIDKey()]

	for _, obj := range request.Kubernetes {
		resource, err := s.GetResourceTypeByKind(obj.GetKind())
		if err != nil {
			return nil, err
		}

		namespace := app.Crd.Namespace
		if _, exist := common.ClusterScopeResourceKind[obj.Kind]; exist {
			namespace = ""
		}

		krs := getKubernetesResourceStore(&model.Query{
			ClusterUUID:  s.UniqueName.ClusterUUID,
			ClusterToken: s.UniqueName.ClusterToken,
			Name:         obj.Name,
			Namespace:    namespace,
			Type:         resource,
		}, s.Logger, nil)

		if common.IsPodController(obj.Kind) {
			//TODO: remove app env
			krs.FetchResource(model.GetOptions{})
			krs.Object.DeleteLabel(common.AppNameKey())
			krs.Object.DeleteLabel(common.AppUIDKey())
			krs.Object.RemovePodTemplateLabels([]string{common.AppNameKey()})
			krs.Object.SetOwnerReferences(nil) //delete OwerReference
			_, err = krs.Request(common.HTTPPut, nil)
		} else {
			err = krs.removeResourceLabels(map[string]string{
				common.AppNameKey(): app.Crd.Name,
				common.AppUIDKey():  uuid,
			})
			krs.Object.SetOwnerReferences(nil) //delete OwerReference
		}

		if err != nil {
			return nil, err
		}
	}
	result, err := s.Request(common.HTTPPut, nil)
	return result, err
}

func (s *KubernetesResourceStore) addResourceLabels(labels map[string]string) error {
	jsonObj := gabs.New()
	jsonObj.SetP(labels, "metadata.labels")
	s.SetRaw(jsonObj.Bytes())
	_, err := s.Request(common.HTTPPatch, nil)
	return err
}

func (s *KubernetesResourceStore) removeResourceLabels(labels map[string]string) error {
	if err := s.FetchResource(model.GetOptions{}); err != nil {
		return err
	}
	if s.Object.Labels != nil {
		for k := range labels {
			s.Object.DeleteLabel(k)
		}
		_, err := s.Request(common.HTTPPut, nil)
		return err
	}
	return nil
}

// ListApplication list application
// params:
// 1. start/end: slice the result
// 2. simple: if true, only return application crd
// returns:
//  []*model.ApplicationResponse: applications query result
//  int: total count of the applications
//  error: nil if success, not nil if failed
func (s *AppStore) ListApplication(ctx context.Context, params map[string]string) ([]*model.ApplicationResponse, int, error) {
	simple := params["simple"] == "true"
	s.Context = ctx
	options := model.ListOptions{
		Context:         ctx,
		NameSearch:      params["name"],
		PartialMetadata: simple,
	}
	err := s.FetchResourceList(options)
	if err != nil {
		return nil, 0, err
	}

	parallel := false

	var data []*model.KubernetesObject
	var total int
	traceFunc(s.Context, "Get App Objects", func(_ opentracing.Span) error {
		data = s.GetKubernetesObjects()
		total = len(data)
		return nil
	})
	if total == 0 {
		return []*model.ApplicationResponse{}, total, nil
	}

	// traceFunc(s.Context, "Sort App Objects", func(span opentracing.Span) error {
	// 	sort.Slice(data, func(i, j int) bool {
	// 		if data[i].GetCreationTimestamp().Time.Equal(data[j].GetCreationTimestamp().Time) {
	// 			// If create time is equal, compare the uid
	// 			return data[i].GetUID() > data[j].GetUID()
	// 		}
	// 		return data[i].GetCreationTimestamp().After(data[j].GetCreationTimestamp().Time)
	// 	})
	// 	return nil
	// })

	if params != nil {
		start, ok := params["start"]
		if ok {
			// only parallel in page query
			parallel = true

			total := len(data)
			start := cast.ToInt(start)
			end := cast.ToInt(params["end"])
			s.Logger.Debugf("Page app list result by: %v-%v", start, end)
			if end <= total {
				data = data[start:end]
			} else if start > total {
				data = data[0:0]
			} else {
				data = data[start:total]
			}
		}
	}

	t := time.Now()

	if parallel {
		var result []*model.ApplicationResponse
		err := traceFunc(s.Context, "List Application Parallel", func(_ opentracing.Span) error {
			appList := make(chan *model.ApplicationResponse, len(data))
			errList := make(chan error, len(data))
			var wg sync.WaitGroup

			for _, obj := range data {
				wg.Add(1)
				go func(obj *model.KubernetesObject) {
					defer wg.Done()
					app, err := obj.ToApplication()
					if err != nil {
						errList <- err
						return
					}

					s.Logger.Debugf("Retrieve sub-resources for app: %s", obj.GetString())

					var resources []*model.KubernetesObject

					if !simple {
						resources, err = s.GetAppSubResources(app)
					}
					if err != nil {
						errList <- err
						return
					}
					if !simple {
						setAppStatus(app, resources, s.Logger)
					}

					appResp := model.ApplicationResponse{}
					appResp.Kubernetes = []*model.KubernetesObject{model.AppCrdToObject(app)}

					appResp.Kubernetes = append(appResp.Kubernetes, resources...)
					appList <- &appResp
				}(obj)
			}
			go func() {
				wg.Wait()
				close(appList)
				close(errList)
			}()
			if len(errList) > 0 {
				for err := range errList {
					return err
				}
			}

			for item := range appList {
				result = append(result, item)
			}
			// sort.SliceStable(result, func(i, j int) bool {
			// 	if result[i].Kubernetes[0].GetCreationTimestamp().Time.Equal(result[j].Kubernetes[0].GetCreationTimestamp().Time) {
			// 		// If create time is equal, compare the uid
			// 		return result[i].Kubernetes[0].UID > result[j].Kubernetes[0].UID
			// 	}
			// 	return result[i].Kubernetes[0].GetCreationTimestamp().After(result[j].Kubernetes[0].GetCreationTimestamp().Time)
			// })
			s.Logger.Debugf("List %d applications cost: %v", len(result), time.Since(t))
			return nil
		})
		if err != nil {
			return nil, 0, err
		}
		return result, total, nil
	}

	// Serially fetch app resources
	var appList []*model.ApplicationResponse
	err = traceFunc(s.Context, "List Application Serially", func(_ opentracing.Span) error {
		for _, obj := range data {
			app, err := obj.ToApplication()
			if err != nil {
				return err
			}
			s.Logger.Debugf("Retrieve sub-resources for app: %s", obj.GetString())

			var resources []*model.KubernetesObject
			if !simple {
				resources, err = s.GetAppSubResources(app)
			}
			if err != nil {
				return err
			}
			if !simple {
				setAppStatus(app, resources, s.Logger)
			}

			appResp := model.ApplicationResponse{}
			appResp.Kubernetes = []*model.KubernetesObject{model.AppCrdToObject(app)}
			appResp.Kubernetes = append(appResp.Kubernetes, resources...)
			appList = append(appList, &appResp)
		}
		return nil
	})
	if err != nil {
		return nil, 0, err
	}
	s.Logger.Debugf("List %d applications cost: %v", len(appList), time.Since(t))
	return appList, total, nil
}

// UpdateAppPhase updates the `assemblyPhase` and `annotations` of the application resource
// through `PATCH` request, and update the cache if success.
func (s *AppStore) UpdateAppPhase(phase string, key, val string) (*rest.Result, error) {
	s.UniqueName.PatchType = string(types.MergePatchType)
	s.Object = nil
	s.Raw = model.GenSimpleApplicationUpdateData(phase, key, val)
	result, err := s.Request(common.HTTPPatch, nil)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (s *AppStore) SendApplicationWarningEvent(app *model.Application, err error) {
	ev := model.Event{
		Message: err.Error(),
		Type:    common.EventTypeWarning,
		Reason:  common.FailedSync,
		UID:     string(app.Crd.GetUID()),
	}
	errSend := SendApplicationEvent(app, &ev, s.Logger)
	if errSend != nil {
		s.Logger.Errorf("Send application event error: %s", errSend.Error())
	}
}

func (s *AppStore) SendApplicationNormalEvent(app *model.Application, message, reason string) {

	ev := model.Event{
		Message: message,
		Type:    common.EventTypeNormal,
		Reason:  reason,
		UID:     string(app.Crd.GetUID()),
	}
	errSend := SendApplicationEvent(app, &ev, s.Logger)
	if errSend != nil {
		s.Logger.Errorf("Send application event error: %s", errSend.Error())
	}
}

func UpdateApplicationPhase(cid string, token string, app *model.KubernetesObject, phase v1beta1.ApplicationAssemblyPhase) error {
	application := model.Application{
		Crd: *app,
		Cluster: model.ClusterInfo{
			UUID:  cid,
			Token: token,
		},
	}
	return PatchApplication(&application, model.GenSimpleApplicationUpdateData(string(phase), "", ""))
}

func UpdateApplicationRevision(cid string, token string, app *model.KubernetesObject, revision int) error {
	application := model.Application{
		Crd: *app,
		Cluster: model.ClusterInfo{
			UUID:  cid,
			Token: token,
		},
	}
	return PatchApplication(&application, model.GenAnnotationPatchData(common.AppRevisionKey(), fmt.Sprintf("%d", revision)))
}

// PatchApplication will update application crd by PATCH method.
// requirements: application.Crd set
func PatchApplication(application *model.Application, raw []byte) error {
	query := model.Query{
		ClusterUUID:  application.GetClusterUUID(),
		ClusterToken: application.GetClusterToken(),
		Namespace:    application.GetNamespace(),
		Type:         "applications",
		Name:         application.Crd.Name,
		PatchType:    "application/merge-patch+json",
	}

	s, err := GetAppStore(&query, common.GetLoggerByKV("app", application.GetUUID()), nil)
	if err != nil {
		return err
	}
	s.SetRaw(raw)

	_, err = s.Request("PATCH", nil)
	return err
}

func SendApplicationEvent(app *model.Application, ev *model.Event, logger common.Log) error {
	ev.Name = app.GetName()
	ev.Namespace = app.GetNamespace()
	// allow caller to custom uid
	if ev.UID == "" {
		ev.UID = string(app.Crd.GetUID())
	}

	event := model.GenKubernetesApplicationEvent(ev)
	obj, err := model.ToKubernetesObject(event)
	if err != nil {
		return err
	}
	krs, err := GetStore(&model.Query{
		ClusterUUID:  app.GetClusterUUID(),
		ClusterToken: app.GetClusterToken(),
		Name:         ev.Name,
		Type:         "events",
		Namespace:    ev.Namespace,
	}, logger, obj)
	if err != nil {
		return err
	}
	_, err = krs.Create()
	return err
}

// VerbosePodMessages return reason and message for pods that is not in Running phase
func VerbosePodMessages(ctx context.Context, application *model.Application, log common.Log) []model.ObjectMessage {

	logger := log.WithField("action", "VerbosePodMessages")

	msg := []model.ObjectMessage{}

	namespace := application.GetNamespace()

	crd := application.Crd.ToAppCrd()

	labelSelector, err := metav1.LabelSelectorAsSelector(crd.Spec.Selector)

	if nil != err {
		logger.WithError(err).Error("parse error: invalid crd selector")
		return msg
	}

	query := model.Query{
		ClusterUUID: application.GetClusterUUID(),
		Namespace:   namespace,
		Type:        "pods",
	}

	s := getKubernetesResourceStore(&query, log, nil)
	options := model.ListOptions{
		Context: ctx,
		Options: metav1.ListOptions{
			LabelSelector: labelSelector.String(),
		},
	}
	err = s.FetchResourceList(options)
	if nil != err {
		logger.WithError(err).Error("get pod list error")
		return msg
	}

	if nil != s.GetObjects() {
		for _, obj := range s.GetKubernetesObjects() {
			pod, err := obj.ToPod()
			if nil != err {
				logger.WithError(err).Error("parse v1 pod error")
				continue
			}
			objMessage := model.GetSimplePodCondition(pod)
			if 0 != len(objMessage) {
				msg = append(msg, objMessage...)
			}
		}
	}

	return msg
}

func setAppStatus(app *v1beta1.Application, subRes []*model.KubernetesObject, logger common.Log) {

	if nil == app {
		return
	}

	obj := model.AppCrdToObject(app)

	if nil == obj {
		return
	}

	appModel := model.Application{
		Crd:        *obj,
		Kubernetes: subRes,
		Logger:     logger,
	}

	// get app status, ignore pod error message
	status := appModel.ComputeApplicationStatus(false)

	app.Spec.AssemblyPhase = status.AppStatus

}
