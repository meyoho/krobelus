package store

import (
	"krobelus/common"
	"krobelus/kubernetes"
	"krobelus/model"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/rest"
)

type NamespaceStore struct {
	ResourceManagerInterface
}

// HandleRequest helps tarns from v3 to v1 common api. Many v3 resources have special needs,
// put it here. This function handle POST/PUT/DELETE request.
func (s *KubernetesResourceStore) HandleRequest(method string) (*rest.Result, error) {
	ns := NamespaceStore{s}

	if method == common.HTTPPost {
		if s.UniqueName.Type == "namespaces" {
			return ns.CreateOrGetNamespace()
		}
	}
	if method == common.HTTPDelete {
		if s.UniqueName.Type == "namespaces" {
			return ns.DeleteNamespace()
		}
	}
	return s.Request(method, nil)
}

// CreateOrGetNamespace will create a namespace or get it (builtin namespace)
// This is a special case for common resource create api, before call this function,
// s.Object/s.UniqueName should be present. Since namespace is a very import resource,
// try to make it successfully as possible
func (s *NamespaceStore) CreateOrGetNamespace() (*rest.Result, error) {
	if !model.IsBuiltInNamespace(s.GetObject().Name) {
		client, err := kubernetes.NewKubeClient(s.GetClusterID(), s.GetClusterToken())
		if err != nil {
			return nil, err
		}
		result, err := s.Request("POST", nil)

		if err == nil {
			err = client.SyncFromDefault(s.GetObject().Name)
			return result, err
		}

		if !apiErrors.IsAlreadyExists(err) {
			return result, err
		}
	}
	s.SetName(s.GetObject().Name)
	result, err := s.Request("GET", nil)
	return result, err
}

// DeleteNamespace will skip builtin namespace and only do a GET request
func (s *NamespaceStore) DeleteNamespace() (*rest.Result, error) {
	method := common.HTTPDelete
	if model.IsBuiltInNamespace(s.GetName()) {
		s.GetLogger().Infof("This is a builtin namespace %s, skip delete", s.GetName())
		method = common.HTTPGet
	}
	return s.Request(method, nil)
}
