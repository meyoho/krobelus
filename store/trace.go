package store

import (
	"context"
	"krobelus/model"
	"os"

	"github.com/juju/errors"

	"github.com/opentracing/opentracing-go"
	opentracinglog "github.com/opentracing/opentracing-go/log"
)

type ListFunc func(model.ListOptions) ([]byte, error)

// TraceWrapper is a wrapper function to trace a code block.
type TraceWrapper func(opentracing.Span) error

func traceListFunc(f ListFunc) ListFunc {
	return func(options model.ListOptions) ([]byte, error) {
		if "false" != os.Getenv("JAEGER_DISABLED") {
			return f(options)
		}
		ctx := options.Context
		if ctx == nil {
			ctx = context.Background()
		}
		span, ctx := opentracing.StartSpanFromContext(ctx, "List Resource")
		defer span.Finish()
		options.Context = ctx
		span.SetTag("kind", options.Options.TypeMeta.Kind)
		span.SetTag("nameFilters", options.NameFilters)

		raw, err := f(options)
		//result := f(options)
		if err != nil {
			span.SetTag("error", true)
			span.LogFields(
				opentracinglog.String("message", err.Error()),
			)
		} else {
			span.SetTag("items bytes", len(raw))
		}
		return raw, err
	}
}

// traceFunc traces the TraceWrapper function.
func traceFunc(ctx context.Context, name string, f TraceWrapper) error {
	if ctx == nil {
		ctx = context.TODO()
	}
	span, _ := opentracing.StartSpanFromContext(ctx, name)
	defer span.Finish()
	err := f(span)
	if err != nil {
		span.SetTag("error", true)
		span.LogFields(
			opentracinglog.String("message", errors.Details(err)),
		)
	}
	return err
}
