package store

import (
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/model"

	"github.com/golang/glog"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	ref "k8s.io/client-go/tools/reference"
)

type eventHelper struct {
	Source v1.EventSource
}

func validateEventType(eventType string) bool {
	switch eventType {
	case v1.EventTypeNormal, v1.EventTypeWarning:
		return true
	}
	return false
}

func (helper *eventHelper) GenerateEvent(object runtime.Object, eventType, reason, message string) *v1.Event {
	r, err := ref.GetReference(scheme.Scheme, object)
	if err != nil {
		glog.Errorf("Could not construct reference to: '%#v' due to: '%v'. Will not report event: '%v' '%v' '%v'", object, err, eventType, reason, message)
		return nil
	}
	if !validateEventType(eventType) {
		glog.Errorf("Unsupported event type: '%v'", eventType)
		return nil
	}
	event := helper.makeEvent(r, eventType, reason, message)
	event.Source = helper.Source
	return event
}

func (helper *eventHelper) makeEvent(ref *v1.ObjectReference, eventType, reason, message string) *v1.Event {
	t := metav1.Time{Time: time.Now().UTC()}
	namespace := ref.Namespace
	if namespace == "" {
		namespace = metav1.NamespaceDefault
	}
	return &v1.Event{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%v.%x", ref.Name, t.UnixNano()),
			Namespace: namespace,
		},
		TypeMeta: metav1.TypeMeta{
			Kind:       common.KubernetesKindEvent,
			APIVersion: common.KubernetesAPIVersionV1,
		},
		// TODO: ref.UID could be customized
		InvolvedObject: *ref,
		Reason:         reason,
		Message:        message,
		FirstTimestamp: t,
		LastTimestamp:  t,
		Count:          1,
		Type:           eventType,
	}
}

// SendKubernetesEvent send  event to k8s cluster using a kubernetes object
func SendKubernetesEvent(cluster, token, eventType, reason, message string, obj runtime.Object, logger common.Log) error {
	objectMeta, err := meta.Accessor(obj)
	if err != nil {
		return err
	}

	helper := eventHelper{
		Source: v1.EventSource{
			Component: common.Component,
		},
	}
	event := helper.GenerateEvent(obj, eventType, reason, message)
	resource, err := model.ToKubernetesObject(event)
	if err != nil {
		return err
	}
	query := model.Query{
		ClusterUUID:  cluster,
		ClusterToken: token,
		Namespace:    objectMeta.GetNamespace(),
		Type:         "events",
	}

	s, err := GetStore(&query, logger, resource)
	if err != nil {
		return err
	}
	_, err = s.Create()
	return err
}
