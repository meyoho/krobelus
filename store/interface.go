package store

import (
	"context"
	"krobelus/common"
	"krobelus/model"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/rest"
)

// ResourceStoreInterface is interface for resource
type ResourceStoreInterface interface {
	// get basic attribute
	GetNamespace() string
	SetNamespace(namespace string)
	GetName() string
	SetName(name string)
	GetSubResources() []string
	GetResourceType() string
	SetResourceType(t string)
	GetClusterID() string
	GetClusterToken() string
	SetPatchType(p string)
	GetObjects() *unstructured.UnstructuredList
	SetObjects(ol *unstructured.UnstructuredList)

	GetLogger() common.Log
	String() string

	GetKubernetesObjects() []*model.KubernetesObject

	SetRaw(raw []byte)
	GetRaw() []byte
	GetObject() *model.KubernetesObject
	// set attribute by object
	SetObject(obj *model.KubernetesObject)
	SetNamespaceAndName(obj *model.KubernetesObject)

	// Set Context for store
	SetContext(ctx context.Context)
}

// Interface is interface for store
type ResourceManagerInterface interface {
	ResourceStoreInterface

	// init
	LoadKubeClient() error

	// discovery
	GetResourceTypeByKindInsensitive(kind string) (string, error)
	GetResourceTypeByKind(kind string) (string, error)
	SyncResourceTypes()

	// origin api with cache sync
	Create() (*unstructured.Unstructured, error)
	Update() (*unstructured.Unstructured, error)
	Get() (*unstructured.Unstructured, error)
	Delete() error

	// compose from basic operations
	CascadeDelete(selector string) error
	CreateOrUpdateResource() (*unstructured.Unstructured, error)
	UpdateAndRetry() (*rest.Result, error)

	// with cache support
	FetchResource(options model.GetOptions) error

	FetchResourceList(options model.ListOptions) error

	// raw rest api
	Request(method string, params map[string]string) (*rest.Result, error)
	ParseResultData(result *rest.Result) error
	HandleRequest(method string) (*rest.Result, error)
}

type NamespaceStoreInterface interface {
	ResourceManagerInterface

	CreateOrGetNamespace() (*rest.Result, error)
	DeleteNamespace() (*rest.Result, error)
}

func GetStoreInterface(query *model.Query, logger common.Log, object *model.KubernetesObject) (ResourceManagerInterface, error) {
	s := getKubernetesResourceStore(query, logger, object)
	err := s.init()
	return &s, err
}
