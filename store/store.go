package store

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go"

	"krobelus/common"
	"krobelus/config"
	"krobelus/kubernetes"
	"krobelus/model"

	"github.com/Jeffail/gabs"
	"github.com/juju/errors"
	"github.com/thoas/go-funk"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

// KubernetesResourceStore combine the ability to retrieve kubernetes resource from cache and cluster
type KubernetesResourceStore struct {
	// request config
	ResourceStore

	// Client will do raw http api
	Client rest.Interface
	// ic do discovery and gen client
	ic kubernetes.DiscoveryInterface
	// ResourceClient is the real client
	ResourceClient dynamic.ResourceInterface
}

// getKubernetesResourceStore generate a KubernetesResourceStore from query/log/object
func getKubernetesResourceStore(query *model.Query, logger common.Log, object *model.KubernetesObject) KubernetesResourceStore {
	return KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: query,
			Logger:     logger,
			Object:     object,
		},
	}
}

func GetStore(query *model.Query, logger common.Log, object *model.KubernetesObject) (*KubernetesResourceStore, error) {
	s := getKubernetesResourceStore(query, logger, object)
	err := s.init()
	return &s, err
}

func (s *KubernetesResourceStore) Copy(resourceType string, useName bool, useObj bool) (*KubernetesResourceStore, error) {
	namespaced, err := s.ic.IsNamespaceScoped(resourceType)
	if err != nil {
		return nil, errors.Annotate(err, "check namespace scoped error")
	}

	ns := getKubernetesResourceStore(&model.Query{
		ClusterUUID:  s.GetClusterID(),
		ClusterToken: s.GetClusterToken(),
		Namespace:    s.GetNamespace(),
		Type:         resourceType,
	}, s.Logger, nil)

	if useName {
		ns.UniqueName.Name = s.GetName()
	}
	if useObj {
		ns.Object = s.Object
	}
	if !namespaced {
		ns.UniqueName.Namespace = ""
	}

	return &ns, ns.init()
}

// CopyWithNewObj generate a new store with origin cluster/logger
func (s *KubernetesResourceStore) CopyWithNewObj(resource string, obj *model.KubernetesObject) (*KubernetesResourceStore, error) {
	namespaced, err := s.ic.IsNamespaceScoped(resource)
	if err != nil {
		return nil, errors.Annotate(err, "check namespace scoped error")
	}
	ns, err := s.Copy(resource, false, false)
	if err != nil {
		return ns, errors.Annotate(err, "copy store error")
	}
	ns.Object = obj
	ns.SetNamespaceAndName(obj)
	if !namespaced {
		ns.UniqueName.Namespace = ""
	}
	return ns, ns.init()
}

func (s *KubernetesResourceStore) GetResourceTypeByKindInsensitive(kind string) (string, error) {
	return s.ic.GetResourceByKindInsensitive(kind)
}

func (s *KubernetesResourceStore) GetResourceTypeByKind(kind string) (string, error) {
	return s.ic.GetResourceByKind(kind)
}

// loadClient provide 2 kubernetes clients, including:
// A dynamic client to discover groups and versions
// Another dynamic client used to call rest api
func (s *KubernetesResourceStore) loadClient() error {
	return traceFunc(s.Context, "loadClient", func(_ opentracing.Span) error {
		err := s.LoadKubeClient()
		if err != nil {
			return err
		}

		gv, err := s.ic.GetGroupVersionByName(s.GetResourceType(), "")
		if err != nil {
			return err
		}
		groupVersion := gv.String()
		if s.Object != nil {
			groupVersion = s.Object.APIVersion
		}

		// remove repeat init
		if s.Client == nil || s.Client.APIVersion().String() != groupVersion {
			s.Client, err = s.ic.RestClientForGroupVersionResource(groupVersion, s.UniqueName.Type)
			if err != nil {
				return err
			}
			s.Logger.Debugf("Load resource store client with query: %+v", s.UniqueName)
		}

		return nil
	})
}

func (s *KubernetesResourceStore) LoadKubeClient() error {
	if s.ic == nil {
		client, err := kubernetes.NewCachedKubeClient(s.GetClusterID(), s.GetClusterToken(), s.Logger)
		if err != nil {
			return err
		}
		s.ic = client
	}
	return nil
}

// setKubernetesParams extract kubernetes params from gin params and set to request
func setKubernetesParams(request *rest.Request, params map[string]string) {
	if params != nil { //nolint:gosimple
		for key, value := range params {
			if !funk.Contains(common.AlaudaQueryStrings, key) {
				request = request.Param(key, value)
			}

		}
	}
}

func (s *KubernetesResourceStore) Request(method string, params map[string]string) (*rest.Result, error) {
	if err := s.loadClient(); err != nil {
		return nil, err
	}
	var request *rest.Request
	switch method {
	case common.HTTPPost:
		request = s.postRequest()
	case common.HTTPDelete:
		request = s.deleteRequest()
	case common.HTTPPut:
		request = s.putRequest()
	case common.HTTPGet:
		if !config.GlobalConfig.Kubernetes.DisableCache {
			if params == nil {
				params = make(map[string]string)
			}
			params["resourceVersion"] = "0"
		}
		request = s.getRequest()
	case common.HTTPPatch:
		request = s.patchRequest()
	default:
		return nil, errors.New(fmt.Sprintf("Unsupported request method: %s", method))
	}

	setKubernetesParams(request, params)

	namespaceScoped, err := s.isNamespaceScoped()
	if err != nil {
		return nil, errors.New(fmt.Sprintf("error discovery resource type: %s", s.UniqueName.Type))
	}

	// force rewrite ns if has body
	ns := s.UniqueName.Namespace
	if ns == "" && s.GetObject() != nil {
		ns = s.GetObject().GetNamespace()
	}
	request.NamespaceIfScoped(ns, namespaceScoped)

	var result rest.Result
	traceFunc(s.Context, "resource-store-request", func(_ opentracing.Span) error {
		t1 := time.Now()
		result = request.Do()

		var code int
		result.StatusCode(&code)
		s.Logger.Debugf("Request kubernetes  %s %s,code: %d cost: %+v", method, request.URL().String(), code, time.Since(t1))
		if result.Error() != nil {
			s.Logger.Infof("Request kubernetes result error: %s", result.Error().Error())
		}
		return nil
	})
	return &result, result.Error()
}

func (s *KubernetesResourceStore) getBody() []byte {
	body := s.Raw
	if s.Object != nil {
		body, _ = s.Object.ToBytes()
	}
	s.Logger.Infof("Trying to create/update resource: %s", string(body))
	return body
}

func (s *KubernetesResourceStore) isNamespaceScoped() (bool, error) {
	return s.ic.IsNamespaceScoped(s.GetResourceType())
}

func (s *KubernetesResourceStore) getRequest() *rest.Request {
	request := s.Client.Get().Resource(s.UniqueName.Type).SubResource(s.UniqueName.SubType...)
	if s.UniqueName.Name != "" {
		request.Name(s.UniqueName.Name)
	}
	return request
}

func (s *KubernetesResourceStore) deleteRequest() *rest.Request {
	request := s.Client.Delete().
		Resource(s.UniqueName.Type).
		SubResource(s.UniqueName.SubType...)
	if s.UniqueName.Name != "" {
		request.Name(s.UniqueName.Name)
	}
	if common.IsPodControllerResource(s.GetResourceType()) {
		// add delete body for Pod Controller
		jsonObj := gabs.New()
		jsonObj.Set("Background", "propagationPolicy")
		body := jsonObj.Bytes()
		s.Logger.Debugf("add delete body for workload delete: %+v", jsonObj.String())
		request.Body(body)

	}
	return request
}

func (s *KubernetesResourceStore) postRequest() *rest.Request {
	request := s.Client.Post().
		Resource(s.UniqueName.Type).
		SubResource(s.UniqueName.SubType...).
		Body(s.getBody())
	if s.UniqueName.Name != "" && s.UniqueName.GetTopSubType() != "" {
		request.Name(s.UniqueName.Name)
	}
	return request
}

func (s *KubernetesResourceStore) putRequest() *rest.Request {
	return s.Client.Put().
		Resource(s.UniqueName.Type).
		Name(s.UniqueName.Name).
		SubResource(s.UniqueName.SubType...).
		Body(s.getBody())
}

func (s *KubernetesResourceStore) patchRequest() *rest.Request {
	return s.Client.Patch(types.PatchType(s.UniqueName.PatchType)).
		Resource(s.UniqueName.Type).
		Name(s.UniqueName.Name).
		SubResource(s.UniqueName.SubType...).
		Body(s.getBody())
}

// FetchResource fetches a resource from cache or cluster.
func (s *KubernetesResourceStore) FetchResource(options model.GetOptions) error {

	// change for test for now
	if err := s.initIfNot(); err != nil {
		return err
	}

	// seems on raw rest api support params(?)
	if options.Params != nil {
		result, err := s.Request(common.HTTPGet, options.Params)
		if err != nil {
			return err
		}
		return s.ParseResultData(result)
	}

	result, err := s.Get()
	if err != nil {
		return err
	}
	s.Object = model.UnstructToObject(result)
	s.Raw, err = result.MarshalJSON()
	return err
}

// FetchResourceList fetches resources from cache or cluster.
func (s *KubernetesResourceStore) FetchResourceList(options model.ListOptions) error {

	if err := s.initIfNot(); err != nil {
		return err
	}

	options.Options.TypeMeta = *s.TypeMeta

	if err := s.loadClient(); err != nil {
		return err
	}

	namespaceScoped, err := s.isNamespaceScoped()
	if err != nil {
		return errors.New(fmt.Sprintf("error discovery resource type: %s", s.UniqueName.Type))
	}

	listFunc := func(options model.ListOptions) ([]byte, error) {
		req := s.Client.Get().
			Resource(s.UniqueName.Type)
		if options.PartialMetadata {
			req.SetHeader("Accept", "application/json;as=PartialObjectMetadataList;v=v1beta1;g=meta.k8s.io")
		}
		req.NamespaceIfScoped(s.UniqueName.Namespace, namespaceScoped)
		// Use 'Param' instead of 'VersionedParams' here for unregistered
		// resource types will convert failed with 'VersionedParams'
		params := options.GetListParams()
		for k, v := range params {
			req.Param(k, v)
		}
		return req.DoRaw()
	}

	raw, err := traceListFunc(listFunc)(options)
	if err != nil {
		s.Logger.Errorf("Get raw byte for %s in %s failed: %+v", s.UniqueName.Type, s.UniqueName.ClusterUUID, err)
		return err
	}

	s.Raw = raw

	// Filter by names filter
	if len(options.NameFilters) > 0 {
		var filters []unstructured.Unstructured
		objects := s.GetObjects()
		for _, o := range objects.Items {
			for _, f := range options.NameFilters {
				if o.GetNamespace() == f.Namespace && o.GetName() == f.Name {
					filters = append(filters, o)
					break
				}
			}
		}
		s.Objects.Items = filters
		s.Raw, err = json.Marshal(s.Objects)
		if err != nil {
			return err
		}
	}

	// Filter the result by params
	if options.NameSearch != "" {
		var filters []unstructured.Unstructured
		nameSearch := strings.ToLower(options.NameSearch)
		objects := s.GetObjects()
		for _, o := range objects.Items {
			if strings.Contains(o.GetName(), nameSearch) {
				filters = append(filters, o)
			}
		}
		s.Objects.Items = filters
		// TODO: may be error?
		s.Raw, err = json.Marshal(s.Objects.Items)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *KubernetesResourceStore) Create() (*unstructured.Unstructured, error) {
	start := time.Now()
	result, err := s.ResourceClient.Create(&s.Object.Unstructured, metaV1.CreateOptions{}, s.GetSubResources()...)
	s.Logger.Debugf("create %s with body %+v cost: %v", s.String(), s.Object, time.Since(start))
	return result, err
}

func (s *KubernetesResourceStore) Update() (*unstructured.Unstructured, error) {
	start := time.Now()
	result, err := s.ResourceClient.Update(&s.Object.Unstructured, metaV1.UpdateOptions{}, s.GetSubResources()...)
	s.Logger.Debugf("update %s with body %+v cost: %v", s.String(), s.Object, time.Since(start))
	return result, err
}

func (s *KubernetesResourceStore) CreateOrUpdateResource() (*unstructured.Unstructured, error) {
	now := s.Object

	err := s.FetchResource(model.GetOptions{SkipCache: true})
	if err == nil {
		s.Logger.Infof("resource already exist when create, update it")
		// exist, merge and update
		old := s.Object
		if err = mergeOriginData(old, now); err != nil {
			return nil, err
		}
		s.Object = now
		return s.Update()
	}

	if apiErrors.IsNotFound(errors.Cause(err)) {
		// not exist, create it
		return s.Create()
	}

	return nil, err
}

func (s *KubernetesResourceStore) UpdateAndRetry() (*rest.Result, error) {
	result, err := s.Request(common.HTTPPut, nil)
	if err != nil && apiErrors.IsConflict(err) {
		s.Logger.Infof("detect conflict when update resource, retry")
		result, err = s.Request(common.HTTPGet, nil)
		if err != nil {
			return result, errors.Annotate(err, "get resource error when retry update")
		}
		origin := s.Object
		if err := s.ParseResultData(result); err != nil {
			return result, errors.Annotate(err, "parse result data error when retry update")
		}
		origin.SetResourceVersion(s.Object.GetResourceVersion())
		result, err = s.Request(common.HTTPPut, nil)
	}

	return result, err
}

// initIfNot init a store if not
// temp solution to avoid repeat init, useful for test scenario when we need to force rest ResourceClient
func (s *KubernetesResourceStore) initIfNot() error {
	if s.ResourceClient != nil && s.TypeMeta != nil {
		return nil
	}
	return s.init()
}

// init do some init job for store
// 1. init discovery client
// 2. If type no set, return
// 3. get TypeMeta
// 4. get ResourceClient
func (s *KubernetesResourceStore) init() error {
	return traceFunc(s.Context, "init", func(_ opentracing.Span) error {
		err := s.LoadKubeClient()
		if err != nil {
			return err
		}

		if s.GetResourceType() == "" {
			return nil
		}

		typeMeta, err := s.getTypeMeta()
		if err != nil {
			return errors.Annotate(err, "get type meta error")
		}
		s.TypeMeta = typeMeta

		groupVersion := ""
		if s.Object != nil {
			groupVersion = s.Object.GetAPIVersion()
		}

		c, err := s.ic.DynamicClientForResource(s.GetResourceType(), groupVersion)
		if err != nil {
			return errors.Annotate(err, "gen resource client error")
		}

		namespaced, err := s.ic.IsNamespaceScoped(s.GetResourceType())
		if err != nil {
			return errors.Annotate(err, "check resource namespace scope error")
		}
		if namespaced {
			s.ResourceClient = c.Namespace(s.GetNamespace())
		} else {
			s.ResourceClient = c
		}
		s.Logger.Debug("init resource store:", s.String())

		return nil
	})
}

// getTypeMeta get TypeMeta from resource type
func (s *KubernetesResourceStore) getTypeMeta() (*metaV1.TypeMeta, error) {
	res, err := s.ic.GetAPIResourceByName(s.GetResourceType(), "")
	if err != nil {
		return nil, err
	}

	gv, err := s.ic.GetGroupVersionByName(s.GetResourceType(), "")
	if err != nil {
		return nil, err
	}
	return &metaV1.TypeMeta{
		Kind:       res.Kind,
		APIVersion: gv.String(),
	}, nil
}

func (s *KubernetesResourceStore) Get() (*unstructured.Unstructured, error) {
	var result *unstructured.Unstructured
	var err error
	traceFunc(s.Context, "fetch-resource", func(_ opentracing.Span) error {
		start := time.Now()
		result, err = s.ResourceClient.Get(s.GetName(), metaV1.GetOptions{}, s.GetSubResources()...)
		s.Logger.Debugf("get %s cost: %v", s.String(), time.Since(start))
		return err
	})
	return result, err
}

func (s *KubernetesResourceStore) Delete() error {
	orphan := false
	options := metaV1.DeleteOptions{
		TypeMeta:         *s.TypeMeta,
		OrphanDependents: &orphan,
	}

	start := time.Now()
	err := s.ResourceClient.Delete(s.GetName(), &options)
	s.Logger.Debugf("delete %s with options %+v, cost: %v", s.String(), options, time.Since(start))
	return err
}

func (s *KubernetesResourceStore) CascadeDelete(selector string) error {
	start := time.Now()
	err := s.Delete()
	if err != nil && !apiErrors.IsNotFound(err) {
		return err
	}

	object := s.Object
	if object != nil && common.IsPodController(object.Kind) {
		// Delete rs and pods under the controller
		if err := s.deleteChildResources("replicasets", selector); err != nil {
			return err
		}
		if err := s.deleteChildResources("pods", selector); err != nil {
			return err
		}
	}
	s.Logger.Debugf("cascade delete %s with selector %s cost: %v", s.String(), selector, time.Since(start))
	return nil
}

func (s *KubernetesResourceStore) deleteCollectionWithLabelSelector(labelSelector string) error {
	s.Logger.Infof("Trying to delete %s with selector: %s", s.GetResourceType(), labelSelector)
	deleteOptions := metaV1.DeleteOptions{TypeMeta: *s.TypeMeta}
	deleteOptions.Kind = "DeleteOptions"
	listOptions := metaV1.ListOptions{
		TypeMeta:      *s.TypeMeta,
		LabelSelector: labelSelector,
	}

	err := s.ResourceClient.DeleteCollection(&deleteOptions, listOptions)
	if err != nil {
		return err
	}

	return nil
}

func (s *KubernetesResourceStore) deleteChildResources(resourceType string, labelSelector string) error {
	ns, err := s.Copy(resourceType, false, false)
	if err != nil {
		return err
	}
	return ns.deleteCollectionWithLabelSelector(labelSelector)
}

func (s *KubernetesResourceStore) ParseResultData(result *rest.Result) error {
	raw, err := result.Raw()
	if err != nil {
		return err
	}
	s.Raw = raw
	s.Object, err = model.BytesToKubernetesObject(raw)
	return err
}

// mergeOriginData will merge exist data with the new one. for some resources, this is needed.
// such as PVC, when bounded, it's spec will changed(bad design).
func mergeOriginData(old *model.KubernetesObject, now *model.KubernetesObject) error {
	now.SetUID(old.GetUID())
	now.SetResourceVersion(old.GetResourceVersion())
	switch old.GetKind() {
	case common.KubernetesKindPVC:
		now.SetField(old.GetField("spec"), "spec")
		//now.Spec = old.Spec
	case common.KubernetesKindService:
		oldService, err := old.ToV1Service()
		if err != nil {
			return err
		}
		nowService, err := now.ToV1Service()
		if err != nil {
			return err
		}
		nowService.Spec.ClusterIP = oldService.Spec.ClusterIP
		return now.ParseV1Service(nowService)
	}
	return nil
}
