package store

import (
	"context"
	"encoding/json"
	"fmt"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type ResourceStore struct {
	// UniqueName is a unique id for a resource
	UniqueName *model.Query

	TypeMeta *metaV1.TypeMeta

	Object *model.KubernetesObject
	// Raw is the raw data of the kubernetes object(s)
	Raw     []byte
	Objects *unstructured.UnstructuredList

	Logger common.Log

	// alauda error
	Error error

	// Context stores context data, e.g. the trace context.
	Context context.Context
}

// SetContext set context for store
func (s *ResourceStore) SetContext(ctx context.Context) {
	if nil == s {
		return
	}
	if nil == ctx {
		s.Context = context.Background()
	} else {
		s.Context = ctx
	}
}

func (s *ResourceStore) GetKubernetesObjects() []*model.KubernetesObject {
	return model.ListToObjects(s.GetObjects())
}

func (s *ResourceStore) GetObjects() *unstructured.UnstructuredList {
	// Lazy decode raw bytes into Objects
	if s.Raw != nil && (s.Objects == nil || len(s.Objects.Items) == 0) {
		result := &unstructured.UnstructuredList{}
		if err := model.DecodeToUnstructList(s.Raw, result); err != nil {
			s.Logger.Errorf("unmarshal error %v", err)
			return nil
		}
		s.Objects = result
	}
	return s.Objects
}

func (s *ResourceStore) SetObjects(ol *unstructured.UnstructuredList) {
	s.Objects = ol
}

func (s *ResourceStore) GetNamespace() string {
	return s.UniqueName.Namespace
}

func (s *ResourceStore) SetNamespace(namespace string) {
	s.UniqueName.Namespace = namespace
}

func (s *ResourceStore) GetName() string {
	return s.UniqueName.Name
}

func (s *ResourceStore) GetLogger() common.Log {
	return s.Logger
}

func (s *ResourceStore) GetSubResources() []string {
	return s.UniqueName.SubType
}

func (s *ResourceStore) SetObject(obj *model.KubernetesObject) {
	s.Object = obj
}

func (s *ResourceStore) GetObject() *model.KubernetesObject {
	return s.Object
}

func (s *ResourceStore) SetNamespaceAndName(obj *model.KubernetesObject) {
	s.UniqueName.Name = obj.GetName()
	s.UniqueName.Namespace = obj.GetNamespace()
}

func (s *ResourceStore) SetPatchType(p string) {
	s.UniqueName.PatchType = p
}

func (s *ResourceStore) SetName(name string) {
	s.UniqueName.Name = name
}

func (s *ResourceStore) GetResourceType() string {
	return s.UniqueName.Type
}

func (s *ResourceStore) SetResourceType(t string) {
	s.UniqueName.Type = t
}

func (s *ResourceStore) GetClusterID() string {
	return s.UniqueName.ClusterUUID
}

func (s *ResourceStore) GetClusterToken() string {
	return s.UniqueName.ClusterToken
}

func (s *ResourceStore) String() string {
	str := fmt.Sprintf("%s:%s/%s", s.GetResourceType(), s.GetNamespace(), s.GetName())
	if s.UniqueName.IsSubType() {
		str += fmt.Sprintf(" /%s", common.CombineString(s.UniqueName.SubType...))
	}
	return str
}

// SetRaw sets data bytes to 'Raw', and clear the 'Object' value.
func (s *ResourceStore) SetRaw(raw []byte) {
	s.Raw = raw
	// Set Object to nil to avoid use Object as request body.
	s.Object = nil
}

func (s *ResourceStore) GetRaw() []byte {
	return s.Raw
}

// InstallCRD installs the CRD to the cluster.
func (s *ResourceStore) InstallCRD(name string, crd *v1beta1.CustomResourceDefinition) error {
	n := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &model.Query{
				ClusterUUID: s.UniqueName.ClusterUUID,
				Name:        name,
				Namespace:   "",
				Type:        "customresourcedefinitions",
			},
			Logger: s.Logger,
		},
	}
	err := n.FetchResource(model.GetOptions{})
	if err == nil {
		return nil
	}
	n.UniqueName.Name = ""

	bt, err := json.Marshal(crd)
	if err != nil {
		return errors.Annotatef(err, "marshal %s crd error", crd.Name)
	}
	obj, err := model.BytesToKubernetesObject(bt)
	if err != nil {
		return errors.Annotate(err, "load bytes to object error")
	}
	n.Object = obj
	_, err = n.Request("POST", nil)
	if err != nil {
		return err
	}
	n.Logger.Infof("Install %s crd done", crd.Name)

	return nil
}
