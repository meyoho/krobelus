package store

import (
	"encoding/json"
	"fmt"
	"sort"
	"sync"

	"krobelus/common"
	"krobelus/model"
	"krobelus/pkg/applicationhistory/v1beta1"

	ac "github.com/alauda/cyborg/pkg/client"
	"github.com/ghodss/yaml"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
)

// AppHistoryStoreInterface describes an interface for Application History store.
type AppHistoryStoreInterface interface {
	ResourceManagerInterface
	// InstallAppHistoryCRD installs the application history CRD to the cluster.
	InstallAppHistoryCRD() error

	// CreateAppHistory creates a new application history object from the 'ApplicationHistoryResource'.
	CreateAppHistory(appHistoryResource *model.ApplicationHistoryResource) (*v1beta1.ApplicationHistory, error)

	// GetAppHistory returns an application history by the selector and revision.
	GetAppHistory(selector string, revision int) (*v1beta1.ApplicationHistory, error)

	// GetAppHistoryDetail returns a application history in detail with extra fields
	// 'resourceDiffsWithLatest' and 'latestRevision'.
	GetAppHistoryDetail() (*unstructured.Unstructured, error)

	// ListAppHistory returns a list of application histories by application selector string.
	ListAppHistory(appSelector string) (*unstructured.UnstructuredList, error)
}

// AppHistoryStore implements the AppHistoryStoreInterface.
type AppHistoryStore struct {
	KubernetesResourceStore
}

// GetAppHistoryStore returns a new AppHistoryStoreInterface.
func GetAppHistoryStore(query *model.Query, logger common.Log, object *model.KubernetesObject) (AppHistoryStoreInterface, error) {
	s, err := GetStore(query, logger, object)
	if err != nil && ac.IsResourceTypeNotFound(err) {
		s.Logger.WithError(err).Warn("use static config for applications types")
		s.TypeMeta = &metav1.TypeMeta{
			Kind:       "ApplicationHistory",
			APIVersion: "app.k8s.io/v1beta1",
		}

		ic, err := dynamic.NewForConfig(s.ic.GetRestConfig())
		if err != nil {
			return nil, err
		}

		s.ResourceClient = ic.Resource(schema.GroupVersionResource{
			Group:    "app.k8s.io",
			Version:  "v1beta1",
			Resource: "applicationhistories",
		}).Namespace(s.GetNamespace())
		return &AppHistoryStore{*s}, nil
	}
	return &AppHistoryStore{*s}, err
}

// InstallAppHistoryCRD installs application history CRD to the cluster.
func (s *AppHistoryStore) InstallAppHistoryCRD() error {
	return s.InstallCRD("applicationhistories.app.k8s.io", &v1beta1.ApplicationHistoryCRD)
}

// ComputeResourceDiffs computes the resource diffs between two KubernetesObject arrays.
// The returned result is a map which has 3 kinds of keys: 'create', 'update' and 'delete'.
func (s *AppHistoryStore) ComputeResourceDiffs(oldResources []*model.KubernetesObject,
	newResources []*model.KubernetesObject) (map[v1beta1.ResourceChangeAction][]v1beta1.ResourceDiffItem, error) {
	diffResources, err := model.ComputeResourceDiffs(oldResources, newResources, model.ObjectKeyWithoutNamespace)
	if err != nil {
		return nil, err
	}
	if len(diffResources) == 0 {
		return nil, nil
	}
	resourcediffs := make(map[v1beta1.ResourceChangeAction][]v1beta1.ResourceDiffItem)
	for k, v := range diffResources {
		for _, o := range v {
			resourcediffs[v1beta1.ResourceChangeAction(k)] = append(
				resourcediffs[v1beta1.ResourceChangeAction(k)],
				v1beta1.ResourceDiffItem{
					Kind: o.GetKind(),
					Name: o.GetName(),
				})
		}
	}
	return resourcediffs, nil
}

// NextRevision returns the next revision by the given old application.
func (s *AppHistoryStore) NextRevision(oldApp *model.Application) int {
	var revision int
	if oldApp == nil {
		revision = 1
	} else {
		lastRevision := model.GetAppRevision(&oldApp.Crd.Unstructured)
		revision = cast.ToInt(lastRevision)
		if revision == 0 {
			// Invalid revision, start from 1
			s.Logger.Warnf("Invalid revision %q, will start from 1", lastRevision)
		}
		revision++
	}
	return revision
}

// GenerateRevisionYAML generates YAML string of the application history from the application request.
func (s *AppHistoryStore) GenerateRevisionYAML(app *model.ApplicationRequest) (string, error) {
	objects := []map[string]interface{}{}
	for _, o := range app.Kubernetes {
		objects = append(objects, o.Object)
	}
	yamlStr, err := yaml.Marshal(objects)
	if err != nil {
		return "", err
	}
	return string(yamlStr), nil
}

// GenerateResourceDiffsWithLastRevision generates resource diffs of the application history
// from the old application and the new application request.
func (s *AppHistoryStore) GenerateResourceDiffsWithLastRevision(oldApp *model.Application,
	newApp *model.ApplicationRequest) (map[v1beta1.ResourceChangeAction][]v1beta1.ResourceDiffItem, error) {
	var oldKubernetes []*model.KubernetesObject
	if oldApp != nil {
		lastRevision := cast.ToInt(model.GetAppRevision(&oldApp.Crd))
		// Compatible with the applications without history revision
		if lastRevision == 0 {
			return nil, nil
		}

		lastAppRev, err := s.GetAppHistory(oldApp.GetAppNameSelectorString(), cast.ToInt(model.GetAppRevision(&oldApp.Crd)))
		if err != nil {
			return nil, err
		}
		oldKubernetes, err = model.GetAppHistoryKubernetesObjects(lastAppRev)
		if err != nil {
			return nil, err
		}
	}
	return s.ComputeResourceDiffs(oldKubernetes, newApp.Kubernetes)
}

// PreprocessHistoryResource cleans the new kubernetes objects by remove the fields
// that are auto generated by kubernetes and should not be stored in the revision.
// And it adds application object to the resource list of new app request if necessary.
func (s *AppHistoryStore) PreprocessHistoryResource(appHistoryResource *model.ApplicationHistoryResource) *model.ApplicationHistoryResource {
	// Add new application object to the resource list if it does not exist.
	newApp := appHistoryResource.Request
	foundApp := false
	for _, o := range newApp.Kubernetes {
		if o.GetKind() == "Application" {
			foundApp = true
		}
		o.CleanAutogenerated()
	}
	if !foundApp && newApp.Crd != nil {
		newApp.Crd.CleanAutogenerated()
		newApp.Kubernetes = append(newApp.Kubernetes, newApp.Crd)
	}

	return appHistoryResource
}

// CreateAppHistory creates a new application history object from the ApplicationHistoryResource.
func (s *AppHistoryStore) CreateAppHistory(appHistoryResource *model.ApplicationHistoryResource) (*v1beta1.ApplicationHistory, error) {
	var err error

	s.PreprocessHistoryResource(appHistoryResource)

	oldApp := appHistoryResource.OldApp
	newApp := appHistoryResource.Request
	appCrd := newApp.Crd
	if appCrd == nil && oldApp != nil {
		appCrd = &oldApp.Crd
	}

	if appCrd == nil {
		return nil, errors.Errorf("application object is null")
	}

	appRevObj := &v1beta1.ApplicationHistory{}
	appRevObj.Kind = "ApplicationHistory"
	appRevObj.APIVersion = "app.k8s.io/v1beta1"
	appRevObj.Spec.Revision = s.NextRevision(oldApp)
	appRevObj.Spec.CreationTimestamp = metav1.Time{Time: appHistoryResource.CreationTimestamp}
	appRevObj.Spec.User = appHistoryResource.User
	appRevObj.Spec.ChangeCause = appHistoryResource.ChangeCause
	appRevObj.Name = fmt.Sprintf("%s-%d", appCrd.GetName(), appRevObj.Spec.Revision)
	appRevObj.Namespace = appCrd.GetNamespace()
	if appHistoryResource.RollbackFrom != nil {
		model.UpdateAnnotations(appRevObj, common.AppRollbackFromKey(), fmt.Sprintf("%d", *appHistoryResource.RollbackFrom))
	}

	// Generate YAML
	yamlStr, err := s.GenerateRevisionYAML(newApp)
	if err != nil {
		return nil, err
	}
	appRevObj.Spec.YAML = yamlStr

	// Compute the resource diffs
	resourceDiffs, err := s.GenerateResourceDiffsWithLastRevision(oldApp, newApp)
	if err != nil {
		return nil, err
	}
	appRevObj.Spec.ResourceDiffs = resourceDiffs

	// Set labels
	appRevObj.SetLabels(appCrd.GetAppNameSelector())

	// Set ownerReference
	controller := false
	blockOwnerDeletion := true
	appRevObj.OwnerReferences = []metav1.OwnerReference{
		{
			APIVersion:         appCrd.GetAPIVersion(),
			Kind:               appCrd.GetKind(),
			UID:                appHistoryResource.AppUID,
			Name:               appCrd.GetName(),
			Controller:         &controller,
			BlockOwnerDeletion: &blockOwnerDeletion,
		},
	}

	s.Object, err = model.ToKubernetesObject(appRevObj)
	if err != nil {
		return nil, err
	}

	s.UniqueName.Namespace = appRevObj.Namespace
	s.UniqueName.Name = appRevObj.Name
	res, err := s.Create()
	if err != nil {
		return nil, err
	}

	// GC the histories after create done, and GC error won't
	// cause create error. It will be executed again at next create.
	err = s.TryGC(appCrd)
	if err != nil {
		s.Logger.Errorf("GC application history error: %v", err)
	}

	appHistory, err := model.UnstructuredToApplicationHistory(res)
	if err != nil {
		return nil, err
	}
	return appHistory, nil
}

// GetAppHistory returns an application history by the selector and revision.
func (s *AppHistoryStore) GetAppHistory(selector string, revision int) (*v1beta1.ApplicationHistory, error) {
	options := model.ListOptions{
		Options: metav1.ListOptions{
			LabelSelector: selector,
		},
	}
	err := s.FetchResourceList(options)
	if err != nil {
		return nil, err
	}

	objects := s.GetObjects()
	var revObj *v1beta1.ApplicationHistory
	for _, obj := range objects.Items {
		raw, err := obj.MarshalJSON()
		if err != nil {
			return nil, err
		}
		appHistory := &v1beta1.ApplicationHistory{}
		err = json.Unmarshal(raw, appHistory)
		if err != nil {
			return nil, err
		}
		if appHistory.Spec.Revision == revision {
			revObj = appHistory
		}
	}

	if revObj == nil {
		return nil, errors.Errorf("application history revision %d not found", revision)
	}

	return revObj, nil
}

// GetAppHistoryDetail returns a application history in detail with extra fields
// 'resourceDiffsWithLatest' and 'latestRevision'.
func (s *AppHistoryStore) GetAppHistoryDetail() (*unstructured.Unstructured, error) {
	obj, err := s.Get()
	if err != nil {
		return nil, err
	}
	appHistory, err := model.UnstructuredToApplicationHistory(obj)
	if err != nil {
		return nil, err
	}
	owner := appHistory.OwnerReferences[0]
	apps, err := s.Copy("applications", false, false)
	if err != nil {
		return nil, err
	}
	apps.UniqueName.Name = owner.Name
	app, err := apps.Get()
	if err != nil {
		return nil, err
	}
	revision := cast.ToInt(model.GetAppRevision(app))
	if revision > 0 {
		ks := &model.KubernetesObject{Unstructured: *app}
		latestRev, err := s.GetAppHistory(ks.GetAppNameSelectorString(), revision)
		if err != nil {
			return nil, err
		}
		appResources, err := model.GetAppHistoryKubernetesObjects(appHistory)
		if err != nil {
			return nil, err
		}
		latestResources, err := model.GetAppHistoryKubernetesObjects(latestRev)
		if err != nil {
			return nil, err
		}

		spec := obj.Object["spec"].(map[string]interface{})
		spec["resourceDiffsWithLatest"], err = s.ComputeResourceDiffs(latestResources, appResources)
		if err != nil {
			return nil, err
		}
		spec["latestRevision"] = latestRev.Spec.Revision
	}
	return obj, nil
}

// TryGC garbege collects the old application histories that exceed the history limit.
func (s *AppHistoryStore) TryGC(app *model.KubernetesObject) error {
	ss, err := s.Copy(s.UniqueName.Type, false, false)
	if err != nil {
		return err
	}
	err = ss.FetchResourceList(model.ListOptions{
		Options: metav1.ListOptions{
			LabelSelector: model.MatchLabelsToParamString(app.GetAppNameSelector()),
		},
	})
	if err != nil {
		return err
	}
	objects := ss.GetObjects()
	historyLimit := app.GetAppHistoryLimit()
	historyCount := len(objects.Items)
	deleteCount := historyCount - historyLimit
	if deleteCount > 0 {
		sort.Slice(objects.Items, func(i, j int) bool {
			hi, _ := model.UnstructuredToApplicationHistory(&objects.Items[i])
			hj, _ := model.UnstructuredToApplicationHistory(&objects.Items[j])
			return hi.Spec.Revision < hj.Spec.Revision
		})
		var wg sync.WaitGroup
		for i := 0; i < deleteCount; i++ {
			i := i
			wg.Add(1)
			go func() {
				defer wg.Done()
				delS, err := s.Copy(s.UniqueName.Type, false, false)
				if err != nil {
					s.Logger.Errorf("Copy store error: %v", err)
					return
				}
				delS.UniqueName.Name = objects.Items[i].GetName()
				err = delS.Delete()
				if err != nil {
					s.Logger.Errorf("Delete application history error: %v", err)
					return
				}
			}()
		}
		wg.Wait()
	}

	return nil
}

// ListAppHistory returns a list of application histories by application selector string.
func (s *AppHistoryStore) ListAppHistory(appSelector string) (*unstructured.UnstructuredList, error) {
	options := model.ListOptions{
		Options: metav1.ListOptions{
			LabelSelector: appSelector,
		},
	}
	err := s.FetchResourceList(options)
	if err != nil {
		return nil, err
	}

	return s.GetObjects(), nil
}
