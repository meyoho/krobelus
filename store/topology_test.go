package store

import (
	"encoding/json"
	"io/ioutil"
	"krobelus/common"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTopology(t *testing.T) {

	for k, directRef := range Topology {
		if common.StringInSlice(k, IgnoreKinds) {
			t.Errorf("Reference in ignore list: key=%s", k)
		}
		for _, ref := range directRef {
			if common.StringInSlice(ref, IgnoreKinds) {
				t.Errorf("Reference in ignore list: key=%s, ref=%s", k, ref)
			}
			if dRef, ok := Topology[ref]; ok {
				if !common.StringInSlice(k, dRef) {
					t.Errorf("Direct reference must be in pair: key=%s, directRef=%s", k, dRef)
				}
			}
		}
	}
}

func TestGetRef(t *testing.T) {

	refs := map[string]map[string][]string{
		"normal": {
			/*
						 a
						/ \
					   b - c
					  / \
					 d - e
					/    \
				   f      g
			*/
			"a": {"b", "c"},
			"b": {"a", "c", "d", "e"},
			"d": {"b", "e", "f"},
			"e": {"b", "d", "g"},
		},
		"dupSubset": {
			"a": {"b", "c", "d"},
			"b": {"a", "c", "d", "e"},
			"c": {"a", "c", "d", "e"},
		},
	}

	// CASE: depth less & qual & larger than topo depth
	Topology = refs["normal"]

	// depth: excepted
	cases := map[int16][]string{
		0:     nil,
		1:     {"b", "c"},
		2:     {"b", "c", "d", "e"},
		32767: {"b", "c", "d", "e", "f", "g"},
		3:     {"b", "c", "d", "e", "f", "g"},
	}

	for depth, excepted := range cases {
		start := time.Now()
		results := GetRef("a", depth)
		t.Logf("GetRef: depth: %v, cost: %v", depth, time.Since(start))
		assert.Equal(t, excepted, results)
	}

	// CASE: duplicate subset

	Topology = refs["dupSubset"]
	results := GetRef("a", int16(2))
	assert.Equal(t, []string{"b", "c", "d", "e"}, results)

}

func TestParseTopology(t *testing.T) {

	s := &TopologyStore{
		Logger: common.GetLoggerByID(""),
	}

	fileName := "../tests/fixtures/topology/objects.json"

	bt, err := ioutil.ReadFile(fileName)

	if nil != err {
		t.Fatal(err)
	}
	err = json.Unmarshal(bt, &s.Objects)
	if nil != err {
		t.Fatal(err)
	}

	s.ParseTopology()

	assert.Equal(t, 2, len(s.Response.Topo["application/nginx"]))

}
