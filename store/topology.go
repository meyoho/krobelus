package store

import (
	"encoding/json"
	"strings"
	"sync"

	"krobelus/common"
	"krobelus/model"

	"k8s.io/apimachinery/pkg/labels"
)

var IgnoreKinds = []string{
	common.KubernetesKindPod,
	common.KubernetesKindEndpoints,
	common.KubernetesKindReplicaSet,
}

var Topology = map[string][]string{
	strings.ToLower(common.KubernetesKindApplication): {
		// workload
		strings.ToLower(common.KubernetesKindDeployment),
		strings.ToLower(common.KubernetesKindDaemonSet),
		strings.ToLower(common.KubernetesKindStatefulSet),
		// volume
		strings.ToLower(common.KubernetesKindSecret),
		strings.ToLower(common.KubernetesKindConfigmap),
		strings.ToLower(common.KubernetesKindPVC),
		// network
		strings.ToLower(common.KubernetesKindService),
		strings.ToLower(common.KubernetesKindRoute),
		strings.ToLower(common.KubernetesKindIngress),
	},
	strings.ToLower(common.KubernetesKindDeployment): {
		strings.ToLower(common.KubernetesKindApplication),
		strings.ToLower(common.KubernetesKindSecret),
		strings.ToLower(common.KubernetesKindConfigmap),
		strings.ToLower(common.KubernetesKindPVC),
		strings.ToLower(common.KubernetesKindService),
	},
	strings.ToLower(common.KubernetesKindDaemonSet): {
		strings.ToLower(common.KubernetesKindApplication),
		strings.ToLower(common.KubernetesKindSecret),
		strings.ToLower(common.KubernetesKindConfigmap),
		strings.ToLower(common.KubernetesKindPVC),
		strings.ToLower(common.KubernetesKindService),
	},
	strings.ToLower(common.KubernetesKindStatefulSet): {
		strings.ToLower(common.KubernetesKindApplication),
		strings.ToLower(common.KubernetesKindSecret),
		strings.ToLower(common.KubernetesKindConfigmap),
		strings.ToLower(common.KubernetesKindPVC),
		strings.ToLower(common.KubernetesKindService),
	},
	strings.ToLower(common.KubernetesKindSecret): {
		strings.ToLower(common.KubernetesKindDeployment),
		strings.ToLower(common.KubernetesKindDaemonSet),
		strings.ToLower(common.KubernetesKindStatefulSet),
		strings.ToLower(common.KubernetesKindApplication),
	},
	strings.ToLower(common.KubernetesKindConfigmap): {
		strings.ToLower(common.KubernetesKindDeployment),
		strings.ToLower(common.KubernetesKindDaemonSet),
		strings.ToLower(common.KubernetesKindStatefulSet),
		strings.ToLower(common.KubernetesKindApplication),
	},
	strings.ToLower(common.KubernetesKindPVC): {
		strings.ToLower(common.KubernetesKindDeployment),
		strings.ToLower(common.KubernetesKindDaemonSet),
		strings.ToLower(common.KubernetesKindStatefulSet),
		strings.ToLower(common.KubernetesKindApplication),
	},
	strings.ToLower(common.KubernetesKindRoute): {
		strings.ToLower(common.KubernetesKindService),
		strings.ToLower(common.KubernetesKindApplication),
	},
	strings.ToLower(common.KubernetesKindService): {
		strings.ToLower(common.KubernetesKindIngress),
		strings.ToLower(common.KubernetesKindApplication),
		strings.ToLower(common.KubernetesKindRoute),
		strings.ToLower(common.KubernetesKindDeployment),
		strings.ToLower(common.KubernetesKindDaemonSet),
		strings.ToLower(common.KubernetesKindStatefulSet),
	},
	strings.ToLower(common.KubernetesKindIngress): {
		strings.ToLower(common.KubernetesKindService),
		strings.ToLower(common.KubernetesKindApplication),
	},
}

type TopologyStore struct {
	Request  *model.TopologyRequest
	Objects  []*model.KubernetesObject
	Response *model.TopologyResponse
	Logger   common.Log
}

// GetRef get ref from the sub ref
func getRef(sources []string, depth int16, visited map[string]bool) []string {

	result := []string{}
	if 0 >= depth {
		return result
	}
	var sub []string

	for _, source := range sources {
		source = strings.ToLower(source)

		refs, ok := Topology[source]

		if !ok {
			continue
		}

		visited[source] = true

		for _, item := range refs {
			if !visited[item] {
				sub = append(sub, item)
				visited[item] = true
			}
		}
	}

	if 0 == len(sub) {
		return result
	}

	result = append(result, sub...)

	depth--
	result = append(result, getRef(sub, depth, visited)...)

	return result
}

func GetRef(source string, depth int16) (result []string) {
	if 0 >= depth {
		return
	}

	source = strings.ToLower(source)

	visited := make(map[string]bool)

	refs, ok := Topology[source]

	if !ok {
		return
	}

	visited[source] = true

	for _, item := range refs {
		visited[item] = true
	}

	result = append(result, refs...)

	depth--

	result = append(result, getRef(refs, depth, visited)...)

	return
}

func getIndex(kind, name string) string {
	index := strings.Join([]string{kind, name}, "/")
	return strings.ToLower(index)
}

func (ts *TopologyStore) ListObjByKind(kind string) ([]*model.KubernetesObject, error) {

	q := &model.Query{
		ClusterUUID:  ts.Request.Cluster.UUID,
		ClusterToken: ts.Request.ClusterToken,
		Namespace:    ts.Request.Namespace.Name,
	}

	st, err := GetStoreInterface(q, ts.Logger, nil)
	if nil != err {
		return nil, err
	}

	q.Type, err = st.GetResourceTypeByKindInsensitive(kind)
	if nil != err {
		return nil, err
	}

	opts := model.ListOptions{}
	err = st.FetchResourceList(opts)
	if nil != err {
		return nil, err
	}
	unstructedList := st.GetObjects()

	items := unstructedList.Items

	var tsObjects []*model.KubernetesObject
	var bt []byte

	if bt, err = json.Marshal(items); nil != err {
		return nil, err
	}

	err = json.Unmarshal(bt, &tsObjects)
	return tsObjects, err
}

func (ts *TopologyStore) LoadObjects() {

	ts.Objects = make([]*model.KubernetesObject, 0)

	kind := strings.ToLower(ts.Request.Kind)

	refs := GetRef(kind, ts.Request.Depth)

	ts.Logger.Debugf("LoadObjects: refs=%+v", refs)

	if 0 == len(refs) {
		return
	}

	query := &model.Query{
		ClusterUUID:  ts.Request.Cluster.UUID,
		ClusterToken: ts.Request.ClusterToken,
		Namespace:    ts.Request.Namespace.Name,
		Name:         ts.Request.Name,
	}

	s, err := GetStoreInterface(query, ts.Logger, nil)
	if nil != err {
		ts.Logger.Errorf("GetStoreInterface error, err=%s", err.Error())
		return
	}
	query.Type, err = s.GetResourceTypeByKindInsensitive(kind)
	if nil != err {
		ts.Logger.Errorf("GetResourceTypeByKindInsensitive error, err=%s", err.Error())
		return
	}
	err = s.FetchResource(model.GetOptions{SkipCache: true})
	if nil != err {
		ts.Logger.Errorf("Get kubernetes data error, err=%s", err.Error())
		return
	}

	ts.Objects = append(ts.Objects, s.GetObject())

	var objects []*model.KubernetesObject

	errChan := make(chan error, len(refs))

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}
	for _, ref := range refs {
		wg.Add(1)
		go func(kind string) {
			defer wg.Done()
			objs, err := ts.ListObjByKind(kind)
			if nil != err {
				errChan <- err
				return
			}
			mutex.Lock()
			objects = append(objects, objs...)
			mutex.Unlock()
		}(ref)
	}

	go func() {
		wg.Wait()
		close(errChan)
	}()

	for err := range errChan {
		ts.Logger.Warningf("Get kubernetes data error, err=%s, skip", err.Error())
	}

	for _, obj := range objects {
		if common.StringInSlice(obj.Kind, IgnoreKinds) {
			continue
		}
		ts.Objects = append(ts.Objects, obj)
	}
	ts.Logger.Debugf("Get topology objects: len=%v", len(ts.Objects))
}

func (ts *TopologyStore) ParseNSTopology() {
	nodes := make(map[string]*model.Node, len(ts.Objects))
	for _, object := range ts.Objects {
		nodes[string(object.UID)] = &model.Node{KubernetesObject: *object}
	}
	ts.Response = &model.TopologyResponse{
		Graph: &model.Graph{Nodes: nodes, Edges: []*model.Edge{}},
		Refer: &model.Refer{Reference: []*model.ReferNode{}, ReferencedBy: []*model.ReferNode{}},
	}

	for _, object := range ts.Objects {
		switch object.Kind {
		case common.KubernetesKindApplication:
			ts.parseReferenceForApplication(object)
		case common.KubernetesKindDeployment:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindStatefulSet:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindDaemonSet:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindService:
			ts.parseReferenceForService(object)
		case common.KubernetesKindHPA:
			ts.parseReferenceForHPA(object)
		case common.KubernetesKindRoute:
			ts.parseReferenceForRoute(object)
		}
	}
}

func (ts *TopologyStore) ParseTopology() {
	indexes := make(map[string]*model.KubernetesObject, len(ts.Objects))
	for _, object := range ts.Objects {
		indexes[getIndex(object.GetKind(), object.GetName())] = object
	}

	ts.Response = &model.TopologyResponse{
		TopologyHelper: model.TopologyHelper{Indexes: indexes},
		Graph:          &model.Graph{Nodes: make(map[string]*model.Node), Edges: []*model.Edge{}},
	}

	for _, object := range ts.Objects {
		switch object.Kind {
		case common.KubernetesKindApplication:
			ts.parseReferenceForApplication(object)
		case common.KubernetesKindDeployment:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindStatefulSet:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindDaemonSet:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindService:
			ts.parseReferenceForService(object)
		case common.KubernetesKindHPA:
			ts.parseReferenceForHPA(object)
		case common.KubernetesKindRoute:
			ts.parseReferenceForRoute(object)
		}
	}
}

func (ts *TopologyStore) ParseReference() {
	name, kind := ts.Request.Name, ts.Request.Kind
	if name == "" && kind == "" {
		return
	}

	target, ok := ts.Response.Indexes[getIndex(kind, name)]
	if !ok || target == nil {
		ts.Logger.Infof("Can not find kubernetes object %s/%s", kind, name)
		return
	}

	graph, refer := ts.Response.Graph, ts.Response.Refer
	for _, edge := range ts.Response.Graph.Edges {
		if edge.From == string(target.UID) {
			refer.Reference = append(refer.Reference, &model.ReferNode{Type: edge.Type, Node: graph.Nodes[edge.To]})
		}
		if edge.To == string(target.UID) {
			refer.ReferencedBy = append(refer.ReferencedBy, &model.ReferNode{Type: edge.Type, Node: graph.Nodes[edge.From]})
		}
	}
}

// ParseResourceTopology only return the topology of a resource.
// ParseNSTopology() Must be called BEFORE it.
func (ts *TopologyStore) ParseResourceTopology(kind, name string, depth int16) {
	target, ok := ts.Response.Indexes[getIndex(kind, name)]
	if !ok || target == nil {
		ts.Logger.Infof("Can not find kubernetes object %s/%s", kind, name)
		return
	}

	uid := string(target.GetUID())

	ts.Response.Graph.Nodes[uid] = &model.Node{KubernetesObject: *target}

	ts.Response.Topo = ts.Response.Topo.Walk([]*model.KubernetesObject{target}, nil, depth)

	ts.Response.Graph = ts.Response.TopologyHelper.ParseGraph()
}

func (ts *TopologyStore) parseReferenceForPodController(from *model.KubernetesObject) {
	template, err := from.GetPodTemplateSpec()
	if err != nil {
		ts.Logger.Errorf("Get pod template error, %s", err.Error())
		return
	}
	containers := template.Spec.Containers
	for _, container := range containers {
		var kind, name string
		for _, env := range container.EnvFrom {
			if env.ConfigMapRef != nil {
				kind = common.KubernetesKindConfigmap
				name = env.ConfigMapRef.Name
			}
			if env.SecretRef != nil {
				kind = common.KubernetesKindSecret
				name = env.SecretRef.Name
			}
			ts.addEdge(from, kind, name, common.Reference)
		}
		for _, env := range container.Env {
			valueFrom := env.ValueFrom
			if valueFrom == nil {
				continue
			}
			if valueFrom.ConfigMapKeyRef != nil {
				kind = common.KubernetesKindConfigmap
				name = valueFrom.ConfigMapKeyRef.Name
			}
			if valueFrom.SecretKeyRef != nil {
				kind = common.KubernetesKindSecret
				name = valueFrom.SecretKeyRef.Name
			}
			ts.addEdge(from, kind, name, common.Reference)
		}
	}

	for _, volume := range template.Spec.Volumes {
		var kind, name string
		if volume.PersistentVolumeClaim != nil {
			kind = common.KubernetesKindPVC
			name = volume.PersistentVolumeClaim.ClaimName
		}
		if volume.ConfigMap != nil {
			kind = common.KubernetesKindConfigmap
			name = volume.ConfigMap.Name
		}
		if volume.Secret != nil {
			kind = common.KubernetesKindSecret
			name = volume.Secret.SecretName
		}
		ts.addEdge(from, kind, name, common.Reference)
	}

	accountName := template.Spec.ServiceAccountName
	if accountName != "" {
		ts.addEdge(from, common.KubernetesKindServiceAccount, accountName, common.Reference)
	}
}

func (ts *TopologyStore) parseReferenceForService(from *model.KubernetesObject) {
	spec, err := from.GetServiceSpec()
	if err != nil {
		ts.Logger.Errorf("Get service spec error, %s", err.Error())
		return
	}
	if len(spec.Selector) == 0 {
		ts.Logger.Debugf("Service %s has no selector, skip", from.Name)
		return
	}
	selector := labels.SelectorFromSet(spec.Selector)
	for _, object := range ts.Objects {
		if !common.IsPodController(object.Kind) {
			continue
		}
		template, _ := object.GetPodTemplateSpec()
		if selector.Matches(labels.Set(template.Labels)) {
			ts.addEdge(from, object.Kind, object.Name, common.Selector)
		}
	}
}

func (ts *TopologyStore) parseReferenceForHPA(from *model.KubernetesObject) {
	spec, err := from.GetHPASpec()
	if err != nil {
		ts.Logger.Errorf("Get HPA spec error, %s", err.Error())
		return
	}
	reference := spec.ScaleTargetRef
	ts.addEdge(from, reference.Kind, reference.Name, common.Reference)
}

func (ts *TopologyStore) parseReferenceForRoute(from *model.KubernetesObject) {
	reference, ok := from.Spec["to"]
	if !ok {
		ts.Logger.Errorf("Can not find to field for Route")
		return
	}
	meta, ok := reference.(map[string]interface{})
	if !ok {
		ts.Logger.Errorf("Assert reference to map failed")
		return
	}
	ts.addEdge(from, meta["kind"].(string), meta["name"].(string), common.Reference)
}

func (ts *TopologyStore) parseReferenceForApplication(from *model.KubernetesObject) {
	spec, err := from.GetApplicationSpec()
	if err != nil {
		ts.Logger.Errorf("Get application spec error, %s", err.Error())
		return
	}
	if spec.Selector == nil || len(spec.Selector.MatchLabels) == 0 {
		ts.Logger.Debugf("Application %s has no selector, skip", from.Name)
		return
	}
	selector := labels.SelectorFromSet(spec.Selector.MatchLabels)
	for _, object := range ts.Objects {
		ls := object.GetLabels()
		if len(ls) == 0 {
			continue
		}
		if selector.Matches(labels.Set(ls)) {
			ts.addEdge(from, object.Kind, object.Name, common.Selector)
		}
	}
}

func (ts *TopologyStore) addHelperEdge(from, to *model.KubernetesObject, edgeType string) {
	if nil == from || nil == to {
		return
	}
	tKey := getIndex(to.GetKind(), to.GetName())

	if nil == ts.Response.Indexes {
		ts.Logger.Errorf("Indexes not initialized")
		return
	}

	fKey := getIndex(from.GetKind(), from.GetName())

	if nil == ts.Response.Indexes[fKey] || nil == ts.Response.Indexes[tKey] {
		ts.Logger.Errorf("Index not in nodes: index=%s", tKey)
		return
	}

	node := &model.EdgeHelper{*to, edgeType}

	if nil == ts.Response.Topo {
		ts.Response.Topo = make(model.Topology)
	}

	nodes, ok := ts.Response.Topo[fKey]

	if !ok {
		nodes = []*model.EdgeHelper{{*to, edgeType}}
	} else {
		nodes = append(nodes, node)
	}

	ts.Response.Topo[fKey] = nodes

}

func (ts *TopologyStore) addEdge(from *model.KubernetesObject, kind string, name string, edgeType string) {
	index := getIndex(kind, name)
	to, ok := ts.Response.Indexes[index]
	if !ok || to == nil {
		ts.Logger.Errorf("Object %s/%s not found", kind, name)
		return
	}
	ts.addHelperEdge(from, to, edgeType)

	switch edgeType {
	case common.Reference:
		ts.addHelperEdge(to, from, common.Referenced)
	case common.Selector:
		ts.addHelperEdge(to, from, common.Selected)
	}
}
