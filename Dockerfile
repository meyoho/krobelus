FROM index.alauda.cn/alaudaorg/alaudabase-alpine-go:1.12.4-alpine3.9.3

COPY . $GOPATH/src/krobelus
WORKDIR $GOPATH/src/krobelus
RUN  make compile-local

FROM index.alauda.cn/alaudaorg/alaudabase-alpine-run:alpine3.9.3

ENV KROBELUS_COMP=all
ENV LOG_SIZE=50
ENV LOG_BACKUP=4

EXPOSE 80

RUN mkdir -p /var/log/mathilde /var/log/supervisor /krobelus
ENV GODEBUG=netdns=cgo


COPY ./run/conf/example_config.toml /etc/krobelus/config.toml
COPY ./run/scripts/*.sh /
COPY --from=0 /go/src/krobelus/krobelus /krobelus/krobelus

RUN chmod +x /*.sh /krobelus/krobelus && echo "OK"

CMD ["/krobelus/krobelus", "api"]
