package config

import (
	"os"
	"testing"

	"github.com/spf13/viper"
)

func TestInitContentConfig(t *testing.T) {
	os.Setenv("REDIS_HOSTWRITER", "1")
	os.Setenv("REDIS_HOSTRADER", "2")
	os.Setenv("READ_HOST_READER", "3")
	os.Setenv("redis_host_reader", "4")
	os.Setenv("REDIS_PORTREADER", "5")
	os.Setenv("REDIS_PORT_READER", "6")
	os.Setenv("redis_port_reader", "7")
	os.Setenv("redis_portreader", "8")
	os.Setenv("REDIS_PORTWRITER", "9")
	os.Setenv("REDIS_PORT_WRITER", "10")
	os.Setenv("redis_port_writer", "11")
	os.Setenv("redis_portwriter", "12")
	//os.Setenv("REDIS_HOST_WRITER","10.0.128.65,10.0.128.91,10.0.128.211")
	os.Setenv("REDIS_HOST_READER", "10.0.128.65,10.0.128.91,10.0.128.211")
	os.Setenv("REDIS_PORT_WRITER", "111,111,111")
	//os.Setenv("REDIS_PORT_READER","111,111,111")
	os.Setenv("REDIS_CONFIG_DIR", "../run/conf/etc/paas")

	InitConfig()
	viper.AddConfigPath("../run/conf")
	if err := InitContentConfig(); err != nil {
		t.Errorf("%s", err.Error())
	}
}
