package config

import (
	"github.com/spf13/viper"
)

func GetMiddlewares() []string {
	return viper.GetStringSlice("Middlewares")
}
