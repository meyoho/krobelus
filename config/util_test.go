package config

import (
	"testing"

	"github.com/spf13/viper"
)

func equal(x []string, y []string) bool {
	if len(x) != len(y) {
		return false
	}
	for i := range x {
		if x[i] != y[i] {
			return false
		}
	}
	return true
}

func TestGetMiddlewares(t *testing.T) {
	slice1 := []string{"aaa", "bbb", "ccc"}
	viper.Set("Middlewares", slice1)
	slice2 := GetMiddlewares()
	if !equal(slice1, slice2) {
		t.Errorf("GetMiddlewares error")
	}
}
