package config

import (
	"os"
	"strings"

	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
	RedisConfig  Redis
)

// Redis is redis's config
type Redis struct {
	//   Prefix string `envconfig:"default=krobelus:"`
	InitTimeout          int      `mapstructure:"REDIS_INIT_TIMEOUT"`
	TypeWriter           string   `mapstructure:"REDIS_TYPE_WRITER"`
	TypeReader           string   `mapstructure:"REDIS_TYPE_READER"`
	HostWriter           []string `mapstructure:"REDIS_HOST_WRITER"`
	HostReader           []string `mapstructure:"REDIS_HOST_READER"`
	PortReader           []string `mapstructure:"REDIS_PORT_READER"`
	PortWriter           []string `mapstructure:"REDIS_PORT_WRITER"`
	DBNameWriter         int      `mapstructure:"REDIS_DB_NAME_WRITER"`
	DBNameReader         int      `mapstructure:"REDIS_DB_NAME_READER"`
	DBPasswordReader     string   `mapstructure:"REDIS_DB_PASSWORD_READER"`
	DBPasswordWriter     string   `mapstructure:"REDIS_DB_PASSWORD_WRITER"`
	MaxConnectionsReader int      `mapstructure:"REDIS_MAX_CONNECTIONS_READER"`
	MaxConnectionsWriter int      `mapstructure:"REDIS_MAX_CONNECTIONS_WRITER"`
	MaxRetriesWriter     int      `mapstructure:"REDIS_MAX_RETRIES_WRITER"`
	MaxRetriesReader     int      `mapstructure:"REDIS_MAX_RETRIES_READER"`
	// KeyPrefix should keep the same, use Reader if not
	KeyPrefixWriter string `mapstructure:"REDIS_KEY_PREFIX_WRITER"`
	KeyPrefixReader string `mapstructure:"REDIS_KEY_PREFIX_READER"`
}

type Config struct {
	Krobelus struct {
		APIPort int `envconfig:"default=8080"`
		// If set to false ,disable cache usage in get/list api
		EnableCache bool `envconfig:"default=true"`
	}

	ClientGo struct {
		Burst int     `envconfig:"default=100"`
		QPS   float32 `envconfig:"default=100.0"`
	}

	Furion struct {
		Endpoint   string `envconfig:"default=http://furion:8080"`
		APIVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=3"`
	}

	Kubernetes struct {
		Timeout              int      `envconfig:"default=6"`
		DiscoveryBlackGroups []string `envconfig:"optional"`
		DisableCache         bool     `envconfig:"default=false"`
	}

	Pod struct {
		// ResourceLabel is the key for a Pod that indicate what resource this pod belongs to
		// format: <label-key>:namespace-resourceType-resourceName
		ResourceLabel string `envconfig:"optional"`
		OrgLabel      string `envconfig:"optional"`
	}

	Riki struct {
		Endpoint   string `envconfig:"default=127.0.0.1"`
		APIVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=5"`
	}

	Darchrow struct {
		Endpoint   string `envconfig:"default=127.0.0.1"`
		APIVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=20"`
	}

	CMB struct {
		DefaultContainerCPURequest string `envconfig:"default=0.5"`
		DefaultContainerMEMRequest string `envconfig:"default=256Mi"`
		DefaultContainerCPULimit   string `envconfig:"default=0.5"`
		DefaultContainerMEMLimit   string `envconfig:"default=256Mi"`
		ContainerCPUMax            string `envconfig:"default=4"`
		ContainerMEMMax            string `envconfig:"default=8Gi"`

		InjectAppPodEnv bool `envconfig:"default=false"`
	}

	WebSocket struct {
		ReadBufferSize   int `envconfig:"default=4096"`
		WriteBufferSize  int `envconfig:"default=4096"`
		HandShakeTimeOut int `envconfig:"default=5"`
		Port             int `envconfig:"default=6070"`
		PingInterval     int `envconfig:"default=20"`
		IdleTimeout      int `envconfig:"default=120"`
	}
}

func InitConfig() {
	err := GlobalConfig.LoadFromEnv()
	if err != nil {
		log.Errorf("Load config from env error : %s", err.Error())
		os.Exit(1)
	}
	err = RedisConfig.LoadMixedParams()
	if err != nil {
		log.Errorf("Load mix params error : %s", err.Error())
		os.Exit(1)
	}
}

const (
	KeyUnitTest                     = "UnitTest"
	KeyGlogLevel                    = "GlogLevel"
	KeyMiddlewares                  = "Middlewares"
	KeyGlusterfsEndpoint            = "GlusterfsEndpoint"
	KeyLabelBaseDomain              = "LabelBaseDomain"
	KeyKubernetesSkipCacheResources = "KubernetesSkipCacheResources"
	KeyAProxyEndpoint               = "AProxyEndpoint"
)

var EnvBindings = []struct {
	key          string
	envKey       string
	defaultValue interface{}
}{
	{KeyUnitTest, "UNIT_TEST", false},
	{KeyGlogLevel, "GLOG_LEVEL", "5"},
	{KeyMiddlewares, "MIDDLEWARES", []string{"JaegerTracing"}},
	{KeyGlusterfsEndpoint, "GLUSTERFS_ENDPOINT", "glusterfs-endpoints"},
	{KeyAProxyEndpoint, "APROXY_ENDPOINT", "https://erebus:443"},
	{KeyLabelBaseDomain, "LABEL_BASE_DOMAIN", "alauda.io"},
	{KeyKubernetesSkipCacheResources, "KUBERNETES_SKIP_CACHE_RESOURCES",
		[]string{"componentstatuses", "controllerrevisions", "events", "endpoints"}},
}

// InitContentConfig init configs about component content, like middlewares and there configs
// ENV: MIDDLEWARES
// ENV: UNIT_TEST
func InitContentConfig() error {

	for _, item := range EnvBindings {
		viper.SetDefault(item.key, item.defaultValue)
		if err := viper.BindEnv(item.key, item.envKey); err != nil {
			return err
		}
	}

	// read config files
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/krobelus")
	viper.AddConfigPath("/")
	viper.AddConfigPath("./run/conf")
	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Info("Config file changed:", e.Name)
	})

	viper.AutomaticEnv()
	return nil
}

// LoadFromEnv load env to the Config struct, also check redis env host/port match.
func (conf *Config) LoadFromEnv() error {
	if err := envconfig.Init(conf); err != nil {
		return err
	}
	return nil
}

// LoadMixedParams will load config mix params,the env params will cover volume params if set
func (redisconfig *Redis) LoadMixedParams() error {
	configDir := "/etc/paas/"
	v := viper.New()
	if dir := os.Getenv("REDIS_CONFIG_DIR"); dir != "" {
		configDir = dir
	}
	v.SetConfigName("redis")
	v.AddConfigPath("../run/conf/")
	v.AddConfigPath(configDir)
	// setDefault value
	v.SetDefault("REDIS_INIT_TIMEOUT", 3)
	v.SetDefault("REDIS_TYPE_WRITER", "normal")
	v.SetDefault("REDIS_TYPE_READER", "normal")
	v.SetDefault("REDIS_DB_NAME_WRITER", 0)
	v.SetDefault("REDIS_DB_NAME_READER", 0)
	v.SetDefault("REDIS_DB_PASSWORD_READER", "")
	v.SetDefault("REDIS_DB_PASSWORD_WRITER", "")
	v.SetDefault("REDIS_MAX_CONNECTIONS_READER", 32)
	v.SetDefault("REDIS_MAX_CONNECTIONS_WRITER", 32)
	v.SetDefault("REDIS_MAX_RETRIES_WRITER", 16)
	v.SetDefault("REDIS_MAX_RETRIES_READER", 16)
	v.SetDefault("REDIS_KEY_PREFIX_WRITER", "krobelus:")
	v.SetDefault("REDIS_KEY_PREFIX_READER", "krobelus:")

	v.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		log.Errorf("read file error: %v", err)
	}
	// let old env support viper's reader,
	convertDataKey := []string{
		"REDIS_HOST_WRITER",
		"REDIS_HOST_READER",
		"REDIS_PORT_WRITER",
		"REDIS_PORT_READER",
	}
	for _, k := range convertDataKey {
		res := v.GetString(k)
		if strings.Contains(res, ",") {
			vs := strings.Split(res, ",")
			v.Set(k, vs)
		}
	}
	// ....
	err := v.Unmarshal(redisconfig)
	if err != nil {
		log.Errorf("Unmarshal  error: %v", err)
		return err
	}
	if redisconfig.KeyPrefixReader != redisconfig.KeyPrefixWriter {
		redisconfig.KeyPrefixWriter = redisconfig.KeyPrefixReader
	}
	return nil
}
