package config

import (
	"os"

	"github.com/alauda/kvlog"
	log "github.com/sirupsen/logrus"
)

func SetLogger() {
	formatter := kvlog.New(kvlog.WithPrimaryFields("request_id", "action"), kvlog.IncludeCaller())
	log.SetFormatter(formatter)
	log.SetOutput(os.Stderr)
	log.SetLevel(log.DebugLevel)
}
