package pkg

/*

type DBError struct {
	MysqlNumber uint16
	PQCode      string
	Output     string
}

func (e *DBError) Error() string {
	return e.PQCode
}

func (e *DBError) IsUniqueViolationError() bool {
	// TODO: || mysql...
	return e.PQCode == "unique_violation"
}

func GetDBError(err error) *DBError {
	if IsPostgres() {
		if err, ok := err.(*pq.Error); ok {
			logrus.Debugf("Parse postgres error code: %s", err.Code.Name())
			dbErr := DBError{
				PQCode:  err.Code.Name(),
				Output: err.Error(),
			}
			logrus.Debugf("%+v", dbErr)
			return &dbErr
		}
	}
	return &DBError{Output: err.Error()}
}
*/

/*

func (c *KubeClient) OldWatchForPods(timeout int64) {
	logger := c.logger
	//timeout = 3600
	watchInterface, err := c.Client.CoreV1().Pods("").Watch(
		meta_v1.ListOptions{Watch: true, TimeoutSeconds: &timeout})
	if err != nil {
		logger.WithError(err).Error("Error retrieving watch interface for pods")
		panic(err.Error())
	}

	events := watchInterface.ResultChan()
	for {
		event, ok := <-events
		if event.Type != "" {
			c.podEventHandler(event.Object, string(event.Type))
		} else {
			log.Printf("Fucking %+v", event.Object)
			log.Printf("Pod watch timed out: %v seconds", timeout)
		}
		if !ok {
			break
		}
	}
	return
}

// RetrieveAllPods will retrieve all pods info from kubernetes cluster and update cache
func (c *KubeClient) RetrieveAllPods() error {
	pods, err := c.Client.CoreV1().Pods("").List(
		meta_v1.ListOptions{LabelSelector: model.GetSvcUidLabel()})
	if err != nil {
		c.logger.WithError(err).Info("Retrieve pods list error!")
		panic(err)
	}
	c.SyncPods(pods)
	return nil

}

func (c *KubeClient) SyncPods(pods *v1.PodList) error {
	for _, pod := range pods.Items {
		c.logger.Debugf("%s", pod.Name)
	}
	return nil
}

*/
