# README #


[TOC]

This is a new start.

## Basic information

Run `make help` to show all commands


### Dockerfiles

* `Dockerfile`: local test
* `Dockerfile.gitlab`: build on gitlab and push to product


### Doc
 * [Confluence/Krobelus](http://confluence.alaudatech.com/display/DEV/Krobelus)
 * The `docs` dir
 * [API DOC](http://internal-api-doc.alauda.cn/krobelus.html)


