package common

import (
	"krobelus/config"
	"testing"

	"github.com/spf13/viper"
)

func TestGetKubernetesTypes(t *testing.T) {
	if GetKubernetesTypeFromKind("Secret") != "secrets" {
		t.Errorf("Trans type secret error")
	}
	if GetKubernetesTypeFromKind("StorageClass") != "storageclasses" {
		t.Errorf("Trans type storageclass error")
	}
	if GetKubernetesTypeFromKind("NetworkPolicy") != "networkpolicies" {
		t.Errorf("Trans type networkpolicies error")
	}
	if GetKubernetesTypeFromKind("Ingress") != "ingresses" {
		t.Errorf("Trans type ingresses error")
	}
	if GetKubernetesTypeFromKind("Endpoints") != "endpoints" {
		t.Errorf("Trans type endpoints error")
	}
}

func TestIsPodController(t *testing.T) {
	if !IsPodController(KubernetesKindDeployment) {
		t.Errorf("%s is podcontroller", KubernetesKindDeployment)
	}
	if !IsPodController(KubernetesKindDaemonSet) {
		t.Errorf("%s is podcontroller", KubernetesKindDaemonSet)
	}
	if !IsPodController(KubernetesKindStatefulSet) {
		t.Errorf("%s is podcontroller", KubernetesKindStatefulSet)
	}
	if IsPodController(KubernetesKindNamespace) {
		t.Errorf("%s is not podcontroller", KubernetesKindNamespace)
	}
}

func TestIsPodControllerResource(t *testing.T) {
	Pod := []string{"deployments", "statefulsets", "daemonsets"}

	for _, res := range Pod {
		if !IsPodControllerResource(res) {
			t.Errorf("TestIsPodControllerResource error")
		}
	}

	ErrRes := "endpoints"
	if IsPodControllerResource(ErrRes) {
		t.Errorf("TestIsPodControllerResource error")
	}
}

func TestKey(t *testing.T) {
	viper.Set(config.KeyLabelBaseDomain, "com.cn")

	app := map[string]string{
		"service.com.cn/uuid":      SvcUIDKey(),
		"service.com.cn/name":      SvcNameKey(),
		"app.com.cn/name":          AppNameKey(),
		"app.com.cn/uuid":          AppUIDKey(),
		"app.com.cn/new-uuid":      AppNewUUIDKey(),
		"resource.com.cn/uuid":     ResourceUIDKey(),
		"project.com.cn/name":      ProjectNameKey(),
		"resource.com.cn/status":   ResourceStatusKey(),
		"app.com.cn/create-method": AppCreateMethodKey(),
		"app.com.cn/display-name":  AppDisplayNameKey(),
		"owners.com.cn/info":       OwnersInfoKey(),
		"app.com.cn/source":        AppSourceKey(),
		"app.com.cn/deleted_at":    AppDeletedAtKey(),
		"app.com.cn/action":        AppActionKey(),
		"app.com.cn/last-replicas": AppLastReplicasKey(),
		"app.com.cn/error-reason":  AppErrorReasonKey(),
	}

	for k, v := range app {
		if k != v {
			t.Errorf("error %s", v)
		}
	}

}

func TestIsSkipCacheResource(t *testing.T) {
	tests := []struct {
		name               string
		resourceType       string
		skipCacheResources []string
		want               bool
	}{
		{
			name:               "skip",
			resourceType:       "endpoints",
			skipCacheResources: []string{"endpoints", "controllerrevisions"},
			want:               true,
		},
		{
			name:               "not skip",
			resourceType:       "endpoints",
			skipCacheResources: []string{"controllerrevisions"},
			want:               false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			viper.Set(config.KeyKubernetesSkipCacheResources, tt.skipCacheResources)
			if got := IsSkipCacheResource(tt.resourceType); got != tt.want {
				t.Errorf("IsSkipCacheResource() = %v, want %v", got, tt.want)
			}
		})
	}
}
