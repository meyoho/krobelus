package common

import (
	"testing"
	"time"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
)

func TestToString(t *testing.T) {
	var inter int = 65
	var arr = [5]int{0, 1, 2, 3, 4}
	if ToString(inter) != "65" {
		t.Errorf("int to string error")
	}
	if ToString(arr) != "[0 1 2 3 4]" {
		t.Errorf("slice to string error")
	}
}

func TestRemoveEmptyString(t *testing.T) {
	input := []string{"a", "", "b"}
	expect := []string{"a", "b"}
	result := RemoveEmptyString(input)
	if len(result) != 2 {
		t.Errorf("RemoveEmptyString error result: %+v %+v", result, expect)
	}
}

func TestStringInSlice(t *testing.T) {
	data := []string{"a", "b", "c"}
	if !StringInSlice("a", data) {
		t.Error("string in slice error")
	}
	if StringInSlice("d", data) {
		t.Error("string not in slice error")
	}
}

func TestCombineString(t *testing.T) {
	data := []string{"a", "b", "c"}
	result := CombineString(data...)
	if result != "a:b:c" {
		t.Error("combine string error")
	}
}

func TestBackoffLimitRetryWhenServerErr(t *testing.T) {
	testNum := 0
	action := func(attempt uint) error {
		testNum++
		return k8sErrors.NewTimeoutError("TimeoutError", 0)
	}
	err := BackoffLimitRetryWhenServerErr(action, 5, 1*time.Second)
	if err == nil || testNum != 6 {
		t.Error("BackoffLimitRetryWhenServerErr failed")
	}
}

func TestIsServerError(t *testing.T) {
	clientErr := k8sErrors.NewUnauthorized("")
	serverErr := k8sErrors.NewTimeoutError("TimeoutError", 0)
	if IsServerError(clientErr) != false || IsServerError(serverErr) != true {
		t.Error("IsServerError not pass")
	}
}
