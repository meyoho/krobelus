package common

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

const (
	RequestIDHeader = "Alauda-Request-ID"
)

type Log struct {
	*log.Entry
}

func GetLog() *log.Logger {
	return log.StandardLogger()
}

func ToLog(entry *log.Entry) Log {
	return Log{entry}
}

func GetLogger(c *gin.Context) Log {
	requestID := c.GetHeader(RequestIDHeader)
	if requestID == "" {
		requestID = NewUUID()
	}
	entry := log.WithField("request_id", requestID)
	return Log{entry}
}

// GetLogger get a logger with request_id in gin context
/*func GetLogger(c *gin.Context) *log.Entry {
	requestID := c.GetHeader(RequestIDHeader)
	return GetLoggerByID(requestID)
}*/

func GetLoggerWithAction(c *gin.Context, action string) Log {
	return Log{GetLogger(c).WithField("action", action)}
}

// GetLogger will get a new logger with request id. If not provide, generate a new one
func GetLoggerByID(requestID string) Log {
	if requestID == "" {
		requestID = NewUUID()
	}
	return Log{log.WithFields(log.Fields{"request_id": requestID})}
}

func GetLoggerByRegionID(regionID string) Log {
	return Log{log.WithFields(log.Fields{"region": regionID})}
}

func GetLoggerByKV(k, v string) Log {
	return Log{log.WithFields(log.Fields{k: v})}
}
