package common

import (
	"strings"

	"krobelus/config"

	"github.com/spf13/viper"
	"github.com/thoas/go-funk"
)

const (
	KubernetesAPIVersionV1 = "v1"
	// KubernetesMetadataKey is key string 'metadata' in k8s resource.
	KubernetesMetadataKey = "metadata"
	// KubernetesAnnotationsKey is key string 'annotations' in k8s resource.
	KubernetesAnnotationsKey = "annotations"
)

// Kubernetes Resources
const (
	KubernetesKindNamespace   = "Namespace"
	KubernetesKindApplication = "Application"
	KubernetesKindService     = "Service"
	KubernetesKindDeployment  = "Deployment"
	KubernetesKindDaemonSet   = "DaemonSet"
	KubernetesKindStatefulSet = "StatefulSet"
	KubernetesKindConfigmap   = "ConfigMap"
	KubernetesKindReplicaSet  = "ReplicaSet"
	KubernetesKindHPA         = "HorizontalPodAutoscaler"
	KubernetesKindPod         = "Pod"
	KubernetesKindSecret      = "Secret"

	KubernetesKindPV             = "PersistentVolume"
	KubernetesKindPVC            = "PersistentVolumeClaim"
	KubernetesKindSC             = "StorageClass"
	KubernetesKindRoute          = "Route"
	KubernetesKindIngress        = "Ingress"
	KubernetesKindEvent          = "Event"
	KubernetesKindEndpoints      = "Endpoints"
	KubernetesKindServiceAccount = "ServiceAccount"
)

// Kubernetes Event Reasons
const (
	FailedSync     = "FailedSync"
	ReasonSynced   = "Synced"
	ReasonStarted  = "Started"
	ReasonStopped  = "Stopped"
	ReasonUpdating = "Updating"
	ReasonDeleting = "Deleting"
	ReasonDeleted  = "Deleted"
)

// kubernetes api params
const (
	ParamLabelSelector = "labelSelector"
	ParamFieldSelector = "fieldSelector"
	ParamNameSearch    = "name"
	ParamPage          = "page"
	ParamPageSize      = "page_size"
	ParamLimit         = "limit"
	ParamContinue      = "continue"
)

// kubernetes event
const (
	EventTypeNormal  = "Normal"
	EventTypeWarning = "Warning"
)

// Kubernetes topology reference kinds
const (
	Reference  = "Reference"
	Referenced = "Referenced"
	Selector   = "Selector"
	Selected   = "Selected"
)

const (
	// ApplicationDefaultHistoryLimit is the default max count of the
	// histories of an application.
	ApplicationDefaultHistoryLimit = 20
)

var (
	ClusterScopeResourceKind = map[string]bool{
		KubernetesKindNamespace: true,
		KubernetesKindPV:        true,
		KubernetesKindSC:        true,
		"ClusterRole":           true,
		"ClusterRoleBinding":    true,
	}

	AppUnsupportedResourceKind = []string{
		KubernetesKindNamespace,
	}
)

// GetKubernetesTypeFromKind get kubernetes type by kind.
// eg: DaemonSet --> daemonsets
// current support:
// 1. Deployment / DaemonSet / StatefulSet
// TODO: handler special case
func GetKubernetesTypeFromKind(kind string) string {
	switch kind {
	case "NetworkPolicy":
		return "networkpolicies"
	case KubernetesKindIngress:
		return "ingresses"
	case KubernetesKindSC:
		return "storageclasses"
	case KubernetesKindEndpoints:
		return "endpoints"
	default:
		return strings.ToLower(kind) + "s"
	}
}

// Kubernetes Deploy Mode. (Pod controllers)
var (
	KubernetesDeployModeKind = map[string]bool{
		KubernetesKindDeployment:  true,
		KubernetesKindDaemonSet:   true,
		KubernetesKindStatefulSet: true,
	}
)

// IsSkipCacheResource return true if the resource type does not cache.
func IsSkipCacheResource(resourceType string) bool {
	skipCacheResources := viper.GetStringSlice(config.KeyKubernetesSkipCacheResources)
	return StringInSlice(resourceType, skipCacheResources)
}

// IsPodController check whether a kubernetes resource kind is a pod controller.
// current support: daemonset/deployment/statefulset
func IsPodController(kind string) bool {
	return KubernetesDeployModeKind[kind]
}

// IsPodControllerResource check if a resource type is a PodController
func IsPodControllerResource(resource string) bool {
	return funk.Contains([]string{"deployments", "statefulsets", "daemonsets",
		"cronjobs", "jobs"}, resource)
}

func getKey(resource, attr string) string {
	if resource != "" {
		return resource + "." + viper.GetString(config.KeyLabelBaseDomain) + "/" + attr
	}
	return viper.GetString(config.KeyLabelBaseDomain) + "/" + attr
}

func SvcUIDKey() string {
	return getKey("service", "uuid")
}

func SvcNameKey() string {
	return getKey("service", "name")
}

func AppNameKey() string {
	return getAppKey("name")
}

func AppUIDKey() string {
	return getAppKey("uuid")
}

func AppNewUUIDKey() string {
	return getAppKey("new-uuid")
}

func ResourceUIDKey() string {
	return getKey("resource", "uuid")
}

func ProjectNameKey() string {
	return getKey("project", "name")
}

func NewProjectNameKey() string {
	return getKey("project", "name")
}

func ResourceStatusKey() string {
	return getKey("resource", "status")
}

func getAppKey(attr string) string {
	return getKey("app", attr)
}

func AppCreateMethodKey() string {
	return getAppKey("create-method")
}

func AppDisplayNameKey() string {
	return getAppKey("display-name")
}

func OwnersInfoKey() string {
	return getKey("owners", "info")
}

func AppSourceKey() string {
	return getAppKey("source")
}

func AppDeletedAtKey() string {
	return getAppKey("deleted_at")
}

// start/stop/starting/stopping
// 1. staring/stopping -> handle it
// 2. start/stop -> what can we do(stop or start) on this app.
func AppActionKey() string {
	return getAppKey("action")
}

func AppLastReplicasKey() string {
	return getAppKey("last-replicas")
}

func AppErrorReasonKey() string {
	return getAppKey("error-reason")
}

// AppRevisionKey returns the key of application revision
func AppRevisionKey() string {
	return getAppKey("revision")
}

// AppHistoryLimitKey returns the key of application history limit
func AppHistoryLimitKey() string {
	return getAppKey("history-limit")
}

// AppRollbackFromKey returns the key of application rollback from
func AppRollbackFromKey() string {
	return getAppKey("rollback-from")
}

// ALB2NameKey returns the key of alb2 name
func ALB2NameKey() string {
	return getKey("alb2", "name")
}

// ALB2FrontendKey returns the key of alb2 frontend
func ALB2FrontendKey() string {
	return getKey("alb2", "frontend")
}

const (
	ResourceStatusInitializing = "Initializing"
)
