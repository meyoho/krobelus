package common

import (
	"bytes"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	opentracing "github.com/opentracing/opentracing-go"
	opentracinglog "github.com/opentracing/opentracing-go/log"
	log "github.com/sirupsen/logrus"
	jaegerConfig "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
)

/*
Property	Description
JAEGER_SERVICE_NAME	The service name
JAEGER_AGENT_HOST	The hostname for communicating with agent via UDP
JAEGER_AGENT_PORT	The port for communicating with agent via UDP
JAEGER_ENDPOINT	The HTTP endpoint for sending spans directly to a collector, i.e. http://jaeger-collector:14268/api/traces
JAEGER_USER	Username to send as part of "Basic" authentication to the collector endpoint
JAEGER_PASSWORD	Password to send as part of "Basic" authentication to the collector endpoint
JAEGER_REPORTER_LOG_SPANS	Whether the reporter should also log the spans
JAEGER_REPORTER_MAX_QUEUE_SIZE	The reporter's maximum queue size
JAEGER_REPORTER_FLUSH_INTERVAL	The reporter's flush interval, with units, e.g. "500ms" or "2s" (valid units)
JAEGER_SAMPLER_TYPE	The sampler type
JAEGER_SAMPLER_PARAM	The sampler parameter (number)
JAEGER_SAMPLER_MANAGER_HOST_PORT	The HTTP endpoint when using the remote sampler, i.e. http://jaeger-agent:5778/sampling
JAEGER_SAMPLER_MAX_OPERATIONS	The maximum number of operations that the sampler will keep track of
JAEGER_SAMPLER_REFRESH_INTERVAL	How often the remotely controlled sampler will poll jaeger-agent for the appropriate sampling strategy,
 with units, e.g. "1m" or "30s" (valid units)
JAEGER_TAGS	A comma separated list of name = value tracer level tags, which get added to all reported spans.
The value can also refer to an environment variable using the format ${envVarName:default}, where the :default is optional,
and identifies a value to be used if the environment variable cannot be found
JAEGER_DISABLED	Whether the tracer is disabled or not. If true, the default opentracing.NoopTracer is used.
JAEGER_RPC_METRICS	Whether to store RPC metrics

By default, the client sends traces via UDP to the agent at localhost:6831.
Use JAEGER_AGENT_HOST and JAEGER_AGENT_PORT to send UDP traces to a different host:port.
If JAEGER_ENDPOINT is set, the client sends traces to the endpoint via HTTP,
making the JAEGER_AGENT_HOST and JAEGER_AGENT_PORT unused.
If JAEGER_ENDPOINT is secured, HTTP basic authentication can be performed by setting
the JAEGER_USER and JAEGER_PASSWORD environment variables.
*/

// InitJaeger init jaeger from envs. if any error occurred, it will return nil.
// Caller must close the closer only when exist.
func InitJaeger() io.Closer {
	if os.Getenv("JAEGER_SERVICE_NAME") == "" {
		os.Setenv("JAEGER_SERVICE_NAME", "krobelus")
	}
	if os.Getenv("JAEGER_SAMPLER_TYPE") == "" {
		os.Setenv("JAEGER_SAMPLER_TYPE", "const")
	}
	if os.Getenv("JAEGER_SAMPLER_PARAM") == "" {
		os.Setenv("JAEGER_SAMPLER_PARAM", "1")
	}

	cfg, err := jaegerConfig.FromEnv()
	if err != nil {
		log.Errorf("jaeger FromEnv() error: %s", err.Error())
		return nil
	}
	log.Debugf("config is %+v", *cfg.Reporter)

	tracer, closer, err := cfg.NewTracer(
		jaegerConfig.Logger(jaegerlog.StdLogger),
	)
	if err != nil {
		log.Errorf("NewTracer() error: %s", err.Error())
		return nil
	}
	opentracing.SetGlobalTracer(tracer)
	return closer
}

const MaxBufferSize = 4096

type ResponseStatusWriter struct {
	gin.ResponseWriter
	buff *bytes.Buffer
}

func NewResponseStatusWriter(resp gin.ResponseWriter) *ResponseStatusWriter {
	r := &ResponseStatusWriter{
		ResponseWriter: resp,
		buff:           &bytes.Buffer{},
	}
	return r
}

func (r *ResponseStatusWriter) Header() http.Header {
	return r.ResponseWriter.Header()
}

func (r *ResponseStatusWriter) Write(b []byte) (int, error) {
	require := len(b)
	available := MaxBufferSize - r.buff.Len()
	if available >= require {
		r.buff.Write(b)
	} else if available > 0 {
		r.buff.Write(b[:available])
	}
	return r.ResponseWriter.Write(b)
}

func (r *ResponseStatusWriter) WriteHeader(code int) {
	r.ResponseWriter.WriteHeader(code)
}

func TracingMiddleWare(c *gin.Context) {
	carrie := opentracing.HTTPHeadersCarrier(c.Request.Header)
	tracer := opentracing.GlobalTracer()
	spanCtx, err := tracer.Extract(opentracing.HTTPHeaders, carrie)
	var span opentracing.Span
	spanName := "krobelus"
	if c.Request.URL.String() == "/" {
		//health check
		c.Next()
		return
	}
	if err == nil {
		span = opentracing.StartSpan(spanName, opentracing.ChildOf(spanCtx))
	} else {
		span = opentracing.StartSpan(spanName)
	}
	defer span.Finish()

	requestID := c.Request.Header.Get("Alauda-Request-ID")
	span.SetTag("request-id", requestID)
	span.SetTag("http.url", c.Request.URL.String())
	span.SetTag("http.method", c.Request.Method)

	ctx := opentracing.ContextWithSpan(c.Request.Context(), span)
	c.Request = c.Request.WithContext(ctx)
	writer := NewResponseStatusWriter(c.Writer)
	c.Writer = writer

	c.Next()
	span.SetTag("http.status_code", writer.Status())
	if writer.Status() >= 400 {
		span.SetTag("error", true)
		span.LogFields(
			opentracinglog.String("message", writer.buff.String()),
		)
		lastErr, exist := c.Get("error")
		if exist {
			if oneErr, ok := lastErr.(error); ok {
				span.LogFields(
					opentracinglog.String("stack", errors.ErrorStack(oneErr)),
				)
			} else if errs, ok := lastErr.([]error); ok {
				var errStrs []string
				for _, err := range errs {
					errStrs = append(errStrs, errors.ErrorStack(err))
				}
				span.LogFields(
					opentracinglog.String("stack", strings.Join(errStrs, "\n")),
				)
			}
		}
	}
}
