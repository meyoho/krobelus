package common

import (
	"net/http"
	"strings"

	"github.com/juju/errors"
)

var (
	ErrNotFound = errors.New("resource not found")

	ErrKubernetesConnect = errors.New("connect kubernetes error")
)

const (
	// RedisNotFoundError is returned by redis if cache miss.
	RedisNotFoundError = "redis: nil"
)

func IsNotExistError(err error) bool {
	if err == nil {
		return false
	}
	return strings.Contains(err.Error(), RedisNotFoundError)
}

const (
	// Source
	Source = 1044

	DarchrowSource = 1030
)

const (
	// Code
	CodeInvalidArgs = "invalid_args"
	CodeExist       = "resource_already_exist"

	CodeNotExist     = "resource_not_exist"
	CodeUnknownIssue = "unknown_issue"

	// Get region info error
	CodeRegionError = "region_error"
	// Error about kubernetes
	CodeKubernetesError        = "kubernetes_error"
	CodeResourceTypeNotExist   = "resource_type_not_exist"
	CodeKubernetesConnectError = "kubernetes_connect_error"

	//Namespace Error
	NamespaceAlreadyExistError = "namespace_already_exist"
	ResourceQuotaCreateError   = "resource_quota_create_error"
	ResourceQuotaUpdateError   = "resource_quota_update_error"
	LimitRangeCreateError      = "limit_range_create_error"
	LimitRangeUpdateError      = "limit_range_update_error"
	SecretCreateError          = "secret_create_error"
	SAAddSecretError           = "sa_add_secret_error"

	JFrogNamespaceCreateError = "jfrog_namespace_create_error"
)

var HTTPCodeMap = map[string]int{
	CodeInvalidArgs:     http.StatusBadRequest,
	CodeExist:           http.StatusBadRequest,
	CodeKubernetesError: http.StatusInternalServerError,
}

type CodeError struct {
	Code    string
	Message string
}

func NewError(code string, err error) CodeError {
	return CodeError{
		Code:    code,
		Message: err.Error(),
	}
}

func (e CodeError) Error() string {
	return e.Message
}

func (e CodeError) APIError() error {
	return BuildError(e.Code, e.Message)
}

type KrobelusError struct {
	Source  int                 `json:"source" example:"1044"`
	Code    string              `json:"code"`
	Message string              `json:"message"`
	Detail  []map[string]string `json:"fields,omitempty" swaggertype:"array,object"`
}

type KrobelusErrors struct {
	Errors []*KrobelusError `json:"errors"`
}

func (es KrobelusErrors) Error() string {
	var messages []string
	for _, err := range es.Errors {
		messages = append(messages, err.Message)
	}
	return strings.Join(messages, "\n")
}

type HTTPError KrobelusError

func (es HTTPError) Error() string {
	return es.Message
}

// HTTPResponseError wraps the error with an http status code.
type HTTPResponseError struct {
	Cause      error
	StatusCode int
}

func (he *HTTPResponseError) Error() string {
	if he.Cause != nil {
		return he.Cause.Error()
	}
	return "unknown error"
}

type HTTPErrorResponse struct {
	Source     int    `json:"source"`
	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
}

func (he HTTPErrorResponse) Error() string {
	return he.Message
}

// Errors for http response

func BuildError(code string, message string) error {
	return BuildSourceError(Source, code, message)
}

func BuildSourceError(source int, code string, message string) error {
	return KrobelusErrors{
		Errors: []*KrobelusError{
			{
				Source:  source,
				Code:    code,
				Message: message,
			},
		},
	}
}

func BuildServerUnknownError(message string) error {
	return BuildError(CodeUnknownIssue, message)
}

func BuildResourceNotExistError(message string) error {
	return BuildError(CodeNotExist, message)
}

func BuildInvalidArgsError(message string) error {
	return BuildError(CodeInvalidArgs, message)
}

func BuildResourceAlreadyExistError(message string) error {
	return BuildError(CodeExist, message)
}

func BuildKubernetesError(message string) error {
	return BuildError(CodeKubernetesError, message)
}

func BuildRegionError(message string) error {
	return BuildError(CodeRegionError, message)
}

func BuildDarchrowErrorResponse(statusCode int, message string) *HTTPErrorResponse {
	return &HTTPErrorResponse{
		Source:     DarchrowSource,
		StatusCode: statusCode,
		Message:    message,
	}
}

func BuildResourceQuotaCreateError(message string) error {
	return BuildError(ResourceQuotaCreateError, message)
}

func BuildResourceQuotaUpdateError(message string) error {
	return BuildError(ResourceQuotaUpdateError, message)
}

func BuildLimitRangeUpdateError(message string) error {
	return BuildError(LimitRangeUpdateError, message)
}

func BuildLimitRangeCreateError(message string) error {
	return BuildError(LimitRangeCreateError, message)
}

func BuildSecretCreateError(message string) error {
	return BuildError(SecretCreateError, message)
}

func BuildSAAddSecretCreateError(message string) error {
	return BuildError(SAAddSecretError, message)
}

func BuildNamespaceAlreadyExistError(message string) error {
	return BuildError(NamespaceAlreadyExistError, message)
}

func BuildResourceTypeNotExistError(message string) error {
	return BuildError(CodeResourceTypeNotExist, message)
}

// BuildKrobelusErrors builds errors of KrobelusErrors.
// The items of 'errs' array must be 'KrobelusErrors' type.
func BuildKrobelusErrors(errs []error) error {
	var kerrs []*KrobelusError
	for _, err := range errs {
		kerr, ok := err.(KrobelusErrors)
		if ok {
			kerrs = append(kerrs, kerr.Errors[0])
		}
	}
	return KrobelusErrors{Errors: kerrs}
}
