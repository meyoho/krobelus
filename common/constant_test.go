package common

import (
	"krobelus/config"
	"testing"

	"github.com/spf13/viper"
)

func TestIsIingStatus(t *testing.T) {
	status := map[bool]bool{
		CurrentCreatingStatus.IsDeployingStatus():      true,
		CurrentUpdatingStatus.IsDeployingStatus():      true,
		CurrentDeletingStatus.IsDeployingStatus():      true,
		CurrentCreatedStatus.IsDeployedStatus():        true,
		CurrentUpdatedStatus.IsDeployedStatus():        true,
		CurrentDeletedStatus.IsDeployedStatus():        true,
		CurrentCreateErrorStatus.IsDeployErrorStatus(): true,
		CurrentUpdateErrorStatus.IsDeployErrorStatus(): true,
		CurrentDeleteErrorStatus.IsDeployErrorStatus(): true,
		CurrentCreatedStatus.IsDeployingStatus():       false,
		CurrentUpdateErrorStatus.IsDeployedStatus():    false,
		CurrentCreatingStatus.IsDeployErrorStatus():    false,
		CurrentDeletingStatus.IsDeleting():             true,
		TargetDeletedState.IsDeleted():                 true,
		TargetStoppedState.IsStopped():                 true,
		TargetStoppedState.IsDeleted():                 false,
		TargetDeletedState.IsStopped():                 false,
		CurrentCreatedStatus.IsDeleting():              false,
	}

	for key, value := range status {
		if key != value {
			t.Error("status error")
		}
	}
}

func TestGetGlusterFSEndpoint(t *testing.T) {
	viper.Set(config.KeyGlusterfsEndpoint, "com.cn")

	if GetGlusterFSEndpoint() != "com.cn" {
		t.Errorf("GetGlusterFSEndpoint error")
	}
}
func TestIsPostPutRequest(t *testing.T) {
	if IsPostPutRequest(HTTPGet) {
		t.Errorf("HTTPGet is not POST or PUT request")
	}
	if !IsPostPutRequest(HTTPPost) {
		t.Errorf("HTTPPost request")
	}
	if !IsPostPutRequest(HTTPPut) {
		t.Errorf("HTTPPut request")
	}
}
