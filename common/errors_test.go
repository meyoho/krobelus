package common

import (
	"testing"

	"github.com/juju/errors"
)

func TestIsNotExistError(t *testing.T) {
	err1 := errors.New("AAAredis: nilBBB")
	err2 := errors.New("redis:nil")

	if !IsNotExistError(err1) {
		t.Errorf("RedisNotFoundError")
	}
	if IsNotExistError(err2) {
		t.Error("ErrNotFound")
	}
}

func TestBuildSourceError(t *testing.T) {
	var err1 error = BuildError("A", "error1")
	var err2 error = BuildError("B", "error2")
	var err3 error = BuildError("C", "error3")
	SE := []error{err1, err2, err3}
	if ER := BuildKrobelusErrors(SE).Error(); ER != "error1\nerror2\nerror3" {
		t.Errorf("Get message error,%s", ER)
	}
}

func TestError(t *testing.T) {
	Err := map[string]string{
		BuildDarchrowErrorResponse(200, "timeout").Error(): "timeout",
		BuildServerUnknownError("Err1").Error():            "Err1",
		BuildResourceNotExistError("Err2").Error():         "Err2",
		BuildInvalidArgsError("Err3").Error():              "Err3",
		BuildResourceAlreadyExistError("Err4").Error():     "Err4",
		BuildKubernetesError("Err5").Error():               "Err5",
		BuildRegionError("Err6").Error():                   "Err6",
		BuildResourceQuotaCreateError("Err7").Error():      "Err7",
		BuildResourceQuotaUpdateError("Err8").Error():      "Err8",
		BuildLimitRangeUpdateError("Err9").Error():         "Err9",
		BuildLimitRangeCreateError("Err10").Error():        "Err10",
		BuildSecretCreateError("Err11").Error():            "Err11",
		BuildSAAddSecretCreateError("Err12").Error():       "Err12",
		BuildNamespaceAlreadyExistError("Err13").Error():   "Err13",
		BuildResourceTypeNotExistError("Err14").Error():    "Err14",
	}

	for k, v := range Err {
		if k != v {
			t.Errorf("Error is %s", k)
		}
	}
}

func TestBuildDarchrowErrorResponse(t *testing.T) {
	B1 := BuildDarchrowErrorResponse(200, "It works")
	B2 := HTTPErrorResponse{
		Source:     DarchrowSource,
		StatusCode: 200,
		Message:    "It works",
	}
	if *B1 != B2 {
		t.Errorf("build error")
	}
}
