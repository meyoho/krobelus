package common

import (
	"krobelus/config"

	"github.com/spf13/viper"
)

const (
	Component = "krobelus"
)

// Cache key prefix

// Service Status

type ResourceTargetState string

const (
	// Target states

	TargetStoppedState ResourceTargetState = "Stopped"
	TargetDeletedState ResourceTargetState = "Deleted"
	// only used by PV now.

	// TargetUpdatedState ResourceTargetState = "Updated"
)

type ResourceStatus string

var (
	ErrorStatusList     = []KubernetesResourceStatus{CurrentCreateErrorStatus, CurrentUpdateErrorStatus, CurrentDeleteErrorStatus}
	DeployingStatusList = []KubernetesResourceStatus{CurrentCreatingStatus, CurrentUpdatingStatus, CurrentDeletingStatus}
	DeployedStatusList  = []KubernetesResourceStatus{CurrentCreatedStatus, CurrentUpdatedStatus, CurrentDeletedStatus}
)

type KubernetesResourceStatus string

const (
	CurrentCreatingStatus    KubernetesResourceStatus = "Creating"
	CurrentCreateErrorStatus KubernetesResourceStatus = "CreateError"
	CurrentCreatedStatus     KubernetesResourceStatus = "Created"
	CurrentUpdatingStatus    KubernetesResourceStatus = "Updating"
	CurrentUpdateErrorStatus KubernetesResourceStatus = "UpdateError"
	CurrentUpdatedStatus     KubernetesResourceStatus = "Updated"
	CurrentDeletingStatus    KubernetesResourceStatus = "Deleting"
	CurrentDeleteErrorStatus KubernetesResourceStatus = "DeleteError"
	CurrentDeletedStatus     KubernetesResourceStatus = "Deleted"
)

func (s KubernetesResourceStatus) IsDeployingStatus() bool {
	for _, item := range DeployingStatusList {
		if s == item {
			return true
		}
	}
	return false
}

func (s KubernetesResourceStatus) IsDeployedStatus() bool {
	for _, item := range DeployedStatusList {
		if s == item {
			return true
		}
	}
	return false
}
func (s KubernetesResourceStatus) IsDeployErrorStatus() bool {
	for _, item := range ErrorStatusList {
		if s == item {
			return true
		}
	}
	return false
}

func (s KubernetesResourceStatus) IsDeleting() bool {
	return s == CurrentDeletingStatus
}

func (target ResourceTargetState) IsDeleted() bool {
	return target == TargetDeletedState
}

func (target ResourceTargetState) IsStopped() bool {
	return target == TargetStoppedState
}

// DB drivers

func GetGlusterFSEndpoint() string {
	return viper.GetString(config.KeyGlusterfsEndpoint)
}

const (
	CustomResourceDefinitionType = "customresourcedefinitions"
)

// Envs will be injected into the pod.container spec
const (
	EnvServiceID   = "__ALAUDA_SERVICE_ID__"
	EncServiceName = "__ALAUDA_SERVICE_NAME__"
	EnvAppName     = "__ALAUDA_APP_NAME__"
)

const (
	HTTPGet    = "GET"
	HTTPPost   = "POST"
	HTTPPut    = "PUT"
	HTTPDelete = "DELETE"
	HTTPPatch  = "PATCH"

	KubernetesTokenHeader = "X-Kubernetes-Auth-Token"
)

// IsPostPutRequest check if a http request is a post or put request
// they always share the same body structure
func IsPostPutRequest(method string) bool {
	return StringInSlice(method, []string{HTTPPut, HTTPPost})
}

var AlaudaQueryStrings = []string{
	"skipCache",
}

type ServiceScaleAction string

const (
	NamespaceDefault      = "default"
	ServiceAccountDefault = "default"
)
