package common

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/Rican7/retry"
	"github.com/Rican7/retry/backoff"
	"github.com/Rican7/retry/strategy"
	"github.com/davecgh/go-spew/spew"
	sysErrors "github.com/pkg/errors"
	"github.com/thoas/go-funk"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/apimachinery/pkg/util/rand"
	"k8s.io/apimachinery/pkg/util/uuid"
)

func ToString(data interface{}) string {
	return spew.Sprintf("%+v", data)
}

// GetTimeDuration generate a time duration of seconds
func GetTimeDuration(t int) time.Duration {
	return time.Duration(t) * time.Second
}

// StringInSlice check whether a string is in an slice.
func StringInSlice(a string, list []string) bool {
	return funk.Contains(list, a)
}

// RemoveEmptyString remove empty string for a slice.
func RemoveEmptyString(list []string) []string {
	return funk.FilterString(list, func(x string) bool {
		return x != ""
	})
}

// NewUUID generate a new uuid
func NewUUID() string {
	return string(uuid.NewUUID())
}

//IsValidUUID checks if a string is uuid
// ref: https://stackoverflow.com/questions/25051675/how-to-validate-uuid-v4-in-go
func IsValidUUID(uuid string) bool {
	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")
	return r.MatchString(uuid)
}

// CombineString is a dump string combine function (by : )
func CombineString(items ...string) string {
	str := ""
	for _, item := range items {
		if str == "" {
			str = item
		} else {
			str = fmt.Sprintf("%s:%s", str, item)
		}
	}
	return str
}

// GenerateRandString will return a fixed length random string
func GenerateRandString(n int) string {
	return rand.String(n)
}

func CombineErrors(errs []error) error {
	return errors.NewAggregate(errs)
}

func MatchRegex(pattern, target string) bool {
	m, _ := regexp.MatchString(pattern, target)
	return m
}

// BackoffLimitRetry is a simple wrapper for third-party retry method
func BackoffLimitRetry(action retry.Action, limit uint, factor time.Duration) error {
	return retry.Retry(
		action,
		strategy.Backoff(backoff.Linear(factor)),
		strategy.Limit(limit),
	)
}

// IsServerError will determine whether the error is a server error
func IsServerError(err error) bool {
	serverErrList := []metav1.StatusReason{
		//metav1.StatusReasonUnknown,
		metav1.StatusReasonServerTimeout,
		metav1.StatusReasonTimeout,
		metav1.StatusReasonInternalError,
		metav1.StatusReasonServiceUnavailable,
	}
	reason := k8sErrors.ReasonForError(sysErrors.Cause(err))
	return funk.Contains(serverErrList, reason)
}

// BackoffLimitRetryWhenServerErr is a retry method,it will retry when k8sErrors is not 4xx,
// If k8sError is 5xx,it will execute one time
func BackoffLimitRetryWhenServerErr(action retry.Action, limit uint, factor time.Duration) error {
	err := BackoffLimitRetry(action, 0, factor)
	if limit == 0 || !IsServerError(err) {
		return err
	}
	return BackoffLimitRetry(action, limit-1, factor)
}

func JSONTag(field reflect.StructField) (string, bool) {
	structTag := field.Tag.Get("json")
	if len(structTag) == 0 {
		return "", false
	}
	parts := strings.Split(structTag, ",")
	tag := parts[0]
	if tag == "-" {
		tag = ""
	}
	omitempty := false
	parts = parts[1:]
	for _, part := range parts {
		if part == "omitempty" {
			omitempty = true
			break
		}
	}
	return tag, omitempty
}
