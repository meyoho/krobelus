[TOC]

# Application History



## Goals

* 多版本
* 整体回滚



## 整体思路

以CRD的方式存储各个版本的Application



## CRD

```yaml
apiVersion: app.k8s.io/v1beta1
kind: ApplicationHistory
metadata:
  name: aaa-1
  namespace: default
  labels:
    app.alauda.io/name: <app-name>
  ownerReferences:
  - apiVersion: app.k8s.io/v1beta1
    controller: true
    blockOwnerDeletion: true
    kind: Application
    name: aaa-1
    uid: d9607e19-f88f-11e6-a518-42010a800195
spec:
  revision: 1
  yaml: "<yaml-data>"
  creationTimestamp: 2019-01-03T09:20:52Z
  user: tom
  // auto
  change-cause: ""

```

* labels: 可用于list history
* ownerReferences: 用于删除application自动删除application histroy
* spec
  * yaml: 保存此版本的完整信息
  * creationTimestamp: 版本时间（用户提交）
  * user: 用户名,操作人



## Change Cause

展示出某一个版本产生的原因，比如更新了镜像版本之类的，改动：

* 前端更新改为patch
* 后端对比



目前后端只能对比出资源的 增/ 删/ 改?, 暂时可用



## History Limit

存储于Application的annotation中，默认为10

Key: `app.alauda.io/history-limit`

更新应用时检查并且清理旧版本



最大值: 100?



## API

### List history versions

GET ..../applicationhistorys?labelSelector=app.alauda.io/name=<app-name>



```json
{
    "revisions": [
        {
            "revision": 1,
            "change-cause": "add resource ",
            "user": "tom",
            "creationTimestamp": "2019-01-03T09:20:52Z"
        }
    ]
}
```



### Rollback

POST ../applications/<app-name>/rollback

```json
{
    "revision": 2
}
```



Response: 201

Rollback不产生新的版本





### GET DETAIL

TODO:





## 其他

* start/stop/等不产生revision