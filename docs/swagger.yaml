basePath: '{{.BasePath}}'
definitions:
  common.KrobelusError:
    properties:
      code:
        type: string
      fields:
        items:
          type: object
        type: array
      message:
        type: string
      source:
        example: 1044
        type: integer
    type: object
  common.KrobelusErrors:
    properties:
      errors:
        items:
          $ref: '#/definitions/common.KrobelusError'
        type: array
    type: object
  model.ApplicationKubernetesObjectList:
    properties:
      kubernetes:
        items:
          type: object
        type: array
    type: object
  model.ApplicationRequest:
    properties:
      crd:
        $ref: '#/definitions/model.KubernetesObject'
        type: object
      kubernetes:
        items:
          $ref: '#/definitions/model.KubernetesObject'
        type: array
      resource:
        $ref: '#/definitions/model.ApplicationResource'
        type: object
    type: object
  model.ApplicationResource:
    properties:
      category:
        type: string
      changeCause:
        type: string
      createMethod:
        type: string
      description:
        type: string
      displayName:
        description: also allows in update request
        example: App
        type: string
      name:
        example: app
        type: string
      namespace:
        example: default
        type: string
      owners:
        items:
          $ref: '#/definitions/model.Owner'
        type: array
      source:
        description: optional and for compatibility
        type: string
      user:
        type: string
    type: object
  model.KubernetesObject:
    properties:
      apiVersion:
        example: <resource_kind>
        type: string
      kind:
        example: <resource_api_version>
        type: string
    type: object
  model.KubernetesObjectList:
    properties:
      apiVersion:
        type: string
      items:
        items:
          $ref: '#/definitions/model.KubernetesObject'
        type: array
      kind:
        type: string
    type: object
  model.Owner:
    properties:
      employee_id:
        example: "12345678"
        type: string
      name:
        example: Tom
        type: string
      phone:
        example: "9527"
        type: string
    type: object
host: '{{.Host}}'
info:
  contact: {}
  description: '{{.Description}}'
  license: {}
  title: '{{.Title}}'
  version: '{{.Version}}'
paths:
  /v1/clusters/{cluster}/{type}:
    post:
      description: Create/Update/Delete/Patch a Resource
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: resource type
        in: path
        name: type
        required: true
        type: string
      - description: '{namespace/}name'
        in: path
        name: name
        required: true
        type: string
      responses:
        "201":
          description: Created
          schema:
            $ref: '#/definitions/model.KubernetesObject'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Process a resource
  /v1/clusters/{cluster}/{type}/{namespace}:
    get:
      description: List a resource, cluster scope or namespaced scope
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: resource type
        in: path
        name: type
        required: true
        type: string
      - description: namespace, end with /
        in: path
        name: namespace
        type: string
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.KubernetesObject'
            type: array
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: List a resource
  /v2/clusters/{cluster}/applicationhistories/{namespace}:
    get:
      description: List application histories
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: app selector
        in: query
        name: labelSelector
        type: string
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.KubernetesObjectList'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: List application histories
  /v2/clusters/{cluster}/applicationhistories/{namespace}/{name}:
    get:
      description: Get application history detail
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application history's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application history's name
        in: path
        name: name
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.KubernetesObject'
            type: object
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Get an application history
  /v2/clusters/{cluster}/applications:
    post:
      consumes:
      - application/json
      description: Create applications
      parameters:
      - description: application create request body
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.ApplicationRequest'
          type: object
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.KubernetesObject'
            type: object
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Create applications
  /v2/clusters/{cluster}/applications/{namespace}:
    get:
      description: List application in a single namespace or all namespaces
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        type: string
      - description: search application by name
        in: query
        name: name
        type: string
      - description: page num
        in: query
        name: page
        type: string
      - description: page size
        in: query
        name: page_size
        type: string
      - description: only return application crd
        in: query
        name: simple
        type: string
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.ApplicationKubernetesObjectList'
            type: array
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: List applications
  /v2/clusters/{cluster}/applications/{namespace}/{name}:
    delete:
      description: Delete an application and it's sub resources, include rs/pods
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application's name
        in: path
        name: name
        required: true
        type: string
      responses:
        "204": {}
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Delete an application
    get:
      description: Get application and it's sub resources
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application's name
        in: path
        name: name
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.KubernetesObject'
            type: array
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Get an application
    put:
      consumes:
      - application/json
      description: |-
        Update application with new resources
        1. input can contains application crd
      parameters:
      - description: application update request body
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.ApplicationRequest'
          type: object
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application's name
        in: path
        name: name
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.KubernetesObject'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Update application
  /v2/clusters/{cluster}/applications/{namespace}/{name}/pods:
    get:
      description: Get pods managed by application with status rewrited
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application's name
        in: path
        name: name
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.KubernetesObject'
            type: array
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Get pods managed by application
  /v2/clusters/{cluster}/applications/{namespace}/{name}/rollback:
    post:
      description: Rollback an application to a revision
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application's name
        in: path
        name: name
        required: true
        type: string
      responses:
        "201": {}
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Rollback an application
  /v2/clusters/{cluster}/applications/{namespace}/{name}/yaml:
    get:
      description: Get applications yaml
      parameters:
      - description: cluster uuid
        in: path
        name: cluster
        required: true
        type: string
      - description: application's namespace
        in: path
        name: namespace
        required: true
        type: string
      - description: application's name
        in: path
        name: name
        required: true
        type: string
      produces:
      - text/plain
      responses:
        "200":
          description: yaml data of kubernetes resource list
          schema:
            items:
              type: string
            type: array
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/common.KrobelusErrors'
            type: object
      summary: Get applications yaml
swagger: "2.0"
