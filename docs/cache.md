# Cache



# key prefix

`krobelus`



# Service list cache

`krobelus:service_instances_count:<service_id>:<map>`


# service instances cache
`krobelus:service_instances:<service_id>:<map>`

# region watcher cache

Key: `krobelus:watcher:regions`

Value: Set(region_ids)



# resource cache

`krobelus:resource:pod/deployment`


# redis queue

`krobelus:queue:task`
`krobelus:queue:group`


# region info cache
`krobelus:region:<region_id>:<region_info>`


# api discovery cache
`krobelus:k8s:<cluster_uuid>:api:discovery:groups`
`krobelus:k8s:<cluster_uuid>:api:discovery:resources:<group_version>`



