// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag at
// 2019-04-15 14:36:05.834185 +0800 CST m=+0.300199876

package docs

import (
	"bytes"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "swagger": "2.0",
    "info": {
        "description": "{{.Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "license": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/v1/clusters/{cluster}/{type}": {
            "post": {
                "description": "Create/Update/Delete/Patch a Resource",
                "summary": "Process a resource",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "resource type",
                        "name": "type",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "{namespace/}name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Created",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.KubernetesObject"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v1/clusters/{cluster}/{type}/{namespace}": {
            "get": {
                "description": "List a resource, cluster scope or namespaced scope",
                "summary": "List a resource",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "resource type",
                        "name": "type",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "namespace, end with /",
                        "name": "namespace",
                        "in": "path"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/model.KubernetesObject"
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applicationhistories/{namespace}": {
            "get": {
                "description": "List application histories",
                "summary": "List application histories",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "app selector",
                        "name": "labelSelector",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.KubernetesObjectList"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applicationhistories/{namespace}/{name}": {
            "get": {
                "description": "Get application history detail",
                "produces": [
                    "application/json"
                ],
                "summary": "Get an application history",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application history's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application history's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.KubernetesObject"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applications": {
            "post": {
                "description": "Create applications",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "Create applications",
                "parameters": [
                    {
                        "description": "application create request body",
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.ApplicationRequest"
                        }
                    },
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.KubernetesObject"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applications/{namespace}": {
            "get": {
                "description": "List application in a single namespace or all namespaces",
                "summary": "List applications",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path"
                    },
                    {
                        "type": "string",
                        "description": "search application by name",
                        "name": "name",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "page num",
                        "name": "page",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "page size",
                        "name": "page_size",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "only return application crd",
                        "name": "simple",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/model.ApplicationKubernetesObjectList"
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applications/{namespace}/{name}": {
            "get": {
                "description": "Get application and it's sub resources",
                "produces": [
                    "application/json"
                ],
                "summary": "Get an application",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/model.KubernetesObject"
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            },
            "put": {
                "description": "Update application with new resources\n1. input can contains application crd",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "Update application",
                "parameters": [
                    {
                        "description": "application update request body",
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.ApplicationRequest"
                        }
                    },
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/model.KubernetesObject"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            },
            "delete": {
                "description": "Delete an application and it's sub resources, include rs/pods",
                "summary": "Delete an application",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "204": {},
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applications/{namespace}/{name}/pods": {
            "get": {
                "description": "Get pods managed by application with status rewrited",
                "produces": [
                    "application/json"
                ],
                "summary": "Get pods managed by application",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/model.KubernetesObject"
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applications/{namespace}/{name}/rollback": {
            "post": {
                "description": "Rollback an application to a revision",
                "summary": "Rollback an application",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "201": {},
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        },
        "/v2/clusters/{cluster}/applications/{namespace}/{name}/yaml": {
            "get": {
                "description": "Get applications yaml",
                "produces": [
                    "text/plain"
                ],
                "summary": "Get applications yaml",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cluster uuid",
                        "name": "cluster",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's namespace",
                        "name": "namespace",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "application's name",
                        "name": "name",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "yaml data of kubernetes resource list",
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/common.KrobelusErrors"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "common.KrobelusError": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "string"
                },
                "fields": {
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                },
                "message": {
                    "type": "string"
                },
                "source": {
                    "type": "integer",
                    "example": 1044
                }
            }
        },
        "common.KrobelusErrors": {
            "type": "object",
            "properties": {
                "errors": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/common.KrobelusError"
                    }
                }
            }
        },
        "model.ApplicationKubernetesObjectList": {
            "type": "object",
            "properties": {
                "kubernetes": {
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                }
            }
        },
        "model.ApplicationRequest": {
            "type": "object",
            "properties": {
                "crd": {
                    "type": "object",
                    "$ref": "#/definitions/model.KubernetesObject"
                },
                "kubernetes": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/model.KubernetesObject"
                    }
                },
                "resource": {
                    "type": "object",
                    "$ref": "#/definitions/model.ApplicationResource"
                }
            }
        },
        "model.ApplicationResource": {
            "type": "object",
            "properties": {
                "category": {
                    "type": "string"
                },
                "changeCause": {
                    "type": "string"
                },
                "createMethod": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                },
                "displayName": {
                    "description": "also allows in update request",
                    "type": "string",
                    "example": "App"
                },
                "name": {
                    "type": "string",
                    "example": "app"
                },
                "namespace": {
                    "type": "string",
                    "example": "default"
                },
                "owners": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/model.Owner"
                    }
                },
                "source": {
                    "description": "optional and for compatibility",
                    "type": "string"
                },
                "user": {
                    "type": "string"
                }
            }
        },
        "model.KubernetesObject": {
            "type": "object",
            "properties": {
                "apiVersion": {
                    "type": "string",
                    "example": "\u003cresource_kind\u003e"
                },
                "kind": {
                    "type": "string",
                    "example": "\u003cresource_api_version\u003e"
                }
            }
        },
        "model.KubernetesObjectList": {
            "type": "object",
            "properties": {
                "apiVersion": {
                    "type": "string"
                },
                "items": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/model.KubernetesObject"
                    }
                },
                "kind": {
                    "type": "string"
                }
            }
        },
        "model.Owner": {
            "type": "object",
            "properties": {
                "employee_id": {
                    "type": "string",
                    "example": "12345678"
                },
                "name": {
                    "type": "string",
                    "example": "Tom"
                },
                "phone": {
                    "type": "string",
                    "example": "9527"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo swaggerInfo

type s struct{}

func (s *s) ReadDoc() string {
	t, err := template.New("swagger_info").Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, SwaggerInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
