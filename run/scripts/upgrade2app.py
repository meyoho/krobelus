#!/usr/bin/env python
import argparse
from subprocess import Popen, PIPE, STDOUT
import json

APP_TEMPLATE = """
apiVersion: app.k8s.io/v1beta1
kind: Application
metadata:
  annotations:
    app.{domain}/create-method: ""
  name: {name}
  namespace: {namespace}
spec:
  assemblyPhase: Succeeded
  componentKinds:
  - group: {group}
    kind: {kind}
  selector:
    matchLabels:
      app.{domain}/name: {name}.{namespace}
"""

GROUPS = {
    'Deployment': 'extensions',
    'StatefulSet': 'apps',
    'DaemonSet': 'extensions',
}

BASE_DOMAIN = 'alauda.io'

def upgrade_to_app(namespace, kind, name=None, base_domain=BASE_DOMAIN):
    if not base_domain:
        base_domain = BASE_DOMAIN
    app_label = 'app.%s/name' % base_domain
    resource_list = []
    if not name:
        p = Popen(['kubectl', 'get', str.lower(kind), '-n', namespace, '-o', 'json'],
                  stdout=PIPE, stdin=PIPE, stderr=STDOUT)
        out = p.communicate()[0]
        resource_list.extend(json.loads(out)['items'])
    else:
        p = Popen(['kubectl', 'get', str.lower(kind), name, '-n', namespace, '-o', 'json'],
                  stdout=PIPE, stdin=PIPE, stderr=STDOUT)
        out = p.communicate()[0]
        resource_list.append(json.loads(out))

    for r in resource_list:
        app_name = r.get('metadata', {}).get('name', '')
        if app_name:
            app_yaml = APP_TEMPLATE.format(
                name=app_name, namespace=namespace,
                group=GROUPS.get(kind, ''), kind=kind,
                domain=base_domain)
            p = Popen(['kubectl', 'create', '-f', '-'],
                      stdout=PIPE, stdin=PIPE, stderr=STDOUT)
            out = p.communicate(input=app_yaml)[0]
            print(out.decode())
            if p.returncode != 0:
                continue

            selector_val = '%s.%s' % (app_name, namespace)
            r['metadata']['labels'][app_label] = selector_val
            r['spec']['template']['metadata']['labels'][app_label] = selector_val
            p = Popen(['kubectl', 'apply', '-f', '-'],
                      stdout=PIPE, stdin=PIPE, stderr=STDOUT)
            out = p.communicate(input=json.dumps(r))[0]
            print(out.decode())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Upgrade workloads to application')
    parser.add_argument('namespace', type=str,
                        help='namespace of the workload resource')
    parser.add_argument('kind', type=str,
                        help='kind of the resource to upgrade, currently support: Deployment, StatefulSet, DaemonSet')
    parser.add_argument('-n', '--name', type=str, required=False,
                        help='name of the resource, optional')
    parser.add_argument('-d', '--domain', type=str, required=False,
                        help='base domain of the application label')

    args = parser.parse_args()
    upgrade_to_app(args.namespace, args.kind, args.name, base_domain=args.domain)
