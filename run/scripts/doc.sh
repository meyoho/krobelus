#!/usr/bin/env bash
export REDIS_HOST_WRITER=127.0.0.1
export REDIS_HOST_READER=127.0.0.1
export REDIS_PORT_READER=6379
export REDIS_PORT_WRITER=6379
swag init
make bin

./krobelus api