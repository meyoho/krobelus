#!/bin/bash

packs="
        krobelus/api/handler
        krobelus/api/schema
        krobelus/api/transition
        krobelus/api/validation
        krobelus/cmd
        krobelus/common
        krobelus/config
        krobelus/db
        krobelus/infra
        krobelus/infra/kubernetes
        krobelus/kubernetes
        krobelus/model
        krobelus/store
        krobelus/watcher
        krobelus/worker/apps
        krobelus/worker/factory
        krobelus/worker/monitor
        krobelus/worker/services
     "

if [ "$1" == "" ]; then
    echo "---------- run test"
	go test -v -race --cover ${packs}
elif [ "$1" == "up" ]; then
    echo "---------- build images"
    docker-compose -p kro-psql-test -f run/docker/psql-compose-test.yaml build
    echo "---------- start up containers"
    docker-compose -p kro-psql-test -f run/docker/psql-compose-test.yaml up -d
    echo "---------- please run unit tests in krobelus containers"
    docker exec -it kropsqltest_krobelus-api_1 make test 
else
    echo "---------- please use to make test with an argument"
    echo "---------- make test up (to setup test environment)"
    echo "---------- make test (to run unit test in container)"
fi
