#!/usr/bin/env bash
which revive || go get -u github.com/mgechev/revive

base=468

now=$(revive -config run/ci/revive.toml -exclude vendor/... -exclude pkg/... -exclude tests/... -formatter friendly ./... | tail -5 | grep "exported" | awk '{print $1; exit}')

if [ "$now" -gt "$base" ]; then
    echo "Exported method/struct ... should have comment or be unexported" && exit 1
fi

echo "Comment check ok"
