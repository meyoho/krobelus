# coding=utf-8
# !/usr/bin/env python

# This script is intent to to migrate old svc/app in ACE 1.x versions to 2.2 version
# In the details, it does the following things:
#   1. read exist resources
#   2. group them by svc/app uuid label
#   3. create Application crd based on the resources groups

# usage:
# 1. install packages
#    > pip install requests click glom
# 2. python app.py


import click
import json
import requests
from glom import glom

APP_TEMPLATE = """{
    "apiVersion": "app.k8s.io/v1beta1",
    "kind": "Application",
    "metadata": {
        "name": "{}",
        "namespace": "{}"
    },
    "spec": {
        "assemblyPhase": "Succeeded",
        "componentKinds": [
        ],
        "descriptor": {},
        "selector": {
            "matchLabels": {
                "{}": "{}"
            }
        }
    }
}"""

RESOURCE_BLACKLIST = ["endpoints", "pods", "replicasets"]


class Apps(object):

    def __init__(self, host, port, domain):
        self.host = host
        self.port = port
        self.domain = domain
        self.resources = []
        self.versions = None
        self.api_resources = None
        self.all = {}

        self.discovery()

    def get_app_uuid(self, item):
        key = "app.{}/uuid".format(self.domain)
        return item.get("metadata", {}).get("labels", {}).get(key, "")

    def get_svc_uuid(self, item):
        key = "service.{}/uuid".format(self.domain)
        return item.get("metadata", {}).get("labels", {}).get(key, "")

    def get_kind_for_resource(self, resource):
        for api_resource in self.api_resources:
            if glom(api_resource, "name") == resource:
                return glom(api_resource, "kind")
        raise Exception("cannot find kind for {}".format(resource))

    def get_group_version_for_resource(self, resource):
        for api_resource in self.api_resources:
            if glom(api_resource, "name") == resource:
                return glom(api_resource, "groupVersion")
        return "v1"

    def discovery(self):
        path = "http://{}:{}/apis".format(self.host, self.port)
        versions = requests.get(path).json().get("groups", [])
        self.versions = versions

        api_resources = []
        resource_list = []
        for version in versions:
            group_version = glom(version, "preferredVersion.groupVersion", default="")
            if group_version:
                version_path = "{}/{}".format(path, group_version)
                result = requests.get(version_path).json()
                resources = glom(result, "resources", default=[])
                for resource in resources:
                    resource["groupVersion"] = glom(result, "groupVersion", default="")
                    api_resources.append(resource)

                    name = resource.get("name", "")
                    if "/" not in name and name not in resource_list and name not in RESOURCE_BLACKLIST:
                        resource_list.append(name)

        version_path = "http://{}:{}/api/v1".format(self.host, self.port)
        result = requests.get(version_path).json()
        resources = glom(result, "resources", default=[])
        for resource in resources:
            resource["groupVersion"] = glom(result, "groupVersion", default="")
            api_resources.append(resource)
            name = resource.get("name", "")
            if "/" not in name and name not in resource_list and name not in RESOURCE_BLACKLIST:
                resource_list.append(name)

        self.api_resources = api_resources
        self.resources = resource_list
        print "discovery done, found {} resources".format(len(self.resources))

    def get_all(self):
        base = "http://{}:{}".format(self.host, self.port)
        for resource in self.resources:
            print "-- get {}".format(resource)
            version = self.get_group_version_for_resource(resource)
            prefix = "api" if version == "v1" else "apis"
            data = requests.get("{}/{}/{}/{}".format(base, prefix, version, resource)).json()
            kind = self.get_kind_for_resource(resource)
            for item in glom(data, "items", default=[]):
                if not item.get("kind", ""):
                    item["kind"] = kind
                if not item.get("apiVersion", ""):
                    item["apiVersion"] = version
                uuid = self.get_app_uuid(item)
                if uuid:
                    print "-- -- found resource for app: {}".format(uuid)
                    if uuid in self.all:
                        self.all[uuid].append(item)
                    else:
                        self.all[uuid] = [item]
                else:
                    uuid = self.get_svc_uuid(item)
                    if uuid:
                        print "-- -- found resource for service: {}".format(uuid)
                        uuid = "svc." + uuid
                        if uuid in self.all:
                            self.all[uuid].append(item)
                        else:
                            self.all[uuid] = [item]

    def get_ns_from_resources(self, resources):
        for item in resources:
            ns = glom(item, "metadata.namespace", default="")
            if ns:
                return ns
        return ""

    def get_name_from_resources(self, resources):
        app_name = ""
        for item in resources:
            name = glom(item, "metadata.name", default="")
            kind = glom(item, "kind", default="")
            # If have service, use it's labels to use as name
            if kind == "Service":
                labels = glom(item, "metadata.labels", default={})
                k = 'app.{}/name'.format(self.domain)
                if k in labels:
                    return labels[k]
            if kind in ["Deployment", "DaemonSet", "StatefulSet"]:
                if not app_name:
                    app_name = name
                else:
                    app_name = "{}.{}".format(app_name, name)
        return app_name

    def get_cks(self, resources):
        cks = []
        kinds = []
        for item in resources:
            gv = glom(item, "apiVersion", default="")
            # print "fuck: apiVersion: {}".format(gv)
            splits = gv.split("/")
            # print "fuck: splits: {}".format(splits)
            group = ""
            if len(splits) == 2:
                group = splits[0]

            kind = glom(item, "kind", default="")
            if kind not in kinds:
                cks.append({
                    "group": group,
                    "kind": kind,
                })
                kinds.append(kind)
        return cks

    def create_all(self):
        for k in self.all:
            print "processing app: {}".format(k)
            data = self.all[k]

            ns = self.get_ns_from_resources(data)
            if not ns:
                print "- [ERROR]: cannot get ns from resources: {}".format(data)
                continue
            name = self.get_name_from_resources(data)
            if not name:
                print " - [ERROR]: cannot generate app name from resources, namespace: {}, skip".format(ns)
                continue
            print " - find namespace: {}".format(ns)
            print " - gen app name: {}".format(name)

            uid = k
            match_labels = {
                "app.{}/uuid".format(self.domain): k
            }
            if k.startswith('svc.'):
                uid = k[4:]
                match_labels = {
                    "service.{}/uuid".format(self.domain): uid
                }

            # update service labels
            for item in data:
                kind = glom(item, "kind", default="")
                if kind != "Service":
                    continue
                selector = glom(item, "spec.selector".format(self.domain), default={})
                key = "service.{}/name".format(self.domain)
                if not selector or key not in selector:
                    print "- [SVC] Service selector format error, skip"
                    continue
                selector = selector[key]
                new = None
                for d in data:
                    if glom(d, "kind", default="") in ["Deployment", "DaemonSet", "StatefulSet"]:
                        if glom(d, "metadata.name", default="") == selector:
                            new = "{}-{}".format(glom(d, "kind", default="").lower(), selector)

                print " - [SVC] update service labels"
                body = {
                    "spec": {
                        "selector": {
                            "service.{}/name".format(self.domain): new
                        }
                    }
                }
                name = glom(item, "metadata.name", default="")
                headers = {'Content-type': 'application/strategic-merge-patch+json', 'Accept': 'application/json'}
                path = "http://{}:{}/api/v1/namespaces/{}/services/{}".format(self.host, self.port, ns, name)
                resp = requests.patch(path, json=body, headers=headers)
                if resp.status_code >= 400:
                    raise Exception(resp.text)
                else:
                    print " - update service labels done"

            # update deployment labels
            for item in data:
                kind = glom(item, "kind", default="")
                if kind not in ["Deployment", "DaemonSet", "StatefulSet"]:
                    continue
                item_name = glom(item, "metadata.name", default="")
                print " - [DEPLOY] update deploy labels"
                body = {
                    "spec":
                        {
                            "selector": {
                                "matchLabels": {
                                    "app.{}/name".format(self.domain): "{}.{}".format(name, ns),
                                    "service.{}/name".format(self.domain): "{}-{}".format(kind.lower(), item_name)
                                }

                            },
                            "template": {
                                "metadata": {
                                    "labels": {
                                        "app.{}/name".format(self.domain): "{}.{}".format(name, ns),
                                        "service.{}/name".format(self.domain): "{}-{}".format(kind.lower(),
                                                                                              item_name)
                                    }
                                }
                            }
                        }
                }
                path = "http://{}:{}/apis/extensions/v1beta1/namespaces/{}/{}/{}".format(self.host, self.port, ns,
                                                                                         kind.lower() + "s",
                                                                                         item_name)
                headers = {'Content-type': 'application/strategic-merge-patch+json', 'Accept': 'application/json'}

                resp = requests.patch(path, json=body, headers=headers)
                if resp.status_code >= 400:
                    raise Exception(resp.text)
                else:
                    print " - update deploy labels done"

            app_data = json.loads(APP_TEMPLATE)
            app_data["metadata"]["name"] = name
            app_data["metadata"]["namespace"] = ns
            app_data["spec"]["componentKinds"] = self.get_cks(data)
            app_data["spec"]["selector"]["matchLabels"] = match_labels
            app_data['metadata']['labels'] = {
                'app.{}/uuid'.format(self.domain): uid
            }

            path = "http://{}:{}/apis/app.k8s.io/v1beta1/namespaces/{}/applications".format(self.host, self.port, ns)
            resp = requests.post(path, json=app_data)
            if resp.status_code >= 400:
                if resp.status_code == 409:
                    print " - [SKIP] application {}/{} already exist, skip create".format(ns, name)
                    continue
                else:
                    raise Exception(resp.text)
            else:
                print " - create application {}/{} done".format(ns, name)


@click.command()
@click.option('--host', help='DB host', default="127.0.0.1")
@click.option('--port', help='DB port', default="8080")
@click.option('--domain', help='customer domain name', default='alauda.io')
def new_apps(host, port, domain):
    """Generate new app from workloads"""
    app = Apps(host=host, port=port, domain=domain)
    app.get_all()
    app.create_all()


if __name__ == '__main__':
    new_apps()
