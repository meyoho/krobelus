#!/bin/bash


cd $KROBELUS

echo "---------- migrating db"
chmod +x $GOPATH/src/krobelus/run/scripts/migrate.sh
$GOPATH/src/krobelus/run/scripts/migrate.sh

go test -v krobelus/api/handler

krobelus api