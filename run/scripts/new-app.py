# coding=utf-8


# !/usr/bin/env python

# usage:
# 1. install packages pip install requests click glom


import requests
import click
from glom import glom
import json

APP_TEMPLATE = """{
    "apiVersion": "app.k8s.io/v1beta1",
    "kind": "Application",
    "metadata": {
        "name": "{}",
        "namespace": "{}"
    },
    "spec": {
        "assemblyPhase": "Succeeded",
        "componentKinds": [
        ],
        "descriptor": {},
        "selector": {
            "matchLabels": {
                "{}": "{}"
            }
        }
    }
}"""


class Apps(object):

    def __init__(self, host, port, domain):
        self.host = host
        self.port = port
        self.domain = domain
        self.resources = []
        self.versions = None
        self.api_resources = None
        self.all = {}

        self.discovery()

    def get_app_uuid(self, item):
        key = "app.{}/uuid".format(self.domain)
        return item.get("metadata", {}).get("labels", {}).get(key, "")

    def get_kind_for_resource(self, resource):
        for api_resource in self.api_resources:
            if glom(api_resource, "name") == resource:
                return glom(api_resource, "kind")
        raise Exception("cannot find kind for {}".format(resource))

    def get_group_version_for_resource(self, resource):
        for api_resource in self.api_resources:
            if glom(api_resource, "name") == resource:
                return glom(api_resource, "groupVersion")
        return "v1"

    def discovery(self):
        path = "http://{}:{}/apis".format(self.host, self.port)
        versions = requests.get(path).json().get("groups", [])
        self.versions = versions

        api_resources = []
        resource_list = []
        for version in versions:
            group_version = glom(version, "preferredVersion.groupVersion", default="")
            if group_version:
                version_path = "{}/{}".format(path, group_version)
                result = requests.get(version_path).json()
                resources = glom(result, "resources", default=[])
                for resource in resources:
                    resource["groupVersion"] = glom(result, "groupVersion", default="")
                    api_resources.append(resource)

                    name = resource.get("name", "")
                    if "/" not in name and name not in resource_list:
                        resource_list.append(name)

        version_path = "http://{}:{}/api/v1".format(self.host, self.port)
        result = requests.get(version_path).json()
        resources = glom(result, "resources", default=[])
        for resource in resources:
            resource["groupVersion"] = glom(result, "groupVersion", default="")
            api_resources.append(resource)
            name = resource.get("name", "")
            if "/" not in name and name not in resource_list:
                resource_list.append(name)

        self.api_resources = api_resources
        self.resources = resource_list
        print "discovery done, found {} resources".format(len(self.resources))

    def get_all(self):
        base = "http://{}:{}".format(self.host, self.port)
        for resource in self.resources:
            version = self.get_group_version_for_resource(resource)
            prefix = "api" if version == "v1" else "apis"
            data = requests.get("{}/{}/{}/{}".format(base, prefix, version, resource)).json()
            kind = self.get_kind_for_resource(resource)
            for item in glom(data, "items", default=[]):
                if not item.get("kind", ""):
                    item["kind"] = kind
                if not item.get("apiVersion", ""):
                    item["apiVersion"] = version
                uuid = self.get_app_uuid(item)
                if uuid:
                    if uuid in self.all:
                        self.all[uuid].append(item)
                    else:
                        self.all[uuid] = [item]

    def get_ns_from_resources(self, resources):
        for item in resources:
            ns = glom(item, "metadata.namespace", default="")
            if ns:
                return ns
        return ""

    def get_name_from_resources(self, resources):
        app_name = ""
        for item in resources:
            name = glom(item, "metadata.name", default="")
            kind = glom(item, "kind", default="")
            if kind in ["Deployment", "DaemonSet", "StatefulSet"]:
                if not app_name:
                    app_name = name
                else:
                    app_name = "{}.{}".format(app_name, name)
        return app_name

    def get_cks(self, resources):
        cks = []
        for item in resources:
            gv = glom(item, "apiVersion", default="")
            # print "fuck: apiVersion: {}".format(gv)
            splits = gv.split("/")
            # print "fuck: splits: {}".format(splits)
            group = ""
            if len(splits) == 2:
                group = splits[0]

            kind = glom(item, "kind", default="")

            cks.append({
                "group": group,
                "kind": kind,
            })
        return cks

    def create_all(self):
        for k in self.all:
            print "processing app: {}".format(k)
            data = self.all[k]

            ns = self.get_ns_from_resources(data)
            if not ns:
                print "- [ERROR]: cannot get ns from resources: {}".format(data)
                continue
            name = self.get_name_from_resources(data)
            if not name:
                print " - [ERROR]: cannot generate app name from resources, namespace: {}, skip".format(ns)
                continue
            print " - find namespace: {}".format(ns)
            print " - gen app name: {}".format(name)

            match_labels = {
                "app.{}/uuid".format(self.domain): k
            }

            app_data = json.loads(APP_TEMPLATE)
            app_data["metadata"]["name"] = name
            app_data["metadata"]["namespace"] = ns
            app_data["spec"]["componentKinds"] = self.get_cks(data)
            app_data["spec"]["selector"]["matchLabels"] = match_labels

            path = "http://{}:{}/apis/app.k8s.io/v1beta1/namespaces/{}/applications".format(self.host, self.port, ns)
            resp = requests.post(path, json=app_data)
            if resp.status_code >= 400:
                if resp.status_code == 409:
                    print " - [SKIP] application {}/{} already exist, skip create".format(ns, name)
                    continue
                else:
                    raise Exception(resp.text)
            else:
                print " - create application {}/{} done".format(ns, name)


@click.command()
@click.option('--host', help='DB host', default="127.0.0.1")
@click.option('--port', help='DB port', default="8080")
@click.option('--domain', help='customer domain name', default='alauda.io')
def new_apps(host, port, domain):
    """Generate new app from workloads"""
    app = Apps(host=host, port=port, domain=domain)
    app.get_all()
    app.create_all()


if __name__ == '__main__':
    new_apps()
