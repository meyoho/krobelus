#!/usr/bin/env bash
gofmt -l $(find . -type f -name '*.go' -not -path "./vendor/*")