# Usage

```bash
# context:ns=alauda-system
kubectl run redis --image=redis 

kubectl expose deployment redis --port=6379 --target-port=6379

# after create krobelus deployment
kubectl expose deployment krobelus --port=8080 --target-port=8080 -n alauda-system
```