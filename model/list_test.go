package model

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	"krobelus/tests"
)

// ok fails the test if an err is not nil.
func ok(tb testing.TB, err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
		tb.FailNow()
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}

func TestFlatObjectList(t *testing.T) {
	object, _ := BytesToKubernetesObject([]byte(tests.PodData))
	secrets, _ := BytesToKubernetesObject([]byte(tests.SecretListData))

	input := []*KubernetesObject{object, secrets}
	result, err := FlatObjectList(input)
	ok(t, err)
	equals(t, 6, len(result))
	equals(t, "Secret", result[1].Kind)

}
