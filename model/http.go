package model

import (
	"krobelus/common"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/rest"
)

// Query is build from url path param and query params.
// The common format should be :
// 1. /namespaces/<namespace>/<resource_type>/<resource_name>/<sub_resource_type>?cluster_uuid=<cluster_uuid>
// 2. /<resource_type>/<resource_name>?cluster_uud=<cluster_uuid>

type Query struct {
	ClusterUUID  string `json:"cluster_uuid"`
	ClusterToken string `json:"cluster_token"`
	Namespace    string `json:"namespace"`
	Name         string `json:"name"`
	Type         string `json:"type"`

	SubType   []string `json:"sub_type"`
	PatchType string   `json:"patch_type"`
}

func (q *Query) IsSubType() bool {
	return len(q.SubType) != 0
}

func (q *Query) GetTopSubType() string {
	if len(q.SubType) > 0 {
		return q.SubType[0]
	}
	return ""
}

//IsNamespacedList checks if this query is a namespaced resource list
func (q *Query) IsListQuery() bool {
	// fuck....
	return q.Name == ""
}

//IsPodLogsQuery checks if this query is a pods logs query
func (q *Query) IsPodLogsQuery() bool {
	return q.GetTopSubType() == "log" && q.Type == "pods"
}

// GetQueryFromContext get basic query from http context
// only init cluster and token and name, namespace, they apply to many apis
func GetQueryFromContext(c *gin.Context) *Query {
	return &Query{
		ClusterUUID:  c.Param("cluster"),
		ClusterToken: GetBearerToken(c),
		Name:         c.Param("name"),
		Namespace:    c.Param("namespace"),
	}
}

//GetBearerToken get token from gin context(http header)
// Access order:
// 1. old jakiro token (also bearer token but with different header key)
// 2. standard kubernetes token
func GetBearerToken(c *gin.Context) string {
	// the old one
	t := c.GetHeader(common.KubernetesTokenHeader)
	if t == "" {
		// the standard one
		t = c.GetHeader("Authorization")
		l := len("Bearer ")
		if len(t) > l {
			return t[l:]
		}
	}
	return t
}

// GetAppQuery get query for an application request
func GetAppQuery(c *gin.Context) *Query {
	query := GetQueryFromContext(c)
	query.Type = "applications"
	return query
}

func GetAppHistoryQuery(c *gin.Context) *Query {
	query := GetQueryFromContext(c)
	query.Type = "applicationhistories"
	return query
}

// GetCRDQuery get query for an crd request
func GetCRDQuery(c *gin.Context) *Query {
	query := GetQueryFromContext(c)
	query.Type = "customresourcedefinitions"
	return query
}

func GetQuery(cluster, token string, resource string, object v1.Object) *Query {
	query := Query{
		ClusterUUID:  cluster,
		ClusterToken: token,
		Namespace:    object.GetNamespace(),
		Name:         object.GetName(),
		Type:         resource,
	}
	return &query
}

type Result struct {
	Body   []byte
	Object runtime.Object
	Code   int
	Error  error

	Extra map[string]interface{}
	// keep a copy of origin data
	Origin *rest.Result
}

func ParseResult(result *rest.Result) *Result {
	if result == nil {
		return &Result{
			Code: 200,
		}
	}

	raw, _ := result.Raw()
	object, _ := result.Get()
	err := result.Error()
	var code int
	result.StatusCode(&code)
	return &Result{
		Body:   raw,
		Object: object,
		Code:   code,
		Error:  err,
		Origin: result,
	}
}

type PageQuery struct {
	Page     int
	PageSize int
}
