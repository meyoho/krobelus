package model

import (
	"strings"
)

// ParseNameKeyToQuery parse a name key to Query
// format: k8s:name:<cluster_uuid>:<namespace><resource_type>:<name>
func ParseNameKeyToQuery(name string) *Query {
	items := strings.Split(name, ":")
	query := Query{
		ClusterUUID: items[2],
		Namespace:   items[3],
		Type:        items[4],
	}
	if len(items) == 6 {
		query.Name = items[5]
	} else {
		query.Name = strings.Join(items[5:], ":")
	}
	query.Name = strings.Replace(query.Name, "$", ":", -1)
	return &query
}
