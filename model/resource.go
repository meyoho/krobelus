package model

import (
	"sort"
)

var ResourceListKindOrder = []string{
	"Namespace",
	"Secret",
	"ConfigMap",
	"PersistentVolume",
	"ServiceAccount",
	"Service",
	"Pod",
	"ReplicationController",
	"Deployment",
	"DaemonSet",
	"Ingress",
	"Job",
}

func SortKubernetesObjects(objs []*KubernetesObject) {
	sort.Slice(objs, func(i, j int) bool {
		x, y := len(ResourceListKindOrder), len(ResourceListKindOrder)
		for c, k := range ResourceListKindOrder {
			if k == objs[i].GetKind() {
				x = c
			}
			if k == objs[j].GetKind() {
				y = c
			}
		}
		return x < y
	})
}
