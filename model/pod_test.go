package model

import (
	"encoding/json"
	"reflect"
	"testing"

	"krobelus/tests"

	v1 "k8s.io/api/core/v1"
)

func TestGetSimplePodCondition(t *testing.T) {

	bt := tests.LoadFile("../tests/fixtures/pod_condition/pod.json")

	pod := &v1.Pod{}
	if err := json.Unmarshal(bt, pod); err != nil {
		t.Errorf("Error unmarshal json: err=%v", err)
	}

	messages, err := json.Marshal(GetSimplePodCondition(pod))

	if err != nil {
		t.Errorf("Error unmarshal json: err=%v", err)
	}

	excepted := tests.LoadFile("../tests/fixtures/pod_condition/excepted.json")

	reflect.DeepEqual(messages, excepted)

}
