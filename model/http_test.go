package model

import (
	"net/http"
	"testing"

	"krobelus/common"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestGetToken(t *testing.T) {
	h := http.Header{}
	h.Add("Authorization", "Bearer aaabbb")

	c := gin.Context{
		Request: &http.Request{
			Header: h,
		},
	}

	t.Run("valid-bearer", func(t *testing.T) {
		assert.Equal(t, "aaabbb", GetBearerToken(&c))
	})

	t.Run("old-valid", func(t *testing.T) {
		h.Add(common.KubernetesTokenHeader, "ab")
		assert.Equal(t, "ab", GetBearerToken(&c))
	})

	t.Run("invalid", func(t *testing.T) {
		h.Del(common.KubernetesTokenHeader)
		h.Del("Authorization")
		assert.Equal(t, "", GetBearerToken(&c))

	})
}
