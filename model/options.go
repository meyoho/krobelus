package model

import (
	"context"
	"fmt"
	"reflect"

	"krobelus/common"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// This model aims to provide kubernetes like CURD options for krobelus store package.
// It's better than separated function args

// GetOptions used for FetchResource
type GetOptions struct {
	// SkipCache skip redis cache when get resource from kubernetes
	SkipCache bool

	// Params is other params bypass to kubernetes
	Params map[string]string
}

// ListOptions is the options when list resources in store
type ListOptions struct {
	// Context is used for tracing
	Context context.Context

	// Only retrieve metadata (name,namespace...), it's a bool actually true/false
	Meta string

	// only retrieve resource that name exact match this filter
	NameFilters []ObjectNameTuple

	// NameSearch search resource in resource list by this name
	NameSearch string

	Options metaV1.ListOptions

	// PartialMetadata decides if only get metadata of the objects.
	PartialMetadata bool
}

func (l ListOptions) GetNameSearchPattern() string {
	if l.NameSearch == "" {
		return "*"
	}
	return fmt.Sprintf("*%s*", l.NameSearch)
}

// GetListParams gets a params map from 'Options' field.
// The json struct tags of the 'ListOptions' are used to
// generate the map key for every field in 'Options'.
func (l ListOptions) GetListParams() map[string]string {
	params := make(map[string]string)
	sv := reflect.ValueOf(l.Options)
	st := sv.Type()
	for i := 0; i < st.NumField(); i++ {
		field := sv.Field(i)
		tag, omitempty := common.JSONTag(st.Field(i))
		if len(tag) == 0 {
			continue
		}
		if omitempty && reflect.DeepEqual(reflect.Zero(field.Type()).Interface(), field.Interface()) {
			continue
		}
		params[tag] = fmt.Sprintf("%v", field.Interface())
	}
	return params
}
