package model

import (
	"encoding/json"
	"reflect"
	"strings"
	"testing"

	"krobelus/common"
	"krobelus/config"
	"krobelus/tests"

	"github.com/spf13/viper"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestAddApplicationComponentKind(t *testing.T) {
	obj, err := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/application.json"))
	if err != nil {
		panic(err)
	}
	crd := obj.ToAppCrd()
	AddApplicationComponentKind(crd, metav1.GroupKind{
		Group: "",
		Kind:  "ConfigMap",
	})
	if len(crd.Spec.ComponentGroupKinds) != 3 {
		t.Error("app gks error:", crd)
	}
}

func TestGetAppLabelSelector(t *testing.T) {
	obj, err := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/application.json"))
	if err != nil {
		panic(err)
	}
	app := obj.ToAppCrd()
	result := GetAppLabelSelector(app)
	data := strings.Split(result, ",")
	if len(data) != 3 {
		t.Errorf("get app selector error: %s", result)
	}

	app.Spec.Selector.MatchLabels = nil
	if GetAppLabelSelector(app) != "" {
		t.Error("get empty app selector error")
	}

	if GetAppSelectorWithName(app)[common.AppNameKey()] != "hello-world.default" {
		t.Error("get only app name selector error")
	}
}

func TestComputeApplicationStatus(t *testing.T) {

	deployment, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/deployment.json"))
	daemonset, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/daemonset.json"))
	statefulset, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/statefulset.json"))
	application, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/application.json"))

	app := Application{
		Cluster:    ClusterInfo{UUID: "1", Token: "1"},
		Crd:        *application,
		Kubernetes: []*KubernetesObject{deployment, daemonset, statefulset},
		Logger:     common.GetLoggerByID(""),
	}

	info := map[string]string{
		app.GetClusterUUID():  "1",
		app.GetClusterToken(): "1",
		app.GetNamespace():    "default",
		app.GetName():         "hello-world",
		app.GetUUID():         "da971fbe-0f2b-11e9-8705-0a580ac7003c",
		string(app.GetType()): "Application",
	}

	for k, v := range info {
		if k != v {
			t.Errorf("Get App info error")
		}
	}
	resp := app.ComputeApplicationStatus(true)

	byteValue, _ := json.MarshalIndent(*resp, "", "  ")

	status := tests.LoadFile("../tests/fixtures/status/status.json")

	if !reflect.DeepEqual(status, byteValue) {
		t.Errorf("ComputeApplicationStatus error: excepted %s,got %s", status, byteValue)
	}

}

func TestSetApplicationNamesapce(t *testing.T) {
	deployment, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/deployment.json"))
	deployment.SetApplicationNamespace("deploy ststus")
	str := deployment.GetNamespace()
	if str != "deploy ststus" {
		t.Errorf("Getnamesapce error:excepted \"app link\",got %s", str)
	}
}

func TestUpdateApplicationWithInputResource(t *testing.T) {
	viper.Set(config.KeyLabelBaseDomain, "alauda.io")
	name1 := "alauda"
	name2 := "aaa"
	//var info string
	Res1 := ApplicationResource{
		Name:         "hello-world",
		Namespace:    "default",
		Description:  "request",
		Category:     "object",
		Source:       "create",
		DisplayName:  &name1,
		CreateMethod: "post",
	}
	Res2 := &ApplicationResource{
		DisplayName: &name2,
		Owners: []Owner{
			{
				Name:       "Lee",
				Phone:      "1234567",
				EmployeeID: "2347",
			},
			{
				Name:       "Zhang",
				Phone:      "7654321",
				EmployeeID: "5757",
			},
		},
	}
	info, _ := json.Marshal(Res2.Owners)
	Annos := map[string]string{
		"app.alauda.io/create-method": "post",
		"app.alauda.io/display-name":  "aaa",
		"app.alauda.io/source":        "create",
		"owners.alauda.io/info":       string(info),
	}

	ks := Res1.ToKubernetesObject()
	app, err := ks.ToApplication()

	if err != nil {
		t.Errorf("ToApplication error")
	}

	status := map[string]string{
		app.Name:      "hello-world",
		app.Namespace: "default",
		app.Annotations["app.alauda.io/display-name"]:  "alauda",
		app.Annotations["app.alauda.io/create-method"]: "post",
		app.Annotations["app.alauda.io/source"]:        "create",
	}

	for k, v := range status {
		if k != v {
			t.Errorf("ToKubernetesObject error")
		}
	}

	UpdateApplicationWithInputResource(app, Res2)

	ano := app.GetAnnotations()
	if !equal(ano, Annos) {
		t.Errorf("UpdateApplicationWithInputResource error: excepted %s,got %s", Annos, ano)
	}
}

func equal(x, y map[string]string) bool {
	if len(x) != len(y) {
		return false
	}
	for k, xv := range x {
		if yv, ok := y[k]; !ok || yv != xv {
			return false
		}
	}
	return true
}

func TestMerageWithInputAppCrd(t *testing.T) {
	viper.Set(config.KeyLabelBaseDomain, "alauda.io")
	application, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/application.json"))
	deployment, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/deployment.json"))
	app, _ := application.ToApplication()
	gen, _ := deployment.ToApplication()

	MergeWithInputAppCrd(app, gen)
	data := app.GetAnnotations()
	if data["deployment.kubernetes.io/revision"] != "1" {
		t.Errorf("MergeWithInputAppCrd error")
	}

	if !IsOldApp(app) {
		t.Errorf("IsOldApp error")
	}
	if !application.IsAppStoppingAction() || application.IsAppStartingAction() {
		t.Errorf("App status error")
	}
}

func TestGenAnnotationPatchData(t *testing.T) {
	phase := "Succeeded"
	value := "starting"
	key := "app.alauda.io/action"

	metadata := "{\"metadata\":{\"annotations\":{\"app.alauda.io/action\":\"starting\"}}}"
	data := GenAnnotationPatchData(key, value)
	if string(data) != metadata {
		t.Errorf("GenAnnotationPatchData error: excepted %s, got %s", metadata, data)
	}

	sourcedata := "{\"metadata\":{\"annotations\":{\"app.alauda.io/action\":\"starting\"}},\"spec\":{\"assemblyPhase\":\"Succeeded\"}}"
	desdata := GenSimpleApplicationUpdateData(phase, key, value)
	if string(desdata) != sourcedata {
		t.Errorf("GenSimpleApplicationUpdateData error")
	}
}
