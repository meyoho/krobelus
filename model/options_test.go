package model

import (
	"context"
	"reflect"
	"testing"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestListOptions_GetListParams(t *testing.T) {
	type fields struct {
		Context     context.Context
		Meta        string
		NameFilters []ObjectNameTuple
		NameSearch  string
		Options     metaV1.ListOptions
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]string
	}{
		{
			name: "normal",
			fields: fields{
				Options: metaV1.ListOptions{
					ResourceVersion: "123",
					Watch:           true,
				},
			},
			want: map[string]string{
				"resourceVersion": "123",
				"watch":           "true",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := ListOptions{
				Context:     tt.fields.Context,
				Meta:        tt.fields.Meta,
				NameFilters: tt.fields.NameFilters,
				NameSearch:  tt.fields.NameSearch,
				Options:     tt.fields.Options,
			}
			if got := l.GetListParams(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListOptions.GetListParams() = %v, want %v", got, tt.want)
			}
		})
	}
}
