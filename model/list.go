package model

import (
	"encoding/json"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
)

// ListToObjects convert unstruct list to object list
func ListToObjects(list *unstructured.UnstructuredList) []*KubernetesObject {
	objects := make([]*KubernetesObject, 0)
	if list == nil {
		return objects
	}
	for _, item := range list.Items {
		objects = append(objects, UnstructToObject(&item))
	}
	return objects
}

// FlatObjectList will extract items from List object if found
func FlatObjectList(objects []*KubernetesObject) ([]*KubernetesObject, error) {
	var result []*KubernetesObject
	for _, item := range objects {
		if item.Kind == "List" {
			f := func(o runtime.Object) error {
				bt, err := json.Marshal(o)
				if err != nil {
					return err
				}
				var ks KubernetesObject
				if err := ks.UnmarshalJSON(bt); err != nil {
					return err
				}
				result = append(result, &ks)
				return nil
			}

			if item.IsList() {
				if err := item.EachListItem(f); err != nil {
					return result, err
				}
			}
		} else {
			result = append(result, item)
		}
	}
	return result, nil
}
