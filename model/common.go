package model

import (
	"time"
)

type ResourceBase struct {
	UUID string `json:"uuid" binding:"required"`
	Name string `json:"name" binding:"required"`
}

type ResourceBasic struct {
	UUID        string    `json:"uuid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type Namespace ResourceBase

type Cluster ResourceBase

type ClusterInfo struct {
	UUID  string
	Token string
}

type ResourceBaseWithType struct {
	UUID string `json:"uuid,omitempty"`
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`
}

type Parent ResourceBaseWithType

type JSONObject map[string]interface{}

// alauda resource type
type ResourceType string

const (
	ApplicationType ResourceType = "Application"
)

type ResourceRatio struct {
	CPU int `json:"cpu"`
	Mem int `json:"mem"`
}

func (r *ResourceRatio) IsEmpty() bool {
	return r.IsCPUEmpty() && r.IsMemEmpty()
}

func (r *ResourceRatio) IsCPUEmpty() bool {
	return r.CPU == 0
}

func (r *ResourceRatio) IsMemEmpty() bool {
	return r.Mem == 0
}
