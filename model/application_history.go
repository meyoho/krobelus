package model

import (
	"encoding/json"
	"time"

	"krobelus/pkg/applicationhistory/v1beta1"

	"github.com/ghodss/yaml"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
)

// ApplicationHistoryResource stores the data for a application history creation.
type ApplicationHistoryResource struct {
	// AppUID is the application UID to create the history.
	AppUID types.UID
	// OldApp is the old application object that is updated,
	// should be null on application creation.
	OldApp *Application
	// Request is the application create/update request.
	Request *ApplicationRequest
	// User is the user who revokes the request.
	User string
	// ChangeCause is the change cause of the application history.
	ChangeCause string
	// CreationTimestamp is the creation timestamp of the application history.
	CreationTimestamp time.Time
	// RollbackFrom is the revision the application history rollbacks from.
	// A 'nil' value indicates that the history is not a rollback one.
	RollbackFrom *int
}

// UnstructuredToApplicationHistory converts an Unstructured object to ApplicationHistory object.
func UnstructuredToApplicationHistory(obj *unstructured.Unstructured) (*v1beta1.ApplicationHistory, error) {
	res := &v1beta1.ApplicationHistory{}
	bytes, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// GetAppHistoryKubernetesObjects returns the KubernetesObject array in the application history YAML.
func GetAppHistoryKubernetesObjects(obj *v1beta1.ApplicationHistory) ([]*KubernetesObject, error) {
	var res []*KubernetesObject
	err := yaml.Unmarshal([]byte(obj.Spec.YAML), &res)
	if err != nil {
		return nil, err
	}
	return res, nil
}
