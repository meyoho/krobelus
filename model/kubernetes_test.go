package model

import (
	"reflect"
	"testing"

	"krobelus/config"
	"krobelus/tests"

	"github.com/spf13/viper"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
)

func TestConfigKS(t *testing.T) {
	labels := map[string]string{
		"app.cmb.cn/name": "nginx.app-status",
		"app.cmb.cn/uuid": "ec640458-0a63-11e9-9ba5-0242ac120007",
	}

	newlabels := map[string]string{
		"app":             "liveness-http",
		"app.cmb.cn/uuid": "ec640458-0a63-11e9-9ba5-0242ac120007",
	}

	ano := map[string]string{
		"deployment.kubernetes.io/revision": "1",
	}

	var ks KubernetesObject
	ks.SetName("example")
	ks.SetNamespace("test")
	ks.SetKind("deployment")
	ks.SetAPIVersion("extensions/v1beta1")
	ks.SetUID(types.UID("5473-43843"))
	ks.SetResourceVersion("5994864")
	ks.SetLabel("app", "liveness-http")
	ks.SetExtraLabels(labels)
	ks.SetAno("deployment.kubernetes.io/revision", "1")
	ks.DeleteLabel("app.cmb.cn/name")
	ks.SetPodReplicas(3)

	if ks.GetName() != "example" {
		t.Errorf("Setname error")
	}

	if ks.GetNamespace() != "test" {
		t.Errorf("SetNamespace error")
	}

	if ks.GetKind() != "deployment" {
		t.Errorf("GetKind error")
	}

	if ks.GetAPIVersion() != "extensions/v1beta1" {
		t.Errorf("SetAPIVersion error")
	}

	if ks.GetUID() != "5473-43843" {
		t.Errorf("GetUID error")
	}

	if ks.GetResourceVersion() != "5994864" {
		t.Errorf("GetResourceVersion error")
	}

	if !equal(ks.GetLabels(), newlabels) {
		t.Errorf("SetLabels error")
	}

	if v, _ := ks.GetPodReplicas(); v != 3 {
		t.Errorf("SetPodReplicas error")
	}

	if !equal(ks.GetAnnotations(), ano) {
		t.Errorf("SetAno error")
	}
}

func TestAddResourceLabelIfNeed(t *testing.T) {
	config.GlobalConfig.Pod.ResourceLabel = "app.com.cn/resource"
	config.GlobalConfig.Pod.OrgLabel = "app.com.cn/org"

	labels := map[string]string{
		"app.com.cn/resource": "app-status-Deployment-liveness-http-deployment",
		"cmb-test":            "app-status-Deployment-liveness-http-deployment",
		"test":                "liveness",
		"app.cmb.cn/name":     "nginx.app-status",
		"app.com.cn/org":      "orgtest",
	}

	ks, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/deployment.json"))
	ks.AddResourceLabelIfNeeded("orgtest")
	podTemplateSpec, _ := ks.GetPodTemplateSpec()
	if !equal(podTemplateSpec.Labels, labels) {
		t.Errorf("TestAddResourceLabelIfNeed error:expected %s,got %s", podTemplateSpec.Labels, labels)
	}
}

func TestAddPodTemplateLabels(t *testing.T) {
	labelA := map[string]string{
		"app.cmb.cn/name": "nginx.app-status",
		"app.com.cn/org":  "orgtest",
	}

	labelB := map[string]string{
		"cmb-test":        "app-status-Deployment-liveness-http-deployment",
		"test":            "liveness",
		"app.cmb.cn/name": "nginx.app-status",
		"app.com.cn/org":  "orgtest",
	}
	ks, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/deployment.json"))
	ks.AddPodTemplateLabels(labelA)
	podTemplateSpec, _ := ks.GetPodTemplateSpec()

	if !equal(podTemplateSpec.Labels, labelB) {
		t.Errorf("AddPodTemplateLabels error: expected %s, got %s", labelB, podTemplateSpec.Labels)
	}

	keys := []string{"app.com.cn/resource", "cmb-test", "test"}
	ks.RemovePodTemplateLabels(keys)
	podTemplateSpec, _ = ks.GetPodTemplateSpec()

	if !equal(podTemplateSpec.Labels, labelA) {
		t.Errorf("RemovePodTemplateLabels error")
	}
}

func TestUpdatePodTemplateAnnotations(t *testing.T) {
	ano := map[string]string{
		"aaa": "bbb",
	}
	ks, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/status/deployment.json"))
	ks.UpdatePodTemplateAnnotations(ano)

	podTemplateSpec, _ := ks.GetPodTemplateSpec()
	if !equal(podTemplateSpec.Annotations, ano) {
		t.Error("UpdatePodTemplateAnnotations error")
	}
}
func TestIsSameObject(t *testing.T) {
	var k1 KubernetesObject
	k1.SetName("test1")
	k1.SetKind("kind1")
	k1.SetAPIVersion("version1")

	var k2 KubernetesObject
	k2.SetName("test1")
	k2.SetKind("kind1")
	k2.SetAPIVersion("version1")

	if !IsSameObject(&k1, &k2) {
		t.Errorf("IsSameObject error")
	}

	k2.SetName("test2")
	if IsSameObject(&k1, &k2) {
		t.Errorf("IsSameObject error")
	}
}

func TestUpdateAnnotations(t *testing.T) {
	var ks KubernetesObject
	viper.Set(config.KeyLabelBaseDomain, "alauda.io")

	UpdateAnnotations(&ks, "deployment.kubernetes.io/revision", "1")
	UpdateAnnotations(&ks, "resource.alauda.io/uuid", "1000")

	ano := map[string]string{
		"deployment.kubernetes.io/revision": "1",
		"resource.alauda.io/uuid":           "1000",
	}
	if !equal(ks.GetAnnotations(), ano) {
		t.Errorf("UpdateAnnotations error")
	}

	if GetAlaudaUID(&ks) != "1000" {
		t.Errorf("GetAlaudaUID error")
	}
}

func TestUpdateLabels(t *testing.T) {
	var ks KubernetesObject
	UpdateLabels(&ks, "app.cmb.cn/name", "nginx.app-status")

	newlabels := map[string]string{
		"app.cmb.cn/name": "nginx.app-status",
	}
	if !equal(ks.GetLabels(), newlabels) {
		t.Errorf("UpdateLabels error")
	}
}

func TestGetSpec(t *testing.T) {
	application, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/application.json"))
	spec, _ := application.GetServiceSpec()
	if spec.ClusterIP != "1.1.1.1" {
		t.Errorf("GetServiceSpec error")
	}

	appspec, _ := application.GetApplicationSpec()
	if appspec.AssemblyPhase != "Succeeded" {
		t.Errorf("GetApplicationSpec error")
	}

	hpaspec, _ := application.GetHPASpec()
	if hpaspec.MaxReplicas != 100 {
		t.Errorf("GetHPASpec error")
	}
}

func TestToV1Service(t *testing.T) {
	application, _ := BytesToKubernetesObject(tests.LoadFile("../tests/fixtures/application.json"))
	service, _ := application.ToV1Service()

	var ks KubernetesObject
	ks.ParseV1Service(service)

	spec, _ := ks.GetServiceSpec()
	if spec.ClusterIP != "1.1.1.1" {
		t.Errorf("ToV1Service error")
	}
}

func TestDecodeToUnstructList(t *testing.T) {
	tests := []struct {
		name    string
		data    string
		expect  unstructured.UnstructuredList
		wantErr bool
	}{
		{
			name: "normal",
			data: `{
				"apiVersion": "v1",
				"items": [
					{
						"apiVersion": "v1",
						"kind": "Service",
						"metadata": {
							"name": "web1",
							"namespace": "default"
						}
					},
					{
						"apiVersion": "v1",
						"kind": "Service",
						"metadata": {
							"name": "web2",
							"namespace": "default"
						}
					}
				],
				"kind": "List",
				"metadata": {
					"resourceVersion": "",
					"selfLink": ""
				}
			}`,
			expect: unstructured.UnstructuredList{
				Items: []unstructured.Unstructured{
					{
						Object: map[string]interface{}{
							"apiVersion": "v1",
							"kind":       "Service",
							"metadata": map[string]interface{}{
								"name":      "web1",
								"namespace": "default",
							},
						},
					},
					{
						Object: map[string]interface{}{
							"apiVersion": "v1",
							"kind":       "Service",
							"metadata": map[string]interface{}{
								"name":      "web2",
								"namespace": "default",
							},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			list := &unstructured.UnstructuredList{}
			if err := DecodeToUnstructList([]byte(tt.data), list); (err != nil) != tt.wantErr {
				t.Errorf("DecodeToUnstructList() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr && !reflect.DeepEqual(list.Items, tt.expect.Items) {
				t.Errorf("Expected %+v, got %+v", tt.expect.Items, list.Items)
			}
		})
	}
}
