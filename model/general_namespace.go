package model

import (
	"krobelus/common"
)

// GeneralNamespaceRequest describes a struct used for creating namespace.
type GeneralNamespaceRequest struct {
	Cluster       string            `json:"-" `
	Namespace     *KubernetesObject `json:"namespace" binding:"required"`
	ResourceQuota *KubernetesObject `json:"resourcequota"`
	LimitRange    *KubernetesObject `json:"limitrange" binding:"required"`
	Logger        common.Log        `json:"-" `
}

// GeneralNamespaceUpdateRequest describes a struct used for updating a namespace.
type GeneralNamespaceUpdateRequest struct {
	Cluster       string            `json:"-" `
	ResourceQuota *KubernetesObject `json:"resourcequota"`
	LimitRange    *KubernetesObject `json:"limitrange"`
	Logger        common.Log        `json:"-" `
}
