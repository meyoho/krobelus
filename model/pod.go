package model

import (
	"fmt"

	v1 "k8s.io/api/core/v1"
)

// GetSimplePodCondition get simple reason and message pod which is not in Running phase
func GetSimplePodCondition(pod *v1.Pod) []ObjectMessage {

	res := []ObjectMessage{}

	msg := map[string]string{}

	if nil == pod {
		return res
	}

	if "" != pod.Status.Reason || "" != pod.Status.Message {
		msg[pod.Status.Reason] = pod.Status.Message
	}

	for _, containerStatus := range pod.Status.ContainerStatuses {
		if !containerStatus.Ready {
			state := containerStatus.State
			if nil != state.Terminated {
				msg[state.Terminated.Reason] = fmt.Sprintf("container termirated: %s", state.Terminated.Message)
			}
			if nil != state.Waiting {
				msg[state.Waiting.Reason] = fmt.Sprintf("container waiting: %s", state.Waiting.Message)
			}
		}
	}

	for _, cond := range pod.Status.Conditions {
		if "" != cond.Reason || "" != cond.Message {
			msg[cond.Reason] = cond.Message
		}
	}

	for reason, message := range msg {
		res = append(res, ObjectMessage{
			Reason:  reason,
			Message: message,
		})
	}

	return res

}
