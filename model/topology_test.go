package model

import (
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/spf13/cast"
	"github.com/stretchr/testify/assert"
)

func (eh *EdgeHelper) UnmarshalJSON(b []byte) error {
	err := eh.KubernetesObject.UnmarshalJSON(b)
	if err != nil {
		return err
	}
	eh.EdgeType = cast.ToString(eh.KubernetesObject.GetField("EdgeType"))

	delete(eh.KubernetesObject.Object, "EdgeType")
	return nil
}

func TestTopologyWalk(t *testing.T) {

	fileName := "../tests/fixtures/topology/topology.json"

	bt, err := ioutil.ReadFile(fileName)
	if nil != err {
		t.Fatal(err)
	}

	topology := make(Topology)

	err = json.Unmarshal(bt, &topology)
	if nil != err {
		t.Fatal(err)
	}

	bt = []byte(`{"kind":"Application","metadata":{"name":"app-1","namespace":"cp-data"}}`)

	app := &KubernetesObject{}
	if err := json.Unmarshal(bt, &app); nil != err {
		t.Fatal(err)
	}

	indexes := []*KubernetesObject{app}

	bt = []byte(`{"kind":"Application","metadata":{"name":"app-2","namespace":"cp-data"}}`)

	app2 := &KubernetesObject{}
	if err := json.Unmarshal(bt, &app2); nil != err {
		t.Fatal(err)
	}

	indexes1 := []*KubernetesObject{app2}

	var (
		depth1 = "../tests/fixtures/topology/depth1.json"
		depth2 = "../tests/fixtures/topology/depth2.json"
		depth3 = "../tests/fixtures/topology/depth3.json"
		depth4 = "../tests/fixtures/topology/depth4.json"
		app_2  = "../tests/fixtures/topology/app-2.json"
	)

	type params struct {
		indexes  []*KubernetesObject
		visited  map[string]bool
		depth    int16
		excepted *string
	}

	cases := []params{
		{
			indexes:  indexes,
			visited:  make(map[string]bool),
			depth:    int16(0),
			excepted: nil,
		},
		{
			indexes:  indexes,
			visited:  make(map[string]bool),
			depth:    int16(1),
			excepted: &depth1,
		},
		{
			indexes:  indexes,
			visited:  make(map[string]bool),
			depth:    int16(2),
			excepted: &depth2,
		},
		{
			indexes:  indexes,
			visited:  make(map[string]bool),
			depth:    int16(3),
			excepted: &depth3,
		},
		{
			indexes:  indexes,
			visited:  make(map[string]bool),
			depth:    int16(4),
			excepted: &depth4,
		},
		{
			indexes:  indexes,
			visited:  make(map[string]bool),
			depth:    int16(32767),
			excepted: &depth4,
		},
		{
			indexes:  indexes1,
			visited:  make(map[string]bool),
			depth:    int16(32767),
			excepted: &app_2,
		},
	}

	for _, param := range cases {

		// CASE: depth 1
		topo := topology.Walk(param.indexes, param.visited, param.depth)

		if nil == param.excepted {
			assert.Empty(t, topo)
		} else {
			bt, err = ioutil.ReadFile(*param.excepted)
			if nil != err {
				t.Fatal(err)
			}
			var excepted Topology

			err = json.Unmarshal(bt, &excepted)
			if nil != err {
				t.Fatal(err)
			}

			json.Marshal(excepted)

			assert.Equal(t, topo, excepted)
		}

	}

}
