package model

import (
	"krobelus/pkg/applicationhistory/v1beta1"
	"reflect"
	"testing"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func TestUnstructuredToApplicationHistory(t *testing.T) {
	tests := []struct {
		name    string
		obj     *unstructured.Unstructured
		want    *v1beta1.ApplicationHistory
		wantErr bool
	}{
		{
			name: "success",
			obj: &unstructured.Unstructured{
				Object: map[string]interface{}{
					"metadata": map[string]interface{}{
						"name":      "foo",
						"namespace": "default",
					},
					"spec": map[string]interface{}{
						"revision":    2,
						"user":        "alice",
						"changeCause": "create",
					},
				}},
			want: &v1beta1.ApplicationHistory{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "foo",
					Namespace: "default",
				},
				Spec: v1beta1.ApplicationHistorySpec{
					Revision:    2,
					User:        "alice",
					ChangeCause: "create",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UnstructuredToApplicationHistory(tt.obj)
			if (err != nil) != tt.wantErr {
				t.Errorf("UnstructuredToApplicationHistory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnstructuredToApplicationHistory() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetAppHistoryKubernetesObjects(t *testing.T) {
	tests := []struct {
		name    string
		obj     *v1beta1.ApplicationHistory
		want    []*KubernetesObject
		wantErr bool
	}{
		{
			name: "success",
			obj: &v1beta1.ApplicationHistory{
				Spec: v1beta1.ApplicationHistorySpec{
					YAML: `
- apiVersion: app.k8s.io/v1beta1
  kind: Application
  metadata:
    name: test
    namespace: default
  spec:
    assemblyPhase: Pending
    componentKinds:
      - group: ""
        kind: Service
      - group: extensions
        kind: Deployment
    selector:
      matchLabels:
        app.alauda.io/name: test.default
- apiVersion: v1
  kind: Service
  metadata:
    name: web
    namespace: default
  spec:
    ports:
      - port: 8080
    selector:
      app: nginx`,
				},
			},
			want: []*KubernetesObject{
				{
					Kind:       "Application",
					APIVersion: "app.k8s.io/v1beta1",
					Name:       "test",
					Namespace:  "default",
					Spec: map[string]interface{}{
						"assemblyPhase": "Pending",
						"componentKinds": []interface{}{
							map[string]interface{}{
								"group": "",
								"kind":  "Service",
							},
							map[string]interface{}{
								"group": "extensions",
								"kind":  "Deployment",
							},
						},
						"selector": map[string]interface{}{
							"matchLabels": map[string]interface{}{
								"app.alauda.io/name": "test.default",
							},
						},
					},
					Unstructured: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "app.k8s.io/v1beta1",
							"kind":       "Application",
							"metadata": map[string]interface{}{
								"name":      "test",
								"namespace": "default",
							},
							"spec": map[string]interface{}{
								"assemblyPhase": "Pending",
								"componentKinds": []interface{}{
									map[string]interface{}{
										"group": "",
										"kind":  "Service",
									},
									map[string]interface{}{
										"group": "extensions",
										"kind":  "Deployment",
									},
								},
								"selector": map[string]interface{}{
									"matchLabels": map[string]interface{}{
										"app.alauda.io/name": "test.default",
									},
								},
							},
						},
					},
				},
				{
					Kind:       "Service",
					APIVersion: "v1",
					Name:       "web",
					Namespace:  "default",
					Spec: map[string]interface{}{
						"ports": []interface{}{
							map[string]interface{}{
								"port": int64(8080),
							},
						},
						"selector": map[string]interface{}{
							"app": "nginx",
						},
					},
					Unstructured: unstructured.Unstructured{
						Object: map[string]interface{}{
							"apiVersion": "v1",
							"kind":       "Service",
							"metadata": map[string]interface{}{
								"name":      "web",
								"namespace": "default",
							},
							"spec": map[string]interface{}{
								"ports": []interface{}{
									map[string]interface{}{
										"port": int64(8080),
									},
								},
								"selector": map[string]interface{}{
									"app": "nginx",
								},
							},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetAppHistoryKubernetesObjects(tt.obj)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAppHistoryKubernetesObjects() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for i, o := range got {
				if !reflect.DeepEqual(o, tt.want[i]) {
					t.Errorf("GetAppHistoryKubernetesObjects() = %v, want %v", o, tt.want[i])
				}
			}
		})
	}
}
