package model

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"krobelus/common"
	"krobelus/pkg/application/v1beta1"

	"github.com/Jeffail/gabs"
	"github.com/spf13/cast"

	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Owner struct {
	Name       string `json:"name" example:"Tom"`
	Phone      string `json:"phone" example:"9527"`
	EmployeeID string `json:"employee_id" example:"12345678"`
}

type ApplicationResource struct {
	Name      string `json:"name" example:"app"`
	Namespace string `json:"namespace" example:"default"`

	// also allows in update request
	DisplayName *string `json:"displayName" example:"App"`
	Owners      []Owner `json:"owners"`

	// optional and for compatibility
	Source       string `json:"source"`
	Category     string `json:"category"`
	Description  string `json:"description"`
	CreateMethod string `json:"createMethod"`
	// User is the user who invoke the application request
	User string `json:"user"`
	// ChangeCause is the cause why the change is invoked.
	ChangeCause string `json:"changeCause"`
}

type ApplicationRequest struct {
	Resource   ApplicationResource `json:"resource" binding:"required"`
	Crd        *KubernetesObject   `json:"crd"`
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
}

type ApplicationImportResourcesRequest struct {
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
}

type ApplicationExportResourcesRequest struct {
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
}

// ApplicationRollbackRequest describes the data that a application rollback
// request should have.
type ApplicationRollbackRequest struct {
	Revision int    `json:"revision" binding:"required"`
	User     string `json:"user"`
}

// DeepCopy deep copies the ApplicationRequest to a new one.
func (app *ApplicationRequest) DeepCopy() *ApplicationRequest {
	newAppReq := &ApplicationRequest{
		Resource: app.Resource,
	}
	if app.Crd != nil {
		newAppReq.Crd = app.Crd.DeepCopy()
	}
	if app.Kubernetes != nil {
		newAppReq.Kubernetes = []*KubernetesObject{}
		for _, o := range app.Kubernetes {
			newAppReq.Kubernetes = append(newAppReq.Kubernetes, o.DeepCopy())
		}
	}
	return newAppReq
}

type Application struct {
	Cluster    ClusterInfo
	Crd        KubernetesObject
	Kubernetes []*KubernetesObject
	Logger     common.Log
}

func (application *Application) GetClusterUUID() string {
	return application.Cluster.UUID
}

func (application *Application) GetClusterToken() string {
	return application.Cluster.Token
}

func (application *Application) GetNamespace() string {
	return application.Crd.Namespace
}

func (application *Application) GetName() string {
	return application.Crd.Name
}

func (application *Application) GetUUID() string {
	return GetAppUUID(&application.Crd)
}

func (application *Application) GetType() ResourceType {
	return ApplicationType
}

// GetAppNameSelector returns the application name selector map.
func (application *Application) GetAppNameSelector() map[string]string {
	return application.Crd.GetAppNameSelector()
}

// GetAppNameSelectorString returns the application name selector string.
func (application *Application) GetAppNameSelectorString() string {
	return application.Crd.GetAppNameSelectorString()
}

// GetAppHistoryLimit returns the application history limit of the application.
func (application *Application) GetAppHistoryLimit() int {
	return application.Crd.GetAppHistoryLimit()
}

func (application *Application) GetLogger() common.Log {
	return common.GetLoggerByKV("application",
		fmt.Sprintf("%s:%s", application.GetNamespace(), application.Crd.Name))
}

type ApplicationResponse struct {
	Kubernetes []*KubernetesObject `json:"kubernetes"`
}

type ApplicationKubernetesObjectList struct {
	Kubernetes []metav1.Object `json:"kubernetes" swaggertype:"array,object"`
}

// WorkloadPhase represent status for kubernetes Workloads, including deployments, daemonsets, statefulsets
type WorkloadPhase string

const (
	// WorkloadRunning means the pod under this Workload has been started and running,
	// and the number of pods is equal to replica count defined.
	WorkloadRunning WorkloadPhase = "Running"
	// WorkloadPending means neither the Workload is in Running nor in Stopped state.
	WorkloadPending WorkloadPhase = "Pending"
	// WorkloadStopped means all of the pod has been deleted,
	// and the defined replica number has been set to zero.
	WorkloadStopped WorkloadPhase = "Stopped"

	// Empty means the application has no Workload in it.
	ApplicationEmpty v1beta1.ApplicationAssemblyPhase = "Empty"
	// PartialRunning means at least one Workload is running inside a appliation.
	ApplicationPartialRunning v1beta1.ApplicationAssemblyPhase = "PartialRunning"
	// Pending means means at least one Workload is pending inside a appliation.
	ApplicationPending v1beta1.ApplicationAssemblyPhase = v1beta1.Pending
	// Running means all of the Workloads in application is running.
	ApplicationRunning v1beta1.ApplicationAssemblyPhase = "Running"
	// Stopped means all of the Workloads in application has been stopped.
	ApplicationStopped v1beta1.ApplicationAssemblyPhase = "Stopped"
)

type ObjectMessage struct {
	Reason  string `json:"reason"`
	Message string `json:"message"`
}

type WorkloadStatus struct {
	Current  int             `json:"current"`
	Desired  int             `json:"desired"`
	Status   WorkloadPhase   `json:"status"`
	Messages []ObjectMessage `json:"messages"`
}

type AppStatusResponse struct {
	AppStatus v1beta1.ApplicationAssemblyPhase `json:"app_status"`
	Workloads map[string]WorkloadStatus        `json:"workloads"`
	Messages  []ObjectMessage                  `json:"messages"`
}

// WorkloadCondition describes the state of a Workload(deployment, daemonset, statefulset) at a certain point.
type WorkloadCondition struct {
	// Type of deployment condition.
	Type string `json:"type"`
	// Status of the condition, one of True, False, Unknown.
	Status string `json:"status"`
	// The reason for the condition's last transition.
	Reason string `json:"reason,omitempty"`
	// A human readable message indicating details about the transition.
	Message string `json:"message,omitempty"`
}

type WorkloadConditions []WorkloadCondition

func (ks *KubernetesObject) getSuspiciousCondition() []ObjectMessage {

	var field interface{}
	var conditions WorkloadConditions

	res := []ObjectMessage{}

	if field = ks.GetField("status", "conditions"); nil == field {
		return res
	}

	bt, err := json.Marshal(field)
	if nil != err {
		return res
	}

	err = json.Unmarshal(bt, &conditions)
	if nil != err {
		return res
	}

	for _, cond := range conditions {
		if "" != cond.Message || "" != cond.Reason {
			res = append(res, ObjectMessage{
				Message: cond.Message,
				Reason:  cond.Reason,
			})
		}
	}

	return res

}

func (ks *KubernetesObject) caculateDeploymentStatus(verbose bool, log common.Log) *WorkloadStatus {
	var desired, observedReplicas, availableReplicas, updatedReplicas, replicas int32
	var current int
	var observedGeneration int64
	var err error

	logger := log.WithField("action", "caculateDeploymentStatus")

	logger.Debugf("kubernetes: obj=%+v", ks)

	// use replicas as int32 other than *int32
	if replicas, err = cast.ToInt32E(ks.GetField("spec", "replicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: spec.replica")
		desired = 0
	} else {
		desired = replicas
	}

	generation := ks.GetGeneration()

	if observedReplicas, err = cast.ToInt32E(ks.GetField("status", "replicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.replica")
		observedReplicas = 0
	}

	if availableReplicas, err = cast.ToInt32E(ks.GetField("status", "availableReplicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.availableReplicas")
		availableReplicas = 0
	}

	if updatedReplicas, err = cast.ToInt32E(ks.GetField("status", "updatedReplicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.updatedReplicas")
		updatedReplicas = 0
	}

	if observedGeneration, err = cast.ToInt64E(ks.GetField("status", "observedGeneration")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.updatedReplicas")
		observedGeneration = 0
	}

	// if updateRelicas is zero, that means the deployment hasn't been updated or not actually take effect
	// so consider this state as not processing
	current = int(availableReplicas)

	ws := &WorkloadStatus{
		Current:  current,
		Desired:  int(desired),
		Messages: []ObjectMessage{},
	}

	logger.Debugf("desired=%v, observedReplicas=%v, availableReplicas=%v, updatedReplicas=%v, observedGeneration=%v generation=%v",
		desired, observedReplicas, availableReplicas, updatedReplicas, observedGeneration, generation)

	if desired > 0 && updatedReplicas == desired && observedReplicas == desired && availableReplicas == desired && observedGeneration >= generation {
		// Running
		ws.Status = WorkloadRunning
	} else if desired == 0 &&
		observedReplicas == desired && updatedReplicas == desired && availableReplicas == desired && observedGeneration >= generation {
		// Stopped
		ws.Status = WorkloadStopped
	} else {
		//Pending:
		// Scale Up
		// Scale Down
		// Rolling update
		// Recreate
		// Unknown
		ws.Status = WorkloadPending
	}

	if verbose {
		ws.Messages = append(ws.Messages, ks.getSuspiciousCondition()...)
	}

	logger.Debugf("Workload status: ws=%+v", ws)

	return ws

}

func (ks *KubernetesObject) caculateDaemonsetStatus(verbose bool, log common.Log) *WorkloadStatus {
	var desired, currentNumberScheduled, numberReady, numberAvailable, updatedNumberScheduled int32
	var updateStrategy string
	var observedGeneration int64
	var current int
	var err error

	logger := log.WithField("action", "caculateDaemonsetStatus")

	if updateStrategy, err = cast.ToStringE(ks.GetField("spec", "updateStrategy", "type")); nil != err {
		logger.WithError(err).Warnf("invalid field: spec,updateStrategy.type")
		updateStrategy = string(appsv1.RollingUpdateDaemonSetStrategyType)
	}

	if desired, err = cast.ToInt32E(ks.GetField("status", "desiredNumberScheduled")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.desiredNumberScheduled")
		desired = 0
	}

	generation := ks.GetGeneration()

	if currentNumberScheduled, err = cast.ToInt32E(ks.GetField("status", "currentNumberScheduled")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.currentNumberScheduled")
		currentNumberScheduled = 0
	}

	if numberReady, err = cast.ToInt32E(ks.GetField("status", "numberReady")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.numberReady")
		numberReady = 0
	}

	if numberAvailable, err = cast.ToInt32E(ks.GetField("status", "numberAvailable")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.numberAvailable")
		numberAvailable = 0
	}

	if updatedNumberScheduled, err = cast.ToInt32E(ks.GetField("status", "updatedNumberScheduled")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.updatedNumberScheduled")
		updatedNumberScheduled = 0
	}

	if observedGeneration, err = cast.ToInt64E(ks.GetField("status", "observedGeneration")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.observedGeneration")
		observedGeneration = 0
	}

	current = int(numberAvailable)

	ws := &WorkloadStatus{
		Current:  current,
		Desired:  int(desired),
		Messages: []ObjectMessage{},
	}

	logger.Debugf(`desired=%v, currentNumberScheduled=%v, numberReady=%v, numberAvailable=%v,
updatedNumberScheduled=%v, observedGeneration=%v generation=%v, updateStrategy=%v`,
		desired, currentNumberScheduled, numberReady, numberAvailable, updatedNumberScheduled, observedGeneration, generation, updateStrategy)

	if desired > 0 && observedGeneration >= generation && currentNumberScheduled == desired && numberReady == desired && numberAvailable == desired {
		if string(appsv1.RollingUpdateDaemonSetStrategyType) == updateStrategy && updatedNumberScheduled != desired {
			//Pending
			ws.Status = WorkloadPending
		} else {
			// Running
			ws.Status = WorkloadRunning
		}
	} else if desired == 0 &&
		observedGeneration >= generation && numberReady == desired && numberAvailable == desired && currentNumberScheduled == desired {
		if string(appsv1.RollingUpdateDaemonSetStrategyType) == updateStrategy && updatedNumberScheduled != desired {
			//Pending
			ws.Status = WorkloadPending
		} else {
			// Stopped
			ws.Status = WorkloadStopped
		}
	} else {
		//Pending
		ws.Status = WorkloadPending
	}

	if verbose {
		ws.Messages = append(ws.Messages, ks.getSuspiciousCondition()...)
	}

	logger.Debugf("Workload status: ws=%+v", ws)

	return ws

}

func (ks *KubernetesObject) caculateStatefulsetStatus(verbose bool, log common.Log) *WorkloadStatus {
	var desired, observedReplicas, readyReplicas, currentReplicas, replicas int32
	var currentRevision, updateRevision, updateStrategy string
	var observedGeneration int64
	var current int
	var err error

	logger := log.WithField("action", "caculateStatefulsetStatus")

	if updateStrategy, err = cast.ToStringE(ks.GetField("spec", "updateStrategy", "type")); nil != err {
		logger.WithError(err).Warnf("invalid field: spec.updateStrategy.type")
		updateStrategy = string(appsv1.RollingUpdateStatefulSetStrategyType)
	}

	generation := ks.GetGeneration()

	// use replicas as int32 other than *int32 for it is from json, not from user input
	if replicas, err = cast.ToInt32E(ks.GetField("spec", "replicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: spec.replicas")
		desired = 0
	} else {
		desired = replicas
	}

	if observedReplicas, err = cast.ToInt32E(ks.GetField("status", "replicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.replicas")
		observedReplicas = 0
	}

	if readyReplicas, err = cast.ToInt32E(ks.GetField("status", "readyReplicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.readyReplicas")
		readyReplicas = 0
	}

	if currentReplicas, err = cast.ToInt32E(ks.GetField("status", "currentReplicas")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.currentReplicas")
		currentReplicas = 0
	}

	if observedGeneration, err = cast.ToInt64E(ks.GetField("status", "observedGeneration")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.observedGeneration")
		observedGeneration = 0
	}

	if currentRevision, err = cast.ToStringE(ks.GetField("status", "currentRevision")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.currentRevision")
		currentRevision = ""
	}

	if updateRevision, err = cast.ToStringE(ks.GetField("status", "updateRevision")); nil != err {
		logger.WithError(err).Warnf("invalid field: status.updateRevision")
		updateRevision = ""
	}

	current = int(readyReplicas)

	ws := &WorkloadStatus{
		Current:  current,
		Desired:  int(desired),
		Messages: []ObjectMessage{},
	}

	logger.Debugf(`desired=%v, observedReplicas=%v, readyReplicas=%v, currentReplicas=%v currentRevision=%v,
updateRevision=%v, observedGeneration=%v generation=%v, updateStrategy=%v`,
		desired, observedReplicas, readyReplicas, currentReplicas,
		currentRevision, updateRevision, observedGeneration, generation, updateStrategy)

	if desired > 0 && observedReplicas == desired && readyReplicas == desired && observedGeneration >= generation {

		if string(appsv1.RollingUpdateDaemonSetStrategyType) == updateStrategy && (currentRevision != updateRevision || currentReplicas != desired) {
			//Pending
			ws.Status = WorkloadPending
		} else {
			// Running
			ws.Status = WorkloadRunning
		}
	} else if desired == 0 && observedReplicas == desired && readyReplicas == desired && observedGeneration >= generation {

		if string(appsv1.RollingUpdateDaemonSetStrategyType) == updateStrategy && (currentRevision != updateRevision || currentReplicas != desired) {
			//Pending
			ws.Status = WorkloadPending
		} else {
			// Stopped
			ws.Status = WorkloadStopped
		}
	} else {
		//Pending

		ws.Status = WorkloadPending
	}

	if verbose {
		ws.Messages = append(ws.Messages, ks.getSuspiciousCondition()...)
	}

	logger.Debugf("Workload status: ws=%+v", ws)

	return ws

}

func (application *Application) computeApplicationWorkloadsStatus(verbose bool) map[string]WorkloadStatus {
	status := map[string]WorkloadStatus{}

	var logger common.Log

	if nil != application.Logger.Logger {
		logger = application.Logger
	} else {
		logger = common.GetLoggerByID("")
	}

	getWorkloadStatus := func(obj *KubernetesObject) (string, *WorkloadStatus) {
		kind := obj.GetKind()
		name := obj.GetName()
		WorkloadName := fmt.Sprintf("%s-%s", strings.ToLower(kind), strings.ToLower(name))

		switch kind {
		case common.KubernetesKindDeployment:
			return WorkloadName, obj.caculateDeploymentStatus(verbose, logger)
		case common.KubernetesKindDaemonSet:
			return WorkloadName, obj.caculateDaemonsetStatus(verbose, logger)
		case common.KubernetesKindStatefulSet:
			return WorkloadName, obj.caculateStatefulsetStatus(verbose, logger)
		default:
			logger.Warnf("Unexcepted Workload kind: kind=%s, skip", kind)
			return "", nil
		}
	}

	for _, Workload := range application.Kubernetes {
		k, v := getWorkloadStatus(Workload)
		if nil != v {
			status[k] = *v
		}
	}

	return status
}

// ComputeApplicationStatus compute application status according to Workloads' status,
// if verbose is true, return the Workloads error message in response
func (application *Application) ComputeApplicationStatus(verbose bool) *AppStatusResponse {
	start := time.Now()
	phase, ok := application.Crd.Spec["assemblyPhase"].(v1beta1.ApplicationAssemblyPhase)

	if !ok || "" == string(phase) {
		phase = v1beta1.Succeeded
	}

	res := &AppStatusResponse{
		AppStatus: phase,
		Messages:  []ObjectMessage{},
		Workloads: map[string]WorkloadStatus{},
	}

	// compute Workload status
	res.Workloads = application.computeApplicationWorkloadsStatus(verbose)

	var running, desired, stopped, pending int

	// combine Workload error message to application
	// collect Workload metrics
	for _, status := range res.Workloads {
		if verbose {
			res.Messages = append(res.Messages, status.Messages...)
		}
		if status.Status == WorkloadRunning {
			running++
			continue
		}
		if status.Status == WorkloadStopped {
			stopped++
			continue
		}
		if status.Status == WorkloadPending {
			pending++
			continue
		}
	}

	// compute app status
	if v1beta1.Succeeded == phase {
		desired = len(res.Workloads)
		if desired == 0 {
			res.AppStatus = ApplicationEmpty
		} else if running == desired {
			res.AppStatus = ApplicationRunning
		} else if stopped == desired {
			res.AppStatus = ApplicationStopped
		} else if 0 != pending {
			res.AppStatus = ApplicationPending
		} else {
			res.AppStatus = ApplicationPartialRunning
		}
	}

	if nil != application.Logger.Logger {
		application.Logger.Debugf("ComputeApplicationStatus %s cost: %s", application.Crd.GetString(), time.Since(start))
	}

	return res

}

func (res *ApplicationResponse) ToCommonKubernetesObjectList() ApplicationKubernetesObjectList {
	var objects []metav1.Object
	for _, obj := range res.Kubernetes {
		objects = append(objects, obj)
	}
	return ApplicationKubernetesObjectList{
		Kubernetes: objects,
	}
}

func GetAppSelectorName(name, namespace string) string {
	return fmt.Sprintf("%s.%s", name, namespace)
}

// UpdateApplicationWithInputResource update an application with provided ApplicationResource
func UpdateApplicationWithInputResource(app *v1beta1.Application, resource *ApplicationResource) {
	ano := app.GetAnnotations()
	if resource.DisplayName != nil {
		ano := app.GetAnnotations()
		ano[common.AppDisplayNameKey()] = *resource.DisplayName
	}

	if len(resource.Owners) != 0 {
		bytes, _ := json.Marshal(resource.Owners)
		ano[common.OwnersInfoKey()] = string(bytes)
	}
	app.SetAnnotations(ano)
}

// ToAppCrd generate a app crd resource with componentGroups unset and pending phase
func (r ApplicationResource) ToAppCrd() *v1beta1.Application {
	var app v1beta1.Application
	app.Kind = "Application"
	app.APIVersion = "app.k8s.io/v1beta1"
	app.Name = r.Name
	app.Namespace = r.Namespace

	app.Spec.Selector = &metav1.LabelSelector{
		MatchLabels: map[string]string{
			common.AppNameKey(): GetAppSelectorName(r.Name, r.Namespace),
		},
	}

	app.Spec.Descriptor = v1beta1.Descriptor{
		Description: r.Description,
		Type:        r.Category,
	}

	ano := map[string]string{
		common.AppCreateMethodKey(): r.CreateMethod,
	}

	if r.Source != "" {
		ano[common.AppSourceKey()] = r.Source
	}
	if r.DisplayName != nil {
		ano[common.AppDisplayNameKey()] = *r.DisplayName
	}
	if len(r.Owners) != 0 {
		bytes, _ := json.Marshal(r.Owners)
		ano[common.OwnersInfoKey()] = string(bytes)
	}
	app.SetAnnotations(ano)

	app.Spec.AssemblyPhase = v1beta1.Pending

	return &app
}

// MergeWithInputAppCrd merge input app crd with the generated one
// 1. Override name/namespace
// 2. merge annotations
func MergeWithInputAppCrd(input *v1beta1.Application, gen *v1beta1.Application) {
	input.SetName(gen.GetName())
	input.SetNamespace(gen.GetNamespace())

	genAno := gen.GetAnnotations()
	if genAno != nil {
		origin := input.GetAnnotations()
		if origin == nil {
			origin = make(map[string]string)
		}
		for k, v := range genAno {
			origin[k] = v
		}
		input.SetAnnotations(origin)
	}
}

// GenSimpleApplicationUpdateData will generate the data needed for patch method
// phase: application phase
// key: application annotation key, if empty, ignore this
// value: application annotation value
func GenSimpleApplicationUpdateData(phase, key, value string) []byte {
	jsonObj := gabs.New()
	jsonObj.SetP(phase, "spec.assemblyPhase")
	if key != "" {
		jsonObj.Set(value, "metadata", "annotations", key)
	}
	return jsonObj.Bytes()
}

func (r ApplicationResource) ToKubernetesObject() *KubernetesObject {
	crd := r.ToAppCrd()
	return AppCrdToObject(crd)
}

func GenAnnotationPatchData(key, value string) []byte {
	jsonObj := gabs.New()
	jsonObj.Set(value, "metadata", "annotations", key)
	return jsonObj.Bytes()
}

func (ks *KubernetesObject) ToAppCrd() *v1beta1.Application {
	bt, _ := ks.ToBytes()
	var app v1beta1.Application
	json.Unmarshal(bt, &app)
	return &app

}

func (ks *KubernetesObject) IsAppStoppingAction() bool {
	return ks.GetAppCurrentAction() == "stopping"
}

func (ks *KubernetesObject) IsAppStartingAction() bool {
	return ks.GetAppCurrentAction() == "starting"
}

// GetAppCurrentAction get app's current action
// values: starting/stopping/""(empty)
func (ks *KubernetesObject) GetAppCurrentAction() string {
	if ks.GetAnnotations() == nil {
		return ""
	}
	return ks.GetAnnotations()[common.AppActionKey()]
}

// GetAppLabelSelectorMap return empty map if not exist
func GetAppLabelSelectorMap(app *v1beta1.Application) map[string]string {
	result := make(map[string]string)
	if app.Spec.Selector == nil || app.Spec.Selector.MatchLabels == nil {
		return result
	}
	return app.Spec.Selector.MatchLabels
}

// GetAppSelectorWithName get app selector with auto-generated name selector
func GetAppSelectorWithName(app *v1beta1.Application) map[string]string {
	origin := GetAppLabelSelectorMap(app)
	appSelectorName := GetAppSelectorName(app.GetName(), app.GetNamespace())
	origin[common.AppNameKey()] = appSelectorName
	return origin
}

func GetAppLabelSelector(app *v1beta1.Application) string {
	selector := GetAppLabelSelectorMap(app)

	result := ""
	for k, v := range selector {
		result = fmt.Sprintf("%s=%s,%s", k, v, result)
	}

	return strings.TrimSuffix(result, ",")
}

func IsOldApp(app *v1beta1.Application) bool {
	selector := app.Spec.Selector.MatchLabels
	for k := range selector {
		if k == common.AppUIDKey() {
			return true
		}
	}
	return false
}

// GetAppUUID get application uuid from object
// For various reasons, we should try the old hardcoded key
func GetAppUUID(object metav1.Object) string {
	v := object.GetLabels()[common.AppUIDKey()]
	if v == "" {
		v = object.GetLabels()["app.alauda.io/uuid"]
	}
	return v
}

// GetAppRevision returns the application revision from the kubernets Object.
func GetAppRevision(object metav1.Object) string {
	return object.GetAnnotations()[common.AppRevisionKey()]
}

// SetApplicationNamespace set resource's namespace to application's namespace if not equal
// namespace: application's namespace, should not be ""
func (ks *KubernetesObject) SetApplicationNamespace(namespace string) {
	if ks.Namespace != "" && ks.Namespace != namespace {
		ks.SetNamespace(namespace)
	}
	if !common.ClusterScopeResourceKind[ks.Kind] {
		ks.SetNamespace(namespace)
	}
}

func (ks *KubernetesObject) GetAppNameSelector() map[string]string {
	appSelectorName := GetAppSelectorName(ks.GetName(), ks.GetNamespace())
	return map[string]string{
		common.AppNameKey(): appSelectorName,
	}
}

func (ks *KubernetesObject) GetAppNameSelectorString() string {
	appSelectorName := GetAppSelectorName(ks.GetName(), ks.GetNamespace())
	return fmt.Sprintf("%s=%s", common.AppNameKey(), appSelectorName)
}

func (ks *KubernetesObject) GetAppHistoryLimit() int {
	limit := 0
	anno := ks.GetAnnotations()
	if anno != nil && anno[common.AppHistoryLimitKey()] != "" {
		limit = cast.ToInt(anno[common.AppHistoryLimitKey()])
	}

	if limit == 0 {
		limit = common.ApplicationDefaultHistoryLimit
	}
	return limit
}

func AddApplicationComponentKind(app *v1beta1.Application, gk metav1.GroupKind) {
	ck := app.Spec.ComponentGroupKinds
	for _, k := range ck {
		// Ignore group here since the same kinds may have different groups
		if k.Kind == gk.Kind {
			return
		}
	}

	ck = append(ck, gk)
	app.Spec.ComponentGroupKinds = ck
}
