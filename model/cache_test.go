package model

import "testing"

func TestNameCacheKey(t *testing.T) {
	key := "k8s:name:474282bd-1085-4f72-9936-b3c1eb64af15::clusterrolebindings:system$controller$persistent-volume-binder"
	name := "system:controller:persistent-volume-binder"
	query := ParseNameKeyToQuery(key)
	if query.Name != name {
		t.Errorf("Error parse resource name: %s", query.Name)
	}
}
