package model

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/pkg/application/v1beta1"

	"github.com/ghodss/yaml"
	jsoniter "github.com/json-iterator/go"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	diff "github.com/yudai/gojsondiff"
	autoScalingV1 "k8s.io/api/autoscaling/v1"
	batchv1beta1 "k8s.io/api/batch/v1beta1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

type Object interface {
	metaV1.Object
	GroupVersionKind() schema.GroupVersionKind
}

// ObjectNameTuple identifies a kubernetes resource in a cluster
type ObjectNameTuple struct {
	Namespace string
	Name      string
}

type VisitObjectFunc func(obj *KubernetesObject, raw string) error

func IsSameObject(l Object, r Object) bool {
	if l.GetName() == r.GetName() && l.GroupVersionKind() == r.GroupVersionKind() {
		return true
	}
	return false
}

// ResourceDiffType is the type of resource diff.
type ResourceDiffType string

const (
	// ResourceDiffCreate is create type of resource diff.
	ResourceDiffCreate ResourceDiffType = "create"
	// ResourceDiffUpdate is update type of resource diff.
	ResourceDiffUpdate ResourceDiffType = "update"
	// ResourceDiffDelete is delete type of resource diff.
	ResourceDiffDelete ResourceDiffType = "delete"
)

// ResourceDiffItems contains object items of the resource diffs.
type ResourceDiffItems map[ResourceDiffType][]*KubernetesObject

// GetDiffResources returns create, update and delete resources in the items.
func (r ResourceDiffItems) GetDiffResources() (create []*KubernetesObject, update []*KubernetesObject, delete []*KubernetesObject) {
	create = r[ResourceDiffCreate]
	update = r[ResourceDiffUpdate]
	delete = r[ResourceDiffDelete]
	return
}

type KubernetesSpec JSONObject

type KubernetesStatus JSONObject

type ObjectMeta struct {
	metaV1.TypeMeta
	metaV1.ObjectMeta `json:"metadata"`
}

type KubernetesObject struct {
	unstructured.Unstructured

	// let old code run, only used for get
	Name            string                 `json:"-"`
	Kind            string                 `json:"kind" example:"<resource_api_version>"`
	Namespace       string                 `json:"-"`
	APIVersion      string                 `json:"apiVersion" example:"<resource_kind>"`
	Spec            map[string]interface{} `json:"-"`
	Labels          map[string]string      `json:"-"`
	Annotations     map[string]string      `json:"-"`
	UID             types.UID              `json:"-"`
	ResourceVersion string                 `json:"-"`
}

func (ks *KubernetesObject) DeepCopy() *KubernetesObject {
	newKO := *ks
	newKO.Unstructured = *ks.Unstructured.DeepCopy()
	return &newKO
}

func (ks *KubernetesObject) MarshalJSON() ([]byte, error) {
	return ks.Unstructured.MarshalJSON()
}

// override some methods
func (ks *KubernetesObject) SetName(name string) { ks.Unstructured.SetName(name); ks.Name = name }
func (ks *KubernetesObject) SetNamespace(namespace string) {
	ks.Unstructured.SetNamespace(namespace)
	ks.Namespace = namespace
}
func (ks *KubernetesObject) SetKind(kind string) { ks.Unstructured.SetKind(kind); ks.Kind = kind }
func (ks *KubernetesObject) SetAPIVersion(apiVersion string) {
	ks.Unstructured.SetAPIVersion(apiVersion)
	ks.APIVersion = apiVersion
}
func (ks *KubernetesObject) SetLabels(labels map[string]string) {
	ks.Unstructured.SetLabels(labels)
	ks.Labels = labels
}
func (ks *KubernetesObject) SetAnnotations(annotations map[string]string) {
	ks.Unstructured.SetAnnotations(annotations)
	ks.Annotations = annotations
}
func (ks *KubernetesObject) SetUID(uid types.UID) { ks.Unstructured.SetUID(uid); ks.UID = uid }
func (ks *KubernetesObject) SetResourceVersion(version string) {
	ks.Unstructured.SetResourceVersion(version)
	ks.ResourceVersion = version
}

func (ks *KubernetesObject) UnmarshalJSON(b []byte) error {
	err := ks.Unstructured.UnmarshalJSON(b)
	if err != nil {
		return err
	}

	ks.Name = ks.GetName()
	ks.Kind = ks.GetKind()
	ks.Namespace = ks.GetNamespace()
	ks.APIVersion = ks.GetAPIVersion()
	spec, ok := ks.GetField("spec").(map[string]interface{})
	if ok {
		ks.Spec = spec
	}
	ks.Labels = ks.GetLabels()
	ks.Annotations = ks.GetAnnotations()
	ks.UID = ks.GetUID()
	ks.ResourceVersion = ks.GetResourceVersion()

	return nil
}

func (ks *KubernetesObject) get(fields ...string) interface{} {
	return getNestedField(ks.Object, fields...)
}

func (ks *KubernetesObject) set(value interface{}, fields ...string) {
	setNestedField(ks.Object, value, fields...)
}

func (ks *KubernetesObject) GetField(fields ...string) interface{} {
	return ks.get(fields...)
}

func (ks *KubernetesObject) SetField(value interface{}, fields ...string) {
	ks.set(value, fields...)
}

func setNestedField(obj map[string]interface{}, value interface{}, fields ...string) {
	m := obj
	if len(fields) > 1 {
		for _, field := range fields[0 : len(fields)-1] {
			if _, ok := m[field].(map[string]interface{}); !ok {
				m[field] = make(map[string]interface{})
			}
			m = m[field].(map[string]interface{})
		}
	}
	m[fields[len(fields)-1]] = value
}

func getNestedField(obj map[string]interface{}, fields ...string) interface{} {
	var val interface{} = obj
	for _, field := range fields {
		if _, ok := val.(map[string]interface{}); !ok {
			return nil
		}
		val = val.(map[string]interface{})[field]
	}
	return val
}

// KubernetesObjectList describes a struct for kubernetes object list.
type KubernetesObjectList struct {
	Items      []*KubernetesObject `json:"items"`
	APIVersion string              `json:"apiVersion"`
	Kind       string              `json:"kind"`
}

type Event struct {
	// filled by source
	Type    string
	Reason  string
	Message string

	// filled by AlaudaResource
	Name      string
	Namespace string
	UID       string
}

// Note: if KubernetesObject cannot include all fields, use this
type KubernetesObjectSpecial struct {
	KubernetesObject `json:",inline"`
	Name             string `json:"name,omitempty"`
}

// copy from auto-scaling api group,
type CrossVersionObjectReference struct {
	// Kind of the referent; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds"
	Kind string `json:"kind" protobuf:"bytes,1,opt,name=kind"`
	// Name of the referent; More info: http://kubernetes.io/docs/user-guide/identifiers#names
	Name string `json:"name" protobuf:"bytes,2,opt,name=name"`
	// API version of the referent
	// +optional
	APIVersion string `json:"apiVersion,omitempty" protobuf:"bytes,3,opt,name=apiVersion"`
}

func UpdateAnnotations(obj interface{}, key, value string) error {
	object, err := meta.Accessor(obj)
	if err != nil {
		return err
	}
	anno := object.GetAnnotations()
	if anno == nil {
		anno = make(map[string]string)
	}
	anno[key] = value
	object.SetAnnotations(anno)

	return nil
}

func (ks *KubernetesObject) GetString() string {
	return ks.getString("/")
}

func (ks *KubernetesObject) DeleteLabel(key string) {
	l := ks.GetLabels()
	if l == nil {
		return
	}
	delete(l, key)
	ks.SetLabels(l)
}

func (ks *KubernetesObject) SetLabel(key, value string) {
	labels := ks.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	labels[key] = value
	ks.SetLabels(labels)
}

// SetExtraLabels add extra labels to object, overwrite if same key exist
func (ks *KubernetesObject) SetExtraLabels(extra map[string]string) {
	labels := ks.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	for k, v := range extra {
		labels[k] = v
	}
	ks.SetLabels(labels)
}

func (ks *KubernetesObject) SetAno(key, value string) {
	an := ks.GetAnnotations()
	if an == nil {
		an = map[string]string{}
	}
	an[key] = value
	ks.SetAnnotations(an)
}

func UpdateLabels(object metaV1.Object, key, value string) {
	labels := object.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
	}
	labels[key] = value
	object.SetLabels(labels)
}

func (ks *KubernetesObject) getString(split string) string {
	return strings.Join([]string{ks.GetNamespace(), ks.GetKind(), ks.GetName()}, split)
}

func (ksl *KubernetesObjectList) String() string {
	var itemsStr []string
	for _, item := range ksl.Items {
		itemsStr = append(itemsStr, fmt.Sprintf("%+v", *item))
	}
	return fmt.Sprintf("Items: [%s]", strings.Join(itemsStr, ","))
}

func GetResourceUIDAnnotation(object metaV1.Object) string {
	return object.GetAnnotations()[common.ResourceUIDKey()]
}

func GetAlaudaUID(object metaV1.Object) string {
	uid := GetResourceUIDAnnotation(object)
	if uid == "" {
		uid = string(object.GetUID())
	}
	return uid
}

func (ks *KubernetesObject) MatchRequirements(reqs []labels.Requirement) bool {
	set := labels.Set(ks.GetLabels())
	for _, req := range reqs {
		if !req.Matches(set) {
			return false
		}
	}
	return true
}

func (ks *KubernetesObject) UpdatePodEnv(svcName, svcID, appName string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Update pod template env error")
	}
	var cs []v1.Container
	for _, c := range podTemplateSpec.Spec.Containers {
		c = updateEnv(v1.EnvVar{Name: common.EnvServiceID, Value: svcName}, c)
		c = updateEnv(v1.EnvVar{Name: common.EncServiceName, Value: svcID}, c)
		if appName != "" {
			c = updateEnv(v1.EnvVar{Name: common.EnvAppName, Value: appName}, c)
		}
		cs = append(cs, c)
	}
	podTemplateSpec.Spec.Containers = cs
	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.set(result, "spec", "template")
	return nil
}

func updateEnv(e v1.EnvVar, c v1.Container) v1.Container {
	found := false
	for i, v := range c.Env {
		if v.Name == e.Name {
			v.Value = e.Value
			c.Env[i] = v
			found = true
			break
		}
	}
	if !found {
		c.Env = append(c.Env, e)
	}
	return c
}

func (ks *KubernetesObject) AddResourceLabelIfNeeded(orgName string) {

	labels := make(map[string]string)
	if config.GlobalConfig.Pod.ResourceLabel != "" {
		labels[config.GlobalConfig.Pod.ResourceLabel] = ks.getString("-")
	}
	if config.GlobalConfig.Pod.OrgLabel != "" && orgName != "" {
		labels[config.GlobalConfig.Pod.OrgLabel] = strings.Trim(orgName, " ")
	}
	if 0 != len(labels) {
		ks.UpdatePodTemplateLabels(labels)
	}
}

func (ks *KubernetesObject) UpdatePodTemplate(annotations, labels map[string]string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Update pod template annotations error")
	}
	if annotations != nil {
		if podTemplateSpec.Annotations == nil {
			podTemplateSpec.Annotations = make(map[string]string)
		}
		for k, v := range annotations {
			podTemplateSpec.Annotations[k] = v
		}
	}

	if labels != nil {
		if podTemplateSpec.Labels == nil {
			podTemplateSpec.Labels = make(map[string]string)
		}
		for k, v := range labels {
			podTemplateSpec.Labels[k] = v
		}
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	setNestedField(ks.Object, result, "spec", "template")
	return nil
}

func (ks *KubernetesObject) UpdatePodTemplateAnnotations(annotations map[string]string) error {
	return ks.UpdatePodTemplate(annotations, nil)
}

func (ks *KubernetesObject) UpdatePodTemplateLabels(labels map[string]string) error {
	return ks.UpdatePodTemplate(nil, labels)
}

func (ks *KubernetesObject) AddPodTemplateLabels(labels map[string]string) error {
	return ks.UpdatePodTemplate(nil, labels)
}

func (ks *KubernetesObject) RemovePodTemplateLabels(keys []string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Remove pod template labels error")
	}

	if podTemplateSpec.Labels == nil {
		return nil
	}
	for _, k := range keys {
		delete(podTemplateSpec.Labels, k)
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	setNestedField(ks.Object, result, "spec", "template")
	return nil
}

func (ks *KubernetesObject) GetPodReplicas() (int, error) {
	if ks.GetKind() == common.KubernetesKindDaemonSet {
		return 0, nil
	}
	return cast.ToIntE(getNestedField(ks.Object, "spec", "replicas"))
}

func (ks *KubernetesObject) SetPodReplicas(rs float64) error {
	if ks.GetKind() == common.KubernetesKindDaemonSet {
		return nil
	}
	ks.set(rs, "spec", "replicas")
	return nil
}

func (ks *KubernetesObject) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	data, ok := getNestedField(ks.Object, "spec", "template").(map[string]interface{})
	if !ok {
		return nil, errors.New("No PodTemplateSpec found in resource spec")
	}
	template := KubernetesSpec(data)
	podTemplateSpec, err := template.GetPodTemplateSpec()
	if err != nil {
		return nil, err
	}
	return podTemplateSpec, nil
}

// GetJobTemplateFromCronJob get job template from cronjob
func (ks *KubernetesObject) GetJobTemplateFromCronJob() (*batchv1beta1.JobTemplateSpec, error) {
	data, err := json.Marshal(getNestedField(ks.Object, "spec", "jobTemplate"))
	if err != nil {
		return nil, err
	}
	jobTemplateSpec := batchv1beta1.JobTemplateSpec{}
	err = json.Unmarshal(data, &jobTemplateSpec)
	return &jobTemplateSpec, err
}

func (ks *KubernetesObject) GetServiceSpec() (*v1.ServiceSpec, error) {
	data, err := json.Marshal(getNestedField(ks.Object, "spec"))
	if err != nil {
		return nil, err
	}
	spec := v1.ServiceSpec{}
	err = json.Unmarshal(data, &spec)
	return &spec, err
}

func (ks *KubernetesObject) GetApplicationSpec() (*v1beta1.ApplicationSpec, error) {
	data, err := json.Marshal(getNestedField(ks.Object, "spec"))
	if err != nil {
		return nil, err
	}
	var spec v1beta1.ApplicationSpec
	err = json.Unmarshal(data, &spec)
	return &spec, err
}

func (ks *KubernetesObject) GetHPASpec() (*autoScalingV1.HorizontalPodAutoscalerSpec, error) {
	data, err := json.Marshal(ks.get("spec"))
	if err != nil {
		return nil, err
	}
	spec := autoScalingV1.HorizontalPodAutoscalerSpec{}
	err = json.Unmarshal(data, &spec)
	return &spec, err
}

func (ks *KubernetesSpec) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	bt, err := json.Marshal(ks)
	if err != nil {
		return nil, err
	}
	ss := v1.PodTemplateSpec{}
	err = json.Unmarshal(bt, &ss)
	return &ss, err
}

func (object JSONObject) ToV1Affinity() (*v1.Affinity, error) {
	bt, err := json.Marshal(object)
	if err != nil {
		return nil, err
	}
	ss := v1.Affinity{}
	err = json.Unmarshal(bt, &ss)
	return &ss, err
}

func ToInterfaceMap(object interface{}) (result map[string]interface{}, err error) {
	var bt []byte
	if bt, err = json.Marshal(object); err != nil {
		return nil, err
	}
	if err = json.Unmarshal(bt, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (ks *KubernetesObject) GetServiceUUID() string {
	return ks.GetLabels()[common.SvcUIDKey()]
}

func (ks *KubernetesObject) ToBytes() ([]byte, error) {
	return json.Marshal(ks)
}

func BytesToKubernetesObject(bytes []byte) (*KubernetesObject, error) {
	var json = jsoniter.ConfigFastest
	var k KubernetesObject
	err := json.Unmarshal(bytes, &k)
	return &k, err
}

func (ks *KubernetesObject) ParseConfigMap(cm *v1.ConfigMap) error {
	bt, err := json.Marshal(cm)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	//ks.APIVersion = cm.APIVersion
	//ks.Kind = cm.Kind
	return nil
}

func (ks *KubernetesObject) ToPod() (*v1.Pod, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	ns := v1.Pod{}
	err = json.Unmarshal(bt, &ns)
	ns.Kind = string("Pod")
	ns.APIVersion = common.KubernetesAPIVersionV1
	return &ns, err
}

func (ks *KubernetesObject) ToApplication() (*v1beta1.Application, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	obj := v1beta1.Application{}
	err = json.Unmarshal(bt, &obj)
	obj.Kind = string(ApplicationType)
	obj.APIVersion = "app.k8s.io/v1beta1"
	return &obj, err
}

func (ks *KubernetesObject) ToV1Service() (*v1.Service, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, err
	}
	service := v1.Service{}
	err = json.Unmarshal(bt, &service)
	return &service, err
}

func (ks *KubernetesObject) ParseV1Service(service *v1.Service) error {
	bt, err := json.Marshal(service)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bt, ks)
	if err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ToV1PV() (*v1.PersistentVolume, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes pv resource to bytes error")
	}
	pv := v1.PersistentVolume{}
	err = json.Unmarshal(bt, &pv)
	return &pv, err
}

func (ks *KubernetesObject) ToV1PVC() (*v1.PersistentVolumeClaim, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes pvc resource to bytes error")
	}
	pvc := v1.PersistentVolumeClaim{}
	err = json.Unmarshal(bt, &pvc)
	return &pvc, err
}

func (ks *KubernetesObject) ParsePVC(pvc *v1.PersistentVolumeClaim) error {
	bt, err := json.Marshal(pvc)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

// CleanAutogenerated cleans the auto generated fields by
// kubernetes in the object.
func (ks *KubernetesObject) CleanAutogenerated() {
	ks.SetCreationTimestamp(metaV1.Time{})
	ks.SetOwnerReferences(nil)
	ks.SetResourceVersion("")
	ks.SetSelfLink("")
	unstructured.RemoveNestedField(ks.Object, "status")

	// Deal with 'spec.template.metadata.annotations.updateTimestamp' in
	// deployment added by Application udpate.
	if ks.GetKind() == "Deployment" {
		unstructured.RemoveNestedField(ks.Object, "spec", "template", common.KubernetesMetadataKey, common.KubernetesAnnotationsKey, "updateTimestamp")
	}
	// Remove application revision annotation in Application.
	if ks.GetKind() == "Application" {
		unstructured.RemoveNestedField(ks.Object, common.KubernetesMetadataKey, common.KubernetesAnnotationsKey, common.AppRevisionKey())
		unstructured.RemoveNestedField(ks.Object, common.KubernetesMetadataKey, common.KubernetesAnnotationsKey, common.AppHistoryLimitKey())
	}
}

func ToKubernetesObject(object interface{}) (*KubernetesObject, error) {
	var obj KubernetesObject
	data, err := json.Marshal(object)
	if err != nil {
		return &obj, err
	}
	if err := json.Unmarshal(data, &obj); err != nil {
		return nil, err
	} else {
		return &obj, nil
	}
}

func AppCrdToObject(app *v1beta1.Application) *KubernetesObject {
	bt, _ := json.Marshal(app)
	var obj KubernetesObject
	json.Unmarshal(bt, &obj)
	return &obj
}

// GenKubernetesApplicationEvent generate  a kubernetes application event
// Note: uuid means alauda uuid
func GenKubernetesApplicationEvent(ev *Event) v1.Event {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	event := v1.Event{
		TypeMeta: metaV1.TypeMeta{
			Kind:       common.KubernetesKindEvent,
			APIVersion: common.KubernetesAPIVersionV1,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      ev.Name + "-" + common.GenerateRandString(5),
			Namespace: ev.Namespace,
		},
		InvolvedObject: v1.ObjectReference{
			Kind:      common.KubernetesKindApplication,
			Name:      ev.Name,
			Namespace: ev.Namespace,
			UID:       types.UID(ev.UID),
		},
		Message: ev.Message,
		Reason:  ev.Reason,
		Source: v1.EventSource{
			Component: fmt.Sprintf("%s.%s", common.Component, hostname),
		},
		Count:          1,
		Type:           ev.Type,
		FirstTimestamp: metaV1.Time{Time: time.Now().UTC()},
		LastTimestamp:  metaV1.Time{Time: time.Now().UTC()},
	}
	return event
}

func RestResultToResource(result *rest.Result) (*KubernetesObject, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error for rest client result")
	}
	return BytesToKubernetesObject(bt)
}

func UnstructToObject(un *unstructured.Unstructured) *KubernetesObject {
	ks := &KubernetesObject{
		Unstructured:    *un,
		Kind:            un.GetKind(),
		APIVersion:      un.GetAPIVersion(),
		Name:            un.GetName(),
		Namespace:       un.GetNamespace(),
		Labels:          un.GetLabels(),
		Annotations:     un.GetAnnotations(),
		UID:             un.GetUID(),
		ResourceVersion: un.GetResourceVersion(),
	}
	spec, found, err := unstructured.NestedMap(un.Object, "spec")
	if found && err == nil {
		ks.Spec = spec
	}
	return ks
}

// DecodeToUnstructList decodes bytes to UnstructuredList. The code is copied
// from apimachinery package, and modified to use jsoniter as the json decoder.
func DecodeToUnstructList(data []byte, list *unstructured.UnstructuredList) error {
	type decodeList struct {
		Items []json.RawMessage
	}

	json := jsoniter.ConfigFastest
	var dList decodeList
	if err := json.Unmarshal(data, &dList); err != nil {
		return err
	}

	if err := json.Unmarshal(data, &list.Object); err != nil {
		return err
	}

	// For typed lists, e.g., a PodList, API server doesn't set each item's
	// APIVersion and Kind. We need to set it.
	listAPIVersion := list.GetAPIVersion()
	listKind := list.GetKind()
	itemKind := strings.TrimSuffix(listKind, "List")

	delete(list.Object, "items")
	list.Items = make([]unstructured.Unstructured, 0, len(dList.Items))
	for _, i := range dList.Items {
		unstruct := &unstructured.Unstructured{}
		if err := json.Unmarshal([]byte(i), &unstruct.Object); err != nil {
			return err
		}
		// This is hacky. Set the item's Kind and APIVersion to those inferred
		// from the List.
		if len(unstruct.GetKind()) == 0 && len(unstruct.GetAPIVersion()) == 0 {
			unstruct.SetKind(itemKind)
			unstruct.SetAPIVersion(listAPIVersion)
		}
		list.Items = append(list.Items, *unstruct)
	}
	return nil
}

func MatchLabelsToParamString(matchLabels map[string]string) string {
	var result []string
	for k, v := range matchLabels {
		result = append(result, fmt.Sprintf("%s=%s", k, v))
	}

	return strings.Join(result, ",")
}

func YAMLToObjects(yamlStr string) ([]*KubernetesObject, error) {
	var object []*KubernetesObject
	err := yaml.Unmarshal([]byte(yamlStr), &object)
	if err != nil {
		return nil, err
	}
	return object, nil
}

// ObjectKeyWithNamespace returns the key string of a kubernetes object with namespace.
// The key format is '{kind}:{namespace}:{name}'.
func ObjectKeyWithNamespace(obj *KubernetesObject) string {
	return fmt.Sprintf("%s:%s:%s", obj.GetKind(), obj.GetNamespace(), obj.GetName())
}

// ObjectKeyWithoutNamespace returns the key string of a kubernetes object without namespace.
// The key format is '{kind}:{name}'.
func ObjectKeyWithoutNamespace(obj *KubernetesObject) string {
	return fmt.Sprintf("%s:%s", obj.GetKind(), obj.GetName())
}

// MakeResourcesMap makes a kubernetes resource map from a resource array, and the key is
// generated by 'keyFunc'.
func MakeResourcesMap(resources []*KubernetesObject, keyFunc ObjectKeyFunc) map[string]*KubernetesObject {
	resourcesMap := make(map[string]*KubernetesObject)
	for _, object := range resources {
		resourcesMap[keyFunc(object)] = object
	}
	return resourcesMap
}

// ObjectKeyFunc used to get key string of an object.
type ObjectKeyFunc func(obj *KubernetesObject) string

// ComputeResourceDiffs computes the resource diffs between old and new resource list.
// Params:
//  - oldResources: old resource list.
//  - newResources: new resource list.
//  - keyFunc: a function used to get the key of an object.
func ComputeResourceDiffs(oldResources []*KubernetesObject, newResources []*KubernetesObject, keyFunc ObjectKeyFunc) (ResourceDiffItems, error) {
	oldResourcesMap := MakeResourcesMap(oldResources, keyFunc)
	newResourcesMap := MakeResourcesMap(newResources, keyFunc)

	resourcediffs := make(ResourceDiffItems)
	var existResources []*KubernetesObject
	for _, object := range oldResources {
		if _, exist := newResourcesMap[keyFunc(object)]; !exist {
			resourcediffs[ResourceDiffDelete] = append(
				resourcediffs[ResourceDiffDelete], object)
		} else {
			existResources = append(existResources, object)
		}
	}

	for _, object := range existResources {
		key := keyFunc(object)
		oldByte, _ := oldResourcesMap[key].MarshalJSON()
		newByte, _ := newResourcesMap[key].MarshalJSON()
		differ := diff.New()
		d, err := differ.Compare(oldByte, newByte)
		if err != nil {
			return nil, err
		}
		if d.Modified() {
			resourcediffs[ResourceDiffUpdate] = append(
				resourcediffs[ResourceDiffUpdate], newResourcesMap[key])
		}
	}

	for _, object := range newResources {
		if _, exist := oldResourcesMap[keyFunc(object)]; !exist {
			resourcediffs[ResourceDiffCreate] = append(
				resourcediffs[ResourceDiffCreate], object)
		}
	}
	if len(resourcediffs) == 0 {
		return nil, nil
	}
	return resourcediffs, nil
}
