package main

import (
	"flag"
	"os"

	"krobelus/cmd"
	"krobelus/config"

	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var (
	app *cli.App
)

func init() {
	// Initialise a CLI app
	app = cli.NewApp()
	app.Name = "krobelus"
	app.Usage = "Alauda Kubernetes Resource Manager"
	app.Author = "Alauda"
	app.Email = "info@alauda.io"
	app.Version = "1.0.0"
}

func setGlogFlag() {
	flag.Set("logtostderr", "true")
	//flag.Set("stderrthreshold", "WARNING")
	flag.Set("v", viper.GetString(config.KeyGlogLevel))
	// This is wa
	flag.Parse()
}

func main() {
	//TODO: merge init config
	config.InitConfig()

	if err := config.InitContentConfig(); err != nil {
		panic(err)
	}

	config.SetLogger()
	setGlogFlag()

	app.Commands = []cli.Command{
		{
			Name:  "api",
			Usage: "launch krobelus api",
			Action: func(c *cli.Context) {
				cmd.API()
			},
		},
	}

	// Run the CLI app
	if err := app.Run(os.Args); err != nil {
		log.Infof(err.Error())
	}
}
