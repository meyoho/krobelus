package infra

import (
	"fmt"
	"time"

	"krobelus/config"

	"github.com/Jeffail/gabs"
	"github.com/juju/errors"
	"github.com/levigross/grequests"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

// region info stuff
const (
	// RegionPlatformV3 support new k8s
	RegionPlatformV3 = "v3"
	// RegionPlatformV4 support new region info and new k8s
	RegionPlatformV4 = "v4"
)

func getRegionInfoFromFurion(regionID string) (string, error) {
	cfg := config.GlobalConfig.Furion
	pathPrefix := fmt.Sprintf("%s/%s/regions", cfg.Endpoint, cfg.APIVersion)
	url := fmt.Sprintf("%s/%s", pathPrefix, regionID)
	resp, err := grequests.Get(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(cfg.Timeout) * time.Second})
	if err != nil {
		return "", err
	}

	data := resp.String()
	if resp.StatusCode != 200 {
		return "", errors.New(fmt.Sprintf("Retrieve region get: %d %s", resp.StatusCode, data))
	}

	if data == "" {
		return "", errors.New(fmt.Sprintf("Empty region info found from furion: %s", regionID))
	}
	return data, nil
}

func getKubernetesConfig(regionStr string) (*KubernetesConfig, error) {
	platformVersion, err := getPlatformVersion(regionStr)
	if err != nil {
		return nil, err
	}
	var keys []string
	if platformVersion == RegionPlatformV3 {
		keys = []string{
			"features.kubernetes.endpoint",
			"features.kubernetes.token",
		}
	} else if platformVersion == RegionPlatformV4 {
		keys = []string{
			"attr.kubernetes.endpoint",
			"attr.kubernetes.token",
		}
	} else {
		return nil, fmt.Errorf("unsupported platform version for region: %s", platformVersion)
	}
	data, err := getPaths(regionStr, keys)
	if err != nil {
		return nil, err
	}
	return &KubernetesConfig{
		Endpoint: data[0],
		Token:    data[1],
	}, nil

}

func getPlatformVersion(region string) (string, error) {
	return getPath(region, "platform_version")
}

// getPath retrieve single value from a json string
// support string/number data
func getPath(region string, path string) (string, error) {
	jsonData, err := gabs.ParseJSON([]byte(region))
	if err != nil {
		return "", err
	}
	return getPathFromJSON(jsonData, path)
}

func getPathFromJSON(jsonData *gabs.Container, path string) (string, error) {
	result := jsonData.Path(path).Data()
	value, ok := result.(string)
	if ok {
		return value, nil
	}
	number, ok := result.(float64)
	if ok {
		return cast.ToString(number), nil
	} else {
		return "", fmt.Errorf("get path %s data for region error", path)
	}
}

func getPaths(region string, paths []string) ([]string, error) {
	jsonData, err := gabs.ParseJSON([]byte(region))
	if err != nil {
		return nil, err
	}

	var results []string
	for _, path := range paths {
		value, err := getPathFromJSON(jsonData, path)
		if err != nil {
			return nil, err
		}
		results = append(results, value)
	}
	return results, nil
}

func getRegionInfo(regionID string) (string, error) {
	if regionID == "" {
		return "", errors.New("empty region uuid when retrieve region info")
	}
	logger := logrus.WithField("region_id", regionID)
	regionStr, err := getRegionInfoFromFurion(regionID)
	if err != nil {
		return "", errors.Annotate(err, "retrieve region info from furion error.")
	}
	logger.Debugf("Retrieved region info %+v", regionStr)

	return regionStr, nil
}

type KubernetesConfig struct {
	Endpoint string
	Token    string
}

// GetKubernetesConfig get kubernetes config from cache or furion.
func GetKubernetesConfig(regionID string) (*KubernetesConfig, error) {
	regionStr, err := getRegionInfo(regionID)
	if err != nil {
		return nil, err
	}
	return getKubernetesConfig(regionStr)
}

// GetKubernetesVersion get kubernetes version fro region info
