package transition

import (
	"krobelus/kubernetes/fake"
	"krobelus/model"
	"krobelus/tests"
	"testing"
)

func TestBuildOwnerRef(t *testing.T) {
	application, _ := model.BytesToKubernetesObject(tests.LoadFile("../../tests/fixtures/application.json"))
	deployment, _ := model.BytesToKubernetesObject(tests.LoadFile("../../tests/fixtures/status/deployment.json"))
	pv, _ := model.BytesToKubernetesObject(tests.LoadFile("../../tests/fixtures/pv.json"))

	var objs []*model.KubernetesObject
	objs = append(objs, deployment)
	objs = append(objs, pv)

	var client *fake.KubeClient
	BuildOwnerRef(application, objs, "50077-09988-1233", client)

	APIVersion := deployment.GetOwnerReferences()[0].APIVersion
	Kind := deployment.GetOwnerReferences()[0].Kind
	Name := deployment.GetOwnerReferences()[0].Name

	if APIVersion != "app.k8s.io/v1beta1" || Kind != "Application" || Name != "hello-world" {
		t.Error("BuildOwnerRef error")
	}

	if pvOwnerReference := pv.GetOwnerReferences(); pvOwnerReference != nil {
		t.Error("BuildOwnerRef error.")
	}

}
