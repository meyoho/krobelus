package transition

import (
	"bytes"
	"encoding/json"
	"sort"

	"krobelus/kubernetes"
	"krobelus/model"

	"github.com/gin-gonic/gin"
	"gopkg.in/yaml.v2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var (
	YamlContentType = []string{"application/x-yaml; charset=utf-8"}
)

// BuildOwnerRef build ownerReference when create resource
func BuildOwnerRef(owner *model.KubernetesObject, items []*model.KubernetesObject, ownerUID types.UID, client kubernetes.DiscoveryInterface) {
	controller := true
	blockOwnerDel := true
	reference := metav1.OwnerReference{
		APIVersion:         owner.APIVersion,
		Kind:               owner.Kind,
		Name:               owner.Name,
		UID:                ownerUID,
		Controller:         &controller,
		BlockOwnerDeletion: &blockOwnerDel,
	}

	var references []metav1.OwnerReference
	references = append(references, reference)

	for _, object := range items {
		name, _ := client.GetResourceByKind(object.Kind)
		if IsNamespaceScoped, _ := client.IsNamespaceScoped(name); IsNamespaceScoped {
			object.SetOwnerReferences(references)
		}
	}
}

func KubernetesJSONToYamlBytes(resources []*model.KubernetesObject) ([]byte, error) {
	// yaml order by resource namespace\name\kind
	byNamespace := func(c1, c2 *model.KubernetesObject) bool {
		return c1.Namespace < c2.Namespace
	}
	byName := func(c1, c2 *model.KubernetesObject) bool {
		return c1.Name < c2.Name
	}
	byKind := func(c1, c2 *model.KubernetesObject) bool {
		return c1.Kind < c2.Kind
	}
	orderedBy(byNamespace, byName, byKind).Sort(resources)

	var result []byte
	for _, rs := range resources {
		raw, _ := json.Marshal(rs)
		item := gin.H{}
		err := json.Unmarshal(raw, &item)
		if err != nil {
			return nil, err
		}
		if _, ok := item["metadata"]; ok {
			delete(item["metadata"].(map[string]interface{}), "creationTimestamp")
		}
		if _, ok := item["spec"]; ok {
			spec := item["spec"].(map[string]interface{})
			template, ok := spec["template"].(map[string]interface{})
			if ok {
				metadata := template["metadata"].(map[string]interface{})
				delete(metadata, "creationTimestamp")
				template["metadata"] = metadata
				spec["template"] = template
				item["spec"] = spec
			}
		}
		if _, ok := item["status"]; ok {
			delete(item, "status")

		}
		raw, err = yaml.Marshal(item)
		if err != nil {
			return nil, err
		}
		result = bytesCombine(result, raw)
	}
	return result, nil
}

func bytesCombine(pBytes ...[]byte) []byte {
	return bytes.Join(pBytes, []byte("---\n"))
}

type lessFunc func(p1, p2 *model.KubernetesObject) bool
type multiSorter struct {
	resources []*model.KubernetesObject
	less      []lessFunc
}

// Sort sorts the argument slice according to the less functions passed to OrderedBy.
func (ms *multiSorter) Sort(resources []*model.KubernetesObject) {
	ms.resources = resources
	sort.Sort(ms)
}

// Len is part of sort.Interface.
func (ms *multiSorter) Len() int {
	return len(ms.resources)
}

// Swap is part of sort.Interface.
func (ms *multiSorter) Swap(i, j int) {
	ms.resources[i], ms.resources[j] = ms.resources[j], ms.resources[i]
}

// Less is part of sort.Interface. It is implemented by looping along the
// less functions until it finds a comparison that discriminates between
// the two items (one is less than the other). Note that it can call the
// less functions twice per call. We could change the functions to return
// -1, 0, 1 and reduce the number of calls for greater efficiency: an
// exercise for the reader.
func (ms *multiSorter) Less(i, j int) bool {
	p, q := ms.resources[i], ms.resources[j]
	// Try all but the last comparison.
	var k int
	for k = 0; k < len(ms.less)-1; k++ {
		less := ms.less[k]
		switch {
		case less(p, q):
			// p < q, so we have a decision.
			return true
		case less(q, p):
			// p > q, so we have a decision.
			return false
		}
		// p == q; try the next comparison.
	}
	// All comparisons to here said "equal", so just return whatever
	// the final comparison reports.
	return ms.less[k](p, q)
}

// orderedBy returns a Sorter that sorts using the less functions, in order.
// Call its Sort method to sort the data.
func orderedBy(less ...lessFunc) *multiSorter {
	return &multiSorter{
		less: less,
	}
}
