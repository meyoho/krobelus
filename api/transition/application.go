package transition

import (
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/kubernetes"
	"krobelus/model"
	"krobelus/pkg/application/v1beta1"
	"krobelus/store"

	"github.com/juju/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// getAppResource get application resource from list of objects, return nil if not found
func getAppResource(objects []*model.KubernetesObject) *model.KubernetesObject {
	for _, item := range objects {
		if item.Kind == common.KubernetesKindApplication {
			return item
		}
	}
	return nil
}

// TranslateApplicationCreatePayload translate app payload for create
// 1. generate crd from requests
// 2. modify origin resource
// 3. also do some verify
// 4. If user have provide it's own crd, use it
// 5. If request contains List object, parse it's items first.
func TranslateApplicationCreatePayload(request *model.ApplicationRequest) error {
	uuid := common.NewUUID()
	var results []*model.KubernetesObject

	// check label value length
	if request.Resource.Name == "" {
		return fmt.Errorf("missing application resource name field")
	}
	if request.Resource.Namespace == "" {
		return fmt.Errorf("missing application resource namespace field")
	}
	if len(request.Resource.Name)+len(request.Resource.Namespace) > 63 {
		return fmt.Errorf("application name or namespace too long")
	}

	newList, err := model.FlatObjectList(request.Kubernetes)
	if err != nil {
		return errors.Annotate(err, "flat List object error")
	}

	request.Kubernetes = newList
	crd := getAppCrdResource(request)

	for _, object := range request.Kubernetes {
		if object == nil || object.Kind == common.KubernetesKindApplication {
			continue
		}

		if common.StringInSlice(object.Kind, common.AppUnsupportedResourceKind) {
			return fmt.Errorf("unsupported resource kind in application: %s", object.Kind)
		}

		groupVersion, err := schema.ParseGroupVersion(object.APIVersion)
		if err != nil {
			return err
		}

		// reset auto-generated fields
		object.SetResourceVersion("")
		object.SetUID("")
		object.SetSelfLink("")
		object.SetGeneration(0)
		UpdateObjectForApplication(object, crd, uuid)

		results = append(results, object)

		model.AddApplicationComponentKind(crd,
			metav1.GroupKind{
				Kind:  object.Kind,
				Group: groupVersion.Group})
	}

	request.Crd = model.AppCrdToObject(crd)
	request.Kubernetes = results
	request.Crd.SetLabel(common.AppUIDKey(), uuid)
	model.UpdateAnnotations(request.Crd, common.AppHistoryLimitKey(), fmt.Sprintf("%d", common.ApplicationDefaultHistoryLimit))
	return nil
}

// getAppCrdResource get app crd resource and object from requests
func getAppCrdResource(request *model.ApplicationRequest) (crd *v1beta1.Application) {
	// If user provide crd, use it and merge with the auto-generated one
	crd = request.Resource.ToAppCrd()

	inputAppObject := getAppResource(request.Kubernetes)
	if inputAppObject != nil {
		input := inputAppObject.ToAppCrd()
		model.MergeWithInputAppCrd(input, crd)
		crd = input
	}
	return crd
}

// UpdateObjectForApplication will update object for application, eg: add labels, annotations....
// 1. remove auto-generated fields'value (uid,resource version....)
// 2. set objects'namespace to app's
// 3. set labels: app name/ app uuid
// 4. If is pod controller, update pod template/ pod env/ pod labels
// 5. If this is a known cluster scope resource, unset it's namespace
func UpdateObjectForApplication(object *model.KubernetesObject, app *v1beta1.Application, uuid string) {
	object.SetApplicationNamespace(app.GetNamespace())

	selector := model.GetAppSelectorWithName(app)
	object.SetExtraLabels(selector)
	object.SetLabel(common.AppUIDKey(), uuid)

	//Note: The origin user of service name/uuid label is dd-agent/alb. In the future, both will be replaced
	// or discarded. dd-agent will be modified to support new app name label, and alb will use kubernetes service
	// directly
	if common.IsPodController(object.Kind) {
		// object.SetLabel(common.LabelServiceName, object.Name)
		//object.SetLabel(common.GetSvcUidLabel(), common.NewUUID())
		object.UpdatePodTemplateLabels(selector)
		ano := map[string]string{
			"updateTimestamp": time.Now().Format(time.RFC3339),
		}
		object.UpdatePodTemplateAnnotations(ano)
	}

	if _, exist := common.ClusterScopeResourceKind[object.Kind]; exist {
		object.Namespace = ""
	}
}

func getApplicationUpdateInputCrd(request *model.ApplicationRequest, oldApp *model.Application) (inputCrd *model.KubernetesObject) {
	if request.Crd != nil {
		inputCrd = request.Crd
	} else {
		for _, item := range request.Kubernetes {
			if model.IsSameObject(item, &(oldApp.Crd)) {
				inputCrd = item
				break
			}
		}
	}

	if inputCrd == nil {
		inputCrd = &oldApp.Crd
	}
	return
}

// Translate app payload for update
func TranslateApplicationUpdatePayload(s store.AppStoreInterface, request *model.ApplicationRequest,
	oldApp *model.Application, client kubernetes.DiscoveryInterface) (*v1beta1.Application, error) {
	newUUID := false
	uuid := oldApp.GetUUID()
	if uuid == "" {
		// The old app crd may come from others sources(helm...),
		uuid = common.NewUUID()
		newUUID = true
		// return nil, fmt.Errorf("cannot parse uuid from application")
	}

	var inputCrd *model.KubernetesObject
	var newCrd *v1beta1.Application

	newList, err := model.FlatObjectList(request.Kubernetes)
	if err != nil {
		return nil, errors.Annotate(err, "flat List object error")
	}
	request.Kubernetes = newList

	inputCrd = getApplicationUpdateInputCrd(request, oldApp)

	inputCrd.SetLabel(common.AppUIDKey(), uuid)
	if newUUID {
		inputCrd.SetAno(common.AppNewUUIDKey(), "true")
	}

	newCrd = inputCrd.ToAppCrd()
	newCrd.SetResourceVersion(oldApp.Crd.GetResourceVersion())
	newCrd.SetUID(oldApp.Crd.GetUID())
	newCrd.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	newCrd.Spec.AssemblyPhase = v1beta1.Succeeded
	model.UpdateApplicationWithInputResource(newCrd, &request.Resource)

	var results []*model.KubernetesObject

	for _, object := range request.Kubernetes {
		if object == nil || object.Kind == common.KubernetesKindApplication {
			continue
		}

		err := translateApplicationUpdateObject(s, object, newCrd, uuid)
		if err != nil {
			return nil, err
		}

		results = append(results, object)
	}

	request.Kubernetes = results
	BuildOwnerRef(inputCrd, request.Kubernetes, newCrd.GetUID(), client)

	model.UpdateAnnotations(newCrd, common.AppHistoryLimitKey(), fmt.Sprintf("%d", common.ApplicationDefaultHistoryLimit))
	return newCrd, nil
}

func translateApplicationUpdateObject(s store.AppStoreInterface, object *model.KubernetesObject,
	newCrd *v1beta1.Application, appUUID string) error {
	if common.StringInSlice(object.Kind, common.AppUnsupportedResourceKind) {
		return fmt.Errorf("unsupported resource kind in application: %s", object.Kind)
	}

	groupVersion, err := schema.ParseGroupVersion(object.APIVersion)
	if err != nil {
		return err
	}
	if _, err := s.GetResourceTypeByKind(object.Kind); err != nil {
		return err
	}

	UpdateObjectForApplication(object, newCrd, appUUID)

	if model.IsOldApp(newCrd) {
		// reset uuid key to avoid key change, may be duplicated
		if common.IsPodController(object.Kind) {
			// old app use uuid in pod template labels and selector
			object.UpdatePodTemplateLabels(map[string]string{
				common.AppUIDKey(): appUUID,
			})
			// set selector to nil to let it automatically fulfilled by kubernetes
			//delete(object.Spec, "selector")
			object.SetField(nil, "spec", "selector")
		}

	}

	model.AddApplicationComponentKind(newCrd,
		metav1.GroupKind{
			Kind:  object.Kind,
			Group: groupVersion.Group})
	return nil
}

// Translate app payload for import
//TODO: update env for pod controller
// TODO: check namespace
func TranslateApplicationImportPayload(request *model.ApplicationImportResourcesRequest, app *model.Application,
	client kubernetes.DiscoveryInterface) error {
	appCrd := app.Crd.ToAppCrd()

	cks := make(map[string]bool)
	ckKeyFunc := func(group, kind string) string {
		return group + "." + kind
	}
	for _, ck := range appCrd.Spec.ComponentGroupKinds {
		cks[ckKeyFunc(ck.Group, ck.Kind)] = true
	}

	newList, err := model.FlatObjectList(request.Kubernetes)
	if err != nil {
		return errors.Annotate(err, "flat List object error")
	}
	request.Kubernetes = newList

	for _, obj := range request.Kubernetes {
		if obj == nil {
			continue
		}

		if common.StringInSlice(obj.Kind, common.AppUnsupportedResourceKind) {
			return fmt.Errorf("unsupported resource kind in application: %s", obj.Kind)
		}

		groupVersion, err := schema.ParseGroupVersion(obj.APIVersion)
		if err != nil {
			return err
		}

		ckKey := ckKeyFunc(groupVersion.Group, obj.Kind)
		if _, exist := cks[ckKey]; !exist {
			cks[ckKey] = true
			appCrd.Spec.ComponentGroupKinds = append(appCrd.Spec.ComponentGroupKinds, metav1.GroupKind{
				Kind:  obj.Kind,
				Group: groupVersion.Group,
			})
		}
	}
	app.Crd = *model.AppCrdToObject(appCrd)
	BuildOwnerRef(&app.Crd, request.Kubernetes, app.Crd.GetUID(), client)
	return nil
}

// Translate app payload for export
// TODO: update pod env for pod controller
func TranslateApplicationExportPayload(request *model.ApplicationExportResourcesRequest, app *model.Application) error {
	appCrd := app.Crd.ToAppCrd()

	cks := make(map[string]bool)
	ckKeyFunc := func(group, kind string) string {
		return group + "." + kind
	}

	getKey := func(obj *model.KubernetesObject) string {
		return fmt.Sprintf("%s:%s:%s", obj.Kind, obj.Namespace, obj.Name)
	}
	// Make a map using gvk as key for fast searching the object.
	removeResourcesMap := make(map[string]*model.KubernetesObject)
	for _, object := range request.Kubernetes {
		if _, exist := common.ClusterScopeResourceKind[object.Kind]; exist {
			object.SetNamespace("")
		} else {
			object.SetNamespace(app.Crd.Namespace)
		}
		removeResourcesMap[getKey(object)] = object
	}

	appCrd.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	for _, obj := range app.Kubernetes {
		if _, exist := removeResourcesMap[getKey(obj)]; exist {
			continue
		}
		groupVersion, err := schema.ParseGroupVersion(obj.APIVersion)
		if err != nil {
			return err
		}

		ckKey := ckKeyFunc(groupVersion.Group, obj.Kind)
		if _, exist := cks[ckKey]; !exist {
			cks[ckKey] = true
			appCrd.Spec.ComponentGroupKinds = append(appCrd.Spec.ComponentGroupKinds, metav1.GroupKind{
				Kind:  obj.Kind,
				Group: groupVersion.Group,
			})
		}
	}
	app.Crd = *model.AppCrdToObject(appCrd)
	return nil
}
