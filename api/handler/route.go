package handler

import (
	"krobelus/api/handler/direct"

	"github.com/gin-gonic/gin"
)

func AddRoutes(r *gin.Engine) {

	direct.AddRoutes(r)

	r.GET("/_ping", Ping)
	r.GET("/", Ping)
	r.GET("/_auth_ping", Ping)
	r.GET("/_diagnose", Diagnose)

}
