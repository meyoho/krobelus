package direct

import (
	"net/http"
	"regexp"

	misc "krobelus/api/handler/misc"
	v1 "krobelus/api/handler/v1"
	v2origin "krobelus/api/handler/v2"
	"krobelus/common"

	"github.com/gin-gonic/gin"
)

// AddRoutes add direct route to gin.
// These apis will receive request from the new API Gateway and return response similar to
// the old ones.  The path will look likes the v1's, but the cluster means cluster name here
func AddRoutes(r *gin.Engine) {
	// Define app routes by regex path.
	v2appRoutes := []struct {
		Method  string
		Pattern *regexp.Regexp
		Handler gin.HandlerFunc
	}{
		{
			http.MethodPost,
			regexp.MustCompile(`^/v2/kubernetes/clusters/[^/]+/applications$`),
			v2origin.CreateApplication,
		},
		{
			http.MethodPut,
			regexp.MustCompile(`^/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/?$`),
			v2origin.UpdateApplication,
		},
		{
			http.MethodPut,
			regexp.MustCompile(`^/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/start$`),
			v2origin.StartStopApplication,
		},
		{
			http.MethodPut,
			regexp.MustCompile(`^/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/stop$`),
			v2origin.StartStopApplication,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/?$`),
			v2origin.GetApplication,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`^/v2/kubernetes/clusters/[^/]+/applications/?$`),
			v2origin.ListApplication,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`^/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/?$`),
			v2origin.ListApplication,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/yaml$`),
			v2origin.GetApplicationYAML,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/yaml$`),
			v2origin.GetApplicationYAML,
		},
		{
			http.MethodDelete,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)$`),
			v2origin.DeleteApplication,
		},
		{
			http.MethodPut,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/add$`),
			v2origin.ImportResourcesToApplication,
		},
		{
			http.MethodPut,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/remove$`),
			v2origin.ExportResourcesFromApplication,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/pods$`),
			v2origin.GetApplicationPods,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/status$`),
			v2origin.GetApplicationStatus,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/address$`),
			v2origin.GetApplicationAddress,
		},
		{
			http.MethodPost,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applications/(?P<namespace>[^/]+)/(?P<name>[^/]+)/rollback$`),
			v2origin.RollbackApplication,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applicationhistories/(?P<namespace>[^/]+)/(?P<name>[^/]+)$`),
			v2origin.GetApplicationHistoryDetail,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/applicationhistories/(?P<namespace>[^/]+)$`),
			v2origin.ListApplicationHistory,
		},
		{
			http.MethodPost,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/resources/?`),
			misc.BatchCreateResource,
		},
		{
			http.MethodGet,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/topology/(?P<namespace>[^/]+)/(?P<kind>[^/]+)/(?P<name>[^/]+)$`),
			v2origin.GetResourceTopology,
		},
		{
			http.MethodPost,
			regexp.MustCompile(`/v2/kubernetes/clusters/[^/]+/general-namespaces/?`),
			v2origin.CreateGeneralNamespace,
		},
	}
	routeForApp := func() gin.HandlerFunc {
		return func(c *gin.Context) {
			if common.StringInSlice(
				c.Param("type"),
				[]string{
					"applications",
					"applicationhistories",
					"resources",
					"topology",
					"general-namespaces"}) {
				var handler gin.HandlerFunc
				for _, route := range v2appRoutes {
					if c.Request.Method != route.Method {
						continue
					}
					match := route.Pattern.FindStringSubmatch(c.Request.URL.Path)
					if len(match) == 0 {
						continue
					}
					handler = route.Handler
					// Remove the 'name' param added before
					newParams := gin.Params{}
					for _, p := range c.Params {
						if p.Key != "name" {
							newParams = append(newParams, p)
						}
					}
					// Inject the new path param
					for i, name := range route.Pattern.SubexpNames() {
						if i != 0 && name != "" {
							newParams = append(newParams, gin.Param{
								Key:   name,
								Value: match[i],
							})
						}
					}
					c.Params = newParams
					break
				}
				if handler != nil {
					handler(c)
					c.Abort()
				}
			}
		}
	}

	v2 := r.Group("/v2/kubernetes/clusters").Use(ResponseRewrite(), routeForApp())
	{
		v2.POST("/:cluster/:type", v1.ProcessResource)
		v2.POST("/:cluster/:type/*name", v1.ProcessResource)
		v2.GET("/:cluster/:type", v1.ListResource)
		v2.GET("/:cluster/:type/*name", v1.GetResource)
		v2.PUT("/:cluster/:type/*name", v1.ProcessResource)
		v2.PATCH("/:cluster/:type/*name", v1.ProcessResource)
		v2.DELETE("/:cluster/:type/*name", v1.ProcessResource)
	}

	misc := r.Group("/v2/misc/clusters")
	{
		misc.GET("/:cluster/resourcetypes", v2origin.GetClusterResourceTypes)
		misc.POST("/:cluster/manual/cronjobs/:namespace/:name", v2origin.CreateJobFromCronjob)
	}
	paas := r.Group("/v2/paas/clusters")
	{
		paas.GET("/:cluster/namespaces/:namespace/workloads/", v2origin.ListWorkload)
	}
}
