package direct

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strings"

	"krobelus/api/handler/util"
	v1 "krobelus/api/handler/v1"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"github.com/gin-gonic/gin"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type bodyWriter struct {
	gin.ResponseWriter
	bodyCache *bytes.Buffer
}

// Write only writes to a buffer
func (w bodyWriter) Write(b []byte) (int, error) {
	return w.bodyCache.Write(b)
	// return w.ResponseWriter.Write(b)
}

// bodyAllowedForStatus is a copy of http.bodyAllowedForStatus non-exported function.
func bodyAllowedForStatus(status int) bool {
	switch {
	case status >= 100 && status <= 199:
		return false
	case status == http.StatusNoContent:
		return false
	case status == http.StatusNotModified:
		return false
	}
	return true
}

// ResponseRewrite is used to modify http response for v1 api to adapt to v2/kubernetes api
func ResponseRewrite() gin.HandlerFunc {
	return func(c *gin.Context) {
		logger := common.GetLogger(c)
		logger.Debugf("Enter middleware ResponseRewrite...")
		originWriter := c.Writer
		c.Writer = &bodyWriter{bodyCache: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Next()
		bw, ok := c.Writer.(*bodyWriter)
		if !ok {
			logger.Warning("get custom bodyWriter error")
			return
		}

		c.Writer = originWriter
		body := bw.bodyCache.Bytes()

		if bodyAllowedForStatus(c.Writer.Status()) && bw.bodyCache.Len() > 0 {
			var result []byte
			var err error
			if common.StringInSlice(c.Param("type"), []string{"applications", "applicationhistories"}) {
				result, err = newAppBody(body, c)
			} else {
				result, err = newBody(body, c)
			}
			if err != nil {
				logger.Warning("rewrite body error: ", err.Error())
			} else {
				logger.Debugf("rewrite response")
				body = result
			}
		}
		c.Data(c.Writer.Status(), "application/json", body)

	}

}

type single struct {
	Actions    []string                `json:"resource_actions"`
	Kubernetes *model.KubernetesObject `json:"kubernetes"`
}

// PaginationList represents paging result
type list struct {
	Count    int      `json:"count"`
	PageSize int      `json:"page_size"`
	Next     *string  `json:"next"`
	Previous *string  `json:"previous"`
	NumPages *int     `json:"num_pages"`
	Continue *string  `json:"continue"`
	Results  []single `json:"results"`
}

// newBody create new response body from old bytes
func newBody(old []byte, c *gin.Context) ([]byte, error) {
	logger := common.GetLogger(c)

	// try single object first,
	// try kubernetes object
	// get/post/update...
	obj, err := model.BytesToKubernetesObject(old)
	if err != nil || strings.HasSuffix(obj.GetKind(), "List") {

		var ul unstructured.UnstructuredList
		var data util.PaginationList

		// fucking stupid code
		err1 := json.Unmarshal(old, &data)
		err2 := json.Unmarshal(old, &ul)

		if err1 != nil && err2 != nil {
			return old, err
		}

		if err1 != nil && err2 == nil {
			params := v1.GetParams(c, []string{
				common.ParamLabelSelector,
				common.ParamFieldSelector,
				common.ParamNameSearch,
				common.ParamPage,
				common.ParamPageSize,
				common.ParamLimit,
				common.ParamContinue,
			})
			s := store.KubernetesResourceStore{
				ResourceStore: store.ResourceStore{
					Logger:  logger,
					Objects: &ul,
				},
			}
			b, err := util.PaginateResult(params, &s)
			if err != nil {
				return old, err
			}

			err = json.Unmarshal(b, &data)
			if err != nil {
				return old, err
			}
		}

		// list operation, this can be tricky
		// seems a lot of crap can be decoded to PaginationList
		if data.Items == nil && data.NumPages == nil {
			return old, nil
		}

		logger.Debugf("find list response")
		var items []single
		for _, item := range data.Items {
			items = append(items, single{
				Kubernetes: model.UnstructToObject(&item),
				Actions:    []string{"*:*"},
			})
		}
		if len(items) == 0 {
			items = make([]single, 0)
		}
		result := list{
			Count:    data.Count,
			Continue: data.Continue,
			PageSize: data.Pagesize,
			Previous: data.Previous,
			NumPages: data.NumPages,
			Next:     data.Next,
			Results:  items,
		}
		return json.Marshal(result)

	} else {
		// single object process
		logger.Debugf("find object response")
		s := single{
			Kubernetes: obj,
			Actions:    []string{"*:*"},
		}
		return json.Marshal(s)

	}

}

// newAppBody create new response application body from old bytes
func newAppBody(old []byte, c *gin.Context) ([]byte, error) {
	logger := common.GetLogger(c)

	if c.Writer.Status() == http.StatusCreated {
		obj := &model.KubernetesObject{}
		json.Unmarshal(old, obj)
		return json.Marshal(single{
			Kubernetes: obj,
			Actions:    []string{"*:*"},
		})
	}

	if c.Param("namespace") != "" && c.Param("name") != "" {
		if strings.HasSuffix(c.Request.URL.Path, "yaml") {
			c.Header("Content-Type", "application/yaml")
		}
		// Get application/pods/history request
		if c.Param("type") == "applicationhistories" {
			obj := &model.KubernetesObject{}
			json.Unmarshal(old, obj)
			return json.Marshal(single{
				Kubernetes: obj,
				Actions:    []string{"*:*"},
			})
		}
		logger.Debugf("convert get application request")
		var resp []single
		objs := []*model.KubernetesObject{}
		err := json.Unmarshal(old, &objs)
		if err != nil {
			return old, nil
		}
		for _, o := range objs {
			resp = append(resp, single{
				Kubernetes: o,
				Actions:    []string{"*:*"},
			})
		}
		return json.Marshal(resp)
	}

	if strings.Contains(c.Request.URL.Path, "applicationhistories/") {
		// List application history
		objs := &model.KubernetesObjectList{}
		json.Unmarshal(old, objs)
		var resp []single
		for _, o := range objs.Items {
			resp = append(resp, single{
				Kubernetes: o,
				Actions:    []string{"*:*"},
			})
		}
		return json.Marshal(resp)
	}

	// List application request
	type singleAppResource struct {
		Actions    []string      `json:"resource_actions"`
		Kubernetes metav1.Object `json:"kubernetes"`
	}

	pageQuery, _ := util.GetPageQuery(c)
	if pageQuery == nil {
		results := []model.ApplicationResponse{}
		if err := json.Unmarshal(old, &results); err != nil {
			return old, nil
		}
		listAll := [][]singleAppResource{}
		for _, o := range results {
			app := []singleAppResource{}
			for _, oo := range o.Kubernetes {
				app = append(app, singleAppResource{
					Actions:    []string{"*:*"},
					Kubernetes: oo,
				})
			}
			listAll = append(listAll, app)
		}
		return json.Marshal(listAll)
	}
	logger.Debugf("convert page query application request")
	// Paging query
	pageList := struct {
		Count    int                         `json:"count"`
		PageSize int                         `json:"page_size"`
		NumPages *int                        `json:"num_pages"`
		Results  []model.ApplicationResponse `json:"results"`
	}{}
	if err := json.Unmarshal(old, &pageList); err != nil {
		return old, nil
	}
	pageListTo := struct {
		Count    int                   `json:"count"`
		PageSize int                   `json:"page_size"`
		NumPages *int                  `json:"num_pages"`
		Results  [][]singleAppResource `json:"results"`
	}{
		Count:    pageList.Count,
		PageSize: pageList.PageSize,
		NumPages: pageList.NumPages,
	}
	for _, o := range pageList.Results {
		app := []singleAppResource{}
		for _, oo := range o.Kubernetes {
			app = append(app, singleAppResource{
				Actions:    []string{"*:*"},
				Kubernetes: oo,
			})
		}
		pageListTo.Results = append(pageListTo.Results, app)
	}

	return json.Marshal(pageListTo)
}
