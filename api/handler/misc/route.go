package misc

import "github.com/gin-gonic/gin"

func AddRoutes(r *gin.Engine) {

	misc := r.Group("/v1/misc/clusters")
	{
		misc.POST("/:cluster/resources", BatchCreateResource)
	}
}
