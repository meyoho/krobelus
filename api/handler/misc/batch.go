package misc

import (
	"net/http"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/kubernetes"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
)

func BatchCreateResource(c *gin.Context) {
	logger := common.GetLogger(c)
	var objList []*model.KubernetesObject
	if err := c.Bind(&objList); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	model.SortKubernetesObjects(objList)

	client, err := kubernetes.NewCachedKubeClient(c.Param("cluster"), util.GetKubernetesTokenHeader(c), common.GetLogger(c))
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	var results []*model.Result
	for _, obj := range objList {
		logger.Debugf("create resource: %+v", obj)
		resource, err := client.GetResourceByKind(obj.GetKind())
		if err != nil {
			util.SetBadRequestResponse(c, err)
			return
		}
		s, err := store.GetStoreInterface(
			model.GetQuery(
				c.Param("cluster"),
				util.GetKubernetesTokenHeader(c),
				resource,
				obj,
			), logger, obj)
		if err != nil {
			util.SetErrorResponse(c, err)
			return
		}
		res, httpErr := util.ProcessResource(s, common.HTTPPost)
		if httpErr != nil && httpErr.StatusCode != http.StatusConflict {
			c.JSON(httpErr.StatusCode, httpErr.Cause)
			return
		}

		if res.Code == http.StatusConflict {
			// Ignore the conflict error
			s.FetchResource(model.GetOptions{})
			res.Body = s.GetRaw()
			res.Code = http.StatusCreated
			res.Error = nil
		}

		res.Extra = map[string]interface{}{
			"resource":  resource,
			"name":      obj.Name,
			"namespace": obj.Namespace,
		}
		results = append(results, res)
	}
	util.SetKubernetesCreateResults(c, results)
}
