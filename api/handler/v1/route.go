package v1

import "github.com/gin-gonic/gin"

func AddRoutes(r *gin.Engine) {
	// General kubernetes resource api group.
	// Support format:
	// /v1/clusters/:cluster/:type  			[GET]
	//
	// For namespaces scope resources
	// /v1/clusters/:cluster/:type/:namespace/      [GET, POST]
	// /v1/clusters/:cluster/:type/:namespace/:name [GET, PUT, DELETE]
	//
	// For cluster scope resources
	// /v1/clusters/:cluster/:type [POST]
	// /v1/clusters/:cluster/:type/:name [GET, PUT, DELETE]

	v1 := r.Group("/v1/clusters")
	{
		v1.POST("/:cluster/:type", ProcessResource)
		v1.POST("/:cluster/:type/*name", ProcessResource)
		v1.GET("/:cluster/:type", ListResource)
		v1.GET("/:cluster/:type/*name", GetResource)
		v1.PUT("/:cluster/:type/*name", ProcessResource)
		v1.PATCH("/:cluster/:type/*name", ProcessResource)
		v1.DELETE("/:cluster/:type/*name", ProcessResource)
	}
}
