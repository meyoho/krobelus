package v1

import (
	"io/ioutil"
	"strings"

	"github.com/opentracing/opentracing-go"

	"github.com/spf13/cast"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

func GetParams(c *gin.Context, keys []string) map[string]string {
	params := map[string]string{}
	for _, key := range keys {
		if value := c.Query(key); value != "" {
			params[key] = value
		}
	}
	return params
}

func getAllParams(c *gin.Context) map[string]string {
	params := map[string]string{}
	for k, v := range c.Request.URL.Query() {
		if len(v) > 0 {
			params[k] = v[0]
		}
	}
	return params
}

// getQuery generate a Query struct from query string, if any param missing, return error
// name format list:
//		/{name}
//		/{namespace}
//		/{namespace}/{name}
//		/{namespace}/{name}/{subtype}
func getQuery(c *gin.Context) *model.Query {
	logger := common.GetLogger(c)
	name := c.Param("name")

	if len(name) > 0 && strings.HasPrefix(name, "/") {
		name = name[1:]
	}

	var namespace string
	var subType string

	result := strings.SplitN(name, "/", 3) // split up to 3 elements

	switch len(result) {
	case 3:
		namespace = result[0]
		name = result[1]
		subType = result[2]
	case 2:
		namespace = result[0]
		name = result[1]
	default:
		name = result[0]
	}
	if namespace == "_" {
		namespace = ""
	}

	contentType := c.Request.Header.Get("Content-Type")

	// Remove "; charset=" if included in header.
	if idx := strings.Index(contentType, ";"); idx > 0 {
		contentType = contentType[:idx]
	}
	patchType := types.PatchType(contentType)

	// Ensure the patchType is one we support

	switch patchType {
	case types.JSONPatchType:
	case types.MergePatchType:
	case types.StrategicMergePatchType:
	default:
		//TODO: remove default
		patchType = types.StrategicMergePatchType
	}

	query := &model.Query{
		ClusterUUID:  c.Param("cluster"),
		ClusterToken: util.GetKubernetesTokenHeader(c),
		Type:         c.Param("type"),
		Namespace:    namespace,
		Name:         name,
		PatchType:    string(patchType),
		SubType:      []string{subType},
	}
	logger.Debugf("getQuery: %+v", query)
	return query
}

func SetKubernetesResult(c *gin.Context, result *model.Result) {
	logger := common.GetLogger(c)
	if result.Code == 0 {
		result.Code = 500
	}
	if result.Error != nil {
		logger.Infof("Request %s failed: %v", c.Request.URL, result.Error)
		c.JSON(result.Code, common.BuildKubernetesError(result.Error.Error()))
	} else {
		logger.Debugf("Request %s get status code: %d", c.Request.URL, result.Code)
		c.Data(result.Code, "application/json", result.Body)
	}
}

// GetResource get a single resource or list a resource in a single namespace
func GetResource(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getQuery(c)

	switch {
	case query.IsListQuery():
		ListResource(c)
		return
	case query.IsPodLogsQuery():
		GetPodLogs(c)
		return
	}

	s, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	s.SetContext(c.Request.Context())

	options := model.GetOptions{
		SkipCache: true,
		Params:    getAllParams(c),
	}

	if err := s.FetchResource(options); err != nil {
		logger.Errorf("Get resource %s in %s failed: %s", query.Type, query.ClusterUUID, err.Error())
		util.SetErrorResponse(c, err)
	} else {
		SetKubernetesResult(c, util.HandleRequestDone("GET", s, nil))
	}
}

// ProcessResource update a resource eg: create/update/delete...
// ProcessResource godoc
// @Summary Process a resource
// @Description Create/Update/Delete/Patch a Resource
// @Success 201 {object} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param type path string true "resource type"
// @Param name path string true "{namespace/}name"
// @Router /v1/clusters/{cluster}/{type} [post]
func ProcessResource(c *gin.Context) {
	method := c.Request.Method
	logger := common.GetLogger(c)

	logger.Debugf("process resource url params: %s %s", method, c.Request.URL)
	query := getQuery(c)

	s := store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: query,
			Logger:     logger,
		},
	}

	if common.IsPostPutRequest(method) {
		var object model.KubernetesObject
		if err := c.ShouldBindBodyWith(&object, binding.JSON); err != nil {
			util.SetBadRequestResponse(c, err)
			return
		}
		s.Object = &object
	}

	if method == common.HTTPPatch {
		raw, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			util.SetBadRequestResponse(c, err)
			return
		}
		s.SetRaw(raw)
	}

	res, httpErr := util.ProcessResource(&s, method)
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, httpErr.Cause)
	} else {
		SetKubernetesResult(c, res)
	}
}

// ListResource godoc
// @Summary List a resource
// @Description List a resource, cluster scope or namespaced scope
// @Success 200 {array} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param type path string true "resource type"
// @Param namespace path string false "namespace, end with /"
// @Router /v1/clusters/{cluster}/{type}/{namespace} [get]
func ListResource(c *gin.Context) {
	//  /:cluster/:uuid/:type
	logger := common.GetLogger(c)
	query := getQuery(c)
	params := GetParams(c, []string{
		common.ParamLabelSelector,
		common.ParamFieldSelector,
		common.ParamNameSearch,
		common.ParamPage,
		common.ParamPageSize,
		common.ParamLimit,
		common.ParamContinue,
	})

	logger.Debugf("List resource %s on cluster %s.", query.Type, query.ClusterUUID)

	ctx := c.Request.Context()

	sp, _ := opentracing.StartSpanFromContext(ctx, "ListResource.GetStoreInterface")
	s, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	s.SetContext(ctx)
	sp.Finish()

	var object model.KubernetesObjectList
	var names []model.ObjectNameTuple
	if err := c.ShouldBindBodyWith(&object, binding.JSON); err == nil {
		for _, item := range object.Items {
			names = append(names, model.ObjectNameTuple{
				Namespace: item.GetNamespace(),
				Name:      item.GetName(),
			})
		}
	}

	options := model.ListOptions{
		Context:     ctx,
		Meta:        "false",
		NameFilters: names,
		NameSearch:  params[common.ParamNameSearch],
		Options: v1.ListOptions{
			LabelSelector: params[common.ParamLabelSelector],
			FieldSelector: params[common.ParamFieldSelector],
		},
	}

	if lmt, ok := params[common.ParamLimit]; ok {
		limit, err := cast.ToInt64E(lmt)
		if nil != err {
			util.SetBadRequestResponse(c, err)
			return
		}
		options.Options.Limit = limit
	}

	if c, ok := params[common.ParamContinue]; ok {
		options.Options.Continue = c
	}

	if err := s.FetchResourceList(options); err != nil {
		logger.Errorf("List resource %s in %s failed: %s", query.Type, query.ClusterUUID, err.Error())
		util.SetErrorResponse(c, err)
	} else {
		raw, _ := util.PaginateResult(params, s)
		s.SetRaw(raw)
		SetKubernetesResult(c, util.HandleRequestDone("GET", s, nil))
	}
}
