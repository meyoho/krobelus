package util

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"krobelus/store"
	"testing"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"krobelus/common"

	"gotest.tools/assert"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func TestFindErrorCodeAndErrorResponse(t *testing.T) {
	var data = []struct {
		input  error
		status int
		code   string
	}{
		{common.NewError(common.CodeInvalidArgs, fmt.Errorf("invalid args")), 400, common.CodeInvalidArgs},
		{apiErrors.NewAlreadyExists(schema.GroupResource{"", "pods"}, "he"), 400, common.CodeExist},
	}
	for _, tt := range data {
		t.Run(tt.code, func(t *testing.T) {
			s, e := findErrorCodeAndErrorResponse(tt.input)
			assert.Equal(t, tt.status, s)
			result := e.(common.KrobelusErrors)
			assert.Equal(t, tt.code, result.Errors[0].Code)
		})
	}
}

func TestPaginateResult(t *testing.T) {

	var (
		emptyFile    = "../../../tests/fixtures/paginate/empty.json"
		fullFile     = "../../../tests/fixtures/paginate/full.json"
		pagedFile    = "../../../tests/fixtures/paginate/page.json"
		limitedFile  = "../../../tests/fixtures/paginate/limit.json"
		continueFile = "../../../tests/fixtures/paginate/continue.json"
	)

	var emptyObjs *unstructured.UnstructuredList
	var fullObjs *unstructured.UnstructuredList

	emptyBt, err := ioutil.ReadFile(emptyFile)
	if nil != err {
		t.Fatal(err)
	}

	if err := json.Unmarshal(emptyBt, &emptyObjs); nil != err {
		t.Fatal(err)
	}

	fullBt, err := ioutil.ReadFile(fullFile)
	if nil != err {
		t.Fatal(err)
	}

	if err := json.Unmarshal(fullBt, &fullObjs); nil != err {
		t.Fatal(err)
	}

	pagedResult, err := ioutil.ReadFile(pagedFile)
	if nil != err {
		t.Fatal(err)
	}

	limitedResult, err := ioutil.ReadFile(limitedFile)
	if nil != err {
		t.Fatal(err)
	}

	continueResult, err := ioutil.ReadFile(continueFile)
	if nil != err {
		t.Fatal(err)
	}

	params := map[string]string{"page": "2", "page_size": "1", "name": "abc"}

	var cases = []struct {
		params   map[string]string
		objs     *unstructured.UnstructuredList
		excepted string
	}{
		{
			nil,
			nil,
			"null",
		},
		{
			nil,
			emptyObjs,
			`{"apiVersion":"v1","items":[],"kind":"PersistentVolumeClaimList","metadata":{"resourceVersion":"11990097","selfLink":"/api/v1/namespaces/default/persistentvolumeclaims"}}`,
		},
		{
			params,
			nil,
			`{"count":0,"page_size":0,"next":null,"previous":null,"num_pages":null,"continue":null,"items":[]}`,
		},
		{
			params,
			emptyObjs,
			`{"count":0,"page_size":0,"next":null,"previous":null,"num_pages":null,"continue":null,"items":[]}`,
		},
		{
			params,
			fullObjs,
			string(pagedResult),
		},
		{
			map[string]string{"page": "2", "page_size": "1", "limit": "1", "name": "abc"},
			fullObjs,
			string(limitedResult),
		},
		{
			map[string]string{"page": "2", "page_size": "1", "limit": "1", "name": "abc", "continue": "123"},
			fullObjs,
			string(continueResult),
		},
	}

	for idx, c := range cases {
		s := &store.KubernetesResourceStore{}
		s.SetObjects(c.objs)
		bt, err := PaginateResult(c.params, s)
		assert.NilError(t, err)

		//t.Logf("bt=%s, %v", string(bt), string(bt) == string(s.GetRaw()))

		assert.Equal(t, string(bt), c.excepted, fmt.Sprintf("case failed: case=%d", idx))
	}

}
