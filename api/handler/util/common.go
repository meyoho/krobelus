package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"krobelus/store"
	"math"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	//"math"
	"net/http"

	"krobelus/common"
	"krobelus/model"

	ac "github.com/alauda/cyborg/pkg/client"
	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

// SetErrorResponse will parse err and set the correspond http status code and error code
//TODO: use for all error handler
func SetErrorResponse(c *gin.Context, err error) {
	if err == nil {
		return
	}
	c.Set("error", err)
	code, final := findErrorCodeAndErrorResponse(err)
	c.JSON(code, final)
}

// findErrorCodeAndErrorResponse will cal the http status code and krobelus error for response
func findErrorCodeAndErrorResponse(err error) (int, error) {
	cause := errors.Cause(err)

	// parse code error first
	if e, ok := cause.(common.CodeError); ok {
		if c, ok := common.HTTPCodeMap[e.Code]; ok {
			return c, e.APIError()
		}
	}

	// k8s errors
	if apiErrors.IsAlreadyExists(cause) {
		return http.StatusBadRequest, common.BuildResourceAlreadyExistError(
			fmt.Sprintf("resource already exist: %s", err.Error()))
	}

	// other errors
	if cause == common.ErrNotFound || common.IsNotExistError(err) || apiErrors.IsNotFound(cause) || ac.IsResourceTypeNotFound(cause) {
		return http.StatusNotFound, common.BuildResourceNotExistError(err.Error())
	} else if cause == common.ErrKubernetesConnect {
		return http.StatusInternalServerError, common.BuildError(common.CodeKubernetesConnectError, err.Error())
	} else {
		return http.StatusInternalServerError, common.BuildServerUnknownError(err.Error())
	}
}

// SetErrorsResponse sets the http response for the error list.
// If the errors are the same type will set the http status that
// represents the error type, else will set 500.
func SetErrorsResponse(c *gin.Context, errs []error) {
	c.Set("error", errs)
	statusMaps := make(map[int]bool)
	var kerrs []error
	for _, err := range errs {
		code, final := findErrorCodeAndErrorResponse(err)
		kerrs = append(kerrs, final)
		statusMaps[code] = true
	}

	statusCode := http.StatusInternalServerError
	if len(statusMaps) == 1 {
		for statusCode = range statusMaps {
			break
		}
	}
	c.JSON(statusCode, common.BuildKrobelusErrors(kerrs))
}

// setBadRequestResponse: 400 / invalid_args
func SetBadRequestResponse(c *gin.Context, err error) {
	c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
}

// SetConflictResponse: 409 conflict
func SetConflictResponse(c *gin.Context, err error) {
	c.JSON(http.StatusConflict, err)
}

// setRegionErrorResponse: 500 / region_error

// setServerUnknownError 500 / unknown_issue
func SetServerUnknownError(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, common.BuildServerUnknownError(err.Error()))
}

func SetK8SErrorResponse(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, common.BuildKubernetesError(err.Error()))
}

// setEmptyResponse: 204 / {}
func SetEmptyResponse(c *gin.Context) {
	c.JSON(http.StatusNoContent, gin.H{})
}

func SetNamespaceResourceCreateErrResponse(c *gin.Context, errs []error) {
	c.JSON(http.StatusInternalServerError, common.BuildKrobelusErrors(errs))
}

func SetNamespaceResourceUpdateErrResponse(c *gin.Context, errs []error) {
	c.JSON(http.StatusInternalServerError, common.BuildKrobelusErrors(errs))
}

// getPageQuery parse page params from request
func GetPageQuery(c *gin.Context) (*model.PageQuery, error) {
	page := c.Query("page")
	pageSize := c.Query("page_size")

	if page == "" || pageSize == "" {
		return nil, nil
	}

	pageNum, err := cast.ToIntE(page)
	if err != nil {
		return nil, err
	}
	pageSizeNum, err := cast.ToIntE(pageSize)
	if err != nil {
		return nil, err
	}
	return &model.PageQuery{
		Page:     pageNum,
		PageSize: pageSizeNum,
	}, nil

}

func GetKubernetesTokenHeader(c *gin.Context) string {
	return model.GetBearerToken(c)
}

// GetOriginRequestBody returns the origin request body from
// gin.Context. Returns 'nil' if no origin body found.
func GetOriginRequestBody(c *gin.Context) []byte {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return nil
	}
	copyBody := ioutil.NopCloser(bytes.NewBuffer(body))
	c.Request.Body = copyBody
	return body
}

// PaginationList represents paging result
type PaginationList struct {
	Count    int                         `json:"count"`
	Pagesize int                         `json:"page_size"`
	Next     *string                     `json:"next"`
	Previous *string                     `json:"previous"`
	NumPages *int                        `json:"num_pages"`
	Continue *string                     `json:"continue"`
	Items    []unstructured.Unstructured `json:"items"`
}

// PaginateResult pagniate the object list
func PaginateResult(params map[string]string, s store.ResourceStoreInterface) ([]byte, error) {

	if nil == s {
		return nil, errors.New("store can't be nil")
	}

	p, hasPage := params[common.ParamPage]
	pS, hasPageSize := params[common.ParamPageSize]
	_, hasContinue := params[common.ParamContinue]
	_, hasLimit := params[common.ParamLimit]

	objs := s.GetObjects()

	if !hasPage && !hasPageSize && !hasContinue && !hasLimit {
		return json.Marshal(objs)
	}

	result := &PaginationList{
		Items: make([]unstructured.Unstructured, 0),
	}

	if nil == objs || 0 == len(objs.Items) {
		// if no items exists, return
		return json.Marshal(result)
	}

	result.Items = objs.Items

	// k8s paging
	if hasContinue || hasLimit {
		c := objs.GetContinue()
		result.Continue = &c
		return json.Marshal(result)
	}

	var page, nextPage, prevPage int
	var pageSize float64
	var err error

	if page, err = cast.ToIntE(p); nil != err {
		page = 1
	}
	if pageSize, err = cast.ToFloat64E(pS); nil != err {
		pageSize = 20
	}

	numPages := cast.ToInt(math.Ceil(float64(len(objs.Items)) / pageSize))

	if page < numPages {
		nextPage = page + 1
	}
	if page > 1 {
		prevPage = page - 1
	}

	if 0 != nextPage {
		next := fmt.Sprintf("?page=%d", nextPage)
		result.Next = &next
	}

	if 0 != prevPage {
		prev := fmt.Sprintf("?page=%d", prevPage)
		result.Previous = &prev
	}

	count := len(objs.Items)

	result.NumPages = &numPages
	result.Pagesize = int(pageSize)
	result.Continue = nil
	result.Count = count

	start := (page - 1) * int(pageSize)

	end := page * int(pageSize)

	if end > count {
		end = count
	}

	if start > end {
		start = end
	}

	//logger := common.GetLog()
	// logger.Infof("fuck: %d %d %d %d", start, end, count, len(objs.Items))

	result.Items = objs.Items[start:end]

	// fuck slice
	if len(result.Items) == 0 {
		result.Items = make([]unstructured.Unstructured, 0)
	}

	return json.Marshal(result)

}
