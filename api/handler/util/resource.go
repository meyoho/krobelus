package util

import (
	"fmt"
	"net/http"
	"strings"

	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	ac "github.com/alauda/cyborg/pkg/client"
	"github.com/gin-gonic/gin"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/rest"
)

func SetKubernetesResultWithMixErr(c *gin.Context, result *rest.Result, err error) {
	if result != nil && result.Error() != nil {
		SetKubernetesResult(c, model.ParseResult(result))
		return
	}
	SetErrorResponse(c, err)
}

func SetKubernetesResult(c *gin.Context, result *model.Result) {
	logger := common.GetLogger(c)
	if result.Code == 0 {
		result.Code = 500
	}
	if result.Error != nil {
		logger.Infof("Request %s failed: %v", c.Request.URL, result.Error)
		c.JSON(result.Code, common.BuildKubernetesError(result.Error.Error()))
	} else {
		logger.Debugf("Request %s get status code: %d", c.Request.URL, result.Code)
		c.Data(result.Code, "application/json", result.Body)
	}
}

func SetKubernetesCreateResults(c *gin.Context, results []*model.Result) {
	logger := common.GetLogger(c)
	var resErrors []error
	var resBodys []string
	for _, r := range results {
		if r.Code == 0 {
			r.Code = 500
		}
		if r.Error != nil {
			logger.Errorf("Request failed for %v", r.Extra)
			resErrors = append(resErrors, common.BuildKubernetesError(
				fmt.Sprintf("create %s-%s-%s error: %s", r.Extra["resource"],
					r.Extra["namespace"], r.Extra["name"], r.Error.Error())))
		} else {
			resBodys = append(resBodys, string(r.Body))
		}
	}
	if len(resErrors) > 0 {
		c.JSON(http.StatusInternalServerError, common.BuildKrobelusErrors(resErrors))
	} else {
		c.Data(http.StatusCreated, "application/json",
			[]byte(fmt.Sprintf("[%s]", strings.Join(resBodys, ","))))
	}
}

func ProcessResource(s store.ResourceManagerInterface, method string) (*model.Result, *common.HTTPResponseError) {

	result, err := s.HandleRequest(method)

	if result == nil && err != nil {
		cause := common.BuildServerUnknownError(err.Error())
		statusCode := http.StatusBadGateway
		if ac.IsResourceTypeNotFound(err) {
			cause = common.BuildResourceTypeNotExistError(err.Error())
			statusCode = http.StatusNotFound
		}
		return nil, &common.HTTPResponseError{
			Cause:      cause,
			StatusCode: statusCode}
	}
	res := HandleRequestDone(method, s, result)
	return res, nil
}

func constructObjectListResult(data []byte) ([]byte, error) {
	objList := &unstructured.UnstructuredList{}
	err := objList.UnmarshalJSON(data)
	if err == nil {
		// Handle with list result directly from cluster rather than the cache,
		// to inject 'apiVersion' and 'kind' field to each item in the list.
		for _, item := range objList.Items {
			item.SetGroupVersionKind(schema.GroupVersionKind{
				Group:   objList.GroupVersionKind().Group,
				Version: objList.GroupVersionKind().Version,
				Kind:    strings.TrimSuffix(objList.GroupVersionKind().Kind, "List"),
			})
		}
		return objList.MarshalJSON()
	}
	return nil, err
}

func HandleRequestDone(method string, s store.ResourceManagerInterface, result *rest.Result) *model.Result {
	res := model.ParseResult(result)

	// Get/List from cache
	// set result.body
	if result == nil && s.GetRaw() != nil && len(s.GetRaw()) > 0 {
		res.Body = s.GetRaw()
	} else if res.Object != nil {
		raw, err := constructObjectListResult(res.Body)
		if err == nil {
			res.Body = raw
		} else {
			s.GetLogger().Errorf("marshal object list error: %s", err.Error())
		}
	}

	switch s.GetResourceType() {
	//case common.PersistentVolumeK8SResType:
	//	err = HandlePVRequestDone(method, s, result)
	case common.CustomResourceDefinitionType:
		if method == common.HTTPPost || method == common.HTTPDelete {
			// Try to trigger the resource type cache sync
			s.SyncResourceTypes()
		}
	}

	// Delete cache object directly in case watcher is not working
	// Also ignore 404 when delete
	if method == common.HTTPDelete {
		if res.Code == 404 {
			s.GetLogger().Warnf("ignore resource 404 when delete: %s", s.String())
			res.Code = 204
			res.Error = nil
		}
	}

	return res
}
