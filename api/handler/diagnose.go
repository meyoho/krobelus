package handler

import (
	"net/http"

	"github.com/alauda/bergamot/diagnose"
	"github.com/gin-gonic/gin"
)

func Ping(c *gin.Context) {
	c.String(http.StatusOK, "krobelus:What I've seen goes far beyond death!")
}

func Diagnose(c *gin.Context) {
	reporter, _ := diagnose.New()
	result := reporter.Check()
	c.JSON(http.StatusOK, result)

}
