package v2

import (
	"strings"

	"krobelus/common"
	"krobelus/model"

	"github.com/gin-gonic/gin"
)

func AppMigration() gin.HandlerFunc {
	return func(c *gin.Context) {

		if !strings.Contains(c.Request.URL.Path, "/applications") &&
			!strings.Contains(c.Request.URL.Path, "/applicationhistories") {
			return
		}

		query := model.GetCRDQuery(c)
		logger := common.GetLogger(c)
		logger.Info("Trying to check app crd in cluster")
		// TODO: call it only when error happens
		if err := InstallCrd(query, logger); err != nil {
			c.AbortWithError(500, err)
			return
		}

		if err := InstallAppHistoryCRD(query, logger); err != nil {
			c.AbortWithError(500, err)
			return
		}
	}

}
