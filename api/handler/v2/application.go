package v2

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"krobelus/api/handler/util"
	"krobelus/api/transition"
	"krobelus/common"
	"krobelus/kubernetes"
	"krobelus/model"
	"krobelus/pkg/application/v1beta1"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/rest"
)

func getAppQuery(c *gin.Context) *model.Query {
	return model.GetAppQuery(c)
}

func InstallCrd(query *model.Query, logger common.Log) error {
	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		return err
	}

	if err := s.InstallAppCrd(); err != nil {
		if !apiErrors.IsAlreadyExists(err) {
			logger.WithError(err).Error("install app crd error")
			return err
		} else {
			logger.Debug("application crd already exist")
		}
	}
	return nil
}

func makeOriginAppRequest(c *gin.Context, request *model.ApplicationRequest) (*model.ApplicationRequest, error) {
	var originReq model.ApplicationRequest
	originBody := util.GetOriginRequestBody(c)
	if len(originBody) > 0 {
		// Unmarshal the origin body and set the app crd if necessary.
		if err := json.Unmarshal(originBody, &originReq); err != nil {
			return nil, err
		}
		if request.Crd != nil {
			originReq.Crd = request.Crd.DeepCopy()
		}
	} else {
		originReq = *request.DeepCopy()
	}
	return &originReq, nil
}

func createApplicationCRD(s store.AppStoreInterface, request *model.ApplicationRequest) (*rest.Result, error) {
	exist, err := s.CheckResourcesExist(request.Kubernetes)
	if exist {
		return nil, common.NewError(common.CodeExist, err)
	}
	if err != nil {
		return nil, err
	}

	result, err := s.CreateAppCrdResource()
	if err != nil {
		return nil, err
	}

	return result, nil
}

// CreateApplication godoc
// @Summary Create applications
// @Description Create applications
// @Accept json
// @Produce json
// @Tags: application,v2
// @Success 200 {object} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Failure 400 {object} common.KrobelusErrors
// @Param body body model.ApplicationRequest true "application create request body"
// @Param cluster path string true "cluster uuid"
// @Router /v2/clusters/{cluster}/applications [post]
func CreateApplication(c *gin.Context) {
	var err error
	defer func() { util.SetErrorResponse(c, err) }()

	logger := common.GetLogger(c)
	cluster := c.Param("cluster")
	clusterToken := util.GetKubernetesTokenHeader(c)

	var request model.ApplicationRequest
	if err = c.ShouldBindBodyWith(&request, binding.JSON); err != nil {
		logger.WithError(err).Error("parse create request application data error")
		err = common.NewError(common.CodeInvalidArgs, err)
		return
	}
	logger.Infof("Create app, request: %s", common.ToString(request))

	if err = transition.TranslateApplicationCreatePayload(&request); err != nil {
		err = common.NewError(common.CodeInvalidArgs, err)
		return
	}
	originReq, err := makeOriginAppRequest(c, &request)
	if err != nil {
		logger.WithError(err).Error("make create origin request error")
		err = common.NewError(common.CodeInvalidArgs, err)
		return
	}

	// app store
	query := model.Query{
		ClusterUUID:  cluster,
		ClusterToken: clusterToken,
		Namespace:    request.Crd.Namespace,
		Type:         "applications",
	}
	s, err := store.GetAppStore(&query, logger, request.Crd)
	if err != nil {
		return
	}

	result, err := createApplicationCRD(s, &request)
	if err != nil {
		return
	}

	var isCreated bool
	result.WasCreated(&isCreated)
	if !isCreated {
		util.SetKubernetesResult(c, model.ParseResult(result))
		return
	}

	// check if sub-resources already exist
	//TODO: parallel
	reqCrd, err := model.RestResultToResource(result)
	if err != nil {
		return
	}

	client, err := kubernetes.NewKubeClient(cluster, clusterToken)
	if err != nil {
		return
	}

	transition.BuildOwnerRef(request.Crd, request.Kubernetes, reqCrd.UID, client)

	if len(request.Kubernetes) > 0 {
		s.ParseResultData(result)
		_, errs := s.CreateAppResources(request.Kubernetes)
		if len(errs) > 0 {
			err = errs[0]
			return
		}

	}

	if err = store.UpdateApplicationPhase(cluster, clusterToken, request.Crd, v1beta1.Succeeded); err != nil {
		return
	}

	if err = genereateApplicationRevision(&query,
		&model.ApplicationHistoryResource{
			AppUID:            reqCrd.UID,
			Request:           originReq,
			User:              request.Resource.User,
			CreationTimestamp: time.Now(),
		}, &model.Application{
			Crd: *request.Crd,
			Cluster: model.ClusterInfo{
				UUID:  cluster,
				Token: clusterToken,
			},
		},
		logger); err != nil {
		return
	}

	util.SetKubernetesResult(c, model.ParseResult(result))
}

func genereateApplicationRevision(query *model.Query, appHistoryCreate *model.ApplicationHistoryResource,
	app *model.Application, logger common.Log) error {
	appHistory, err := CreateAppHistory(query, appHistoryCreate, logger)
	if err != nil {
		return err
	}

	if err = store.UpdateApplicationRevision(app.Cluster.UUID, app.Cluster.Token, &app.Crd, appHistory.Spec.Revision); err != nil {
		return err
	}
	return nil
}

// UpdateApplication godoc
// @Summary Update application
// @Description Update application with new resources
// @Description 1. input can contains application crd
// @Accept json
// @Produce json
// @Tags: application,v2
// @Success 200 {object} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Param body body model.ApplicationRequest true "application update request body"
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application's namespace"
// @Param name path string true "application's name"
// @Router /v2/clusters/{cluster}/applications/{namespace}/{name} [put]
func UpdateApplication(c *gin.Context) {
	var err error
	defer func() { util.SetErrorResponse(c, err) }()
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationRequest
	if err = c.ShouldBindBodyWith(&request, binding.JSON); err != nil {
		logger.WithError(err).Error("parse update request application data error")
		err = common.NewError(common.CodeInvalidArgs, err)
		return
	}
	logger.Infof("Update app, request: %s", common.ToString(request))

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		return
	}

	oldApp, err := s.GetApplication()
	if err != nil {
		err = common.NewError(common.CodeKubernetesError, err)
		return
	}

	result, errs := updateApplication(c, query, s, &request, oldApp, nil)
	if len(errs) > 0 {
		util.SetErrorsResponse(c, errs)
		return
	}

	util.SetKubernetesResult(c, model.ParseResult(result))
}

func updateApplication(c *gin.Context, query *model.Query, s store.AppStoreInterface,
	request *model.ApplicationRequest, oldApp *model.Application, rollbackFrom *int) (*rest.Result, []error) {
	client, err := kubernetes.NewKubeClient(query.ClusterUUID, query.ClusterToken)
	if err != nil {
		return nil, []error{err}
	}

	newCrd, err := transition.TranslateApplicationUpdatePayload(s, request, oldApp, client)
	if err != nil {
		err = common.NewError(common.CodeInvalidArgs, err)
		return nil, []error{err}
	}
	originReq, err := makeOriginAppRequest(c, request)
	if err != nil {
		return nil, []error{err}
	}

	if len(originReq.Kubernetes) == 0 && len(request.Kubernetes) > 0 {
		// Request may be originated from rollback
		originReq.Kubernetes = request.Kubernetes
	}

	newCrdObj := model.AppCrdToObject(newCrd)
	s.SetObject(newCrdObj)
	originReq.Crd = newCrdObj
	result, errs := s.UpdateApplication(request, oldApp)
	if len(errs) > 0 {
		return nil, errs
	}

	appHistoryResource := &model.ApplicationHistoryResource{
		AppUID:            oldApp.Crd.UID,
		OldApp:            oldApp,
		Request:           originReq,
		User:              request.Resource.User,
		ChangeCause:       request.Resource.ChangeCause,
		CreationTimestamp: time.Now(),
		RollbackFrom:      rollbackFrom,
	}
	appHistory, err := CreateAppHistory(query, appHistoryResource, s.GetLogger())
	if err != nil {
		return nil, []error{err}
	}

	if err = store.UpdateApplicationRevision(query.ClusterUUID, query.ClusterToken, &oldApp.Crd, appHistory.Spec.Revision); err != nil {
		return nil, []error{err}
	}

	return result, nil
}

// GetApplication godoc
// @Summary Get an application
// @Description Get application and it's sub resources
// @Produce json
// @Tags: application,v2
// @Success 200 {array} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Failure 404 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application's namespace"
// @Param name path string true "application's name"
// @Router /v2/clusters/{cluster}/applications/{namespace}/{name} [get]
func GetApplication(c *gin.Context) {
	query := getAppQuery(c)
	logger := common.GetLogger(c)
	logger.Debugf("=== %+v", c.Request.Header)

	objects, err := getApplication(query, logger)
	if err != nil {
		util.SetErrorResponse(c, err)
	} else {
		c.JSON(http.StatusOK, objects)
	}

}

// ListApplication godoc
// @Summary List applications
// @Description List application in a single namespace or all namespaces
// @Success 200 {array} model.ApplicationKubernetesObjectList
// @Failure 500 {object} common.KrobelusErrors
// @Param cluster   path  string true  "cluster uuid"
// @Param namespace path  string false "application's namespace"
// @Param name      query string false "search application by name"
// @Param page      query string false "page num"
// @Param page_size query string false "page size"
// @Param simple    query string false "only return application crd"
// @Router /v2/clusters/{cluster}/applications/{namespace} [get]
func ListApplication(c *gin.Context) {
	query := getAppQuery(c)
	logger := common.GetLogger(c)

	pageQuery, err := util.GetPageQuery(c)
	if err != nil {
		util.SetBadRequestResponse(c, errors.Annotate(err, "parse page query error"))
		return
	}

	params := make(map[string]string)
	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	var others []*model.ApplicationResponse

	if pageQuery != nil {
		start := pageQuery.PageSize * (pageQuery.Page - 1)
		end := pageQuery.PageSize * pageQuery.Page
		params["start"] = cast.ToString(start)
		params["end"] = cast.ToString(end)
	}

	if c.Query("name") != "" {
		params["name"] = c.Query("name")
	}
	if c.Query("simple") != "" {
		params["simple"] = c.Query("simple")
	}

	newAppCount := 0

	result, total, err := s.ListApplication(c.Request.Context(), params)
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}
	newAppCount = total
	others = result

	results := mergeApplicationsObjectList(others)
	if pageQuery != nil {
		c.JSON(http.StatusOK, gin.H{
			"num_pages": pageQuery.Page,
			"page_size": pageQuery.PageSize,
			"count":     newAppCount,
			"results":   results,
		})
	} else {
		c.JSON(http.StatusOK, results)
	}

}

// mergeOldAndNewApplications convert old and new app to the same response format
func mergeApplicationsObjectList(new []*model.ApplicationResponse) []model.ApplicationKubernetesObjectList {
	var result []model.ApplicationKubernetesObjectList

	for _, item := range new {
		result = append(result, item.ToCommonKubernetesObjectList())
	}
	return result
}

func getApplication(query *model.Query, logger common.Log) (objects []*model.KubernetesObject, err error) {
	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		return
	}
	if err = s.FetchResource(model.GetOptions{}); err != nil {
		return
	} else {
		app := s.GetObject().ToAppCrd()
		objects = []*model.KubernetesObject{model.AppCrdToObject(app)}

		result, err := s.GetAppSubResources(app)
		if err != nil {
			s.GetLogger().WithError(err).Error("get app resources error")
			return objects, err
		}
		objects = append(objects, result...)
	}
	return
}

// StopApplication stop/start a application
// When consider old apps: things are a little complicated:
// 1. If the app was running before, we just need to install the crd
// 2. If the app was stopped before, we need to install the crd first, annotate the workload in cluster
// and then let the handler to do the  regular `start` action
func StartStopApplication(c *gin.Context) {
	var err error
	defer func() { util.SetErrorResponse(c, err) }()

	logger := common.GetLogger(c)
	query := getAppQuery(c)
	isStop := strings.HasSuffix(c.Request.URL.Path, "stop")
	action := "stopping"
	if !isStop {
		action = "starting"
	}

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		return
	}
	if err := s.FetchResource(model.GetOptions{}); err != nil {
		return
	} else {
		// check if already in stopping/staring status
		current := s.GetObject().GetAppCurrentAction()
		if current == "starting" || current == "stopping" {
			err = common.NewError(common.CodeInvalidArgs, fmt.Errorf("app is already in %s state for now", current)) //nolint:ineffassign
			return
		}

		if err = store.PatchApplication(&model.Application{
			Cluster: model.ClusterInfo{
				UUID:  query.ClusterUUID,
				Token: query.ClusterToken,
			},
			Crd: *s.GetObject(),
		}, model.GenSimpleApplicationUpdateData("Pending", common.AppActionKey(), action)); err != nil {
			return
		}
	}

	if isStop {
		if err := s.StopApplication(); err != nil {
			return
		}
	} else {
		if err := s.StartApplication(); err != nil {
			return
		}
	}

	util.SetEmptyResponse(c)
}

// GetApplicationPods godoc
// @Summary Get pods managed by application
// @Description Get pods managed by application with status rewrited
// @Produce json
// @Tags: application,v2
// @Success 200 {array} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Failure 404 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application's namespace"
// @Param name path string true "application's name"
// @Router /v2/clusters/{cluster}/applications/{namespace}/{name}/pods [get]
func GetApplicationPods(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	var app *v1beta1.Application

	if err := s.FetchResource(model.GetOptions{}); err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	app = s.GetObject().ToAppCrd()

	query.Type = "pods"
	query.Name = ""
	ss, _ := store.GetStoreInterface(query, logger, nil)
	options := model.ListOptions{
		Context: c.Request.Context(),
		Options: v1.ListOptions{
			LabelSelector: model.GetAppLabelSelector(app),
		},
	}
	err = ss.FetchResourceList(options)
	if err != nil {
		util.SetK8SErrorResponse(c, err)
	} else {
		// pods, err := model.SetPodObjectsStatus(s.GetKubernetesObjects())
		c.JSON(http.StatusOK, ss.GetKubernetesObjects())
	}
}

// GetApplicationYAML godoc
// @Summary  Get applications yaml
// @Description Get applications yaml
// @Produce plain
// @Tags: application,v2
// @Success 200 {array} string "yaml data of kubernetes resource list"
// @Failure 500 {object} common.KrobelusErrors
// @Failure 404 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application's namespace"
// @Param name path string true "application's name"
// @Router /v2/clusters/{cluster}/applications/{namespace}/{name}/yaml [get]
func GetApplicationYAML(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	objects, err := getApplication(query, logger)
	if objects != nil {
		for _, object := range objects {
			object.SetResourceVersion("")
			object.SetGeneration(0)
			object.SetSelfLink("")
			object.SetUID("")
		}
		bt, err := transition.KubernetesJSONToYamlBytes(objects)
		if err != nil {
			logger.WithError(err).Error("parse app resources to yaml error")
			util.SetServerUnknownError(c, err)
			return
		} else {
			c.Writer.Header()["Content-Type"] = transition.YamlContentType
			c.Writer.Write(bt)
			c.Status(http.StatusOK)
			return
		}
	} else {
		util.SetK8SErrorResponse(c, err)
		return
	}
}

// DeleteApplication godoc
// @Summary Delete an application
// @Description Delete an application and it's sub resources, include rs/pods
// @Tags: application,v2
// @Success 204
// @Failure 500 {object} common.KrobelusErrors
// @Failure 404 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application's namespace"
// @Param name path string true "application's name"
// @Router /v2/clusters/{cluster}/applications/{namespace}/{name} [delete]
func DeleteApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	logger.Infof("Delete app %+v", query)

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	app, err := s.GetApplication()
	if err != nil {
		if apiErrors.IsNotFound(err) {
			logger.Warnf("resource not found when delete application: %s", err.Error())
			util.SetEmptyResponse(c)
		} else {
			util.SetK8SErrorResponse(c, err)
		}
		return
	}

	err = s.DeleteApplication(app)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	util.SetEmptyResponse(c)
}

// RollbackApplication godoc
// @Summary Rollback an application
// @Description Rollback an application to a revision
// @Tags: application,v2
// @Success 201
// @Failure 500 {object} common.KrobelusErrors
// @Failure 404 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application's namespace"
// @Param name path string true "application's name"
// @Router /v2/clusters/{cluster}/applications/{namespace}/{name}/rollback [POST]
func RollbackApplication(c *gin.Context) {
	var err error
	defer func() { util.SetErrorResponse(c, err) }()
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationRollbackRequest
	if err = c.ShouldBindBodyWith(&request, binding.JSON); err != nil {
		logger.WithError(err).Error("parse rollback request application data error")
		err = common.NewError(common.CodeInvalidArgs, err)
		return
	}
	logger.Infof("Rollback app, request: %s", common.ToString(request))

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		return
	}

	app, err := s.GetApplication()
	if err != nil {
		err = common.NewError(common.CodeKubernetesError, err)
		return
	}

	appRevision, err := GetAppHistory(query, app, request.Revision, logger)
	if err != nil {
		return
	}

	objs, err := model.YAMLToObjects(appRevision.Spec.YAML)
	if err != nil {
		return
	}

	appReq := &model.ApplicationRequest{
		Resource: model.ApplicationResource{
			User: request.User,
		},
	}
	for _, o := range objs {
		if o.GetKind() == "Application" {
			appReq.Crd = o
		} else {
			appReq.Kubernetes = append(appReq.Kubernetes, o)
		}
	}

	result, errs := updateApplication(c, query, s, appReq, app, &appRevision.Spec.Revision)
	if len(errs) > 0 {
		util.SetErrorsResponse(c, errs)
		return
	}

	parseRes := model.ParseResult(result)
	parseRes.Code = http.StatusCreated
	util.SetKubernetesResult(c, parseRes)
}

func ImportResourcesToApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationImportResourcesRequest
	if err := c.Bind(&request); err != nil {
		logger.WithError(err).Error("parse request application data error")
		util.SetBadRequestResponse(c, err)
		return
	}
	logger.Infof("Import app, request: %s", common.ToString(request))

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	app, err := s.GetApplication()
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	client, err := kubernetes.NewKubeClient(query.ClusterUUID, query.ClusterToken)
	if err != nil {
		return
	}

	err = transition.TranslateApplicationImportPayload(&request, app, client)
	if err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}

	s.SetObject(&app.Crd)
	_, err = s.ImportResourcesToApplication(&request, app)
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	util.SetEmptyResponse(c)
}

func ExportResourcesFromApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationExportResourcesRequest
	if err := c.Bind(&request); err != nil {
		logger.WithError(err).Error("parse export request application data error")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Infof("Export app, request: %s", common.ToString(request))

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	app, err := s.GetApplication()
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	err = transition.TranslateApplicationExportPayload(&request, app)
	if err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	s.SetObject(&app.Crd)
	_, err = s.ExportResourcesFromApplication(&request, app)
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	util.SetEmptyResponse(c)
}

func GetApplicationStatus(c *gin.Context) {
	logger := common.GetLogger(c)

	logger.Info("Start")

	query := getAppQuery(c)

	_, verbose := c.GetQuery("verbose")

	s, err := store.GetAppStore(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	app, err := s.GetApplication()
	if err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}

	logger.Debugf("get application finish")

	app.Logger = logger

	status := app.ComputeApplicationStatus(verbose)

	// get extra error message from pod
	if verbose {
		messages := store.VerbosePodMessages(c.Request.Context(), app, logger)
		status.Messages = append(status.Messages, messages...)
	}

	c.JSON(http.StatusOK, status)

	logger.Info("Finish")

}

func getWorkloadLabels(objects []*model.KubernetesObject) (workloadLabels map[string]map[string]interface{}) {
	workloadLabels = map[string]map[string]interface{}{}
	for _, o := range objects {
		if common.IsPodController(o.GetKind()) {
			s, _, _ := unstructured.NestedMap(o.Object, "spec", "template", "metadata", "labels")
			if s == nil {
				continue
			}
			workloadLabels[fmt.Sprintf("%s-%s", o.GetKind(), o.GetName())] = s
		}
	}
	return
}

func getServices(s *store.KubernetesResourceStore, workloadLabels map[string]map[string]interface{}) (
	services map[string]map[string]interface{}, err error) {
	services = make(map[string]map[string]interface{})
	s.SetResourceType("services")
	err = s.FetchResourceList(model.ListOptions{})
	if err != nil {
		return
	}
	labelMatch := func(labels, selector map[string]interface{}) bool {
		for k, v := range selector {
			lv, ok := labels[k]
			if ok && lv.(string) == v.(string) {
				return true
			}
		}
		return false
	}
	lbAnno := "loadbalancer.alauda.io/bind"
	for _, svc := range s.GetObjects().Items {
		annos := svc.GetAnnotations()
		isBind := true
		if annos == nil || annos[lbAnno] == "" {
			isBind = false
		}
		selector, _, _ := unstructured.NestedMap(svc.Object, "spec", "selector")
		if selector == nil {
			continue
		}
		for n, l := range workloadLabels {
			if services[n] == nil {
				services[n] = map[string]interface{}{}
			}
			if labelMatch(l, selector) {
				var sets map[string]bool
				_, ok := services[n]["sets"]
				if !ok {
					sets = map[string]bool{}
				} else {
					sets = services[n]["sets"].(map[string]bool)
				}
				sets[svc.GetName()] = true
				services[n]["is_bind"] = isBind
				services[n]["sets"] = sets
			}
		}
	}
	return
}

func getAddresses(s *store.KubernetesResourceStore, services map[string]map[string]interface{}) (
	addresses map[string]map[string][]interface{}, err error) {
	addresses = make(map[string]map[string][]interface{})
	lbs := map[string]*model.KubernetesObject{}
	s, err = s.Copy("alaudaloadbalancer2", false, false)
	if err != nil {
		return
	}
	err = s.FetchResourceList(model.ListOptions{})
	if err != nil {
		return
	}
	for _, o := range s.GetObjects().Items {
		o.Object["frontends"] = map[string]*model.KubernetesObject{}
		lbs[o.GetName()] = &model.KubernetesObject{Unstructured: o}
	}

	s, err = s.Copy("frontends", false, false)
	if err != nil {
		return
	}
	err = s.FetchResourceList(model.ListOptions{})
	if err != nil {
		return
	}
	for _, o := range s.GetObjects().Items {
		labels := o.GetLabels()
		if labels == nil {
			continue
		}
		alb, ok := labels[common.ALB2NameKey()]
		if !ok {
			continue
		}
		lbs[alb].Object["frontends"].(map[string]*model.KubernetesObject)[o.GetName()] =
			&model.KubernetesObject{Unstructured: o}
		for wl, svc := range services {
			_, ok := addresses[wl]
			if !ok {
				addresses[wl] = map[string][]interface{}{}
			}
			svcSet := svc["sets"].(map[string]bool)
			svcInUse := map[string][]string{}
			services, _, _ := unstructured.NestedSlice(o.Object, "spec", "serviceGroup", "services")
			for _, svcO := range services {
				ns, _, _ := unstructured.NestedString(svcO.(map[string]interface{}), "namespace")
				name, _, _ := unstructured.NestedString(svcO.(map[string]interface{}), "name")
				if ns == s.GetNamespace() && svcSet[name] {
					port, _, _ := unstructured.NestedString(svcO.(map[string]interface{}), "port")
					svcInUse[name] = append(svcInUse[name], port)
				}
			}
			for k, svcO := range svcInUse {
				addrs := addresses[wl][k]
				for port := range svcO {
					protocol, _, _ := unstructured.NestedString(o.Object, "spec", "protocol")
					if protocol == "" {
						protocol = "tcp"
					}
					host, _, _ := unstructured.NestedString(lbs[alb].Object, "spec", "address")
					frontend, _, _ := unstructured.NestedInt64(o.Object, "spec", "port")
					addr := map[string]interface{}{
						"name":               alb,
						"protocol":           protocol,
						"host":               host,
						"url":                "",
						"dsl":                "",
						"frontend":           frontend,
						"port":               port,
						"kind":               "Frontends",
						"resource_namespace": o.GetNamespace(),
						"resource_name":      o.GetName(),
					}
					addrs = append(addrs, addr)
				}
				addresses[wl][k] = addrs
			}
		}
	}
	return
}

// GetApplicationAddress returns the exposed address in the application resources.
// The function is implemented to be compatible with jakiro API.
func GetApplicationAddress(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)
	var err error
	defer func() {
		if err != nil {
			util.SetErrorResponse(c, err)
		}
	}()

	objects, err := getApplication(query, logger)
	if err != nil {
		return
	}

	workloadLabels := getWorkloadLabels(objects)
	s, err := store.GetStore(query, logger, nil)
	if err != nil {
		return
	}

	services, err := getServices(s, workloadLabels)
	if err != nil {
		return
	}

	addresses, err := getAddresses(s, services)
	if err != nil {
		return
	}

	c.JSON(http.StatusOK, addresses)
}
