package v2

import (
	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/pkg/applicationhistory/v1beta1"
	"krobelus/store"
	"net/http"

	"github.com/gin-gonic/gin"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

func getAppHistoryQuery(c *gin.Context) *model.Query {
	return model.GetAppHistoryQuery(c)
}

func getAppHistoryStoreFromAppQuery(query *model.Query, logger common.Log) (store.AppHistoryStoreInterface, error) {
	q := *query
	q.Type = "applicationhistories"
	return store.GetAppHistoryStore(&q, logger, nil)
}

// InstallAppHistoryCRD installs the application history CRD.
func InstallAppHistoryCRD(query *model.Query, logger common.Log) error {
	s, err := store.GetAppHistoryStore(query, logger, nil)
	if err != nil {
		return err
	}

	if err := s.InstallAppHistoryCRD(); err != nil {
		if !apiErrors.IsAlreadyExists(err) {
			logger.WithError(err).Error("install app history crd error")
			return err
		}
		logger.Debug("application history crd already exist")
	}
	return nil
}

// CreateAppHistory creates the app history for the application.
func CreateAppHistory(query *model.Query, appHistoryCreate *model.ApplicationHistoryResource,
	logger common.Log) (*v1beta1.ApplicationHistory, error) {
	s, err := getAppHistoryStoreFromAppQuery(query, logger)
	if err != nil {
		return nil, err
	}

	res, err := s.CreateAppHistory(appHistoryCreate)
	if err != nil {
		logger.Errorf("Create app history error: %v", err)
		return nil, err
	}

	return res, nil
}

// GetAppHistory returns the application history of a specific revision of the application.
func GetAppHistory(query *model.Query, app *model.Application, revision int,
	logger common.Log) (*v1beta1.ApplicationHistory, error) {
	s, err := getAppHistoryStoreFromAppQuery(query, logger)
	if err != nil {
		return nil, err
	}

	return s.GetAppHistory(app.GetAppNameSelectorString(), revision)
}

// GetApplicationHistoryDetail godoc
// @Summary Get an application history
// @Description Get application history detail
// @Produce json
// @Tags: applicationhistory,v2
// @Success 200 {object} model.KubernetesObject
// @Failure 500 {object} common.KrobelusErrors
// @Failure 404 {object} common.KrobelusErrors
// @Param cluster path string true "cluster uuid"
// @Param namespace path string true "application history's namespace"
// @Param name path string true "application history's name"
// @Router /v2/clusters/{cluster}/applicationhistories/{namespace}/{name} [get]
func GetApplicationHistoryDetail(c *gin.Context) {
	query := getAppHistoryQuery(c)
	logger := common.GetLogger(c)

	var err error
	defer func() { util.SetErrorResponse(c, err) }()

	s, err := store.GetAppHistoryStore(query, logger, nil)
	if err != nil {
		return
	}

	obj, err := s.GetAppHistoryDetail()
	if err != nil {
		return
	}

	c.JSON(http.StatusOK, obj)
}

// ListApplicationHistory godoc
// @Summary List application histories
// @Description List application histories
// @Success 200 {object} model.KubernetesObjectList
// @Failure 500 {object} common.KrobelusErrors
// @Param cluster   path  string true  "cluster uuid"
// @Param namespace   path  string true  "namespace"
// @Param labelSelector   query  string false "app selector"
// @Router /v2/clusters/{cluster}/applicationhistories/{namespace} [get]
func ListApplicationHistory(c *gin.Context) {
	query := getAppHistoryQuery(c)
	logger := common.GetLogger(c)
	appSelector := c.Query(common.ParamLabelSelector)

	var err error
	defer func() { util.SetErrorResponse(c, err) }()

	s, err := store.GetAppHistoryStore(query, logger, nil)
	if err != nil {
		return
	}

	res, err := s.ListAppHistory(appSelector)
	if err != nil {
		return
	}

	c.JSON(http.StatusOK, res)
}
