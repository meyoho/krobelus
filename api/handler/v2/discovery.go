package v2

import (
	"net/http"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/kubernetes"

	"github.com/gin-gonic/gin"
)

// GetClusterResourceTypes gets resource types from cluster.
func GetClusterResourceTypes(c *gin.Context) {
	logger := common.GetLogger(c)
	cluster := c.Param("cluster")
	logger.Infof("Get resource types from %s", cluster)
	client, err := kubernetes.NewKubeClient(cluster, util.GetKubernetesTokenHeader(c))
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	rl, err := client.Discovery.GetApiResourceList()
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	c.JSON(http.StatusOK, rl)
}
