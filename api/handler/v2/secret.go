package v2

import (
	"fmt"
	"net/http"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/json"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
)

// CreateSecret create kubernetes secret with human friendly api interface
func CreateSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "CreateSecret")
	logger.Info("Start")
	req := model.SecretRequest{Log: logger}
	if err := c.Bind(&req); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	secret, err := req.GenerateSecret()
	if err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	bytes, _ := json.Marshal(secret)
	cluster := c.Param("cluster")
	clusterToken := util.GetKubernetesTokenHeader(c)
	query := model.Query{
		Namespace:    secret.Namespace,
		Type:         "Secrets",
		ClusterUUID:  cluster,
		ClusterToken: clusterToken,
	}
	s, err := store.GetStoreInterface(&query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	s.SetRaw(bytes)
	result, err := s.Request(common.HTTPPost, nil)
	if err != nil {
		util.SetKubernetesResultWithMixErr(c, result, err)
		return
	}
	if req.SAName != "" {
		patchBytes := []byte(fmt.Sprintf(`{"imagePullSecrets": [{"name": "%s"}]}`, secret.Name))
		query := model.Query{
			Type:         "Serviceaccounts",
			ClusterUUID:  cluster,
			ClusterToken: clusterToken,
			Namespace:    secret.Namespace,
			Name:         req.SAName,
			PatchType:    string(types.StrategicMergePatchType),
		}
		sas, err := store.GetStoreInterface(&query, logger, nil)
		if err != nil {
			util.SetErrorResponse(c, err)
			return
		}
		sas.SetRaw(patchBytes)
		result, err := sas.Request(common.HTTPPatch, nil)
		if err != nil {
			util.SetKubernetesResultWithMixErr(c, result, err)
			return
		}
	}
	util.SetKubernetesResult(c, model.ParseResult(result))
}

func UpdateSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "GetSecret")
	logger.Info("Start")
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")
	req := map[string]string{}
	if err := c.Bind(&req); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	query := model.Query{
		Name:         name,
		Namespace:    namespace,
		ClusterUUID:  cluster,
		ClusterToken: util.GetKubernetesTokenHeader(c),
		Type:         "secrets",
	}
	s, err := store.GetStoreInterface(&query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	if err := s.FetchResource(model.GetOptions{}); err != nil {
		util.SetErrorResponse(c, err)
	}
	result, err := s.Request(common.HTTPGet, nil)
	if err != nil {
		util.SetKubernetesResultWithMixErr(c, result, err)
		return
	}
	secret := &v1.Secret{}
	if err := result.Into(secret); err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}
	secret.Data = nil
	if err := model.AddKeyToSecret(secret, req); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	if bytes, err := json.Marshal(secret); err != nil {
		util.SetErrorResponse(c, err)
		return
	} else {
		s.SetRaw(bytes)
	}
	result, err = s.Request(common.HTTPPut, nil)
	if err != nil {
		util.SetKubernetesResultWithMixErr(c, result, err)
		return
	}

	if err := result.Into(secret); err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}
	c.JSON(http.StatusOK, secret)
}

func PatchSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "GetSecret")
	logger.Info("Start")
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")
	req := model.Origin{}
	if err := c.Bind(&req); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	if 0 == len(req) {
		util.SetEmptyResponse(c)
		return
	}
	patchBytes, err := json.Marshal(map[string]interface{}{
		"data": req.Encode(),
	})
	if err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	query := model.Query{
		Name:         name,
		Namespace:    namespace,
		ClusterUUID:  cluster,
		ClusterToken: util.GetKubernetesTokenHeader(c),
		Type:         "Secrets",
		PatchType:    string(types.StrategicMergePatchType),
	}
	s, err := store.GetStoreInterface(&query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	s.SetRaw(patchBytes)
	result, err := s.Request(common.HTTPPatch, nil)
	if err != nil {
		util.SetKubernetesResultWithMixErr(c, result, err)
		return
	}

	secret := &v1.Secret{}
	if err := result.Into(secret); err != nil {
		util.SetK8SErrorResponse(c, err)
		return
	}
	c.JSON(http.StatusOK, secret)
}

func GetSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "GetSecret")
	logger.Info("Start")
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")
	query := &model.Query{
		Type:         "Secrets",
		Name:         name,
		Namespace:    namespace,
		ClusterUUID:  cluster,
		ClusterToken: util.GetKubernetesTokenHeader(c),
	}
	s, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	if err := s.FetchResource(model.GetOptions{}); err != nil {
		util.SetErrorResponse(c, err)
	}
	secret := model.Origin{}
	secret.Parse(s.GetObject().GetField("data").(map[string]interface{}))
	secretData := secret.GetDecodedSecret()
	logger.Debugf("get secret data decoded : data=%s", secretData)
	c.JSON(http.StatusOK, secretData)
}
