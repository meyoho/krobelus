package v2

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"k8s.io/api/apps/v1beta2"
	"k8s.io/api/autoscaling/v2beta1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
)

const (
	FailedScalingReplicas = "FailedScalingReplicas"
	SkipScalingReplicas   = "SkipScalingReplicas"
	ScalingReplicas       = "ScalingReplicas"
)

var (
	errTmpl = "scale deployment error : name=%s namespace=%s err=%+v"
)

func validateStructType(data interface{}) error {
	if nil == data {
		return fmt.Errorf("expected a pointer, but received a nil")
	}
	t := reflect.TypeOf(data)

	if t.Kind() != reflect.Ptr {
		return fmt.Errorf("expected a pointer, but received a %s", t.Kind().String())
	} else {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		return fmt.Errorf("expected a struct, but received a %s", t.Kind().String())
	}
	return nil
}

func parseVersionedStruct(bytes []byte, i interface{}) error {
	if err := validateStructType(i); nil != err {
		return err
	}
	return json.Unmarshal(bytes, &i)
}

func getPossibleHPA(cluster string, token string, deploy *v1beta2.Deployment, logger common.Log) (*v2beta1.HorizontalPodAutoscaler, error) {
	getFromHPAList := func() (*v2beta1.HorizontalPodAutoscaler, error) {
		logger.Debugf("get hpa list start")
		q := &model.Query{
			ClusterUUID:  cluster,
			ClusterToken: token,
			Namespace:    deploy.Namespace,
			Type:         "horizontalpodautoscalers",
		}
		st, err := store.GetStoreInterface(q, logger, nil)
		if err != nil {
			return nil, err
		}

		res, err := st.Request(common.HTTPGet, nil)
		if nil != err {
			return nil, err
		}
		hapListBytes, err := res.Raw()
		if nil != err {
			return nil, err
		}

		hpaList := &v2beta1.HorizontalPodAutoscalerList{}
		if err := parseVersionedStruct(hapListBytes, hpaList); nil != err {
			return nil, err
		}

		// TODO(lgong): handle multiple hpa manage one deployment case
		for _, hpa := range hpaList.Items {
			ref := hpa.Spec.ScaleTargetRef
			logger.Debugf("hpa.Spec.ScaleTargetRef=%s/%s/%s, deploy=%s/%s/%s", ref.APIVersion, ref.Name, ref.Kind, deploy.APIVersion, deploy.Name, deploy.Kind)
			if deploy.Name == ref.Name && deploy.Kind == ref.Kind {
				return &hpa, nil
			}
		}
		return nil, fmt.Errorf("no hpa found: apiversion=%s, name=%s, kind=%s", deploy.APIVersion, deploy.Name, deploy.Kind)
	}

	query := &model.Query{
		ClusterUUID:  cluster,
		ClusterToken: token,
		Namespace:    deploy.Namespace,
		Name:         deploy.Name,
		Type:         "horizontalpodautoscalers",
	}

	s, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		return nil, err
	}
	res, err := s.Request(common.HTTPGet, nil)
	if nil != err {
		if nil != res && nil != res.Error() && errors.IsNotFound(res.Error()) {
			// if status code is 404 traverse the list and compare their reference
			scaler, err := getFromHPAList()
			return scaler, err
		} else {
			logger.Errorf(errTmpl, deploy.Name, deploy.Namespace, err)
			return nil, err
		}
	} else {
		logger.Debugf("get autoscaler success: %+v, %+v", res, err)
		scalerBytes, err := res.Raw()
		if nil != err {
			logger.Errorf(errTmpl, deploy.Name, deploy.Namespace, err)
			return nil, err
		}
		scaler := &v2beta1.HorizontalPodAutoscaler{}
		if err := parseVersionedStruct(scalerBytes, scaler); nil != err {
			logger.Errorf(errTmpl, deploy.Name, deploy.Namespace, err)
			return nil, err
		}
		return scaler, nil
	}

}

// ScaleDeployment scale a deployment's replica on demand
func ScaleDeployment(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "ScaleDeployment")
	logger.Info("Start")

	util.SetEmptyResponse(c)

	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")

	var step int64 = 1
	var err error

	scale := c.Query("step")
	if "" != scale {
		step, err = strconv.ParseInt(scale, 10, 32)
		if err != nil {
			logger.Errorf(errTmpl, name, namespace, err)
			return
		}
	}

	query := &model.Query{
		ClusterUUID:  cluster,
		ClusterToken: util.GetKubernetesTokenHeader(c),
		Namespace:    namespace,
		Name:         name,
		Type:         "deployments",
	}
	deployStore, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	res, err := deployStore.Request(common.HTTPGet, nil)
	if err != nil {
		logger.Errorf(errTmpl, name, namespace, err)
		return
	}

	deployBytes, err := res.Raw()
	if nil != err {
		logger.Errorf(errTmpl, name, namespace, err)
		return
	}

	deploy := &v1beta2.Deployment{}
	if err := parseVersionedStruct(deployBytes, deploy); nil != err {
		logger.Errorf(errTmpl, name, namespace, err)
		return
	}

	scaler, err := getPossibleHPA(cluster, util.GetKubernetesTokenHeader(c), deploy, logger)

	if nil != err {
		logger.Errorf(errTmpl, name, namespace, err)
		store.SendKubernetesEvent(cluster, query.ClusterToken, v1.EventTypeNormal, SkipScalingReplicas, err.Error(), deploy, logger)
		return
	}

	// validate replica
	getValidateReplica := func(delta, replicas, min, max int32) int32 {
		val := replicas + delta
		if min <= val && val <= max {
			return val
		}
		return 0
	}

	// update deploy
	newReplica := getValidateReplica(int32(step), *deploy.Spec.Replicas, *scaler.Spec.MinReplicas, scaler.Spec.MaxReplicas)
	if 0 == newReplica {
		store.SendKubernetesEvent(cluster, query.ClusterToken, v1.EventTypeWarning, SkipScalingReplicas, "replica can't be zero", deploy, logger)
		return
	}
	patchBytes := []byte(fmt.Sprintf(`{"spec":{"replicas":%d}}`, newReplica))
	query.PatchType = string(types.StrategicMergePatchType)
	deployStore.SetRaw(patchBytes)
	_, err = deployStore.Request(common.HTTPPatch, nil)
	if nil != err {
		logger.Errorf(errTmpl, name, namespace, err)
		store.SendKubernetesEvent(cluster, query.ClusterToken, v1.EventTypeWarning, FailedScalingReplicas, err.Error(), deploy, logger)
		return
	}

	if err := store.SendKubernetesEvent(cluster,
		query.ClusterToken,
		v1.EventTypeNormal,
		ScalingReplicas,
		fmt.Sprintf("Scaled deployment %s replicasto %d", deploy.Name, newReplica),
		deploy,
		logger); nil != err {
		logger.Errorf(errTmpl, name, namespace, err)
	}

	logger.Info("Finish")
}
