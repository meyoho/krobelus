package v2

import (
	"net/http"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

func GetNamespaceTopology(c *gin.Context) {
	name := c.Query("name")
	kind := c.Query("kind")

	topologyStore := &store.TopologyStore{
		Logger:   common.GetLogger(c),
		Response: &model.TopologyResponse{},
		Request: &model.TopologyRequest{
			Name:      name,
			Kind:      kind,
			Namespace: model.Namespace{Name: c.Param("namespace")},
			Cluster:   model.Cluster{UUID: c.Param("cluster")},
		},
	}

	topologyStore.LoadObjects()
	topologyStore.ParseNSTopology()
	for _, node := range topologyStore.Response.Graph.Nodes {
		node.Object["name"] = node.GetName()
		node.Object["namespace"] = node.GetNamespace()
	}
	if name == "" && kind == "" {
		c.JSON(http.StatusOK, topologyStore.Response.Graph)
	} else {
		topologyStore.ParseReference()
		c.JSON(http.StatusOK, topologyStore.Response.Refer)
	}
}

func GetResourceTopology(c *gin.Context) {
	kind := c.Param("kind")
	name := c.Param("name")
	depth, err := cast.ToInt16E(c.Query("depth"))
	if nil != err || 0 >= depth {
		depth = int16(2)
	}

	topologyStore := &store.TopologyStore{
		Logger:   common.GetLogger(c),
		Response: &model.TopologyResponse{},
		Request: &model.TopologyRequest{
			Namespace:    model.Namespace{Name: c.Param("namespace")},
			Kind:         kind,
			Name:         name,
			Cluster:      model.Cluster{UUID: c.Param("cluster")},
			ClusterToken: util.GetKubernetesTokenHeader(c),
			Depth:        depth,
		},
	}

	topologyStore.LoadObjects()

	topologyStore.ParseTopology()

	topologyStore.ParseResourceTopology(kind, name, depth)

	c.JSON(http.StatusOK, topologyStore.Response.Graph)
}
