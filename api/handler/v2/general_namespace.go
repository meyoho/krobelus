package v2

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"sync"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin/binding"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/strategicpatch"
	"k8s.io/client-go/rest"
)

type createOtherResourcesFunc func(req *model.GeneralNamespaceRequest, token string, ignoreExists bool) error
type updateResourcesFunc func(request *model.GeneralNamespaceUpdateRequest, token string) ([]byte, error)

const (
	Resourcequotas  = "resourcequotas"
	Limitranges     = "limitranges"
	Serviceaccounts = "serviceaccounts"
	Namespaces      = "namespaces"
	Secrets         = "secrets"
	SubAction       = "SubAction"
)

// CreateGeneralNamespace creates a namespace used for .
// It will also create a resource quota and several secrets.
func CreateGeneralNamespace(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "CreateGeneralNamespace")
	logger.Info("Start")

	cluster := c.Param("cluster")
	req := model.GeneralNamespaceRequest{
		Cluster: cluster,
		Logger:  logger,
	}
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}
	project := c.Query("project_name")

	model.UpdateAnnotations(req.Namespace, common.ResourceStatusKey(), common.ResourceStatusInitializing)
	model.UpdateLabels(req.Namespace, common.ProjectNameKey(), project)
	model.UpdateLabels(req.Namespace, common.NewProjectNameKey(), project)

	if req.ResourceQuota != nil && req.Namespace.Name != req.ResourceQuota.Namespace {
		util.SetBadRequestResponse(c, errors.New("ResourceQuota needs to comply with the Namespace restrictions"))
		return
	}
	if req.Namespace.Name != req.LimitRange.Namespace {
		util.SetBadRequestResponse(c, errors.New("LimitRange needs to comply with the Namespace restrictions"))
		return
	}
	if model.IsBuiltInNamespace(req.Namespace.Name) {
		util.SetBadRequestResponse(c, fmt.Errorf("this is a builtin namespace %s, skip", req.Namespace.Name))
		return
	}
	logger.Debugf("Create  generalNamespaceReq: %+v", &req)

	s, err := store.GetStoreInterface(&model.Query{
		Type:         Namespaces,
		ClusterUUID:  cluster,
		ClusterToken: util.GetKubernetesTokenHeader(c),
		Name:         req.Namespace.Name}, logger, req.Namespace)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	ignoreExists := false
	// Create namespace
	result, err := s.Request(common.HTTPPost, nil)
	if err != nil {
		if k8sErrors.IsAlreadyExists(err) {
			// For already existed namespaces, if the project label exists,
			// assume that the creation is successful.
			errF := s.FetchResource(model.GetOptions{})
			if errF != nil {
				util.SetErrorResponse(c, errF)
				return
			}
			labels := s.GetObject().GetLabels()
			if labels == nil || labels[common.NewProjectNameKey()] != project {
				util.SetConflictResponse(c, common.BuildNamespaceAlreadyExistError(err.Error()))
				return
			}
			ignoreExists = true
		} else {
			util.SetKubernetesResultWithMixErr(c, result, err)
			return
		}
	}

	// Create Other Resources
	if errs := createNamespaceOtherResources(&req, s.GetClusterToken(), ignoreExists); len(errs) == 0 {
		delete(s.GetObject().Annotations, common.ResourceStatusKey())
		if res, err := s.Request(common.HTTPPut, nil); err != nil {
			logger.Errorf("Update namespace state error: %v", err)
			util.SetKubernetesResultWithMixErr(c, res, err)
			return
		}
	} else {
		logger.Errorf("Create other resources error: %+v", errs)
		util.SetNamespaceResourceCreateErrResponse(c, errs)
		return
	}

	if ignoreExists {
		util.SetConflictResponse(c, common.BuildNamespaceAlreadyExistError(err.Error()))
	} else {
		util.SetKubernetesResult(c, model.ParseResult(result))
	}
}

func createNamespaceOtherResources(req *model.GeneralNamespaceRequest, token string, ignoreExists bool) []error {
	var errs []error
	lock := &sync.Mutex{}
	wg := sync.WaitGroup{}

	if nil == req {
		return errs
	}
	createFuncs := []createOtherResourcesFunc{
		createResQuota,
		createLmtRange,
	}
	for _, fn := range createFuncs {
		wg.Add(1)
		go func(fn createOtherResourcesFunc) {
			defer wg.Done()
			err := fn(req, token, ignoreExists)
			if err != nil {
				lock.Lock()
				defer lock.Unlock()
				errs = append(errs, err)
			}
		}(fn)
	}
	wg.Wait()
	return errs
}

func createResQuota(req *model.GeneralNamespaceRequest, token string, ignoreExists bool) error {
	if nil == req || nil == req.ResourceQuota {
		return nil
	}
	logger := req.Logger
	logger.Entry = logger.Entry.WithField(SubAction, "CreateResQuota")
	query := model.Query{
		ClusterUUID:  req.Cluster,
		ClusterToken: token,
		Type:         Resourcequotas,
		Namespace:    req.ResourceQuota.Namespace,
		Name:         req.ResourceQuota.Name,
	}
	s, err := store.GetStoreInterface(&query, logger, req.ResourceQuota)
	if err != nil {
		return err
	}
	_, err = s.Request(common.HTTPPost, nil)
	if err != nil {
		if k8sErrors.IsAlreadyExists(err) && ignoreExists {
			return nil
		}
		return common.BuildResourceQuotaCreateError(err.Error())
	}
	return nil
}

func createLmtRange(req *model.GeneralNamespaceRequest, token string, ignoreExists bool) error {
	if nil == req {
		return nil
	}
	logger := req.Logger
	logger.Entry = logger.Entry.WithField(SubAction, "CreateLmtRange")
	query := model.Query{
		ClusterUUID:  req.Cluster,
		ClusterToken: token,
		Type:         Limitranges,
		Namespace:    req.LimitRange.Namespace,
		Name:         req.LimitRange.Name,
	}
	s, err := store.GetStoreInterface(&query, logger, req.LimitRange)
	if err != nil {
		return err
	}
	_, err = s.Request(common.HTTPPost, nil)
	if err != nil {
		if k8sErrors.IsAlreadyExists(err) && ignoreExists {
			return nil
		}
		return common.BuildLimitRangeCreateError(err.Error())
	}
	return nil
}

// patchSimple retrieve and patch a namespaced object
func patchSimple(obj *model.KubernetesObject, tp, cluster string, dataStruct interface{}, token string, logger common.Log) ([]byte, error) {
	if nil == obj {
		return nil, nil
	}
	s, err := store.GetStoreInterface(&model.Query{
		ClusterUUID:  cluster,
		ClusterToken: token,
		Type:         tp,
		Namespace:    obj.Namespace,
		Name:         obj.Name,
		PatchType:    string(types.StrategicMergePatchType),
	}, logger, nil)
	if err != nil {
		return nil, err
	}

	getMixedError := func(r *rest.Result, err error) error {
		if nil != r && nil != r.Error() {
			return r.Error()
		}
		if nil != err {
			return err
		}
		return nil
	}

	result, err := s.Request(common.HTTPGet, nil)
	if er := getMixedError(result, err); nil != er {
		return nil, er
	}
	oldQuota, err := result.Raw()
	if err != nil {
		return nil, err
	}
	newQuota, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	patchBytes, err := strategicpatch.CreateTwoWayMergePatch(oldQuota, newQuota, dataStruct)

	logger.Debugf("patchBytes: oldQuota=%s, newQuota=%s patchBytes=%s", oldQuota, newQuota, patchBytes)
	if err != nil {
		return nil, err
	}
	s.SetRaw(patchBytes)
	result, err = s.Request(common.HTTPPatch, nil)
	if er := getMixedError(result, err); nil != er {
		return nil, er
	}

	bytes, err := result.Raw()
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func updateResQuota(req *model.GeneralNamespaceUpdateRequest, token string) ([]byte, error) {
	if nil == req {
		return nil, nil
	}
	logger := req.Logger
	logger.Entry = logger.Entry.WithField(SubAction, "UpdateResQuota")
	if bytes, err := patchSimple(req.ResourceQuota, Resourcequotas, req.Cluster, v1.ResourceQuota{}, token, logger); nil != err {
		return nil, common.BuildResourceQuotaUpdateError(err.Error())
	} else {
		return bytes, nil
	}
}

func updateLmtRange(req *model.GeneralNamespaceUpdateRequest, token string) ([]byte, error) {
	if nil == req {
		return nil, nil
	}
	logger := req.Logger
	logger.Entry = logger.Entry.WithField(SubAction, "UpdateLmtRange")
	if bytes, err := patchSimple(req.LimitRange, Limitranges, req.Cluster, v1.LimitRange{}, token, logger); nil != err {
		return nil, common.BuildLimitRangeUpdateError(err.Error())
	} else {
		return bytes, nil
	}
}

func updateNamespaceResources(req *model.GeneralNamespaceUpdateRequest, token string) ([]string, []error) {
	var errs []error
	var res []string
	if nil == req {
		return res, errs
	}
	var updateFunc []updateResourcesFunc
	if nil != req.LimitRange {
		updateFunc = append(updateFunc, updateLmtRange)
	}
	if nil != req.ResourceQuota {
		updateFunc = append(updateFunc, updateResQuota)
	}
	wg := sync.WaitGroup{}
	l := &sync.Mutex{}
	for _, fn := range updateFunc {
		wg.Add(1)
		go func(fn updateResourcesFunc) {
			defer wg.Done()
			if bt, err := fn(req, token); nil != err {
				l.Lock()
				defer l.Unlock()
				errs = append(errs, err)
			} else {
				l.Lock()
				defer l.Unlock()
				res = append(res, string(bt))
			}
		}(fn)
	}
	wg.Wait()
	return res, errs

}

// UpdateCMBNamespace updates a namespace used for CMB.
// Now supports updating following data:
// * resource quota
// * limit range
func UpdateGeneralNamespace(c *gin.Context) {
	var req model.GeneralNamespaceUpdateRequest
	cluster := c.Param("cluster")
	name := c.Param("name")

	if model.IsBuiltInNamespace(name) {
		util.SetBadRequestResponse(c, fmt.Errorf("this is a builtin namespace %s, skip", name))
		return
	}

	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.SetBadRequestResponse(c, err)
		return
	}

	logger := common.GetLoggerWithAction(c, "UpdateGeneralNamespace")
	logger.Debugf("Update general namespace: %+v", &req)

	req.Cluster = cluster
	req.Logger = logger

	if nil != req.ResourceQuota {
		req.ResourceQuota.Namespace = name
	}

	if nil != req.LimitRange {
		req.LimitRange.Namespace = name
	}

	var errs []error
	var results []string

	// Create Other Resources
	if results, errs = updateNamespaceResources(&req, util.GetKubernetesTokenHeader(c)); len(errs) != 0 {
		logger.Errorf("Update namespace resources error: %+v", errs)
		util.SetNamespaceResourceUpdateErrResponse(c, errs)
		return
	} else {
		c.Data(http.StatusOK, "application/json", []byte(fmt.Sprintf("[%s]", strings.Join(results, ","))))
	}
}
