package v2

import (
	"fmt"
	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"
	"net/http"
	"strings"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	funk "github.com/thoas/go-funk"
	v1 "k8s.io/api/core/v1"
)

// ListWorkload lists workloads(includes deployment, daemonset and statefulset) in a namespace.
func ListWorkload(c *gin.Context) {
	logger := common.GetLogger(c)
	query := model.GetQueryFromContext(c)

	s, err := store.GetStore(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	wlKinds := [][]string{
		{"deployments", "Deployment"},
		{"daemonsets", "Daemonset"},
		{"statefulsets", "StatefulSet"}}
	wllist := map[string][]unstructured.Unstructured{}
	for _, wlk := range wlKinds {
		wls, err := s.Copy(wlk[0], false, false)
		if err != nil {
			util.SetErrorResponse(c, err)
			return
		}
		err = wls.FetchResourceList(model.ListOptions{})
		if err != nil {
			util.SetErrorResponse(c, err)
			return
		}

		wllist[wlk[1]] = append(wllist[wlk[1]], wls.GetObjects().Items...)
	}
	c.JSON(http.StatusOK, wllist)
}

// StartStopWorkload start or stop subresource of application
func StartStopWorkload(c *gin.Context) {
	logger := common.GetLogger(c)
	query := model.GetQueryFromContext(c)
	query.Type = c.Param("type")
	isStop := strings.HasSuffix(c.Request.URL.Path, "stop")

	if !funk.Contains([]string{"deployments", "statefulsets"}, query.Type) {
		err := common.NewError(common.CodeInvalidArgs,
			fmt.Errorf("resource type %s can't to start ot stop", query.Type))
		util.SetErrorResponse(c, err)
		return
	}

	s, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	if err = s.FetchResource(model.GetOptions{}); err != nil {
		util.SetErrorResponse(c, err)
		return
	}

	var reason, action string
	object := s.GetObject()
	replicas, err := object.GetPodReplicas()
	if err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	if isStop {
		if replicas == 0 {
			util.SetEmptyResponse(c)
			return
		}
		object.SetAno(common.AppLastReplicasKey(), cast.ToString(replicas))
		object.SetPodReplicas(0)
		reason = "WorkloadStoped"
		action = "stop"
	} else {
		if replicas != 0 {
			util.SetEmptyResponse(c)
			return
		}
		lastReplicas, ok := object.GetAnnotations()[common.AppLastReplicasKey()]
		if !ok || lastReplicas == "0" {
			lastReplicas = "1"
		}
		object.SetPodReplicas(cast.ToFloat64(lastReplicas))
		reason = "WorkloadStarted"
		action = "start"
	}
	if err = updateWorkload(query, logger, object, reason, action); err != nil {
		util.SetErrorResponse(c, err)
		return
	}
	util.SetEmptyResponse(c)
}

//updateWorkload update workload status
func updateWorkload(query *model.Query, logger common.Log, object *model.KubernetesObject,
	reason string, action string) error {
	sas, err := store.GetStoreInterface(query, logger, object)
	if err != nil {
		return err
	}
	_, err = sas.Request(common.HTTPPut, nil)
	if err != nil {
		store.SendKubernetesEvent(query.ClusterUUID, query.ClusterToken,
			v1.EventTypeWarning, fmt.Sprintf("FailedStartstop"), err.Error(), object, logger)
		return err
	}
	if err := store.SendKubernetesEvent(query.ClusterUUID,
		query.ClusterToken,
		v1.EventTypeNormal,
		reason,
		fmt.Sprintf("%s workload for %s", action, query.Name),
		object,
		logger); nil != err {
		logger.Errorf("%s workload for %s error:err=%+v", action, query.Type, err)
		return err
	}
	return nil
}
