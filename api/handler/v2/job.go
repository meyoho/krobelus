package v2

import (
	"encoding/json"
	"fmt"
	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"
	"time"

	"github.com/gin-gonic/gin"
	batchv1 "k8s.io/api/batch/v1"
	batchv1beta1 "k8s.io/api/batch/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
)

// controllerKind contains the schema.GroupVersionKind for this controller type.
var controllerKind = batchv1beta1.SchemeGroupVersion.WithKind("CronJob")

//CreateJobFromCronjob create job form CronJob Template
func CreateJobFromCronjob(c *gin.Context) {
	var err error
	defer func() { util.SetErrorResponse(c, err) }()
	logger := common.GetLoggerWithAction(c, "CreateJobFromCronjob")
	logger.Info("Start create Job")
	cluster := c.Param("cluster")
	clusterToken := util.GetKubernetesTokenHeader(c)
	query := model.GetQueryFromContext(c)
	query.Type = "cronjobs"

	s, err := store.GetStoreInterface(query, logger, nil)
	if err != nil {
		return
	}
	if err = s.FetchResource(model.GetOptions{}); err != nil {
		return
	}

	objects := s.GetObject()
	jobTemplate, err := objects.GetJobTemplateFromCronJob()
	if err != nil {
		return
	}

	name := fmt.Sprintf("%s-%d", objects.GetName(), time.Now().Unix())
	labels := copyLabels(jobTemplate)
	annotations := copyAnnotations(jobTemplate)

	myJob := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:          labels,
			Annotations:     annotations,
			Name:            name,
			OwnerReferences: []metav1.OwnerReference{*metav1.NewControllerRef(objects, controllerKind)},
		},
		Spec: jobTemplate.Spec,
	}

	var newJob model.KubernetesObject
	bt, _ := json.Marshal(myJob)
	json.Unmarshal(bt, &newJob)

	logger.Debugf("New Job:%v", newJob)
	query2 := &model.Query{
		Namespace:    c.Param("namespace"),
		Type:         "jobs",
		ClusterUUID:  cluster,
		ClusterToken: clusterToken,
	}

	sas, err := store.GetStoreInterface(query2, logger, nil)
	if err != nil {
		return
	}
	sas.SetObject(&newJob)
	result, err := sas.Request(common.HTTPPost, nil)
	if err != nil {
		return
	}

	util.SetKubernetesResult(c, model.ParseResult(result))
}

func copyLabels(template *batchv1beta1.JobTemplateSpec) labels.Set {
	l := make(labels.Set)
	for k, v := range template.Labels {
		l[k] = v
	}
	return l
}

func copyAnnotations(template *batchv1beta1.JobTemplateSpec) labels.Set {
	a := make(labels.Set)
	for k, v := range template.Annotations {
		a[k] = v
	}
	return a
}
