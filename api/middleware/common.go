package middleware

import (
	"krobelus/common"
	"krobelus/config"

	"github.com/gin-gonic/gin"
)

type SetupFunc func()

type Config struct {
	Name        string
	HandlerFunc gin.HandlerFunc
	SetupFunc   SetupFunc
}

type Middleware interface {
	GetHandlerFunc() gin.HandlerFunc
	GetName() string
	Setup()
}

var AllMiddlewares map[string]Middleware

func Register(m Middleware) {
	if AllMiddlewares == nil {
		AllMiddlewares = make(map[string]Middleware)
	}
	AllMiddlewares[m.GetName()] = m
}

func (m *Config) GetName() string {
	return m.Name
}

func (m *Config) GetHandlerFunc() gin.HandlerFunc {
	return m.HandlerFunc
}

func (m *Config) Setup() {
	if m.SetupFunc != nil {
		m.SetupFunc()
	}
}

func InitMiddlewares(r *gin.Engine) {
	// get enabled
	logger := common.GetLog()
	enabled := config.GetMiddlewares()

	// add built in middlewares, may be moved to config in the feature
	//enabled = append(enabled, []string{ClusterWatch}...)

	for _, name := range enabled {
		item, ok := AllMiddlewares[name]
		if ok {
			logger.Infof("Enable middleware: %s", item.GetName())
			item.Setup()
			r.Use(item.GetHandlerFunc())
		}
	}

}
