package middleware

import (
	"fmt"
	"regexp"
	"strings"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"

	"github.com/gin-gonic/gin/binding"

	"github.com/gin-gonic/gin"
)

func init() {
	Register(NewApplicationOwnersCheckMiddleware())
}

func NewApplicationOwnersCheckMiddleware() *Config {
	return &Config{
		Name:        RequireOwnersInfoInApplication,
		HandlerFunc: ApplicationOwnersCheck,
	}
}

// Currently contacts info is stored as an annotation in the only workload object
// Link: http://confluence.alaudatech.com/display/DEV/Kubernetes+Yaml+Explain#KubernetesYamlExplain-CMB
// Link: http://confluence.alaudatech.com/display/DEV/Application+Crd+Explain
func ApplicationOwnersCheck(c *gin.Context) {

	if !strings.Contains(c.Request.URL.Path, "/applications") {
		return
	}

	query := model.GetAppQuery(c)
	logger := common.GetLogger(c)

	if c.Request.Method == common.HTTPPost && query.Name == "" {
		logger.Debug("receive application create request, check owners info")
		checkOwnersInBody(c)
		return
	}

}

func validateOwners(owner model.Owner) error {
	if m, _ := regexp.MatchString("^(ho|HO|[0-9]{2})[0-9]{6}$", owner.EmployeeID); !m {
		return fmt.Errorf("owner.employee_id format error: %s, need to be 8 digits or (HO|ho) with 6 digits", owner.EmployeeID)
	}
	return nil
}

func checkOwnersInBody(c *gin.Context) {
	logger := common.GetLogger(c)
	var request model.ApplicationRequest
	if err := shouldBindHTTPClonedBody(c, &request, binding.JSON); err != nil {
		logger.WithError(err).Error("parse request application data error")
		util.SetBadRequestResponse(c, err)
		c.Abort()
		return
	}

	// check owners info
	var err error
	if len(request.Resource.Owners) == 0 {
		logger.Error("no owners info found in request body")
		err = fmt.Errorf("missing contacts info")
	} else {
		for _, owner := range request.Resource.Owners {
			err = validateOwners(owner)
			if nil != err {
				break
			}
		}
	}

	if nil != err {
		util.SetBadRequestResponse(c, err)
		c.Abort()
		return
	}

}
