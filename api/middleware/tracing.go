package middleware

import "krobelus/common"

func NewTracingMiddleware() *Config {
	return &Config{
		Name:        JaegerTracing,
		HandlerFunc: common.TracingMiddleWare,
		SetupFunc:   initJaeger,
	}
}

func initJaeger() {
	common.InitJaeger()
}

func init() {
	Register(NewTracingMiddleware())
}
