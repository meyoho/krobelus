package middleware

import (
	"bytes"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

// rewriteHTTPBody rewrite request body
func rewriteHTTPBody(c *gin.Context, bt []byte) {
	c.Set(gin.BodyBytesKey, bt)
}

// shouldBindHTTPClonedBody binds cloned body of the request, ensure that
// the origin request body is not modified.
func shouldBindHTTPClonedBody(c *gin.Context, obj interface{}, bb binding.BindingBody) error {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return err
	}
	defer func() {
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	}()

	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	err = c.ShouldBindBodyWith(obj, bb)
	if err != nil {
		return err
	}
	return nil
}
