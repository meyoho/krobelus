package middleware

// List of middlewares
const (
	// check application request data contains owners info
	RequireOwnersInfoInApplication = "RequireOwnersInfoInApplication"

	// tracing
	JaegerTracing = "JaegerTracing"
)
