package middleware

import (
	"regexp"

	"krobelus/api/handler/util"
	"krobelus/common"
	"krobelus/model"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/gin-gonic/gin/json"
)

func init() {
	Register(NewPodLabelMiddleware())
}

func NewPodLabelMiddleware() *Config {
	return &Config{
		Name:        "PodLabel",
		HandlerFunc: PreparePodLabelRequests,
	}
}

// PreparePodLabelRequests prepare request data according to demands
func PreparePodLabelRequests(c *gin.Context) {
	logger := common.GetLogger(c)

	project := c.Query("project_name")

	if m, _ := regexp.MatchString("/v2/.*/applications", c.Request.URL.Path); !m {
		return
	}

	if !common.StringInSlice(c.Request.Method, []string{"POST", "PUT"}) {
		return
	}

	logger.Debug("Enter middleware: ", "PodLabel")

	var request model.ApplicationRequest
	if err := shouldBindHTTPClonedBody(c, &request, binding.JSON); err != nil {
		return
	}
	if request.Kubernetes == nil {
		return
	}
	logger.Debugf("receive %s request for application, check resource requests", c.Request.Method)

	for _, obj := range request.Kubernetes {
		if !common.IsPodController(obj.GetKind()) {
			continue
		}
		obj.AddResourceLabelIfNeeded(project)
	}

	newBody, err := json.Marshal(request)
	if err != nil {
		logger.Errorf("marshal request error: %+v", err)
		util.SetBadRequestResponse(c, err)
		c.Abort()
		return
	}
	rewriteHTTPBody(c, newBody)
}
