OS = Linux

VERSION = 0.0.1

CURDIR = $(shell pwd)
SOURCEDIR = $(CURDIR)
COVER = $($3)

ECHO = echo
RM = rm -rf
MKDIR = mkdir

# If the first argument is "cover"...
ifeq (cover,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (mysql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (psql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (test, $(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: all


.DEFAULT_GOAL := all

test:
	./run/scripts/test.sh $(RUN_ARGS)

unit-test:
	go test -v -cover -coverprofile=coverage.out ./api/... ./common ./config ./infra/... ./model ./store

show-coverage:
	go tool cover -html=coverage.out -o coverage.html
	open coverage.html

setup:
	go get -u github.com/golang/dep/cmd/dep

dep:
	dep ensure -v

fmt:
	./run/scripts/check-fmt.sh

cover:
	./scripts/gen-cover.sh $(RUN_ARGS)


lint:
	which golangci-lint || go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
	golangci-lint run -c run/ci/golangci.yml

check-comment:
	bash run/scripts/check-comment.sh


bin:
	go build -ldflags "-w -s" -v -o krobelus krobelus

compile-local:
	CGO_ENABLED=0 go build -ldflags "-w -s" -v -o krobelus krobelus

build: bin lint unit-test

clean:
	./scripts/clean.sh

compile:
	go build -ldflags "-w -s" -v -o $(ALAUDACI_DEST_DIR)/bin/krobelus krobelus


doc-dep:
	go get -u github.com/swaggo/swag/cmd/swag
	go get -u github.com/swaggo/gin-swagger
	go get -u github.com/swaggo/gin-swagger/swaggerFiles

doc:
	bash ./run/scripts/doc.sh

all: setup fmt bin unit-test

image:
	docker build -t krobelus .

mysql:
	docker-compose -p kro-mysql -f run/docker/mysql-compose.yaml build --force-rm
	docker-compose -p kro-mysql -f run/docker/mysql-compose.yaml up -d

psql:
	docker pull index.alauda.cn/alaudaorg/vision
	docker-compose -p kro-psql -f run/docker/psql-compose.yaml $(RUN_ARGS)

compose:
	docker-compose -p kro-psql -f run/docker/psql-compose.yaml build --force-rm
	docker-compose -p kro-psql -f run/docker/psql-compose.yaml up -d

compose-linux:
	go build -ldflags "-w -s" -v -o krobelus krobelus
	docker pull index.alauda.cn/alaudaorg/vision
	docker-compose -p kro-psql -f run/docker/psql-compose.linux.yaml build --force-rm
	docker-compose -p kro-psql -f run/docker/psql-compose.linux.yaml up -d



cloc:
	cloc . --exclude-dir=vendor,.idea

install:
	go install

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "unit-test         - run unit test"
	@$(ECHO) "lint              - lint code"
	@$(ECHO) "check-comment     - check comment for exported function/struct...."
	@$(ECHO) "bin               - build binary"
	@$(ECHO) "build             - run lint, build binary and run unit-test"
	@$(ECHO) "show-coverage     - show code coverage in browser"
	@$(ECHO) "compose           - docker compose to build and start this project"
	@$(ECHO) "setup             - get dep tool"
	@$(ECHO) "cloc              - cal code lines"
	@$(ECHO) "dep               - make dependencies(vendor) update to date (need http proxy)"
	@$(ECHO) "image             - build docker image"
