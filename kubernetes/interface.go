package kubernetes

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type DiscoveryInterface interface {
	GetRestConfig() *rest.Config

	RestClientForGroupVersionResource(groupVersion string, resourceType string) (rest.Interface, error)
	RestClientConfig(resourceType string) (*rest.Config, error)
	ConfigForResource(name string, preferredVersion string) (rest.Config, error)

	GetResourceByKind(kind string) (string, error)
	GetResourceByKindInsensitive(kind string) (string, error)

	IsNamespaceScoped(resource string) (bool, error)

	DynamicClientForResource(resource string, version string) (dynamic.NamespaceableResourceInterface, error)

	GetAPIResourceByName(name string, preferredVersion string) (*metav1.APIResource, error)

	GetGroupVersionByName(name string, preferredVersion string) (schema.GroupVersion, error)
}
