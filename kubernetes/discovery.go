package kubernetes

import (
	"fmt"

	"github.com/juju/errors"
	"k8s.io/client-go/rest"
)

// GetResourceByKindInsensitive gets the name of resource type by the resource kind.
func (c *KubeResourceClient) GetResourceByKindInsensitive(kind string) (string, error) {
	r, err := c.Discovery.GetApiResourceByKindInsensitive(kind)
	if err != nil {
		return "", errors.New(fmt.Sprintf("resource kind '%s' not found", kind))
	}
	return r.Name, nil
}

// getResourceByKind gets the name of resource type by the resource kind.
func (c *KubeResourceClient) GetResourceByKind(kind string) (string, error) {
	return c.Discovery.GetResourceTypeByKind(kind)
}

// ConfigForResource generates the REST config of k8s client for the resource type.
func (c *KubeResourceClient) ConfigForResource(name string, preferredVersion string) (rest.Config, error) {
	return c.Discovery.ConfigForResource(name, preferredVersion)
}
