package kubernetes

import (
	"strings"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"
	appScheme "krobelus/pkg/application/scheme"

	"github.com/spf13/viper"

	ac "github.com/alauda/cyborg/pkg/client"
	"github.com/juju/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

func init() {
	// Aggregate ServiceCatalog scheme to enable decode with ServiceCatalog resources
	appScheme.AddToScheme(scheme.Scheme)
}

type KubeClientConfig struct {
	config *rest.Config

	Timeout  int
	Endpoint string
	Token    string
	RegionID string
}

type KubeResourceClient struct {
	// Discovery do discovery job
	Discovery *ac.KubeClient
	// Client handle resource CURD
	Client dynamic.Interface
	Config *KubeClientConfig
	Logger common.Log
}

func (c *KubeResourceClient) IsNamespaceScoped(resource string) (bool, error) {
	return c.Discovery.IsNamespaceScoped(resource)
}

func (c *KubeResourceClient) DynamicClientForResource(resource string, version string) (dynamic.NamespaceableResourceInterface, error) {
	return c.Discovery.DynamicClientForResource(resource, version)
}

func (c *KubeResourceClient) GetAPIResourceByName(name string, preferredVersion string) (*metaV1.APIResource, error) {
	return c.Discovery.GetApiResourceByName(name, preferredVersion)
}

func (c *KubeResourceClient) GetGroupVersionByName(name string, preferredVersion string) (schema.GroupVersion, error) {
	return c.Discovery.GetGroupVersionByName(name, preferredVersion)
}

const (
	ResourceNamespaceParam = "namespace"
)

// NewKubeClient generate a new KubeClient based on regionID. It need to  retrieve kubernetes cluster
// info from cache or furion.
// also will  add region to watch list
func NewKubeClient(regionID string, token string) (*KubeResourceClient, error) {
	return NewCachedKubeClient(regionID, token, common.GetLoggerByRegionID(regionID))
}

// NewCachedKubeClient generate a kubernetes client on region and redis settings
// 1. get kubernetes access info from region info
// 2. generate client
// This function support name based and uuid based cluster, in the former situation, we wil use
// the new proxy to access kubernetes clusters, otherwise we will use furion to retrieve the endpoint/token
// information for the target kubernetes cluster
func NewCachedKubeClient(regionID string, token string, logger common.Log) (*KubeResourceClient, error) {
	if regionID == "" {
		return nil, errors.New("region uuid/name is empty when generate kube client")
	}

	isClusterID := common.IsValidUUID(regionID)
	bearerToken := token
	var host string

	// The old path
	if isClusterID {
		cluster, err := infra.GetKubernetesConfig(regionID)
		if err != nil {
			return nil, err
		}
		if bearerToken == "" {
			bearerToken = cluster.Token
		}
		host = cluster.Endpoint

	} else {
		// use aproxy
		host = viper.GetString(config.KeyAProxyEndpoint) + "/kubernetes/" + regionID
	}

	cfg := &rest.Config{
		Host:        host,
		BearerToken: bearerToken,
		Timeout:     common.GetTimeDuration(config.GlobalConfig.Kubernetes.Timeout),
	}
	cfg.Insecure = true
	cfg.Burst = config.GlobalConfig.ClientGo.Burst
	cfg.QPS = config.GlobalConfig.ClientGo.QPS

	// regionID in here is only used for distinguish，so either name or uuid is ok
	d, err := ac.NewKubeClient(cfg, regionID)
	if err != nil {
		return nil, errors.Annotate(err, "gen alauda client error")
	}

	c, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, errors.Annotate(err, "generate dynamic client error")
	}

	client := KubeResourceClient{
		Logger:    logger,
		Discovery: d,
		Client:    c,
		Config: &KubeClientConfig{
			Timeout:  config.GlobalConfig.Kubernetes.Timeout,
			Endpoint: host,
			Token:    bearerToken,
			RegionID: regionID,
			config:   cfg,
		},
	}

	return &client, nil
}

func (c *KubeResourceClient) GetRestConfig() *rest.Config {
	return c.Config.config
}

// RestClientForGroupVersionResource returns a REST client by groupVersion and resource.
func (c *KubeResourceClient) RestClientForGroupVersionResource(groupVersion string, resourceType string) (rest.Interface, error) {
	gv, err := schema.ParseGroupVersion(groupVersion)
	if err != nil {
		return nil, err
	}
	return c.clientForGroupVersionResource(gv.WithResource(resourceType))
}

// clientForGroupVersionResource returns a REST client by group, version and resource.
func (c *KubeResourceClient) clientForResource(resourceType string) (rest.Interface, error) {
	// handle incorrect usages
	resourceType = strings.ToLower(resourceType)
	return c.clientForGroupVersionResource(schema.GroupVersionResource{Resource: resourceType})
}

func (c *KubeResourceClient) RestClientConfig(resourceType string) (*rest.Config, error) {
	clientCfg, err := c.ConfigForResource(resourceType, "")
	if err != nil {
		return nil, err
	}
	return &clientCfg, nil
}

// clientForGroupVersionResource returns a REST client by group, version and resource.
func (c *KubeResourceClient) clientForGroupVersionResource(gvr schema.GroupVersionResource) (rest.Interface, error) {
	clientCfg, err := c.ConfigForResource(gvr.Resource, gvr.GroupVersion().String())
	if err != nil {
		return nil, err
	}

	return rest.RESTClientFor(&clientCfg)
}

// extractRequestParams extracts params map to the request.
// The 'params' now supports the following params:
// - namespace: specify the namespace of the request.
func (c *KubeResourceClient) extractRequestParams(request *rest.Request, params map[string]interface{}) *rest.Request {
	for key, obj := range params {
		switch key {
		case "namespace":
			request.Namespace(obj.(string))

		}
	}
	return request
}

// CreateResource creates a k8s resource. The created resource object will be output to the 'outResult' arg.
func (c *KubeResourceClient) CreateResource(resourceName string, resourceObj interface{},
	params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.clientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Post().
		Resource(resourceName).
		Body(resourceObj)

	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=POST url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// GetResource gets a k8s resource. The resource object will be output to the 'outResult' arg.
func (c *KubeResourceClient) GetResource(resourceName string, name string, options *metaV1.GetOptions,
	params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.clientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Get().
		Resource(resourceName).
		Name(name).
		VersionedParams(options, scheme.ParameterCodec)

	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=GET url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// ListResource lists resources by options, the result will be output to the 'outResult' arg.
func (c *KubeResourceClient) ListResource(resourceName string, options *metaV1.ListOptions,
	params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.clientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Get().
		Resource(resourceName).
		VersionedParams(options, scheme.ParameterCodec)
	return c.extractRequestParams(request, params).Do().Into(outResult)
}

// UpdateResource updates the resource by options, and output the updated resource to the 'outResult' arg.
func (c *KubeResourceClient) UpdateResource(resourceName string, name string, resourceObj interface{},
	params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.clientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Put().
		Resource(resourceName).
		Name(name).
		Body(resourceObj)
	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=UPDATE url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// PatchResource patches the resource by options, and output the patched resource to the 'outResult' arg.
func (c *KubeResourceClient) PatchResource(resourceName string, name string, pt types.PatchType, data []byte,
	params map[string]interface{}, outResult runtime.Object, subresources ...string) error {
	client, err := c.clientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Patch(pt).
		Resource(resourceName).
		SubResource(subresources...).
		Name(name).
		Body(data)
	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=PATCH url=%s", request.URL().String())
	return request.Do().Into(outResult)
}
