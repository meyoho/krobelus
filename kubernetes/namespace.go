package kubernetes

import (
	"encoding/json"

	"krobelus/common"

	v1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

// SyncFromDefault will sync docker pull secrets and gluster-fs endpoints from default namespace
// after then new namespace have been created. Hope this will  only a temp solution.
func (c *KubeResourceClient) SyncFromDefault(name string) error {
	if err := c.syncSecretsFromDefault(name); err != nil {
		return err
	}
	if err := c.syncSAFromDefault(name); err != nil {
		return err
	}
	if err := c.syncEndpointsFromDefault(name); err != nil {
		return err
	}
	return nil
}

func (c *KubeResourceClient) syncSAFromDefault(namespace string) error {
	options := metaV1.GetOptions{}
	sa := &v1.ServiceAccount{}
	err := c.GetResource("serviceaccounts", "default", &options,
		map[string]interface{}{ResourceNamespaceParam: common.NamespaceDefault}, sa)
	if err != nil {
		return err
	}

	newSA := &v1.ServiceAccount{}
	err = c.GetResource("serviceaccounts", "default", &options,
		map[string]interface{}{ResourceNamespaceParam: namespace}, newSA)
	if err != nil {
		return err
	}

	newSA.ImagePullSecrets = sa.ImagePullSecrets
	return c.UpdateResource("serviceaccounts", newSA.Name, newSA,
		map[string]interface{}{ResourceNamespaceParam: namespace}, newSA)
}

func (c *KubeResourceClient) syncSecretsFromDefault(namespace string) error {
	options := metaV1.ListOptions{}
	secs := &v1.SecretList{}
	err := c.ListResource("secrets", &options,
		map[string]interface{}{ResourceNamespaceParam: common.NamespaceDefault}, secs)
	if err != nil {
		return err
	}

	var items []v1.Secret
	for _, item := range secs.Items {
		if item.Type == v1.SecretTypeDockercfg || item.Type == v1.SecretTypeDockerConfigJson {
			items = append(items, item)
		}
	}
	if len(items) == 0 {
		c.Logger.Warn("No secretes found in default namespace, skip sync")
		return nil
	}
	for _, item := range items {
		item.ObjectMeta = metaV1.ObjectMeta{Name: item.Name}
		sec := &v1.Secret{}
		if err := c.CreateResource("secrets", &item,
			map[string]interface{}{ResourceNamespaceParam: namespace}, sec); err != nil {
			if k8sErrors.IsAlreadyExists(err) {
				c.Logger.Warnf("secret %s already exist, update it", item.Name)
				if err := c.UpdateResource("secrets", item.Name, &item,
					map[string]interface{}{ResourceNamespaceParam: namespace}, sec); err != nil {
					return err
				}
			} else {
				return err
			}
		}

	}
	return nil

}

func (c *KubeResourceClient) syncEndpointsFromDefault(namespace string) error {
	options := metaV1.ListOptions{}
	eps := &v1.EndpointsList{}
	err := c.ListResource("endpoints", &options,
		map[string]interface{}{ResourceNamespaceParam: common.NamespaceDefault}, eps)
	if err != nil {
		return err
	}

	if len(eps.Items) == 0 {
		return nil
	}

	var ep *v1.Endpoints

	for _, item := range eps.Items {
		if item.Name == common.GetGlusterFSEndpoint() {
			ep = &item
			break
		}
	}

	if nil == ep {
		return nil
	}

	newEp := v1.Endpoints{
		TypeMeta: metaV1.TypeMeta{
			Kind:       ep.Kind,
			APIVersion: ep.APIVersion,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      ep.Name,
			Namespace: namespace,
		},
		Subsets: ep.Subsets,
	}

	result := &v1.Endpoints{}
	err = c.CreateResource("endpoints", &newEp,
		map[string]interface{}{ResourceNamespaceParam: namespace}, result)

	if err != nil {
		if k8sErrors.IsAlreadyExists(err) {
			c.Logger.Warnf("endpoint %s already exist, update it", ep.Name)
			bys, _ := json.Marshal(newEp)
			if err = c.PatchResource("endpoints", newEp.Name, types.StrategicMergePatchType, bys,
				map[string]interface{}{ResourceNamespaceParam: namespace}, result); err != nil {
				return err
			}
		} else {
			return err
		}
	}

	var servicePorts []v1.ServicePort

	for _, subnet := range ep.Subsets {
		for _, port := range subnet.Ports {
			servicePorts = append(servicePorts, v1.ServicePort{Port: port.Port, Protocol: port.Protocol})
		}
	}

	newSvc := v1.Service{
		TypeMeta: metaV1.TypeMeta{
			Kind:       common.KubernetesKindService,
			APIVersion: ep.APIVersion,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      ep.Name,
			Namespace: namespace,
		},
		Spec: v1.ServiceSpec{
			Ports: servicePorts,
		},
	}

	svcResult := &v1.Service{}
	err = c.CreateResource("services", &newSvc,
		map[string]interface{}{ResourceNamespaceParam: namespace}, svcResult)

	if err != nil {
		if k8sErrors.IsAlreadyExists(err) {
			c.Logger.Warnf("service %s already exist, update it", newSvc.Name)

			bys, _ := json.Marshal(newSvc)

			if err = c.PatchResource("services", newSvc.Name, types.StrategicMergePatchType, bys,
				map[string]interface{}{ResourceNamespaceParam: namespace}, result); err != nil {
				return err
			}
		} else {
			return err
		}
	}

	return nil

}
