package fake

import (
	"krobelus/common"

	"github.com/alauda/cyborg/pkg/client"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	dynamicFake "k8s.io/client-go/dynamic/fake"
	"k8s.io/client-go/rest"
	restFake "k8s.io/client-go/rest/fake"
)

type KubeClient struct {
	Resources []*v1.APIResource
	Versions  []*v1.APIGroup
}

func (c *KubeClient) IsNamespaceScoped(resource string) (bool, error) {
	if common.StringInSlice(resource, []string{"namespaces", "persistentvolumes"}) {
		return false, nil
	}
	return true, nil
}

func (c *KubeClient) DynamicClientForResource(resource string, version string) (dynamic.NamespaceableResourceInterface, error) {
	groupVersion := schema.GroupVersion{}
	if version == "" {
		gv, err := c.GetGroupVersionByName(resource, version)
		if err != nil {
			return nil, err
		}
		groupVersion = gv
	} else {
		gv, err := schema.ParseGroupVersion(version)
		if err != nil {
			return nil, err
		}
		groupVersion = gv
	}

	return dynamicFake.NewSimpleDynamicClient(runtime.NewScheme()).Resource(schema.GroupVersionResource{
		Group:    groupVersion.Group,
		Version:  groupVersion.Version,
		Resource: resource,
	}), nil
}

func (c *KubeClient) GetAPIResourceByName(name string, preferredVersion string) (*v1.APIResource, error) {
	for _, item := range c.Resources {
		if item.Name == name {
			return item, nil
		}
	}
	return nil, client.NewTypeNotFoundError("api resource not found")
}

func (c *KubeClient) GetGroupVersionByName(name string, preferredVersion string) (schema.GroupVersion, error) {
	if preferredVersion != "" {
		return schema.ParseGroupVersion(preferredVersion)
	}

	for _, item := range c.Versions {
		if name == item.Name {
			return schema.ParseGroupVersion(item.PreferredVersion.GroupVersion)
		}
	}
	return schema.GroupVersion{}, client.NewTypeNotFoundError("group version not found")
}

func (c *KubeClient) GetRestConfig() *rest.Config {
	return &rest.Config{}
}

func (c *KubeClient) RestClientForGroupVersionResource(groupVersion string, resourceType string) (rest.Interface, error) {
	return &restFake.RESTClient{}, nil
}

func (c *KubeClient) RestClientConfig(resourceType string) (*rest.Config, error) {
	return &rest.Config{}, nil
}

func (c *KubeClient) ConfigForResource(name string, preferredVersion string) (rest.Config, error) {
	return rest.Config{}, nil
}

func (c *KubeClient) GetResourceByKindInsensitive(kind string) (string, error) {
	return common.GetKubernetesTypeFromKind(kind), nil
}

func (c *KubeClient) GetResourceByKind(kind string) (string, error) {
	return common.GetKubernetesTypeFromKind(kind), nil
}
