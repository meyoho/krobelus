#!/usr/bin/env bash
echo "mode: count" >coverage.out
for pkg in $(go list -f="{{if .TestGoFiles}}{{.ImportPath}}{{end}}" ./...); do
    go test -v -covermode=count -coverprofile=temp.cov $pkg >temp.out || exit 1
    cat temp.out | tee -a p.out && tail -n +2 temp.cov >>coverage.out
done

rm -rf temp.out

echo "cal total coverage:"
cat p.out | grep ^ok | awk '{print $5}' | awk '{print substr($0, 1, length($0)-1)}' | awk '{sum+=sprintf("%f",$1)}END{printf "%.2f",sum/NR}' | xargs -I {} echo "coverage: {}% of statements"
rm -rf p.out
