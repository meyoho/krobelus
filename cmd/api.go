package cmd

import (
	"fmt"

	"krobelus/api/handler"
	"krobelus/api/middleware"
	"krobelus/config"
	_ "krobelus/docs"

	_ "net/http/pprof"

	"github.com/DeanThompson/ginpprof"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	statusApi "gopkg.in/appleboy/gin-status-api.v1"
)

// @title Swagger Example API
// @version 1.0
// @description This is a sample server Petstore server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host petstore.swagger.io
// @BasePath /v2
func API() {
	log.Debugf("Current env settings: %+v", config.GlobalConfig)
	gin.SetMode(gin.ReleaseMode)
	//flag.Set("logtostderr", "true")
	//flag.Set("stderrthreshold", "WARNING")
	//flag.Set("v", "6")
	// This is wa
	//flag.Parse()

	r := gin.Default()

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	middleware.InitMiddlewares(r)

	handler.AddRoutes(r)
	// debug apis
	ginpprof.Wrap(r)
	r.GET("/debug/api/status", statusApi.StatusHandler)

	// use ginSwagger middleware to
	// r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	log.Info("Krobelus starting...")

	// Listen and Server in 0.0.0.0:8080
	addr := fmt.Sprintf(":%d", config.GlobalConfig.Krobelus.APIPort)
	r.Run(addr)
}
